Xilinx Setup
============

Vivado Download / Installation
------------------------------

Go to https://xilinx.com/support/download.html to download a release of Vivado.
Unfortunately, this is paid software, so getting a license is an exercise left
to the reader.  Some targets can be built with a free WebPack license, though this
will be for the smaller "educational" FPGAs.  Physical boards often come with a
license to build for that board, so if you are acquiring a board try to make sure
you will wind up with a license.

The current version of Vivado used for the OpenChip project files is:  2021.1

Vivado_init.tcl
---------------

Vivado will look for a file called Vivado_init.tcl, and if found, it will be
sourced into the tool at startup.  The following paths will be checked, in this
order:

1) <XilinxInstall>/Vivado/<version>/scripts/Vivado_init.tcl

2) (Windows) %APPDATA%/Xilinx/Vivado/<version>/Vivado_init.tcl
   (Linux)   $HOME/.Xilinx/Vivado/<version>/Vivado_init.tcl

3) (Windows) %APPDATA%/Xilinx/Vivado/Vivado_init.tcl
   (Linux)   $HOME/.Xilinx/Vivado/Vivado_init.tcl

The author usees option (1), although (3) could be a second choice. (2) is version
specific and may cause confusion when a new Vivado release fails, if these steps are
not fresh in memory.  Until there is need for version-specific tweaks, (2) is not
recommended.

The first thing to put into the Vivado_init.tcl:

# assuming <XilinxInstall> is /opt/Xilinx, and BoardStore is directly under...
# on windows the path is probably "c:\\Xilinx\\BoardStore", note the escaped "\"
set_param board.repoPaths [list "/opt/Xilinx/BoardStore"]

Next, typically on Windows, Vivado defaults to a low number of threads.  Conversely,
on Linux, it maybe desireable to limit the number of threads (*).  Consider dealing
with this via:

set_param general.maxThreads 8

(*) Many phases of FPGA build are single threaded, so providing 8 threads will not
generate results 8X faster.  Esp when trying to do a lot of work (say running
regressions, or compiling a userspace app on many boards) it maybe preferable to
run more instances in parallel, limiting them to 2-4 threads, to avoid the crunches
when many jobs try to claim 8 threads.

oc_vivado.tcl
-------------

OpenChip relies on a library of TCL functions that are used throughout the flow.
Most targets will have TCL "hooks" to load this file as needed during synthesis
and implementation.  Some actions that can be done at the Xilinx TCL Console,
such as loading a Userspace app, rely on functions in this file.  This requires
that the user either manually "sources" the oc_vivado.tcl, or, that Vivado_init.tcl
does the job every time.

Advantages:
- less manual steps
- in future, probably more functions will be enabled by oc_vivado.tcl (additional
  GUI buttons, etc)

Disadvantages:
- non-OpenChip projects may not want it, get confused by it, etc

The recommended approach is to put the following code in Vivado_init.tcl, which
SHOULD activate only when an OpenChip project is being loaded.  This method
requires that the user starts Vivado in the target directory (i.e. <oc>/targets/u200),
because the pwd is assumed to be within that directory.
