#!/usr/bin/perl

my $xpr = undef;
while (@ARGV) {
    my $arg = shift(@ARGV);
    if (-f $arg) { $xpr = $arg; }
    else { die("Expect XPR file as argument (else would just default to one in the cwd)\n"); }
}

if (not defined $xpr) {
    my @xprs = glob("*.xpr");
    if (scalar(@xprs)>1) { die("There seem to be multiple XPRs in this dir, please provide one as argument\n"); }
    if (scalar(@xprs)==0) { die("You didn't provide an XPR file as argument, and there isn't one in the cwd?\n"); }
    $xpr = pop(@xprs);
}

my $new = $xpr.".new";
my $old = $xpr.".old";
open(IN, $xpr) or die ("Cannot open $xpr for reading!\n");
open(OUT, ">$new") or die ("Cannot open $new for writing!\n");
my $fail = 0;
my $linenum = 1;
my $done_ip_cache = 0;
while (<IN>) {
    my $line = $_;
    chomp($line);
    if ($line =~ /(\s*\<Project Version\=\"\d+\" Minor=\"\d+\") Path="[^\"]+\"(\>\s*)/) {
        $line = $1.$2;
    }
    if ($line =~ /(\s*\<Option Name\=\"IPCachePermission\" Val\=\")\w+(\"\/\>\s*)/) {
        if ($done_ip_cache) { $line = undef; } # we only output the disable line once
        else { $line = $1."disable".$2; }
        $done_ip_cache = 1;
    } elsif ($line =~ /IPCachePermission/) {
        print("ERROR: Didn't understand IPCachePermission on line $linenum\n");
        $fail = 1;
    }
    if ($line =~ /(\s*\<Option Name\=\"IPUserFilesDir\" Val\=\")[^\"]+(\"\/\>\s*)/) {
        $line = $1."\$PIPUSERFILESDIR".$2;
    } elsif ($line =~ /IPUserFilesDir/) {
        print("ERROR: Didn't understand IPUserFilesDir on line $linenum\n");
        $fail = 1;
    }
    if ($line =~ /(\s*\<Option Name\=\"IPStaticSourceDir\" Val\=\")[^\"]+(\"\/\>\s*)/) {
        $line = $1."\$PIPUSERFILESDIR/ipstatic".$2;
    } elsif ($line =~ /IPStaticSourceDir/) {
        print("ERROR: Didn't understand IPStaticSourceDir on line $linenum\n");
        $fail = 1;
    }
    if (($line =~ /\s*\<Attr Name=\"ImportPath\" Val=\"[^\"]+\"\/\>\s*/) or
        ($line =~ /\s*\<Attr Name=\"ImportTime\" Val=\"[^\"]+\"\/\>\s*/) or
        ($line =~ /\s*\<Define Name\=\"OC_BUILD_/) or
        ($line =~ /\s*\<Option Name\=\"BoardPartRepoPaths\"/) or
        ($line =~ /\s*\<Option Name\=\"SourceMgmtMode\"/)) {
       $line = undef; # we remove these lines
    }
    if ($line =~ /(\s*\<Run Id\=\")(\S+)(\".* AutoIncrementalDir\=\")[^\"]+(\".*\>\s*)/) {
        $line = $1.$2.$3."\$PSRCDIR/utils_1/imports/$2".$4;
    }
    if (defined $line) { print OUT $line."\n"; }
    $linenum++;
}
close(OUT);
close(IN);
if ($fail) {
    print("ERROR: Errors seen above, not replacing $xpr with $new\n");
} else {
    rename($xpr, $old);
    rename($new, $xpr);
}
