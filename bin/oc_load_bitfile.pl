#!/usr/bin/perl

use File::Find qw(find);

# SPDX-License-Identifier: MPL-2.0

my $text_default = "\e[0m";
my $text_red = "\e[31m";
my $text_green = "\e[32m";
my $PROGNAME = "OC_LOAD_BITFILE";

my $clean = 1;
my $host = "localhost";
my $port = 3121;
my $usb = 0;
my $bitfile = undef;
my @targets = ();

while (scalar(@ARGV)) {
    my $arg = shift(@ARGV);
    if (($arg eq "-h") or ($arg eq "-help")) { script_usage(); exit(0); }
    elsif ($arg =~ /^\-noclean/) { $clean = 0; }
    elsif ($arg =~ /^\-host/) { $host = shift(@ARGV); }
    elsif ($arg =~ /^\-port/) { $port = shift(@ARGV); }
    elsif ($arg =~ /^\-usb/) { $usb = shift(@ARGV); }
    elsif (-f "$arg") { $bitfile = $arg; }
    else { push(@targets, $arg); } # we haven't yet been given a command, this is a target
}

if ((defined $bitfile) and (scalar(@targets)>0)) { script_error("Cannot provide bitfile AND search terms"); }

if (not defined $bitfile) {
    my $error = 0;
    my @bitfiles;
    find( sub { $_ = $File::Find::name; push @bitfiles, $_ if /\.bit$/}, "." );
    foreach my $candidate (@bitfiles) {
        my $passing = 1;
        foreach my $target (@targets) {
            if (not $candidate =~ /$target/) { $passing = 0; }
        }
        if ($passing) {
            if (not defined $bitfile) {
                $bitfile = $candidate;
                script_info("Found bitfile $bitfile");
            } else {
                script_error("Also matched $candidate, please tighten search terms...", 0);
                $error = 1;
            }
        }
    }
    if ($error) { exit -1; }
    if (not defined $bitfile) { script_error("Failed to find a matching bitfile"); }
}

my $script_file = "./oc_load_bitfile.temp.tcl";
my $log = "./oc_load_bitfile.log";
open(OUT, ">$script_file") or script_error("Cannot screate $script_file");
print OUT "open_hw_manager\n";
print OUT "connect_hw_server -url ${host}:${port}\n";
print OUT "refresh_hw_server -force_poll\n";
print OUT "set hw_targets [get_hw_targets */xilinx_tcf/Xilinx/*]\n";
print OUT "if { [llength \$hw_targets] <= $usb } { puts \"\[ERROR\] OC_LOAD_BITFILE: hw_target #${usb} doesn't exist!!\"; exit; }\n";
print OUT "set hw_target [lindex \$hw_targets $usb]\n";
print OUT "current_hw_target \$hw_target\n";
print OUT "open_hw_target\n";
print OUT "refresh_hw_target\n";
print OUT "set hw_device [lindex [get_hw_devices] 0]\n";
print OUT "puts \"DEVICE ID: [get_property DID \$hw_device]\"\n";
print OUT "puts \"PART     : [get_property PART \$hw_device]\"\n";
print OUT "current_hw_device \$hw_device\n";
print OUT "refresh_hw_device -update_hw_probes false -quiet \$hw_device\n";
print OUT "set_property PROGRAM.FILE $bitfile \$hw_device\n";
print OUT "program_hw_devices [current_hw_device]\n";
print OUT "close_hw_target\n";
close(OUT);

my $cmd = "vivado -mode batch -quiet -source ${script_file} -log $log  -nojournal";
print("EXEC: ${text_green}${cmd}${text_default}\n");
system($cmd);

if ($clean) {
    unlink($script_file);
}

script_info("Done.  Have a nice day.");

sub sprint_pass_fail {
    my ($pass, $valid, $color) = @_;
    if (not defined $valid) { $valid = 0; }
    if (not defined $color) { $color = 0; }
    if ($valid == 0) { return "N/A "; }
    if ($pass) { return $color ? "${text_green}PASS${text_default}" : "PASS"; }
    return $color ? "${text_red}FAIL${text_default}" : "FAIL";
}

sub get_root_dir {
    if ((-d "./bin") and (-d "./targets")) { return "."; }
    # do something more interesting later
    else { script_error("This utility needs to be run at the root of a valid repo, for safety"); }
}

sub script_info {
    my ($text) = @_;
    if (not defined $text) { $text = "Unknown internal info"; }
    print("[INFO] $PROGNAME: ${text}\n");
}

sub script_error {
    my ($text, $code) = @_;
    if (not defined $text) { $text = "Unknown internal error"; }
    if (not defined $code) { $code = -1; }
    $ret = ($code ? " (ret $code)" : "");
    print("${text_red}[ERROR] $PROGNAME: ${text}${ret}\n${text_default}");
    if ($code) { exit $code; }
}

sub script_usage {
    print <<EOM;

oc_load_bitfile.pl [-host <host>] [bitfile]

Where:

[-host <host>]     Connects to <hostname>, for USB JTAG download (default localhsot)
[-port <port>]     Uses TCP Port <port> on the host (default 3121)
[-usb <usb>]       Uses USB device <usb> on the host (default 0)
[bitfile]          Either a bitfile, or erms to isolate a bitfile (*.bit) if there is more than one below the pwd

Runs a regression test on a set of targets.

Examples:

oc_load_bitfile.pl perf    # will find any bitfiles that have "perf" in the path

EOM
}
