
i2c_ser = 0
i2c_channel = 0
i2c_address = 0
i2c_data = 0

def i2c_config(ser, channel):
    global i2c_ser
    global i2c_channel
    global i2c_address
    global i2c_data
    i2c_ser = ser
    i2c_channel = channel
    i2c_address = 0x0001
    i2c_data = 0x33 # both outputs, manual mode and tristated
    csr_write(i2c_ser, i2c_channel, 0, i2c_address, i2c_data)

def i2c_show():
#    data = csr_read(i2c_ser, i2c_channel, 0, i2c_address)
#    print("I2C: SCL=%d (%d) SDA=%d (%d) " % (1 if data&0x08 else 0,
#                                             1 if data&0x02 else 0,
#                                             1 if data&0x80 else 0,
#                                             1 if data&0x20 else 0))
    return

def i2c_set_scl(data):
    global i2c_data
    if (data):
        i2c_data |= 0x02 # tristate scl
    else:
        i2c_data &= ~0x02 # lower scl
    csr_write(i2c_ser, i2c_channel, 0, i2c_address, i2c_data)
    i2c_show();

def i2c_get_scl():
    data = csr_read(i2c_ser, i2c_channel, 0, i2c_address)
    i2c_show();
    return 1 if (data & 0x08) else 0

def i2c_set_sda(data):
    global i2c_data
    if (data):
        i2c_data |= 0x20 # tristate scl
    else:
        i2c_data &= ~0x20 # lower scl
    csr_write(i2c_ser, i2c_channel, 0, i2c_address, i2c_data)
    i2c_show();

def i2c_get_sda():
    data = csr_read(i2c_ser, i2c_channel, 0, i2c_address)
    i2c_show();
    return 1 if (data & 0x80) else 0

def delay_us(us):
#    time.sleep(0.000001 * us)
    return

def i2c_start():
    i2c_set_sda(1)
    delay_us(10)
    i2c_set_scl(1)
    delay_us(10)
    i2c_set_sda(0)
    delay_us(10)
    i2c_set_scl(0)
    delay_us(10)

def i2c_stop():
    i2c_set_sda(0)
    delay_us(10)
    i2c_set_scl(1)
    delay_us(10)
    i2c_set_sda(1)
    delay_us(10)

def i2c_write_byte(data = 0):
    global quiet
#    print("i2c write byte %02x" % data)
    for bit in range(8):
#        print("i2c transferring bit %d" % (7-bit))
        i2c_set_sda(1 if (data & 0x80) else 0)
        data <<= 1
        delay_us(10)
        i2c_set_scl(1)
        delay_us(10)
        i2c_set_scl(0)
        delay_us(10)
#    print("i2c checking ack")
    i2c_set_sda(1)
    delay_us(10)
    i2c_set_scl(1)
    delay_us(10)
    ack = (0 if i2c_get_sda() else 1)
    i2c_set_scl(0)
    return ack

def i2c_read_byte(ack = 1):
    data = 0
    i2c_set_sda(1)
    for bit in range(8):
        data <<= 1
        i2c_set_scl(1)
        delay_us(10)
        while (i2c_get_scl() == 0):
            delay_us(10)
        data |= i2c_get_sda()
        i2c_set_scl(0)
        delay_us(10)
    i2c_set_sda(0 if ack else 1)
    delay_us(10)
    i2c_set_scl(1)
    delay_us(10)
    i2c_set_scl(0)
    delay_us(10)
    i2c_set_sda(1)
    return data

def i2c_scan():
    print("* Scanning I2C")
    print("    x0 x1 x2 x3 x4 x5 x6 x7 x8 x9 xA xB xC xD xE xF")
    for address in range(0x00, 0x80):
        if ((address & 0xf) == 0):
            line = ("%xx  " % (address>>4))
        i2c_start()
        ack = i2c_write_byte((address<<1) | 1) # reading
        line += ("** " if ack else "-- ")
        i2c_stop()
        if ((address & 0xf) == 0xf):
            print(line)

def i2c_scan2():
    print("* Scanning I2C")
    address =0
    i2c_start()
    ack = i2c_write_byte((address<<1) | 1) # reading
    print("   Got ACK: %d" % ack)
    i2c_stop()
