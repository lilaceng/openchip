#!/usr/bin/perl

# SPDX-License-Identifier: MPL-2.0

my $text_default = "\e[0m";
my $text_red = "\e[31m";
my $text_green = "\e[32m";
my $PROGNAME = "OC_REGRESSION";

if (scalar(@ARGV) eq 0) { script_usage(); script_error("Need arguments"); }

my $not = 0;
my @targets = ();
my $xpr = 1;
my $user = undef;
my $test = "chip_test";
my $sim = 0;
my $build = 0;
my $asis = 0;
my $clean = 1;
my @tcl_lines = ();

while (scalar(@ARGV)) {
    my $arg = shift(@ARGV);
    if ($arg =~ /^\-h/) { script_usage(); exit(0); }
    elsif ($arg =~ /^\-not/) { $not = 1; }
    elsif ($arg =~ /^\-xpr/) { $xpr = 1; }
    elsif ($arg =~ /^\-sim/) { $sim = 1; }
    elsif ($arg =~ /^\-noclean/) { $clean = 0; }
    elsif ($arg =~ /^\-build/) { $build = 1; }
    elsif ($arg =~ /^\-test/) { $test = shift(@ARGV); } # which test to run for sims
    elsif ($arg =~ /^\-user/) { $user = shift(@ARGV); } # name of userspace to load
    elsif ($arg =~ /^\-asis/) { $asis = 1; }
    elsif ($arg =~ /^\-tcl/) { push(@tcl_lines, shift(@ARGV)); } # raw tcl command
    else { push(@targets, $arg); } # we haven't yet been given a command, this is a target
}
if ($not and (scalar(@targets)==0)) {
    script_error("Need to give at least one target with -not argument");
}
if ((defined $user) and (not -d "user/$user")) {
    script_error("Didn't find user/$user though was given -user $user argument");
}
if (($sim+$build) == 0) {
    script_error("Need to provide -sim and/or -build to have something to do");
}
my $root_dir = get_root_dir();
print "$PROGNAME: root: $root_dir\n";
if ( $xpr ) { printf "$PROGNAME: XPR targets %s%s\n", ($not ? "not " : ""), join(',', @targets); }

# *** create the list of targets

# * start with a list of known targets
my %known_targets = ();
if (not -d "$root_dir/targets") { script_error("Encountered unexpected non-directory: $root_dir/targets"); }
opendir(my $DIR, "$root_dir/targets") or script_error("Unable to open directory: $root_dir/targets");
while (readdir $DIR) {
    my $short = $_;
    if (($short eq ".") or ($short eq "..")) { next; }
    my $target_dir = "$root_dir/targets/$short";
    if (-f "$target_dir/${short}.xpr") { $known_targets{$short} = "$target_dir/${short}.xpr"; }
    elsif ($short =~ /^([^_]+)_/) {
        if (-f "$target_dir/${1}.xpr") { $known_targets{$short} = "$target_dir/${1}.xpr"; }
    }
}
closedir($DIR);
printf "$PROGNAME: known targets: %s\n", join(',',sort keys %known_targets);

# * create a list of targets to work on
my %work_targets = ();
if ($not or (scalar(@targets)==0)) { # if we are inverting a target list, or we don't have a target list...
    # if we have targets, we are inverting, so remove them from known_targets
    foreach my $target (@targets) {
        if (exists $known_targets{$target}) { delete $known_targets{$target}; }
        else { script_error("Target $target doesn't exist"); }
    }
    # regardless of whether we had targets or not, we now add all (remaining?) known_targets
    foreach my $known_target (sort keys %known_targets) { $work_targets{$known_target} = $known_targets{$known_target}; }
} else {
    # we got an explicit target list
    foreach my $target (@targets) {
        if (exists $known_targets{$target}) { $work_targets{$target} = $known_targets{$target}; }
        else { script_error("Target $target doesn't exist"); }
    }
}

printf "$PROGNAME: Resolved targets %s\n", join(',', sort keys %work_targets); 

# *** create a script file
my $script_file = "$root_dir/oc_regression.temp.tcl";
open(OUT, ">$script_file") or script_error("Cannot screate $script_file");
printf OUT "source bin/oc_vivado.tcl -quiet\n";
if (defined $user) {
    printf OUT "oc_load_userspace $user\n";
} elsif (not $asis) {
    printf OUT "oc_unload_userspace\n";
}
foreach my $tcl_line (@tcl_lines) {
    printf OUT "$tcl_line\n";
}
if ($sim) {
    if (not $asis) {
        printf OUT "set_property top $test [get_filesets sim_1]\n";
    }
    printf OUT "launch_simulation\n";
}
if ($build) {
    if (not $asis) {
        printf OUT "reset_run [current_run -synthesis]\n";
        printf OUT "reset_run [current_run -implementation]\n";
    }
    printf OUT "launch_runs [current_run -synthesis]\n";
    printf OUT "wait_on_run [current_run -synthesis]\n";
    printf OUT "launch_runs -to_step write_bitstream [current_run -implementation]\n";
    printf OUT "wait_on_run [current_run -implementation]\n";
}
close(OUT);

# *** iterate the targets
foreach my $target (sort keys %work_targets) {
    if ($xpr) { # we validated above that we have an XPR file
        my $tclargs = "";
        my $xpr = $work_targets{$target};
        my $log = "vivado.${target}.log";
        my $cmd = "vivado -mode batch -source ${script_file} -log $log -nojournal ${xpr}${tclargs}";
        print("EXEC: ${text_green}${cmd}${text_default}\n");
        open(OUT, ">${log}.start"); print OUT "\n"; close(OUT);
        system($cmd);
        open(OUT, ">${log}.stop"); print OUT "\n"; close(OUT);
    }
}

# *** report the results
open(OUT, ">>oc_regression.txt") or script_error("Cannot open oc_regression.txt for appending!");
foreach my $target (sort keys %work_targets) {
    if ($xpr) { # we validated above that we have an XPR file
        my $log = "vivado.${target}.log";
        my $sim_pass = 0;
        my $synth_pass = 0;
        my $impl_pass = 0;
        open(IN, $log) or script_error("Couldn't open $log");
        while(<IN>) {
            if (/TEST PASSED/) { $sim_pass = 1; }
            if (/chip_test\: DONE \(PASSED\)/) { $sim_pass = 1; }
            if (/synth_design completed successfully/) { $synth_pass = 1; }
            if (/report_methodology completed successfully/) { $impl_pass = 1; }
        }
        close(IN);
        my $sim_pass_string = "SIM: ".sprint_pass_fail($sim_pass, $sim, 1);
        my $synth_pass_string = "SYNTH: ".sprint_pass_fail($synth_pass, $build, 1);
        my $impl_pass_string = "IMPL: ".sprint_pass_fail($impl_pass, $build, 1);
        my $user_string = ((defined $user) ? " ($user)" : "");
        $start_time = (stat "${log}.start" ) [9];
        $stop_time = (stat "${log}.stop" ) [9];
        printf "$PROGNAME: %-16s $sim_pass_string $synth_pass_string $impl_pass_string   %5.1fmin$user_string\n",
               $target, (($stop_time-$start_time)/60.0);
        $sim_pass_string = "SIM: ".sprint_pass_fail($sim_pass, $sim);
        $synth_pass_string = "SYNTH: ".sprint_pass_fail($synth_pass, $build);
        $impl_pass_string = "IMPL: ".sprint_pass_fail($impl_pass, $build);
        printf OUT "%-16s $sim_pass_string $synth_pass_string $impl_pass_string   %5.1fmin$user_string\n",
               $target, (($stop_time-$start_time)/60.0);
    }
}
close(OUT);

# *** cleanup
if ($clean) {
    foreach my $target (sort keys %work_targets) {
        if ($xpr) { # we validated above that we have an XPR file
            my $log = "vivado.${target}.log";
            unlink($log);
            unlink($log.".start");
            unlink($log.".stop");
        }
        unlink($script_file);
    }
}

script_info("Done.  Have a nice day.");



sub sprint_pass_fail {
    my ($pass, $valid, $color) = @_;
    if (not defined $valid) { $valid = 0; }
    if (not defined $color) { $color = 0; }
    if ($valid == 0) { return "N/A "; }
    if ($pass) { return $color ? "${text_green}PASS${text_default}" : "PASS"; }
    return $color ? "${text_red}FAIL${text_default}" : "FAIL";
}

sub get_root_dir {
    if ((-d "./bin") and (-d "./targets")) { return "."; }
    # do something more interesting later
    else { script_error("This utility needs to be run at the root of a valid repo, for safety"); }
}

sub script_info {
    my ($text) = @_;
    if (not defined $text) { $text = "Unknown internal info"; }
    print("[INFO] $PROGNAME: ${text}\n");
}

sub script_error {
    my ($text, $code) = @_;
    if (not defined $text) { $text = "Unknown internal error"; }
    if (not defined $code) { $code = -1; }
    $ret = ($code ? " (ret $code)" : "");
    print("${text_red}[ERROR] $PROGNAME: ${text}${ret}\n${text_default}");
    if ($code) { exit $code; }
}

sub script_usage {
    print <<EOM;

oc_regression.pl [-xpr] [-not] [target..target] [-user <app>] [-sim] [-build] [-tcl <code>..-tcl <code>]

Where:
-xpr               Work on XPR type targets (this is the default)
-not               The list of targets are the ones NOT to work on
-asis              Don't make changes to project (load/unload userspace, etc).  Run as-is.
[target]           Optionally, a list of specific targets
[-sim]             Run simulations
[-build]           Build FPGA
[-user <app>]      Load userspace with <app>
[-tcl "<code>"]    TCL statement to run after loading target (and userspace if any)

Runs a regression test on a set of targets.

Examples:

oc_regression.pl -not u200 vcu1525 -user ethernet_switch -sim -build

EOM
}
