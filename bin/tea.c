#include <stdint.h>
#include <stdio.h>

#define DELTA 0x9e3779b9
#define MX (((z>>5^y<<2) + (y>>3^z<<4)) ^ ((sum^y) + (key[(p&3)^e] ^ z)))

void btea(uint32_t *v, int n, uint32_t const key[4]) {
    uint32_t y, z, sum;
    unsigned p, rounds, e;
    if (n > 1) {          /* Coding Part */
      rounds = 6 + 52/n;
      sum = 0;
      z = v[n-1];
      do {
        sum += DELTA;
        e = (sum >> 2) & 3;
        for (p=0; p<n-1; p++) {
          y = v[p+1];
          z = v[p] += MX;
        }
        y = v[0];
        z = v[n-1] += MX;
      } while (--rounds);
    } else if (n < -1) {  /* Decoding Part */
      n = -n;
      rounds = 6 + 52/n;
      sum = rounds*DELTA;
      y = v[0];
      do {
        e = (sum >> 2) & 3;
        for (p=n-1; p>0; p--) {
          z = v[p-1];
          printf("DEBUG0: n=%d p=%d y=%08x z=%08x v[0]=%08x v[1]=%08x\n", n, p, y, z, v[0], v[1]);
          y = v[p] -= MX;
          printf("DEBUG1: n=%d p=%d y=%08x z=%08x v[0]=%08x v[1]=%08x\n", n, p, y, z, v[0], v[1]);
        }
        z = v[n-1];
        printf("DEBUG2: n=%d p=%d y=%08x z=%08x v[0]=%08x v[1]=%08x\n", n, p, y, z, v[0], v[1]);
        y = v[0] -= MX;
        printf("DEBUG3: n=%d p=%d y=%08x z=%08x v[0]=%08x v[1]=%08x\n", n, p, y, z, v[0], v[1]);
        sum -= DELTA;
        printf("DEBUG4: sum=%08x\n", sum);
      } while (--rounds);
    }
  }


int main () {

  uint32_t k[4];
  uint32_t d[2];

  k[0] = 0x11111111;
  k[1] = 0x22222222;
  k[2] = 0x33333333;
  k[3] = 0x44444444;

  d[0] = 0x1430a485;
  d[1] = 0x89abcdef;

  printf("Encrypt setup...\n");
  for (int i=0; i<4; i++) printf("k[%d] = 0x%08x\n", i, k[i]);
  for (int i=0; i<2; i++) printf("d[%d] = 0x%08x\n", i, d[i]);
  printf("About to encrypt...\n");
  btea(d, 2, k);
  printf("Post-encrypt...\n");
  for (int i=0; i<4; i++) printf("k[%d] = 0x%08x\n", i, k[i]);
  for (int i=0; i<2; i++) printf("d[%d] = 0x%08x\n", i, d[i]);
  printf("Decrypt setup...\n");
  k[0] = 0x11111111;
  k[1] = 0x22222222;
  k[2] = 0x33333333;
  k[3] = 0x44444444;
  for (int i=0; i<4; i++) printf("k[%d] = 0x%08x\n", i, k[i]);
  for (int i=0; i<2; i++) printf("d[%d] = 0x%08x\n", i, d[i]);
  printf("About to decrypt...\n");
  btea(d, -2, k);
  printf("Post-decrypt...\n");
  for (int i=0; i<4; i++) printf("k[%d] = 0x%08x\n", i, k[i]);
  for (int i=0; i<2; i++) printf("d[%d] = 0x%08x\n", i, d[i]);

}
