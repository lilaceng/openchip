#!/usr/bin/perl

use Data::Dumper;

# SPDX-License-Identifier: MPL-2.0

use Term::ReadKey;

my $text_default = "\e[0m";
my $text_red = "\e[31m";
my $text_green = "\e[32m";
my $PROGNAME = "OC_FOREACH_TARGET";

if (scalar(@ARGV) eq 0) { script_usage(); script_error("Need arguments"); }

my $not = 0;
my @targets = ();
my $xpr = 1;
my $script = undef;
my $tcl = undef;
my @args = ();

while (scalar(@ARGV)) {
    my $arg = shift(@ARGV);
    if ((defined $script) or (defined $tcl)) { push(@args, $arg); } # we've already got command, all else is args
    elsif ($arg =~ /^\-h/) { script_usage(); exit(0); }
    elsif ($arg =~ /^\-not/) { $not = 1; }
    elsif ($arg =~ /^\-xpr/) { $xpr = 1; }
    elsif ($arg =~ /^\-tcl/) { $tcl = shift(@ARGV); } # raw tcl command
    elsif ($arg =~ /^\-script/) { $script = shift(@ARGV); } # script command
    elsif (-f $arg) { $script = $arg; } # inferred script command
    else { push(@targets, $arg); } # we haven't yet been given a command, this is a target
}
if ($not and (scalar(@targets)==0)) {
    script_error("Need to give at least one target with -not argument");
}
if ((not defined $script) and (not defined $tcl)) {
    script_error("Need to provide a TCL command (-tcl) or script (-script) to have something to do");
}
my $root_dir = get_root_dir();
print "$PROGNAME: root: $root_dir\n";
if ( $xpr ) { printf "$PROGNAME: XPR targets %s%s\n", ($not ? "not " : ""), join(',', @targets); }

# *** create the list of targets

# * start with a list of known targets
my %known_targets = ();
if (not -d "$root_dir/targets") { script_error("Encountered unexpected non-directory: $root_dir/targets"); }
opendir(my $DIR, "$root_dir/targets") or script_error("Unable to open directory: $root_dir/targets");
while (readdir $DIR) {
    my $short = $_;
    if (($short eq ".") or ($short eq "..")) { next; }
    my $target_dir = "$root_dir/targets/$short";
    if (-f "$target_dir/${short}.xpr") { $known_targets{$short} = "$target_dir/${short}.xpr"; }
    elsif ($short =~ /^([^_]+)_/) {
        if (-f "$target_dir/${1}.xpr") { $known_targets{$short} = "$target_dir/${1}.xpr"; }
    }
}
closedir($DIR);
printf "$PROGNAME: known targets: %s\n", join(',',sort keys %known_targets);

# * create a list of targets to work on
my %work_targets = ();
if ($not or (scalar(@targets)==0)) { # if we are inverting a target list, or we don't have a target list...
    # if we have targets, we are inverting, so remove them from known_targets
    foreach my $target (@targets) {
        if (exists $known_targets{$target}) { delete $known_targets{$target}; }
        else { script_error("Target $target doesn't exist"); }
    }
    # regardless of whether we had targets or not, we now add all (remaining?) known_targets
    foreach my $known_target (sort keys %known_targets) { $work_targets{$known_target} = $known_targets{$known_target}; }
} else {
    # we got an explicit target list
    foreach my $target (@targets) {
        if (exists $known_targets{$target}) { $work_targets{$target} = $known_targets{$target}; }
        else { script_error("Target $target doesn't exist"); }
    }
}

printf "$PROGNAME: Resolved targets %s\n", join(',', sort keys %work_targets); 

# *** create a script file if we were given straight tcl, after which we always assume script mode
my $script_file;
if (defined $script) {
    $script_file = $script;
}
else { # we must have been given a TCL command directly
    $script_file = "$root_dir/oc_foreach_target.temp.tcl";
    open(OUT, ">$script_file") or script_error("Cannot screate $script_file");
    printf OUT "%s%s%s\n", $tcl, (scalar(@args)?" ":""), join(" ",@args);
    close(OUT);
    @args = (); # we've already incorporated the arguments
}

# *** iterate the targets
foreach my $target (sort keys %work_targets) {
    if ($xpr) { # we validated above that we have an XPR file
        my $tclargs = (scalar(@args) ? (" -tclargs ".join(' ', @args)) : "");
        my $xpr = $work_targets{$target};
        my $cmd = "vivado -mode batch -source ${script_file} -log vivado.${target}.log -nojournal ${xpr}${tclargs}";
        print("EXEC: ${text_green}${cmd}${text_default}\n");
        system($cmd);
    }
}

print "$PROGNAME: Done.  Have a nice day.\n";






sub get_root_dir {
    if ((-d "./bin") and (-d "./targets")) { return "."; }
    # do something more interesting later
    else { script_error("This utility needs to be run at the root of a valid repo, for safety"); }
}

sub script_error {
    my ($text, $code) = @_;
    if (not defined $text) { $text = "Unknown internal error"; }
    if (not defined $code) { $code = -1; }
    print("${text_red}[ERROR] $PROGNAME: $text (ret $code)\n${text_default}");
    exit $code;
}

sub script_usage {
    print <<EOM;

oc_foreach_target.pl [-xpr] [-not] [target..target] [[-script] <file>] [-tcl <cmd>] [arg..arg]

Where:
-xpr               Work on XPR type targets (this is the default)
-not               The list of targets are the ones NOT to work on
[target]           Optionally, a list of specific targets
[-script] <file>   The script (generally TCL) to run on the project
[-tcl] <cmd>       A TCL command to run within the project
[arg..arg]         Optionally, a list of arguments ot the TCL command or script

Runs a command or script on every matching target.  The cwd of the project will be the root of
the openchip repo (which is where this should be run from), and TCL may need to be aware. 

Examples:

oc_foreach_target.pl add_files -fileset sim_1 sim/sim_fifo.sv  # runs a TCL command, note path

EOM
}
