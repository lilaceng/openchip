#!/usr/bin/perl

# SPDX-License-Identifier: MPL-2.0

use Term::ReadKey;

my $text_default = "\e[0m";
my $text_red = "\e[31m";
my $text_green = "\e[32m";
my $PROGNAME = "OC_RENAME";

if (scalar(@ARGV) eq 0) { script_usage(); script_error("Need arguments"); }

my $keep_orig = 0;
my $partial_match = 0;
my $interactive = 0;
my $response_all = 1;
my $from = undef;
my $to = undef;
while (scalar(@ARGV)) {
    my $arg = shift(@ARGV);
    if ($arg =~ /^\-h/) { script_usage(); exit(0); }
    elsif ($arg =~ /^\-k/) { $keep_orig = 1; }
    elsif ($arg =~ /^\-p/) { $partial_match = 1; }
    elsif ($arg =~ /^\-i/) { $interactive = 1; $response_all = undef; $|=1; }
    elsif ($arg =~ /^\-/) { script_usage(); script_error("Unknown option: \"$arg\""); }
    elsif ($arg =~ /\w/) {
        if (defined $from) {
            if (defined $to) { script_usage(); script_error("Too many arguments with: \"$arg\""); }
            $to = $arg;
            print "$PROGNAME: Renaming to \"$to\"\n";
        } else {
            $from = $arg;
            print "$PROGNAME: Renaming from \"$from\"\n";
        }
    }
}

my $root_dir = get_root_dir();
print "$PROGNAME: Renaming starting at root: $root_dir\n";
rename_dir($root_dir);

print "$PROGNAME: Done.  Have a nice day.\n";

sub get_response {
    my ($text) = @_;
    while (1) {
        if (defined $response_all) {
            print("[INFO] ${text_green}${text}${text_default}\n");
            return $response_all;
        }
        print("[QUERY] $text (y/n/Y/N): ");
        ReadMode 'cbreak';
        my $key = ReadKey(0);
        ReadMode 'normal';
        print("[$key]\n");
        if ($key eq "Y") { $response_all = 1; }
        elsif ($key eq "y") {
            print("[INFO] ${text_green}${text}${text_default}\n");
            return 1;
        }
        elsif ($key eq "N") { $response_all = 0;  }
        elsif ($key eq "n") { return 0; }
        else { print("[WARNING] Didn't understand \"$key\"\n"); }
    }
}

sub rename_file {
    my ($dir, $short, $full) = @_;
    if (not is_source_file($short)) {
        # print("[DEBUG] rename_file $dir $short $full, not a source file, returning\n");
        return;
    }
#    print("[DEBUG] rename_file $dir $short $full\n");
    if (not -f $full) { script_error("Encountered unexpected non-file: $full"); }
    # prepare our regexp
    my $regexp = (($partial_match ? "" : (($to =~ /^\w/) ? "\\b" : "")).
                  quotemeta($from).
                  ($partial_match ? "" : (($to =~ /\w$/) ? "\\b" : "")));
#    print("[DEBUG] regexp $regexp\n");
    if ($short =~ /$regexp/) {
        # we need to rename the file, it matches
        my $new_short = $short;
        $new_short =~ s/$regexp/$to/;
        my $new_full = "$dir/$new_short";
        if (get_response("Rename $full -> ${text_green}$new_full${text_default}")) {
            rename($full,$new_full) or script_error("Failed to rename \"$full\" to \"$new_full\"");
            $full = $new_full;
        }
    }
    elsif ($full =~ /$regexp/) {
        # we're moving a file between directories
        my $new_full = $full;
#        print("[DEBUG] full $full regexp $regexp on $to\n");
        if ($new_full =~ s/$regexp/$to/) {
#            print("[DEBUG] new_full $new_full\n");
        } else {
            print("[DEBUG] regexp $regexp failed on $to\n");
        }
        if (get_response("Move $full -> ${text_green}$new_full${text_default}")) {
            rename($full, $new_full) or script_error("Failed to rename \"$full\" to \"$new_full\"");
            $full = $new_full;
        }
    }
    # look through the file, see if we're gonna need to rewrite it
    my $found = 0;
#    print("[DEBUG] opening $full\n");
    open(FILE, $full) or script_error("Unable to open file: $full");
    my $line = 0;
    while (<FILE>) {
        my $text = $_;
        $line++;
        if ($text =~ /$regexp/) {
            $found = 1;
            print("[INFO] [$full:$line] $text");
        }
    }
    close(FILE);
    # if we found it, we need to go through and change it
    if ($found and get_response("Update $full")) {
        my $orig_full = $full.".orig";
#        print("[DEBUG] renaming $full -> $orig_full\n");
        rename($full, $orig_full) or script_error("Failed to rename \"$full\" to \"$orig_full\"");
        open(IN, $orig_full) or script_error("Unable to open file: $orig_full");
        open(OUT, ">$full") or script_error("Unable to open file for writing: $full");
        $line = 0;
        while(<IN>) {
            my $text = $_;
            $line++;
            if ($text =~ s/$regexp/$to/g) {
                print("[INFO] [$full:$line] ${text_green}$text${text_default}");
            }
            print OUT $text;
        }
        close(OUT);
        close(IN);
        if (not $keep_orig) {
#        print("[DEBUG] removing $orig_full\n");
            unlink($orig_full);
        }
    }
}

sub rename_dir {
    my ($dir) = @_;
#    print("[DEBUG] rename_dir $dir\n");
    if (not -d $dir) { script_error("Encountered unexpected non-directory: $dir"); }
    opendir(my $DIR, $dir) or script_error("Unable to open directory: $dir");
    while (readdir $DIR) {
        my $short = $_;
        if (($short eq ".") or ($short eq "..")) { next; }
        my $full = "$dir/$short";
        if (-d $full) { rename_dir($full); }
        elsif (-f $full) { rename_file($dir, $short, $full); }
        else { script_error("Not sure how to handle: $full"); }
    }
    closedir($DIR);
}

sub is_source_file {
    my ($name) = @_;
    return (($name =~ /\.v$/) or ($name =~ /\.vh$/) or ($name =~ /\.sv$/) or
            ($name =~ /\.vhd$/) or ($name =~ /\.h$/) or ($name =~ /\.cpp$/) or
            ($name =~ /\.c$/) or ($name =~ /\.h$/) or ($name =~ /\.cpp$/) or
            ($name =~ /\.xci$/) or ($name =~ /\.xpr$/) or 
            ($name =~ /\.xdc$/) or ($name =~ /\.sdc$/) or
            ($name =~ /\.pl$/) or ($name =~ /\.py$/) or ($name =~ /\.tcl$/) or
            # ($name =~ /\.txt$/) or
            0
        );
}

sub get_root_dir {
    if ((-d "./bin") and (-d "./targets")) { return "."; }
    # do something more interesting later
    else { script_error("This utility needs to be run at the root of a valid repo, for safety"); }
}

sub script_error {
    my ($text, $code) = @_;
    if (not defined $text) { $text = "Unknown internal error"; }
    if (not defined $code) { $code = -1; }
    print("${text_red}[ERROR] $PROGNAME: $text (ret $code)\n${text_default}");
    exit $code;
}

sub script_usage {
    print <<EOM;

oc_rename.pl [-k] [-p] <from> <to>

Where:
-k         Keep old versions of files
-p         Partial match, used to modify prefixes, etc, but be careful...
-i         Interactive mode

Refactors occurences of <from> into <to>, renaming files and replacing the terms within all (source) files.
Any open projects should be closed during this operation, during which they will be updated.
This should work for all OpenChip source code, either through updating source or this utility :)

Examples:

oc_rename.pl lib_synchronizer lplib_synchronizer  # renames the files, and all occurences of lib_synchronizer in the source
oc_rename.pl targets/tb.sv sim/tb.sv # moves the file, and updates references to the new path throughout source

EOM
}
