#!/usr/bin/python3

# SPDX-License-Identifier: MPL-2.0

import mmap
import os
import struct
import time
import getopt,sys
import subprocess
import cmd
import socket
import shlex
import traceback

from re import match, search, escape, compile, findall
try:
  import readline
  got_readline = 1
except:
  got_readline = 0
  pass

# maybe at some point, pull in this code, as PySerial is pure python.  Currently it needs "pip install PySerial"...
import serial

class FakeSerial:
    def __init__(self, port, timeout) -> None:
        try:
            tcpTuple = port.split(":", 1)[1].split(",")
            self.ip = tcpTuple[0]
            self.baseport = int(tcpTuple[1])
            self.devno = int(tcpTuple[2])
            self.inBuf = bytearray()
            self.timeout = timeout
        except:
            raise AttributeError("Fake serial port must be specified as: tcp:IPAddress,UARTBasePort,UARTDeviceNumber")
        self.sck = socket.create_connection(("127.0.0.1", self.baseport+self.devno))
        self.sck.settimeout(0)
    def read(self, nBytes):

        starttime = time.monotonic()

        while (len(self.inBuf) < nBytes) and ((time.monotonic() - starttime) < self.timeout):
            try:
                data = self.sck.recv(4096)
            except BlockingIOError:
                data = b''
            self.inBuf.extend(data)

        requested = self.inBuf[:nBytes]
        nextdata = self.inBuf[nBytes:]
        self.inBuf = nextdata
        return bytes(requested)
    def write(self, data):
        ret = self.sck.send(data)
        return ret
    def close(self):
        self.sck.close()
    @property
    def in_waiting(self):
        return len(self.inBuf)
    @property
    def out_waiting(self):
        return 0

debug = 0

if got_readline:
    histfile = os.path.join(os.path.expanduser("~"), ".oc_tty_history")
    try:
        readline.read_history_file(histfile)
        readline.set_history_length(1000)
    except FileNotFoundError:
        pass

class HandledError(Exception):
    pass

def main():
    print("\nOC_TTY: OC Framework serial debugger\n")

    try:
        opts,args = getopt.getopt(sys.argv[1:],
                                  "hs:b:",
                                  ["help", "serial=", "baud="])
    except getopt.GetoptError as err:
        print(str(err))
        usage()
        sys.exit(2)

    # setup defaults before parsing args
    port = "COM1" if os.name == 'nt' else "/dev/ttyS0"
    baud = 460800
#    baud = 115200

    for opt,arg in opts:
        if opt in ['-h', '--help']:
            usage()
            sys.exit(0)
        if opt in ['-s', '--serial']:
            port = arg
        if opt in ['-b', '--baud']:
            baud = arg

    try:
      ser = serial_open(port, baud)
    except Exception as e:
      print(str(e))
      usage()
      sys.exit(2)

    interactive(ser)
    serial_close(ser)


interactive_channel = 0
interactive_space = 0
mode_binary = 1

def interactive_get_channel_space(parts):
    global interactive_channel
    global interactive_space
    if (len(parts)): # we have been given channel + space
        if (len(parts)<2):
            return 2 # return 2 when we can't proceeed, caller prints an appropriate error
        channel = eval_number(parts.pop(0))
        space = eval_number(parts.pop(0))
        if ((channel < 0) or (channel > 0xff)):
            print("ERROR: channel %02x is not an 8-bit value" % (channel))
            return 1 # return 1, which tells caller that we've already printed an error message
        elif ((space < 0) or (space > 0xf)):
            print("ERROR: space %04x is not a 4-bit value" % (space))
            return 1
        else:
            interactive_channel = channel
            interactive_space = space
    return 0

def eval_true(text):
    text = apply_global_vars(text)
    if text=="t" or text=="true" or text=="1" or text=="yes" or text=="on": return 1
    if text=="f" or text=="false" or text=="0" or text=="no" or text=="off": return 0
    print("ERROR: expected a binary value, got '%s', assuming that means 'false'" % (text))
    return 0

def eval_number(text, ishex=False, check_uint_bits=False, check_int_min=False, check_int_max=False):
    # if we've been given an int already, set the value to that
    if isinstance(text, int):
      value = text
    else:
      mult = 1
      # do variable substitution
      text = apply_global_vars(text)
      # look for a prefix for human friendly number, note these suffixes work on hex numbers too
      m = match(r'^(.*)[Kk]$', text)
      if m:
        mult = 1024
        text = m.group(1)
      m = match(r'^(.*)[Mm]$', text)
      if m:
        mult = 1024*1024
        text = m.group(1)
      m = match(r'^(.*)[Gg]$', text)
      if m:
        mult = 1024*1024*1024
        text = m.group(1)
      # substition is done, multiplier is extracted, time to look for a number
      value = False
      if not ishex: # if we've been told it's hex, we don't do this match, because hex can look decimal
        m = match(r'^(\d+)$', text)
        if m: value = int(m.group(1)) * mult
      if (type(value) != int):
        m = match(r'^0x([\da-fA-F]+)$', text) # if it's starts with 0x, then it's hex
        if m: value = int(m.group(1), 16) * mult
      if (type(value) != int):
        m = match(r'^([\da-fA-F]+)$', text) # if it has a-f, then it's hex
        if m: value = int(m.group(1), 16) * mult
    # if we don't have value as an int by here, we are out of ideas
    if (type(value) != int):
        print("ERROR: expected a number, got '%s'" % (text))
        raise HandledError
    # if we're given a number of uint bits to check, convert to int min/max
    if (type(check_uint_bits) == int):
      check_int_min = 0
      check_int_max = (1 << check_uint_bits)-1
    # check value is within int min/max
    if (type(check_int_min) == int):
        if (value < check_int_min):
            print("ERROR: expected a number >= %d, got %d" % (check_int_min, value))
            raise HandledError
    if (type(check_int_max) == int):
        if (value > check_int_max):
            print("ERROR: expected a number <= %d, got %d" % (check_int_max, value))
            raise HandledError
    return value

def eval_regex(regex, text):
    global m
    text = apply_global_vars(text)
    m = match(regex, text)
    return m

def memtest_usage():
    print("INFO: memtest <channel>")

global_vars = {}
def apply_global_vars(text):
    global global_vars
    for key in global_vars.keys():
        text = text.replace("$%s" % key, global_vars[key])
    return text

source_stack = []
def interactive_get_line(ser):
  global source_stack
  if (len(source_stack) > 0):
    # we are sourcing commands from a file, if there's more than one we finish the latest (last) one
    source_lines = source_stack[len(source_stack)-1]
    line = source_lines.pop(0)
    full_line = ""
    m = match(r'^(.*)\\$', line)
    while (m):
      full_line += m.group(1)
      line = source_lines.pop(0)
      m = match(r'^(.*)\\$', line)
    full_line += line
    if (len(source_lines) == 0):
      source_stack.pop(-1) # we are done with all lines from the latest list, pop the list off the stack
    return full_line
  else:
    # we have no lines from source commands
    line = input('OC_TTY->')
    full_line = ""
    m = match(r'^(.*)\\$', line)
    while (m):
      full_line += m.group(1)
      line = input('        ')
      m = match(r'^(.*)\\$', line)
    full_line += line
    return full_line

chip_info = []
chip_info_loaded = False

def chip_info_load(ser):
    global chip_info
    global chip_info_loaded
    if chip_info_loaded: return
    print("INFO: Loading chip info")
    serial_write(ser, "I", expect_echo=1)
    chip_info = ser.read(64)
    chip_info_loaded = True
    serial_write(ser, "", expect_prompt=1)

def process_tokens(ser, parts):
    global debug
    global interactive_channel
    global interactive_space
    global mode_binary
    global global_vars
    global source_stack
    global chip_info
    global m
    command = parts.pop(0)
    try:
      if command == 'quit' or command == 'q':
        readline.write_history_file(histfile)
        sys.exit(0)
      elif command == 'history' or command == 'h' or command == 'hi' or command == 'hist':
        if (len(parts) < 1):  num_to_print = 10
        else:                 num_to_print = eval_number(parts.pop(0))
        num_items = readline.get_current_history_length() + 1
        history_items = [ readline.get_history_item(i) for i in range(1, num_items)  ]
        for item in history_items[-num_to_print:]:
          print(item)
      elif command == 'ping' or command == 'p':
        ping(ser)
      elif command == 'reset':
        reset(ser)
      elif command == 'debug':
        if (len(parts) < 1):
          print("INFO: debug level is currently %d, change with 'debug <level>'" % (debug))
          return
        debug = eval_number(parts.pop(0))
      elif command == 'source':
        if (len(parts) < 1):
          print("INFO: source <script file>")
          print("ERROR: need a script file to source")
          return
        if (not os.path.exists(parts[0])):
          print("ERROR: %s does not appear to be a file" % (parts[0]))
          return
        file1 = open(parts[0], 'r')
        lines = file1.readlines()
        source_stack.append(lines) # push the list of lines onto the list of sources in progress
        file1.close()
      elif command == 'import':
        if (len(parts) < 1):
          print("INFO: import <python file>")
          print("ERROR: need a python file to import")
          return
        if (not os.path.exists(parts[0])):
          print("ERROR: %s does not appear to be a file" % (parts[0]))
          return
        exec(open(parts.pop(0)).read(), globals())
      elif command == 'ascii':
        mode_binary = 0
      elif command == 'binary':
        if (len(parts) < 1):
          print("INFO: binary mode is: %s" % ("on" if mode_binary else "off"))
          return
        mode_binary = eval_true(parts.pop(0))
      elif command == 'set':
        if (len(parts) < 1):
          print("INFO: set <var> [<value>]")
          print("ERROR: need at least a variable name")
          return
        key = parts.pop(0)
        if (len(parts)): # set
          global_vars[key] = parts.pop(0)
        else: # get
          if key in global_vars:
            print("INFO: $%s = %s" % (key, global_vars[key]))
          else:
            print("ERROR: $%s does not exist" % (key))
      elif command == 'test':
        print("WARNING: no test defined")
      elif command == 'info':
        chip_info_load(ser)
        print("INFO: Bitstream ID         : %08x" % ((chip_info[0] << 24) | (chip_info[1] << 16) | (chip_info[2] << 8) | (chip_info[3]) ))
        print("INFO: Build Date           : %04x/%02x/%02x" % ((chip_info[4] << 8) | (chip_info[5]), (chip_info[6]), (chip_info[7]) ))
        print("INFO: Build Time           : %02x:%02x" % ((chip_info[8]), (chip_info[9]) ))
        temp = (chip_info[10] << 8) | (chip_info[11])
        text = "unknown"
        if   (temp == 1): text="Xilinx"
        print("INFO: Target Vendor        : %04x (%s)" % (temp, text))
        temp = (chip_info[12] << 8) | (chip_info[13])
        text = "unknown"
        if   (temp == 1): text="vcu1525"
        elif (temp == 2): text="u200"
        elif (temp == 3): text="u50"
        elif (temp == 4): text="u55n"
        elif (temp == 5): text="u50c"
        elif (temp == 6): text="jc35"
        print("INFO: Target Board         : %04x (%s)" % (temp, text))
        temp = (chip_info[14] << 8) | (chip_info[15])
        text = "unknown"
        if   (temp == 1): text="Ultrascale+"
        print("INFO: Target Library       : %04x (%s)" % (temp, text))
        print("INFO: ABBC Channels        : %d" % (chip_info[16]))
        print("INFO: First User Channel   : %d" % (chip_info[17]))
        print("INFO: UART Channels        : %d" % (chip_info[18]))
        userspace = ""
        for i in range (0, 16):
          userspace += chr(chip_info[32+i])
        print("INFO: Userspace            : %s" % (userspace))
        uuid = ""
        for i in range (0, 16):
          uuid += ("%02x" % (chip_info[48+i]))
        print("INFO: UUID                 : %s" % (uuid))
      elif command == 'probe':
        chip_info_load(ser)
        abbcs = chip_info[16]
        abbcFirstUser = chip_info[17]
        print("PROBE: Scanning from 0-%0d" % (abbcFirstUser-1))
        for channel in range(0, abbcFirstUser):
          id = csr_read(ser, channel, 0, 0)
          if ((id >> 16) == 0x0001):
            print("PROBE: Found PLL on channel[%d]" % (channel))
          elif ((id >> 16) == 0x0002):
            print("PROBE: Found CHIPMON on channel[%d]" % (channel))
          elif ((id >> 16) == 0x0003):
            print("PROBE: Found PROTECT on channel[%d]" % (channel))
          elif ((id >> 16) == 0x0004):
            print("PROBE: Found HBM on channel[%d]" % (channel))
          elif ((id >> 16) == 0x0005):
            print("PROBE: Found IIC on channel[%d]" % (channel))
          elif ((id >> 16) == 0x0006):
            print("PROBE: Found LED on channel[%d]" % (channel))
          elif ((id >> 16) == 0x0007):
            print("PROBE: Found GPIO on channel[%d]" % (channel))
          else:
            print("PROBE ERROR: Unknown IP (0x%04x) channel[%d]" % (id, channel))
      elif command == 'perf':
        ops = 10
        start = time.time()
        for i in range(ops):
          csr_write(ser, 2, 0, 1, i, verbose=len(parts))
        writes_done = time.time()
        for i in range(ops):
          csr_read(ser, 2, 0, 5, verbose=len(parts))
        reads_done = time.time()
        print("INFO: CSR performance: %.1f write/sec, %.1f read/sec" % (ops/(writes_done-start), ops/(reads_done-writes_done)))
      elif command == 'read' or command == 'rd' or command == 'r':
        if (len(parts) < 1):
          print("INFO: read|rd|r <address> [<channel> <space> [<length> [<jump>]]]")
          print("ERROR: need at least an address to read")
          return
        address = eval_number(parts.pop(0), ishex=True)
        if interactive_get_channel_space(parts):
          if interactive_get_channel_space(parts) == 2:
            print("INFO: read|rd|r <address> [<channel> <space> [<length> [<jump>]]]")
            print("ERROR: need a channel and space after address")
        if ((address < 0) or (address > 0xffffffff)):
          print("ERROR: address %08x is not a 32-bit value" % (address))
        elif len(parts):
          length = eval_number(parts.pop(0), ishex=True)
          jump = 4
          if (len(parts)):
            jump = eval_number(parts.pop(0), ishex=True)
          on_this_line = 0
          for offset in range(0, length, jump):
            if (on_this_line == 0):
              print("%08x : " % (address+offset), end='')
            data = csr_read(ser, interactive_channel, interactive_space, address + offset)
            print("%08x " % data, end='')
            on_this_line += 1
            if on_this_line == 8:
              print("")
              on_this_line = 0
            if (on_this_line):
              print("")
        else:
          csr_read(ser, interactive_channel, interactive_space, address, verbose=1)
      elif command == 'write' or command == 'wr' or command == 'w':
        if (len(parts) < 2):
          print("INFO: write|wr|w <address> <data> [<channel> <space> [<length> [<jump>]]]")
          print("ERROR: need at least an address and data to write")
          return
        address = eval_number(parts.pop(0), ishex=True)
        data = eval_number(parts.pop(0), ishex=True)
        if interactive_get_channel_space(parts):
          if interactive_get_channel_space(parts) == 2:
            print("INFO: write|wr|w <address> <data> [<channel> <space> [<length> [<jump>]]]")
            print("ERROR: need a channel and space after address")
            return
        if ((address < 0) or (address > 0xffffffff)):
          print("ERROR: address %08x is not a 32-bit value" % (address))
        elif ((data < 0) or (data > 0xffffffff)):
          print("ERROR: data %08x is not a 32-bit value" % (data))
        else:
          csr_write(ser, interactive_channel, interactive_space, address, data, verbose=1)
      elif command == 'pll':
        command_pll(ser, parts)
      elif command == 'chipmon':
        command_chipmon(ser, parts)
      elif command == 'protect':
        command_protect(ser, parts)
      elif command == 'hbm':
        command_hbm(ser, parts)
      elif command == 'memtest':
        command_memtest(ser, parts)
      elif ("command_%s" % command) in globals(): # for commands pulled in via source
        globals()["command_%s" % command](ser, parts)
      else:
        print("ERROR: Didn't understand command: '%s'" % (command))
    except HandledError:
      return
    except Exception as e:
      exc_type, exc_obj, exc_tb = sys.exc_info()
      print("ERROR: Unexpected exception (%s:%s): %s" % (exc_tb.tb_frame.f_code.co_filename, exc_tb.tb_lineno, str(e)))
      print(traceback.format_exc())
      return

def interactive(ser):
    global debug
    global m
    serial_flush(ser)
    while 1:
        line = interactive_get_line(ser)
        m = match(r'^(.*)\#.*$', line)
        if m: line = m.group(1)
        parts = shlex.split(line)
        if len(parts) == 0:
            continue
        process_tokens(ser, parts)

def command_pll(ser, parts):
    if (len(parts) < 1):
        print("INFO: pll <channel>")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    if (len(parts) == 0):
      dump_pll(ser, channel)
      return
    while (len(parts)):
        cmd = parts.pop(0)
        if (eval_regex(r'^measure$', cmd)):
           measure_pll(ser, channel, 0, 2)
           return
        elif (eval_regex(r'^reset$', cmd)):
           reset_pll(ser, channel)
           return
        elif (eval_regex(r'^ramp$', cmd)):
           orig = csr_read(ser, channel, 1, 0x9)
           keep = orig & 0x00008fff
           start = (orig >> 12) & 7
           for i in range(start, -2, -1): # start at current value, then move it downwards (up in freq)
               frac_phase = i if (i>=0) else 7 # translate -1 to 7
               print("PLL INFO: writing CLK0 frac_phase to %d" % frac_phase)
               reset_pll(ser, channel, enable=1)
               csr_write(ser, channel, 1, 0x9, keep | (frac_phase<<12))
               reset_pll(ser, channel, disable=1)
               measure_pll(ser, channel, 0)
               parts_copy = parts[:]
               process_tokens(ser, parts_copy)
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x9, orig)
           reset_pll(ser, channel, disable=1)
           return
        elif (eval_regex(r'^clk0_up$', cmd)):
           orig = csr_read(ser, channel, 1, 0x9)
           keep = orig & 0x00008fff
           newval = ((orig >> 12) & 7)-1
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x9, keep | (newval<<12))
           reset_pll(ser, channel, disable=1)
           measure_pll(ser, channel, 0)
           return
        elif (eval_regex(r'^clk0_down$', cmd)):
           orig = csr_read(ser, channel, 1, 0x9)
           keep = orig & 0x00008fff
           newval = ((orig >> 12) & 7)+1
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x9, keep | (newval<<12))
           reset_pll(ser, channel, disable=1)
           measure_pll(ser, channel, 0)
           return
        elif (eval_regex(r'^all_up$', cmd)):
           orig = csr_read(ser, channel, 1, 0x14)
           keep = orig & 0xf000
           low = (orig >> 0) & 31 # clock low time
           high = (orig >> 6) & 31 # clock high time
           # we need to increment, and will increment whichever is lower
           if (low <= high): low += 1
           else: high += 1
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x14, keep | (low<<0) | (high<<6))
           reset_pll(ser, channel, disable=1)
           measure_pll(ser, channel, 0)
           return
        elif (eval_regex(r'^all_down$', cmd)):
           orig = csr_read(ser, channel, 1, 0x14)
           keep = orig & 0xf000
           low = (orig >> 0) & 31
           high = (orig >> 6) & 31
           if (low <= high): high -= 1
           else: low -= 1
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x14, keep | (low<<0) | (high<<6))
           reset_pll(ser, channel, disable=1)
           measure_pll(ser, channel, 0)
           return
        elif (eval_regex(r'^scan$', cmd)):
           orig = csr_read(ser, channel, 1, 0x9)
           keep = orig & 0x00008fff
           for i in range(0, 8, 1):
               print("PLL INFO: writing CLK0 frac_phase to %d" % i)
               reset_pll(ser, channel, enable=1)
               csr_write(ser, channel, 1, 0x9, keep | (i<<12))
               reset_pll(ser, channel, disable=1)
               measure_pll(ser, channel, 0)
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x9, orig)
           reset_pll(ser, channel, disable=1)
           return
        elif (eval_regex(r'^scan2$', cmd)):
           orig = csr_read(ser, channel, 1, 0x14)
           keep = orig & 0xffc0
           for i in range(5,11):
               print("PLL INFO: writing FB low_time to %d" % i)
               reset_pll(ser, channel, enable=1)
               csr_write(ser, channel, 1, 0x14, keep | (i<<0))
               reset_pll(ser, channel, disable=1)
               measure_pll(ser, channel, 0)
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x14, orig)
           reset_pll(ser, channel, disable=1)
           return
        elif (eval_regex(r'^scan3$', cmd)):
           orig = csr_read(ser, channel, 1, 0x15)
           keep = orig & 0x8fff
           for i in range(3,9):
               print("PLL INFO: writing FB frac phase to %d" % i)
               reset_pll(ser, channel, enable=1)
               csr_write(ser, channel, 1, 0x15, keep | (i<<12))
               reset_pll(ser, channel, disable=1)
               measure_pll(ser, channel, 0)
               measure_pll(ser, channel, 0)
               measure_pll(ser, channel, 0)
           reset_pll(ser, channel, enable=1)
           csr_write(ser, channel, 1, 0x15, orig)
           reset_pll(ser, channel, disable=1)
           return
        elif (eval_regex(r'^throttle=(.+)$', cmd)):
            throttle_pll(ser, channel, eval_number(m.group(1)))
        elif (eval_regex(r'^freq=(.+)$', cmd)):
            freq = eval_number(m.group(1))
            # we are going to try to program the PLL to this freq
            clock0frac = (csr_read(ser, channel, 1, 0x9) >> 12) & 0x7
        else:
            print("PLL ERROR: didn't understand option: %s" % cmd)
            error = 1
            return


def command_chipmon(ser, parts):
    if (len(parts) < 1):
        print("INFO: chipmon <channel>")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    dump_chipmon(ser, channel)

def command_hbm(ser, parts):
    if (len(parts) < 1):
        print("INFO: hbm <channel>")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    dump_hbm(ser, channel)

def command_led(ser, parts):
    if (len(parts) < 1):
        print("INFO: led <channel> ...")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    if (len(parts) == 0):
      dump_led(ser, channel)
      return
    leds = csr_read(ser, channel, 0, 0x0) & 0xff
    led = -1
    brightness = 0x3f
    command = -1 # report status
    while (len(parts)):
        cmd = parts.pop(0)
        if (eval_regex(r'^(\d+)$', cmd)):
           led = eval_number(m.group(1))
        elif (eval_regex(r'^status$', cmd)):
           command = -1
        elif (eval_regex(r'^off$', cmd)):
           command = 0
        elif (eval_regex(r'^on$', cmd)):
           command = 1
        elif (eval_regex(r'^blink$', cmd)):
           command = 2
        elif (eval_regex(r'^heartbeat$', cmd)):
           command = 3
        elif (eval_regex(r'^brightness=(.+)', cmd)):
          brightness = eval_number(m.group(1))
        elif (eval_regex(r'^count=(.+)', cmd)):
          brightness = eval_number(m.group(1))
        else:
            print("LED ERROR: didn't understand option: %s" % cmd)
            error = 1
            return
    if command == -1: # we reporting status
      if led == -1: # on all LEDs
        for i in range(leds):
          print("LED %3d : %s" % (i, led_status(ser, channel, i)))
      else:
        print("LED %3d : %s" % (led, led_status(ser, channel, led)))
    else:
      if led == -1: # on all LEDs
        for i in range(leds):
          csr_write(ser, channel, 0, 0x2 + i, ((brightness<<8) | command) )
      else:
        csr_write(ser, channel, 0, 0x2 + led, ((brightness<<8) | command) )

def led_status(ser, channel, led):
  data = csr_read(ser, channel, 0, 0x2 + led)
  if ((data & 0x3) == 1): return ("on (%d%% bright)" % (100 * ((data >> 8) & 0x3f) / 63))
  elif ((data & 0x3) == 2): return ("blink (%d times)" % ((data >> 8) & 0x7))
  elif ((data & 0x3) == 3): return ("heartbeat (%d%% bright)" % (100 * ((data >> 8) & 0x3f) / 63))
  return "off"

def command_iic(ser, parts):
    if (len(parts) < 1):
        print("INFO: iic <channel> ...")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    if (len(parts) == 0):
      dump_iic(ser, channel)
      return
    while (len(parts)):
        cmd = parts.pop(0)
        if (eval_regex(r'^manual$', cmd)):
           i2c_config(ser, channel)
        elif (eval_regex(r'^scan$', cmd)):
           i2c_scan()
        else:
            print("IIC ERROR: didn't understand option: %s" % cmd)
            error = 1
            return

gpio_names = {}

def command_gpio(ser, parts):
    if (len(parts) < 1):
        print("INFO: gpio <channel> ...")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    if (len(parts) == 0):
      dump_gpio(ser, channel)
      return
    gpios = csr_read(ser, channel, 0, 0x0) & 0xff
    gpio = -1
    command = -1 # report status
    name = ""
    while (len(parts)):
        cmd = parts.pop(0)
        if (eval_regex(r'^(\d+)$', cmd)):
           gpio = eval_number(m.group(1))
        elif (eval_regex(r'^name=(.+)$', cmd)):
           name = m.group(1)
        elif (eval_regex(r'^status$', cmd)):
           command = -1
        elif (eval_regex(r'^tri', cmd)):
           command = 0
        elif (eval_regex(r'^low$', cmd)):
           command = 16
        elif (eval_regex(r'^high$', cmd)):
           command = 17
        else:
            print("GPIO ERROR: didn't understand option: %s" % cmd)
            error = 1
            return

    if name != "": # we are setting the name
      if gpio == -1: # on all GPIOs
        print("Need to specify a GPIO number in order to name it")
        error = 1
        return
      gpio_names[gpio] = name
    elif command == -1: # we reporting status
      if gpio == -1: # on all GPIOs
        for i in range(gpios):
          if i in gpio_names: gpio_name = gpio_names[i]
          else: gpio_name = "Unnamed"
          print("GPIO %3d [%-20s]: %s" % (i, gpio_name, gpio_status(ser, channel, i)))
      else:
        if gpio in gpio_names: gpio_name = gpio_names[gpio]
        else: gpio_name = "Unnamed"
        print("GPIO %3d [%-20s]: %s" % (gpio, gpio_name, gpio_status(ser, channel, gpio)))
    else:
      if gpio == -1: # on all GPIOs
        for i in range(gpios):
          csr_write(ser, channel, 0, 0x1 + i, command)
      else:
        csr_write(ser, channel, 0, 0x1 + gpio, command)

def gpio_status(ser, channel, gpio):
  data = csr_read(ser, channel, 0, 0x1 + gpio)
  if ((data & 0x11) == 0x10): outstring = "low"
  elif ((data & 0x11) == 0x11): outstring = "high"
  else: outstring = "tri"
  if (data & 0x100): instring = "high"
  else: instring = "low"
  if ((outstring != "tri") and (instring != outstring)): errstring = " (MISMATCH)"
  else: errstring = ""
  return "OUT: %-4s IN: %-4s%s" % (outstring, instring, errstring)

memtest_results = []

def memtest_test(ser, channel, parts):
    global memtest_results
    global test_status
    # this is a bit weird, if we get a 'test' command in command_memtest, it calls memtest_test, which then calls back into
    # commmand_memtest multiple times.
    args = {'ops': '0x40000000', # 1Bi operations
            'verbose': '1',
            'burst': '1',
            }
    size = 0x10000000 # 256MB, default size we access per engine
    expected_results = [ '5d340004066c8dcb' ]
    long_test = False
    got_expected = False
    test_status = True
    init = 1
    incrementing = False
    random = False
    # now allow overriding the above
    while (len(parts)):
        cmd = parts.pop(0)
        if (eval_regex(r'^long$', cmd)):
            long_test = True
            args['ops'] = '0xffffffff' # 4Mbi operations ... ~20s
            expected_results = [ '5d340004066c8dcb', '5d340004915e8968', '5d340004f6abeaa2', '5d3400050e3d2602' ]
        elif (eval_regex(r'^expect=(.+)$', cmd)):
            if not got_expected:
                got_expected = True
                expected_results.clear()
            expected_results.append(m.group(1))
        elif (eval_regex(r'^(\w+)=(.+)$', cmd)):
            if m.group(1) in args:
                args[m.group(1)] = m.group(2)
            else:
                test_status = False
                print("MEMTEST ERROR: 'memtest test' didn't understand how to set '%s'" % (cmd))
                return
        else:
            test_status = False
            print("MEMTEST ERROR: 'memtest test' didn't understand '%s'" % (cmd))
            return
    # expand args, making it easier to call the test but respecting user choices
    if incrementing:
      if not 'addr_incr_mask' in args:
        args['addr_incr_mask'] = "0x%x" % ((eval_number(args['size'])-1) & ~(32 * eval_number(args['burst'])))
      if not 'addr_incr' in args:
        args['addr_incr'] = "0x%x" % (32 * eval_number(args['burst']))
      if not 'addr_rand_mask' in args:
        args['addr_rand_mask'] = "0x%x" % 0
    if random:
      if not 'addr_incr_mask' in args:
        args['addr_incr_mask'] = "0x%x" % 0
      if not 'addr_incr' in args:
        args['addr_incr'] = "0x%x" % 0
      if not 'addr_rand_mask' in args:
        args['addr_rand_mask'] = "0x%x" % ((eval_number(args['size'])-1) & ~(32 * eval_number(args['burst'])))
    # setup the test args from the dict
    test_args = []
    for arg in args:
        test_args.append("%s=%s" % (arg, args[arg]))
    # now we init the HBM
    if init:
        command_memtest(ser, [channel, "write"] + test_args)
    # dump the config
    if eval_number(args['verbose'])>1:
        dump_memtest(ser, channel)
    # now we do a read test
    command_memtest(ser, [channel, "read"] + test_args)
    # check resulting nonces
    if len(memtest_results) != eval_number(args['count']):
        print("MEMTEST WARNING: count=%s but mining finished with %d nonces" % (args['count'], len(memtest_results)))
        test_status = False
    while (len(expected_results) and len(memtest_results)):
        expected_result = eval_number(expected_results.pop(0), ishex=True)
        memtest_result = memtest_results.pop(0)
        if (expected_result != memtest_result):
            print("MEMTEST ERROR: expected %s but got %s" % (expected_result, memtest_result))
            test_status = False
    if test_status:
        print("MEMTEST INFO: TEST PASSED")

def command_memtest(ser, parts):
    if (len(parts) < 1):
        memtest_usage()
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    if (len(parts) == 0):
        dump_memtest(ser, channel)
        return
    ops = 1
    port_en = 0xffffffff
    addr = 0x0
    addr_incr = 0x20
    addr_incr_mask = 0xffffffffffffffff
    addr_rand_mask = 0
    addr_port_shift = 0
    addr_port_mask = 0
    read_max_id = 0
    write_max_id = 0
    waits = 0
    burst = 1
    measure = 0
    command = 0
    poll = 1
    reading = 0
    writing = 0
    pattern = -1
    error = 0
    stats = 0
    dump = 0
    verbose = 1
    expected_signatures = 32 * [ -1 ]
    get_sigs = 0
    while (len(parts)):
        cmd = parts.pop(0)
        if (eval_regex(r'^test$', cmd)):
            memtest_test(ser, channel, parts)
        elif (eval_regex(r'^write$', cmd)):
            command |= (1<<8)
            writing = 1
        elif (eval_regex(r'^read$', cmd)):
            command |= (1<<16)
            reading = 1
        elif (eval_regex(r'^stats$', cmd)):
            stats = 1
        elif (eval_regex(r'^get_sigs$', cmd)):
            get_sigs = 1
        elif (eval_regex(r'^verbose=(.+)$', cmd)):
            verbose = eval_number(m.group(1))
        elif (eval_regex(r'^dump$', cmd)):
            dump = 1
        elif (eval_regex(r'^ops=(.+)$', cmd)):
            ops = eval_number(m.group(1))
        elif (eval_regex(r'^port_en=(.+)$', cmd)):
            port_en = eval_number(m.group(1), check_uint_bits=32)
        elif (eval_regex(r'^addr=(.+)$', cmd)):
            addr = eval_number(m.group(1), ishex=True, check_uint_bits=64)
        elif (eval_regex(r'^addr_incr=(.+)$', cmd)):
            addr_incr = eval_number(m.group(1), ishex=True, check_uint_bits=64)
        elif (eval_regex(r'^addr_incr_mask=(.+)$', cmd)):
            addr_incr_mask= eval_number(m.group(1), ishex=True, check_uint_bits=64)
        elif (eval_regex(r'^addr_rand_mask=(.+)$', cmd)):
            addr_rand_mask= eval_number(m.group(1), ishex=True, check_uint_bits=64)
        elif (eval_regex(r'^waits=(.+)$', cmd)):
            waits = eval_number(m.group(1), check_uint_bits=32)
        elif (eval_regex(r'^burst=(.+)$', cmd)):
            burst = eval_number(m.group(1), check_int_min=1, check_int_max=8)
        elif (eval_regex(r'^pattern=(.+)$', cmd)):
            pattern = eval_number(m.group(1), ishex=True, check_uint_bits=1)
        elif (eval_regex(r'^signature=(.+)$', cmd)):
            expected_signatures = 32 * [ eval_number(m.group(1), ishex=True, check_uint_bits=32) ]
        elif (eval_regex(r'^signature_(.+)=(.+)$', cmd)):
            expected_signatures[eval_number(m.group(1))] = eval_number(m.group(2), ishex=True, check_uint_bits=32)
        elif (eval_regex(r'^addr_port_shift=(.+)$', cmd)):
            addr_port_shift = eval_number(m.group(1), check_uint_bits=5)
        elif (eval_regex(r'^addr_port_mask=(.+)$', cmd)):
            addr_port_mask = eval_number(m.group(1), check_uint_bits=5)
        elif (eval_regex(r'^write_max_id=(.+)$', cmd)):
            write_max_id = eval_number(m.group(1), check_uint_bits=6)
        elif (eval_regex(r'^read_max_id=(.+)$', cmd)):
            read_max_id = eval_number(m.group(1), check_uint_bits=6)
        elif (eval_regex(r'^prescale$', cmd)):
            command |= 2
        elif (eval_regex(r'^write_len_rand$', cmd)):
            command |= (1 << (4+8))
        elif (eval_regex(r'^read_len_rand$', cmd)):
            command |= (1 << (4+16))
        elif (eval_regex(r'^write_data_rotate$', cmd)):
            command |= (1 << (5+8))
        elif (eval_regex(r'^write_data_rand$', cmd)):
            command |= (1 << (6+8))
        elif (eval_regex(r'^read_data_reorder$', cmd)):
            command |= (1 << (5+16))
        elif (eval_regex(r'^measure$', cmd)):
            measure = 1
        elif (eval_regex(r'^nopoll$', cmd)):
            poll = 0
        else:
            print("ERROR: didn't understand option: %s" % cmd)
            error = 1
            return
    if (measure):
        memtest_measure(ser, channel)
    if writing or reading:
        if verbose>0: print("MEMTEST: Setting up Memtest engine...")
        csr_write(ser, channel, 0, 0x0004, ops-1)
        csr_write(ser, channel, 0, 0x0008, port_en)
        csr_write(ser, channel, 0, 0x0010, (addr>>0) & 0xffffffff)
        csr_write(ser, channel, 0, 0x0014, (addr>>32) & 0xffffffff)
        csr_write(ser, channel, 0, 0x0018, (addr_incr>>0) & 0xffffffff)
        csr_write(ser, channel, 0, 0x001c, (addr_incr>>32) & 0xffffffff)
        csr_write(ser, channel, 0, 0x0020, (addr_incr_mask>>0) & 0xffffffff)
        csr_write(ser, channel, 0, 0x0024, (addr_incr_mask>>32) & 0xffffffff)
        csr_write(ser, channel, 0, 0x0040, waits)
        csr_write(ser, channel, 0, 0x0044, burst-1)
        csr_write(ser, channel, 0, 0x0048, (read_max_id<<8) | write_max_id)
        csr_write(ser, channel, 0, 0x004c, (addr_port_mask<<16) | addr_port_shift)
        csr_write(ser, channel, 0, 0x0050, (addr_rand_mask>>0) & 0xffffffff)
        csr_write(ser, channel, 0, 0x0054, (addr_rand_mask>>32) & 0xffffffff)
        if (pattern == -1) or (pattern == 0):
            pattern = 0x11111111
            patterninc = 0x11111111
        elif (pattern == 1):
            pattern = 0x03020100
            patterninc = 0x04040404
        for i in range(8):
            csr_write(ser, channel, 0, 0x0100+(i*4), pattern)
            pattern += patterninc
        ports = 0
        for i in range(32):
            if (port_en & (1 << i)): ports += 1
        csr_write(ser, channel, 0, 0x0000, command)
        if dump or verbose>1: dump_memtest(ser, channel, short=1)
        if verbose>0: print("MEMTEST: Starting %d ops on %d ports..." % (ops, ports))
        status = 0
        csr_write(ser, channel, 0, 0x0000, command | 0x00000001)
        start = time.time()
        if stats:
          show_membist_stats(ser, channel, verbose)
        if (poll):
            while ((status & 0x80000000) == 0):
              status = csr_read(ser, channel, 0, 0x0000)
            stop = time.time()
            seconds = stop-start
            ops *= ports
            if verbose>0: print("MEMTEST: Done after %8.2f seconds" % (seconds))
            if writing:
                print("MEMTEST: 0x%08x write ops (BL=%d): %8.3fMops/s (%8.3fMB/s) -- %8.3fMops/s (%8.3fMB/s) per port" %
                      (ops, burst, (ops / (seconds*1000000)), ((ops*burst*32) / (seconds*1000000)),
                       (ops / (seconds*1000000*ports)), ((ops*burst*32) / (seconds*1000000*ports))))
            if reading:
                print("MEMTEST: 0x%08x read  ops (BL=%d): %8.3fMops/s (%8.3fMB/s) -- %8.3fMops/s (%8.3fMB/s) per port" %
                      (ops, burst, (ops / (seconds*1000000)), ((ops*burst*32) / (seconds*1000000)),
                       (ops / (seconds*1000000*ports)), ((ops*burst*32) / (seconds*1000000*ports))))
                if get_sigs:
                  sigs_per_line = 4
                  for p in range(32):
                    csr_write(ser, channel, 0, 0x0008, (1<<p))
                    signature = csr_read(ser, channel, 0, 0x0028)
                    if ((p % sigs_per_line) == 0): print("     ", end="")
                    print(" signature_%d=0x%08x" % (p, signature), end="")
                    if ((p % sigs_per_line) == (sigs_per_line-1)): print("")
                  if ((ports%sigs_per_line) != 0): print("") # newline if we don't have full number of ports per line
                else: # checking signatures
                  failure = 0
                  one_passed = 0
                  for p in range(32):
                    expected_signature = expected_signatures[p]
                    if (expected_signature != -1):
                      csr_write(ser, channel, 0, 0x0008, (1<<p))
                      signature = csr_read(ser, channel, 0, 0x0028)
                      if (signature == expected_signature):
                        one_passed = 1
                      else:
                        failure = 1
                        print("MEMTEST ERROR: got signature 0x%08x instead of 0x%08x on port %d" %
                              (signature, expected_signature, p))
                  if (failure == 0) and one_passed:
                    if verbose>0: print("MEMTEST: signatures matched - OK")
            if verbose>0: print("MEMTEST: cycles: 0x%08x" % (csr_read(ser, channel, 0, 0x0030)))
            csr_write(ser, channel, 0, 0x0000, 0) # idle the engines

def get_membist_stat(ser, channel, port, stat):
  csr_write(ser, channel, 0, 0x0058, (port<<16)+stat)
  return csr_read(ser, channel, 0, 0x005c)

def show_membist_stats(ser, channel, verbose):
  ports = 32 # need to get this from the DUT
  latency_min = 0xffff
  latency_max = 0
  latency_total = 0
  contexts_max = 0
  contexts_total = 0
  for p in range(ports):
    csr_write(ser, channel, 0, 0x0058, (p<<16)+0x00)
    temp = get_membist_stat(ser, channel, p, 0x00)
    port_contexts_max = (temp>>16)&0xffff
    port_contexts_avg = (temp>>0)&0xffff
    csr_write(ser, channel, 0, 0x0058, (p<<16)+0x00)
    temp = get_membist_stat(ser, channel, p, 0x01)
    port_latency_max = (temp>>16)&0xffff
    port_latency_min = (temp>>0)&0xffff
    temp = get_membist_stat(ser, channel, p, 0x02)
    port_latency_avg = (temp>>0)&0xffff
    if verbose>1:
      print("MEMEST: PORT[%3d]: Contexts:                %6d (max), %6d (avg)" % (p, port_contexts_max, port_contexts_avg))
      print("MEMEST: PORT[%3d]: Latency:  %6d (min) - %6d (max), %6d (avg)" % (p, port_latency_min, port_latency_max, port_latency_avg))
    if port_contexts_max > contexts_max: contexts_max = port_contexts_max
    contexts_total += port_contexts_avg
    if port_latency_min < latency_min: latency_min = port_latency_min
    if port_latency_max > latency_max: latency_max = port_latency_max
    latency_total += port_latency_avg
  contexts_avg = (contexts_total / ports)
  latency_avg = (latency_total / ports)
  if verbose>1:
    print("MEMEST: -------------------------------------------------------------------")
  if verbose>0:
    print("MEMEST: ALL PORTS: Contexts:                %6d (max), %6d (avg)" % (contexts_max, contexts_avg))
    print("MEMEST: ALL PORTS: Latency:  %6d (min) - %6d (max), %6d (avg)" % (latency_min, latency_max, latency_avg))

def command_protect(ser, parts):
    if (len(parts) < 1):
        print("INFO: protect <channel> [unlock <key>]")
        print("ERROR: need at least a channel")
        return
    channel = eval_number(parts.pop(0))
    if (len(parts)): # do we have more subcommands?
        if (parts[0] == "unlock"):
            if (len(parts) < 2):
                print("INFO: protect <channel> [unlock <key>]")
                print("ERROR: need a key after 'unlock'")
                return
            key64 = eval_number(parts.pop(0), ishex=True)
            csr_write(ser, channel, 0, 1, (key64>>32)&0xffffffff)
            csr_write(ser, channel, 0, 2, (key64>>0)&0xffffffff)
            csr_write(ser, channel, 0, 0, 0x00000001)
            data = csr_read(ser, channel, 0, 0)
            if (data & 0x40000000):
                if (data & 0x80000000):
                    print("INFO: Protect on channel[%d] has been unlocked" % (channel))
                    csr_write(ser, channel, 0, 0, 0x00000000)
                    return
                else:
                    print("ERROR: Protect on channel[%d] rejected the key!" % (channel))
                    dump_protect(ser, channel)
                    csr_write(ser, channel, 0, 0, 0x00000000)
                    return
            else:
                print("ERROR: Protect on channel[%d] failed to decrypt?!" % (channel))
                dump_protect(ser, channel)
                csr_write(ser, channel, 0, 0, 0x00000000)
                return
        else:
            print("INFO: protect <channel> [unlock <key>]")
            print("ERROR: didn't understand '%s'" % (parts[0]))
            return
    dump_protect(ser, channel)

def serial_open(port, baud):
    if port.startswith("tcp:"):
        if (debug): print("DEBUG: fakeserial {}".format(port))
        ser = FakeSerial(port, timeout=0.5)
        if (debug): print("DEBUG: fakeserial port open")
    else:
        if (debug): print("DEBUG: serial %s@%dbps..." % (port, baud))
        ser = serial.Serial(port=port, baudrate=baud, timeout=0.5)
        if (debug): print("DEBUG: Serial port open")
    return ser

def serial_close(ser):
    ser.close()
    if (debug): print("DEBUG: Serial port closed")

def serial_write(ser, txt, expect_echo=0, expect_prompt=0, pace_with_echo=0, pace_with_cpu=0):
    txtbytes = txt.encode()
    if (debug>5): print("SERIAL_WRITE: writing '%s' (%d bytes)%s%s" % (txtbytes, len(txtbytes),
                                                                       " (echo)" if expect_echo else "",
                                                                       " (prompt)" if expect_prompt else ""))
#    sent = 0
#    for b in txtbytes:
#        print("DEBUG char: %s" % b)
#        ser.write(b'%c' % b)
#        sent += 1
#        junk = 0
#        if (pace_with_echo):
#            while (ser.in_waiting < sent): junk += 1
#        if (pace_with_cpu):
#            while (junk<1000): junk += 1
#            junk = 0
#            print("...")
#            time.sleep(0.001)
#        print("DEBUG: sleep went for %.6s seconds" % (stop - start))
#        waiting = ser.out_waiting
#        while waiting:
#            print("SERIAL_WRITE: waiting (%d bytes in output fifo)" % (waiting))
#            time.sleep(0.01)
    start = time.time()
    ser.write(txtbytes)
    written = time.time()
    echo = written
    if expect_echo:
#        junk = 0
#        while (ser.in_waiting < len(txtbytes)): junk += 1
        echo = time.time()
        respbytes = ser.read(len(txtbytes))
        resp = respbytes.decode(encoding='UTF-8')
        if resp != txt:
            print("ERROR: sent '%s', expected echo, got: '%s' (%d bytes)" % (txtbytes, respbytes, len(respbytes)))
    if expect_prompt:
#        junk = 0
#        while (ser.in_waiting < 3): junk += 1
        respbytes = ser.read(3)
        if respbytes != b'\r\n>':
            print("ERROR: expected prompt, got: '%s' (%d bytes)" % (respbytes, len(respbytes)))
    done = time.time()
    # print("DEBUG: %.6fs to write, %.6fs for echo, %.6fs total" % (written-start, echo-start, done-start))

def serial_read(ser, length=1):
    if (debug>5): print("SERIAL_READ: reading %d bytes..." % (length))
    start = time.time()
    readbytes = ser.read(length)
    doneread = time.time()
    read = readbytes.decode(encoding='UTF-8')
    if (debug>5): print("SERIAL_READ: '%s' (%d bytes)" % (readbytes, len(readbytes)))
    done = time.time()
    # print("DEBUG: %.6fs to read, %.6fs total" % (doneread-start, done-start))
    return read

def serial_flush(ser):
    if (debug>5): print("SERIAL_FLUSH: flushing serial...")
    s = "a"
    flushed = 0
    while (len(s) > 0):
        s = ser.read(100)
        flushed += len(s)
    if (debug>5): print("SERIAL_FLUSH: flush done, got '%s' (%d bytes)" % (s, flushed))

def ping(ser):
    status = "ok"
    if debug: print("PING: flushing anything in channel")
    serial_flush(ser)
    if debug: print("PING: pinging serial, sending \\n for prompt")
    ser.write(b'\n')
    s = ser.read(4)
    if s != b'\n\r\n>':
        print("ERROR: ping didn't receive expected prompt, got: '%s' (%d bytes)" % (s, len(s)))
        status = "failed"
    if debug: print("PING: pinging serial, sending ! for error")
    ser.write(b'^')
    s = ser.read(20)
    if s != b'^\r\nSYNTAX ERROR\r\n\r\n>':
        print("ERROR: ping didn't receive expected error, got: '%s' (%d bytes)" % (s, len(s)))
        status = "failed"
    print("PING: ping %s" % (status))

def reset(ser):
    if debug: print("RESET: sending 64 '!' chars")
    for i in range(64):
      ser.write(b'!')
    s = ser.read(68) # read enough to get all the echos
    time.sleep(0.1)
    s = ser.read(32) # read anything else that comes in after reset, like prompt, syntax error because partial byte
    print("RESET: reset complete")

def csr_read_ascii (ser, channel, space, address, verbose=0):
    serial_write(ser, "c%02x\n" % (channel), expect_echo=1)
    serial_write(ser, "%02x %08x %08x\n" % (0x10 | space, address, 0), expect_echo=1)
    status = int(serial_read(ser, 2),16)
    data = int(serial_read(ser, 8),16)
    serial_write(ser, "~", expect_prompt=1)
    if (verbose or (debug>3)):
        print("  %08x <- [%02x][%01x][%08x] (ascii)" % (data, channel, space, address))
    return data

def csr_read_binary (ser, channel, space, address, verbose=0):
    serial_write(ser, "C", expect_echo=1)
    ser.write(b'%c' % (channel))
    ser.write(b'%c' % (9))
    ser.write(b'%c' % (5))
    ser.write(b'%c' % (0x10 | space))
    ser.write(b'%c' % ((address >> 24) & 0xff))
    ser.write(b'%c' % ((address >> 16) & 0xff))
    ser.write(b'%c' % ((address >> 8) & 0xff))
    ser.write(b'%c' % ((address >> 0) & 0xff))
    for i in range(4):
        ser.write(b'%c' % (0))
    s = ser.read(1)
    status = s[0]
    s = ser.read(4)
    data = 0
    for i in range(4):
        data += (s[i] << ((3-i)*8))
    if (verbose or (debug>3)):
        print("  %08x <- [%02x][%01x][%08x]" % (data, channel, space, address))
    return data

def csr_write_ascii (ser, channel, space, address, data, verbose=0):
    serial_write(ser, "c%02x\n" % (channel), expect_echo=1)
    serial_write(ser, "%02x %08x %08x\n" % (0x20 | space, address, data), expect_echo=1)
#    serial_read(ser, 10)  # at some point, should add status feedback for writes
    serial_write(ser, "~", expect_prompt=1)
    if (verbose or (debug>3)):
        print("  %08x -> [%02x][%01x][%08x] (ascii)" % (data, channel, space, address))

def csr_write_binary (ser, channel, space, address, data, verbose=0):
    serial_write(ser, "C", expect_echo=1)
    ser.write(b'%c' % (channel))
    ser.write(b'%c' % (9))
    #ser.write(b'%c' % (5))
    ser.write(b'%c' % (0)) # temp fix
    ser.write(b'%c' % (0x20 | space))
    ser.write(b'%c' % ((address >> 24) & 0xff))
    ser.write(b'%c' % ((address >> 16) & 0xff))
    ser.write(b'%c' % ((address >> 8) & 0xff))
    ser.write(b'%c' % ((address >> 0) & 0xff))
    ser.write(b'%c' % ((data >> 24) & 0xff))
    ser.write(b'%c' % ((data >> 16) & 0xff))
    ser.write(b'%c' % ((data >> 8) & 0xff))
    ser.write(b'%c' % ((data >> 0) & 0xff))
    # s = ser.read(1)
    # status = s[0]
    # s = ser.read(4)
    if (verbose or (debug>3)):
        print("  %08x -> [%02x][%01x][%08x]" % (data, channel, space, address))

def csr_read (ser, channel, space, address, verbose=0):
    global mode_binary
    if mode_binary: return csr_read_binary(ser, channel, space, address, verbose)
    else:           return csr_read_ascii(ser, channel, space, address, verbose)

def csr_write (ser, channel, space, address, data, verbose=0):
    global mode_binary
    if mode_binary: csr_write_binary(ser, channel, space, address, data, verbose)
    else:           csr_write_ascii(ser, channel, space, address, data, verbose)

def chipmon_code_to_temp(code):
    return (((code * 507.5921310)/65536)-279.42657680)

def chipmon_code_to_volt(code, voltagerange="normal"):
    if voltagerange=="normal":
        return 3.0*(code/65536)
    else:
        return 6.0*(code/65536)

def dump_chipmon (ser, channel):
    print("Dumping ChipMon on channel[%d]..." % (channel))
    dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"],
                                                [0,0,"INTERNAL_REF"] ])
    dump_reg(ser, channel, 0, 0x0001, "OcChipMon0", [ [29,29,"JTAG_BUSY"], [29,29,"JTAG_MODIFIED"],
                                                      [28,28,"JTAG_LOCKED"], [0,0,"RESET"] ])
    dump_reg(ser, channel, 0, 0x0002, "OcChipMon1", [ [31,16,"ALARM"], [0,0,"OVER_TEMP"] ])
    dump_reg(ser, channel, 1, 0x003e, "Flags1", [ [3,0,"ALARM[11:8]"] ])
    dump_reg(ser, channel, 1, 0x003f, "Flags2", [ [11,11,"JTAG_DISABLE"], [10,10,"JTAG_RESTRICTED"],
                                                  [9,9,"INTERNAL_REFERENCE"],
                                                  [7,4,"ALARM[6:3]"], [3,3,"OVER_TEMP"], [2,0,"ALARM[2:0]"]])
    dump_reg(ser, channel, 1, 0x0040, "ConfigReg0", [ [15,15,"DISABLE_CAL"], [13,12,"SAMPLE_AVERAGING"],
                                                      [11,11,"EXT_MUX_MODE"], [10,10,"BIPOLAR_INPUTS"],
                                                      [9,9,"EVENT_DRIVEN"], [8,8,"10_CYCLE_SLOW_ACQ"],
                                                      [5,0,"CHANNEL"] ])
    dump_reg(ser, channel, 1, 0x0041, "ConfigReg1", [ [15,12,"SEQUENCER_MODE"], [11,8,"ALARM_DISABLE[6:3]"],
                                                      [7,4,"CAL[3:2:-:0]"], [3,1,"ALARM_DISABLE[2:0]"],
                                                      [0,0,"OVER_TEMP_DISABLE"]])
    dump_reg(ser, channel, 1, 0x0042, "ConfigReg2", [ [15,8,"ADCCLK_DIV"] ])
    dump_reg(ser, channel, 1, 0x0043, "ConfigReg3", [ [14,8,"I2C_ADDR"], [7,7,"I2C_ENABLE"],
                                                      [3,0,"ALARM_DISABLE[11:8]"]])
    dump_reg(ser, channel, 1, 0x0044, "ConfigReg4", [ [11,10,"SLOW_EOS"], [9,8,"SLOW_SEQ"], [3,0,"PMBUS_HRIO"]])

    dump_reg(ser, channel, 1, 0x0046, "SequenceReg0", [ [3,3,"CHSEL_USER3"], [2,2,"CHSEL_USER2"],
                                                        [1,1,"CHSEL_USER1"], [0,0,"CHSEL_USER0"] ])
    dump_reg(ser, channel, 1, 0x0048, "SequenceReg1", [ [14,14,"CHSEL_BRAM_AVG"], [13,13,"CHSEL_VREFN"],
                                                        [12,12,"CHSEL_VREFP"], [11,11,"CHSEL_VPVN"],
                                                        [10,10,"CHSEL_AUX_AVG"], [9,9,"CHSEL_INT_AVG"],
                                                        [8,8,"CHSEL_TEMP"], [7,7,"CHSEL_VCC_PSAUX"],
                                                        [6,6,"CHSEL_VCC_PSINTFP"], [5,5,"CHSEL_VCC_PSINTLP"],
                                                        [0,0,"CHSEL_SYSMON_CAL"] ])
    dump_reg(ser, channel, 1, 0x0049, "SequenceReg2", [ [15,0,"CHSEL_AUX[15:0]"]])
    dump_reg(ser, channel, 1, 0x007a, "SlowChSel0", [ [14,14,"SLOW_BRAM"], [13,13,"SLOW_VREFN"], [12,12,"SLOW_VREFP"],
                                                      [11,11,"SLOW_VPVN"], [10,10,"SLOW_AUX_AVG"], [9,9,"SLOW_INT_AVG"],
                                                      [8,8,"SLOW_TEMP"], [7,7,"SLOW_VCC_PSAUX"], [6,6,"SLOW_VCC_PSINTFP"],
                                                      [5,5,"SLOW_VCC_PSINTLP"], [0,0,"SLOW_SYSMON"]])
    dump_reg(ser, channel, 1, 0x007b, "SlowChSel1", [ [15,0,"SLOW_AUX[15:0]"]])
    dump_reg(ser, channel, 1, 0x007c, "SlowChSel2", [ [3,0,"SLOW_USER[3:0]"]])
    dump_reg(ser, channel, 1, 0x0047, "AvgChSel0", [ [3,0,"AVG_USER[3:0]"]])
    dump_reg(ser, channel, 1, 0x004a, "AvgChSel1", [ [15,0,"AVG_AUX[15:0]"]])
    dump_reg(ser, channel, 1, 0x004b, "AvgChSel2", [ [14,14,"AVG_BRAM"],
                                                     [11,11,"AVG_VPVN"], [10,10,"AVG_AUX_AVG"], [9,9,"AVG_INT_AVG"],
                                                     [8,8,"AVG_TEMP"], [7,7,"AVG_VCC_PSAUX"], [6,6,"AVG_VCC_PSINTFP"],
                                                     [5,5,"AVG_VCC_PSINTLP"]])
    dump_reg(ser, channel, 1, 0x004c, "SeqInMode0", [ [11,11,"INSEL_VPVN"]])
    dump_reg(ser, channel, 1, 0x004d, "SeqInMode1", [ [15,0,"INSEL_AUX[15:0]"]])
    dump_reg(ser, channel, 1, 0x004e, "SeqAcq0", [ [11,11,"ACQ_VPVN"]])
    dump_reg(ser, channel, 1, 0x004f, "SeqAcq1", [ [15,0,"ACQ_AUX[15:0]"]])

    data = dump_reg(ser, channel, 1, 0x0050, "Temperature Upper", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0051, "VCCint Upper", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0052, "VCCaux Upper", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0053, "Over Temp Upper", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0054, "Temperature Lower", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0055, "VCCint Lower", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0056, "VCCaux Lower", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0057, "Over Temp Lower", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0058, "VCCbram Upper", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x005c, "VCCbram Lower", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0000, "Temperature", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0001, "VCCint", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0002, "VCCaux", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0006, "VCCbram", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0020, "Max Temperature", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0021, "Max VCCint", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0022, "Max VCCaux", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0023, "Max VCCbram", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0024, "Min Temperature", [], end='')
    print(" (%.1fC)" % (chipmon_code_to_temp(data)))
    data = dump_reg(ser, channel, 1, 0x0025, "Min VCCint", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0026, "Min VCCaux", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))
    data = dump_reg(ser, channel, 1, 0x0027, "Min VCCbram", [], end='')
    print(" (%.3fV)" % (chipmon_code_to_volt(data)))

def dump_hbm (ser, channel):
    print("Dumping HBM on channel[%d]..." % (channel))
    dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"],
                                                [11,4,"AXI_PORTS"], [0,0,"STACKS"] ])
    dump_reg(ser, channel, 0, 0x0001, "OcHbm", [ [31,31,"STACK1_THERMAL_ERROR"], [30,30,"STACK0_THERMAL_ERROR"],
                                                 [25,25,"APB1_ERROR"], [24,24,"APB0_ERROR"],
                                                 [21,15,"STACK1_TEMP"], [14,8,"STACK0_TEMP"],
                                                 [1,1,"RESET_APB"], [0,0,"RESET_AXI"] ])

def dump_led (ser, channel):
    print("Dumping LED on channel[%d]..." % (channel))
    data = dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"], [7,0,"LED_COUNT"] ])
    count = data & 0xff
    dump_reg(ser, channel, 0, 0x0001, "Prescale", [ [9,0,"CYCLES"] ])
    for i in range(count):
      dump_reg(ser, channel, 0, 0x0002+i, "LedControl%d" % i, [ [13,0,"PARAM"], [1,0,"MODE"] ])

def dump_iic (ser, channel):
    print("Dumping IIC on channel[%d]..." % (channel))
    data = dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"], [0,0,"OFFLOAD_ENABLE"] ])
    offload = data & 0x1
    dump_reg(ser, channel, 0, 0x0001, "Pins", [ [9,9,"OFFLOAD_INTR"], [8,8,"OFFLOAD_DEBUG"],
                                                [7,7,"SDA"], [5,5,"SDA_TRISTATE"], [4,4,"SDA_MANUAL"],
                                                [3,3,"SCL"], [1,1,"SCL_TRISTATE"], [0,0,"SCL_MANUAL"] ])
    if offload:
      dump_reg(ser, channel, 1, 0x001c, "GlobalInterrupt", [ [31,31,"ENABLE"] ])
      dump_reg(ser, channel, 1, 0x0020, "InterruptStatus", [ [7,7,"TX_HALF_EMPTY"], [6,6,"NOT_ADDR_SLAVE"], [5,5,"ADDR_SLAVE"],
                                                             [4,4,"IIC_NOT_BUSY"], [3,3,"RX_FULL"], [2,2,"TX_EMPTY"],
                                                             [1,1,"TX_ERROR/SLAVE_COMPLETE"], [0,0,"ARB_LOST"] ])
      dump_reg(ser, channel, 1, 0x0028, "InterruptEnable", [ [7,7,"TX_HALF_EMPTY"], [6,6,"NOT_ADDR_SLAVE"], [5,5,"ADDR_SLAVE"],
                                                             [4,4,"IIC_NOT_BUSY"], [3,3,"RX_FULL"], [2,2,"TX_EMPTY"],
                                                             [1,1,"TX_ERROR/SLAVE_COMPLETE"], [0,0,"ARB_LOST"] ])
      dump_reg(ser, channel, 1, 0x0040, "SoftReset", [ [3,0,"KEY (0xA)"] ])
      dump_reg(ser, channel, 1, 0x0100, "Control", [ [6,6,"GN_EN (GeneralCallEnable)"], [5,5,"RSTA (RepeatedStart)"],
                                                     [4,4,"TXAK (TransmitAckEnable)"], [3,3,"TX (TransmitMode)"],
                                                     [2,2,"MSMS (MasterSlaveModeSelect)"], [1,1,"TX_FIFO_RESET"], [0,0,"ENABLE"] ])
      dump_reg(ser, channel, 1, 0x0104, "Status", [ [7,7,"TX_FIFO_EMPTY"], [6,6,"RX_FIFO_EMPTY"], [5,5,"RX_FIFO_FULL"],
                                                    [4,4,"TX_FIFO_FULL"], [3,3,"SRW (SlaveReadWrite)"], [2,2,"BB (BusBusy)"],
                                                    [1,1,"AAS (AddrAsSlave)"], [0,0,"ABGC (AddrAsGeneralCall)"] ])
      dump_reg(ser, channel, 1, 0x0110, "SlaveAddress", [ [7,1,"ADDRESS"], [0,0,"RESERVED"] ])
      dump_reg(ser, channel, 1, 0x0114, "TxOccupancy")
      dump_reg(ser, channel, 1, 0x0118, "RxOccupancy")
      dump_reg(ser, channel, 1, 0x011c, "TenBitAddress", [ [2,0,"ADDRESS_MSBS"] ])
      dump_reg(ser, channel, 1, 0x0120, "RxIntrThreshold")
      dump_reg(ser, channel, 1, 0x0124, "GPOutput", [ [0,0,"DEBUG_TO_CSR"] ])
      dump_reg(ser, channel, 1, 0x0128, "TSUSTA (SetupRepeatedStart)")
      dump_reg(ser, channel, 1, 0x012C, "TSUSTO (SetupRepeatedStop)")
      dump_reg(ser, channel, 1, 0x0130, "THDSTA (HoldRepeatedStart)")
      dump_reg(ser, channel, 1, 0x0134, "TSUDAT (SetupData)")
      dump_reg(ser, channel, 1, 0x0138, "TBUF (BusFree)")
      dump_reg(ser, channel, 1, 0x013C, "THIGH (ClockHigh)")
      dump_reg(ser, channel, 1, 0x0140, "TLOW (ClockLow)")
      dump_reg(ser, channel, 1, 0x0144, "THDDAT (HoldData)")

def dump_gpio (ser, channel):
    print("Dumping GPIO on channel[%d]..." % (channel))
    data = dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"], [7,0,"GPIO_COUNT"] ])
    count = data & 0xff
    for i in range(count):
      dump_reg(ser, channel, 0, 0x0002+i, "GpioControl%d" % i, [ [8,8,"IN"], [4,4,"DRIVE"], [0,0,"OUT"] ])

def fix_wrap32 (start, end):
    if (end > start): return (end-start)
    else: return (0x100000000+end-start)

def memtest_measure (ser, channel, seconds=2):
    start_time = time.time()
    axil_under0 = csr_read(ser, channel, 0, 0x0310)
    axil_since0 = csr_read(ser, channel, 0, 0x0314)
    axim_under0 = csr_read(ser, channel, 0, 0x0038)
    axim_since0 = csr_read(ser, channel, 0, 0x003c)
    time.sleep(seconds)
    stop_time = time.time()
    axil_under1 = csr_read(ser, channel, 0, 0x0310)
    axil_since1 = csr_read(ser, channel, 0, 0x0314)
    axim_under1 = csr_read(ser, channel, 0, 0x0038)
    axim_since1 = csr_read(ser, channel, 0, 0x003c)
    burst_length = csr_read(ser, channel, 0, 0x0044)+1
    seconds = stop_time - start_time
    if (axil_under0 != axil_under1): print("ERROR: AXI-Lite seeing cycles under reset increment?")
    if (axim_under0 != axim_under1): print("ERROR: AXI-Memory seeing cycles under reset increment?")
    axil_clocks = fix_wrap32(axil_since0, axil_since1)
    axim_clocks = fix_wrap32(axim_since0, axim_since1)
    print("AXI-Lite   clock: %8.3fMHz (reset: %8d cyc since, %d cyc within)" % (axil_clocks / (seconds * 1000000),
                                                                                axil_since1, axil_under1))
    print("AXI-Memory clock: %8.3fMHz (reset: %8d cyc since, %d cyc within)" % (axim_clocks / (seconds * 1000000),
                                                                                axim_since1, axim_under1))

def dump_memtest (ser, channel, short=0):
    print("Dumping Memtest on channel[%d]..." % (channel))
    dump_reg(ser, channel, 0, 0x0000, "Control", [ [21,21,"READ_DATA_REORDER"], [20,20,"READ_LENGTH_RANDOM"], [16,16,"READ"],
                                                   [13,13,"WRITE_DATA_ROTATE"], [12,12,"WRITE_LENGTH_RANDOM"], [8,8,"WRITE"],
                                                   [1,1,"PRESCALE"], [0,0,"GO"] ])
    dump_reg(ser, channel, 0, 0x0004, "Ops")
    dump_reg(ser, channel, 0, 0x0008, "PortEn")
    dump_reg(ser, channel, 0, 0x0010, "AddrLSB")
    dump_reg(ser, channel, 0, 0x0014, "AddrMSB")
    dump_reg(ser, channel, 0, 0x0018, "AddrIncLSB")
    dump_reg(ser, channel, 0, 0x001c, "AddrIncMSB")
    dump_reg(ser, channel, 0, 0x0020, "AddrIncMaskLSB")
    dump_reg(ser, channel, 0, 0x0024, "AddrIncMaskMSB")
    dump_reg(ser, channel, 0, 0x0028, "Signature")
    dump_reg(ser, channel, 0, 0x002c, "Errors")
    dump_reg(ser, channel, 0, 0x0030, "OpCycles")
    dump_reg(ser, channel, 0, 0x0034, "MagicMEMT")
    dump_reg(ser, channel, 0, 0x0038, "AxiMemCyclesUnderReset")
    dump_reg(ser, channel, 0, 0x003c, "AxiMemCyclesSinceReset")
    dump_reg(ser, channel, 0, 0x0040, "WaitStates")
    dump_reg(ser, channel, 0, 0x0044, "BurstLength")
    dump_reg(ser, channel, 0, 0x0048, "AxiID", [ [15,8,"ReadMaxId"], [7,0,"WriteMaxId"] ])
    dump_reg(ser, channel, 0, 0x004c, "PortShift", [ [23,16,"PORT_ADDR_MASK"], [7,0,"PORT_ADDR_SHIFT"] ])
    dump_reg(ser, channel, 0, 0x0050, "AddrRandMaskLSB")
    dump_reg(ser, channel, 0, 0x0054, "AddrRandMaskMSB")
    dump_reg(ser, channel, 0, 0x0058, "PortStatusSelect", [ [31, 16, "PORT"], [15, 0, "CSR"] ])
    dump_reg(ser, channel, 0, 0x005c, "PortStatusValue")
    if (short): return
    for i in range(8):
        dump_reg(ser, channel, 0, 0x0100 + i*4, "WriteDataBuffer%d" % i)
    for i in range(8):
        dump_reg(ser, channel, 0, 0x0200 + i*4, "ReadDataBuffer%d" % i)
    dump_reg(ser, channel, 0, 0x0300, "SecondsSinceLoad")
    dump_reg(ser, channel, 0, 0x0304, "SecondsSinceReset")
    dump_reg(ser, channel, 0, 0x0308, "AxiLiteWrites")
    dump_reg(ser, channel, 0, 0x030c, "AxiLiteReads")
    dump_reg(ser, channel, 0, 0x0310, "CyclesUnderReset")
    dump_reg(ser, channel, 0, 0x0314, "CyclesSinceReset")
    dump_reg(ser, channel, 0, 0x0318, "Version")

def dump_protect (ser, channel):
    print("Dumping Protect on channel[%d]..." % (channel))
    dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"],
                                                [2,2,"ENABLE_PARANOIA"], [1,1,"EN_TIMED_LICENSE"], [0,0,"EN_SKELETON_KEY"] ])
    dump_reg(ser, channel, 0, 0x0001, "Control", [ [31,31,"UNLOCKED"], [30,30,"DECRYPT_DONE"], [0,0,"DECRYPT_GO"] ])
    dump_reg(ser, channel, 0, 0x0002, "CipherTextInLSB")
    dump_reg(ser, channel, 0, 0x0003, "CipherTextInMSB")
    dump_reg(ser, channel, 0, 0x0004, "FpgaSerial")
    dump_reg(ser, channel, 0, 0x0005, "BitstreamID")
    dump_reg(ser, channel, 0, 0x0006, "PlainTextOutLSB")
    dump_reg(ser, channel, 0, 0x0007, "PlainTextOutMSB")

def throttle_pll(ser, channel, disable_eighths = 0, quiet = 0):
    csr_write(ser, channel, 0, 0x02, ((1 << disable_eighths)-1) << 8)
    if not quiet:
        print("PLL INFO: throttled clock[0] to %d/8ths disabled" % disable_eighths)

def reset_pll(ser, channel, enable = 0, disable = 0):
    if (enable == 0) and (disable == 0):
        enable = 1
        disable = 1
    if enable:
        # before asserting reset, ramp down the throttle
        for i in range(0, 9, 1):
            throttle_pll(ser, channel, i, quiet=1)
        csr_write(ser, channel, 0, 0x01, 1) # assert reset
        print("PLL INFO: reset asserted")
    if disable:
        csr_write(ser, channel, 0, 0x02, 0xff) # make sure clocks are throttled
        csr_write(ser, channel, 0, 0x01, 0) # release reset
        # after releasing reset, ramp up the throttle
        for i in range(8, -1, -1):
            throttle_pll(ser, channel, i, quiet=1)
        print("PLL INFO: reset deasserted")

def measure_pll(ser, channel, clock, measure_seconds = 1):
    start = csr_read(ser, channel, 0, (clock+2)) >> 16
    start_time = time.time()
    time.sleep(measure_seconds)
    stop = csr_read(ser, channel, 0, (clock+2)) >> 16
    stop_time = time.time()
    if (stop < start): stop += 0x10000
    print("PLL INFO: clock[%d] is %.3fMHz" % (clock, ((stop-start)/(stop_time-start_time))))

def dump_pll (ser, channel):
    print("Dumping PLL on channel[%d]..." % (channel))
    for clock in range(1):
        print("Measuring clock %d..." % (clock))
        measure_pll(ser, channel, clock)
    cfg = dump_reg(ser, channel, 0, 0x0000, "OcID", [ [31,16,"ID"],
                                                      [8,8,"THROTTLE_THERMAL"], [7,7,"THROTTLE_MAP"], [6,6,"MEASURE_ENABLE"],
                                                      [5,0,"CLOCKS"] ])
    clocks = ((cfg>>0) & 0x3f)
    dump_reg(ser, channel, 0, 0x0001, "OcPll0", [ [31,31,"DUMMY_DATA"], [30,30,"DUMMY_ENABLE"],
                                                  [25,25,"FB_STOPPED"], [24,24,"IN_STOPPED"],
                                                  [21,21,"THERMAL_ERROR"], [20,20,"THERMAL_WARNING"],
                                                  [17,17,"CDDC_DONE"], [16,16,"LOCKED"],
                                                  [5,5,"POWER_DOWN"], [4,4,"CDDC_REQ"], [0,0,"RESET"] ])
    for i in range(clocks):
      dump_reg(ser, channel, 0, 0x0002+i, "OcPllClk%d"%i, [ [15,8,"THROTTLE_MAP"], [1,1,"ENABLE_THERMAL"], [0,0,"FORCE_THERMAL"] ])
    dump_reg(ser, channel, 1, 0x004f, "FiltReg2", [ [15,8,"RESISTOR"], [7,4,"CAPACITOR"] ])
    dump_reg(ser, channel, 1, 0x004e, "FiltReg1", [ [15,8,"CHARGE PUMP"] ])
    dump_reg(ser, channel, 1, 0x0027, "PowerReg", [ [15,0,"INTERPOLATOR"] ])
    dump_reg(ser, channel, 1, 0x001a, "LockReg3", [ [14, 10, "REF DELAY"], [9, 0, "SAT_HIGH"] ])
    dump_reg(ser, channel, 1, 0x0019, "LockReg2", [ [14, 10, "FB DELAY"], [9, 0, "UNLOCK_CNT"] ])
    dump_reg(ser, channel, 1, 0x0018, "LockReg1", [ [9, 0, "LOCK_CNT"] ])
    dump_reg(ser, channel, 1, 0x0016, "DivReg", [ [13, 13, "EDGE"], [11, 6, "HIGH TIME"], [5, 0, "LOW_TIME"] ])
    dump_reg(ser, channel, 1, 0x0015, "CLKFBOUT", [ [14,12,"FRAC_PHASE"],
                                                    [11,11,"FRAC_EN"],
                                                    [10,10,"FRAC_WF_R"],
                                                    [7,7,"EDGE"],
                                                    [6,6,"NO_COUNT"],
                                                    [5,0,"FRAC"] ])
    dump_reg(ser, channel, 1, 0x0014, "CLKFBOUT", [ [15,13,"PHASE_SELECT_RISE"],
                                                    [12,12,"COUNTER_EN"],
                                                    [11,6,"HIGH_TIME"],
                                                    [5,0,"LOW_TIME"] ])
    dump_reg(ser, channel, 1, 0x0013, "CLKFBOUT/CLKOUT[6]", [ [15,13,"PHASE_SELECT_FALL"],
                                                              [12,12,"FRAC_WF_R"],
                                                              [10,10,"CLKOUT6_CDDC_EN"],
                                                              [7,7,"CLKOUT6_EDGE"],
                                                              [6,6,"CLKOUT6_NO_COUNT"],
                                                              [5,0,"CLKOUT6_DELAY"] ])
    dump_reg(ser, channel, 1, 0x0012, "CLKOUT[6]", [ [15,13,"PHASE_SELECT"],
                                                     [12,12,"COUNTER_EN"],
                                                     [11,6,"HIGH_TIME"],
                                                     [5,0,"LOW_TIME"] ])
    for clkout in [4, 3, 2, 1]:
        dump_reg(ser, channel, 1, 0x0011-((4-clkout)*2), "CLKOUT[%d]"%clkout, [ [10,10,"CDDC_EN"],
                                                                                [7,7,"EDGE"],
                                                                                [6,6,"NO_COUNT"],
                                                                                [5,0,"DELAY"] ])
        dump_reg(ser, channel, 1, 0x0010-((4-clkout)*2), "CLKOUT[%d]"%clkout, [ [15,13,"PHASE_SELECT"],
                                                                                [12,12,"COUNTER_EN"],
                                                                                [11,6,"HIGH_TIME"],
                                                                                [5,0,"LOW_TIME"] ])

    dump_reg(ser, channel, 1, 0x0009, "CLKOUT[0]", [ [ 15, 15, "CDDC_EN" ],
                                                     [ 14, 12, "FRAC_PHASE" ],
                                                     [ 11, 11, "FRAC_EN" ],
                                                     [ 10, 10, "FRAC_WF_R" ],
                                                     [  9,  8, "MX" ],
                                                     [  7,  7, "EDGE" ],
                                                     [  6,  6, "NO_COUNT" ],
                                                     [  5,  0, "DELAY" ] ])
    dump_reg(ser, channel, 1, 0x0008, "CLKOUT[0]", [ [ 15, 13, "PHASE_SELECT_RISE" ],
                                                     [ 12, 12, "COUNTER_EN" ],
                                                     [ 11,  6, "HIGH_TIME" ],
                                                     [  5,  0, "LOW_TIME" ] ])
    dump_reg(ser, channel, 1, 0x0007, "CLKOUT[0]/CLKOUT[5]", [ [ 15, 13, "PHASE_SELECT_FALL" ],
                                                               [ 12, 12, "FRAC_WF_F" ],
                                                               [ 10, 10, "CLKOUT5_CDDC_EN" ],
                                                               [  7,  7, "CLKOUT5_EDGE" ],
                                                               [  6,  6, "CLKOUT5_NO_COUNT" ],
                                                               [  5,  0, "CLKOUT6_DELAY" ] ])
    dump_reg(ser, channel, 1, 0x0006, "CLKOUT[5]", [ [ 15, 13, "PHASE_SELECT" ],
                                                     [ 12, 12, "COUNTER_EN" ],
                                                     [ 11,  6, "HIGH_TIME" ],
                                                     [  5,  0, "LOW_TIME" ] ])

def dump_reg (ser, channel, space, address, name, fields=[], end='\n'):
    data = csr_read(ser, channel, space, address)
    print("REG [%02x][%01x][%08x] (%-28s) = %08x" % (channel, space, address, name, data), end=end)
    for field in fields:
        msb = field[0]
        lsb = field[1]
        fieldname = field[2]
        width = (msb-lsb+1)
        mask = (1<<width)-1
        if msb == lsb:
            print("                 [%2d] %-28s   = %8x" % (lsb, fieldname, ((data >> lsb) & 1)))
        else:
            digits = int((width+3)/4)
            fmt = "              [%%2d:%%2d] %%-28s   = %s%%0%dx" % (' '*(8-digits), digits)
            print(fmt % (msb, lsb, fieldname, ((data >> lsb) & mask)), end=end)
    return data

def usage():
    print("")
    print("Usage: oc_tty.py [-h|--help] [-s|--serial <port>] [-b|--baud <rate>]")
    print("--help                This screen")
    print("--serial <port>       Select serial port. Defaults to COM1 on Windows, else /dev/ttyS0")
    print("--baud                Select baud rate. Defaults to 115200")
    print("")


if __name__ == "__main__":
    main()
