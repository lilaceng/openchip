User-Space Applications
=======================

Running An Existing Application
-------------------------------

- Choose a target, such as u55n
- Make a copy of the target dir, i.e. u55n_myapp (optional, you can work in u55n, but it's harder to pull/merge revisions of the OC framework). 
- Open the project in Vivado 
- Source "<OC_ROOT>/bin/oc_vivado.tcl" (usually by typing "**source ../../bin/oc_vivado.tcl**"  ... soon there will be a method that pulls this in automatically whenever Vivado opens an OC project...) 
- Choose a userspace app, i.e. "myapp"
- In the TCL Console, type "**oc_load_userspace myapp**" (this should "just work", if not, file an issue!)



Creating A New Application
--------------------------

- Create a directory for the application under <OC_ROOT>/user
- Create a subdirectory for design RTL (design)
- Create an oc_user.sv wrapper, with I/O compatible to the default (<OC_ROOT>/top/oc_user.sv)
- Place whatever other sources are required by the application within your app directory (or subdirectories)
- If desired, create subdirectory for custom XDC/SDC/TCL (constraints) and/or testbenches (sim)
- Apps not upstreamed/distributed via OC should be distributed as a ZIP which can be unzipped in <OC_ROOT>/user (i.e. myapp.zip results in <OC_ROOT>/user/myapp/design, etc) 
- Test that your app can be added to existing targets via "oc_load_userspace" so users don't need to "add sources" manually to get your app to compile

Lightweight applications should try to remain "flat" (no subdirectories) which will let the "oc_load_userspace" command automagically add the project.  In future we'll provide a way for more complex applications to put TCL (or something) in <OC_ROOT>/user in order for complex apps to be handled more gracefully.

Applications should be distributed in source form (no compiled blobs)!

When developing your own closed source app, it is recommended to follow the same directory structure as open source apps.  OCLIB components may expect that they are being used within an OpenChip directory structure.
