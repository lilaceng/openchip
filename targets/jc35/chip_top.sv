
module chip_top
  #(
    // Misc
    parameter integer Seed = `OC_FROM_DEFINE_ELSE(TARGET_SEED, 0), // seed to generate varying implementation results
    // RefClocks
    parameter integer RefClockCount = 2,
    parameter integer RefClockHz [RefClockCount-1:0] = { 200000000, 100000000 },
    parameter integer RefClockTop = 0,
    // PLLs
    parameter integer PllCount = 1,
    parameter integer PllClockRef [PllCount-1:0] = { 0 }, // which reference clock, per PLL
    parameter bit     PllAbbcEnable [PllCount-1:0] = { 1 }, // whether to include CSRs, per PLL
    parameter bit     PllMeasureEnable [PllCount-1:0] = { 1 }, // whether to include clock measure logic, per PLL
    parameter integer PllClocksMax = 4, // max number of clocks per PLL
    parameter integer PllClocks [PllCount-1:0] = { 1 }, // number of clocks, per PLL
    parameter integer PllClockHz [PllCount-1:0] [PllClocksMax-1:0] = // frequency, per clock, per PLL
                      '{ '{ `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK3_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK2_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK1_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK0_HZ,450000000) } }, // 450MHz default for Pll[0]/Clock[0]
    // ChipMons
    parameter integer ChipMonCount = `OC_FROM_DEFINE_ELSE(TARGET_CHIPMON_COUNT,1),
    `OC_CREATE_SAFE_WIDTH(ChipMonCount),
    parameter bit     ChipMonAbbcEnable [ChipMonCountSafe-1:0] = { 1 },
    parameter bit     ChipMonI2CEnable [ChipMonCountSafe-1:0] = { 0 }, // JC35 doesn't connect FPGA SYSMON I2C to PCIe
    // Protection
    parameter integer ProtectCount = `OC_FROM_DEFINE_ELSE(TARGET_PROTECT_COUNT,0),
    // IIC
    parameter integer IicCount = `OC_FROM_DEFINE_ELSE(TARGET_IIC_COUNT,1),
    `OC_CREATE_SAFE_WIDTH(IicCount),
    parameter integer IicOffloadEnable = `OC_FROM_DEFINE_ELSE(TARGET_IIC_OFFLOAD,1),
    // LED
    parameter integer LedCount = `OC_FROM_DEFINE_ELSE(TARGET_LED_COUNT,7),
    `OC_CREATE_SAFE_WIDTH(LedCount),
    // FAN
    parameter integer FanCount = `OC_FROM_DEFINE_ELSE(TARGET_LED_COUNT,1),
    `OC_CREATE_SAFE_WIDTH(FanCount),
    // UARTs
    parameter integer UartCount = 1,
    parameter integer UartManagerChannel = 0,
    parameter integer UartBaud [UartCount-1:0] = { `OC_FROM_DEFINE_ELSE(TARGET_UART0_BAUD,1000000) },
    // HBM
    parameter integer HbmCount = `OC_FROM_DEFINE_ELSE(TARGET_HBM_COUNT,0),
    parameter integer HbmRefClock = `OC_FROM_DEFINE_ELSE(TARGET_HBM_REFCLOCK,1) // NOTE: default to 200MHz reference on this board
    )
  (
   input  SYSCLK0_200_P,
   input  SYSCLK0_200_N,
   input  BMC_UART_TXD_IN,
   output BMC_UART_RXD_OUT,
   inout I2C_SCL_IO,
   inout I2C_SDA_IO,
   output LED_A, LED_B, LED_C, LED_D,
   output LED_RGB_R, LED_RGB_G, LED_RGB_B,
   input fan_sense,
   output fan_ctl
   );

  /* REFCLKS */
  (* dont_touch = "yes" *)
  logic [RefClockCount-1:0] clockRef;

  IBUFDS uIBUF_SYSCLK0 (.O(clockRef[1]), .I(SYSCLK0_200_P), .IB(SYSCLK0_200_N));
  BUFGCE_DIV #(.BUFGCE_DIVIDE(2)) uDIV_CLOCKREF1(.O(clockRef[0]), .CE(1'b1), .CLR(1'b0), .I(clockRef[1]));

  /* CHIPMON */
  logic [ChipMonCountSafe-1:0]  chipMonScl;
  logic [ChipMonCountSafe-1:0]  chipMonSclTristate;
  logic [ChipMonCountSafe-1:0]  chipMonSda;
  logic [ChipMonCountSafe-1:0]  chipMonSdaTristate;
  assign chipMonScl = 1'b1;
  assign chipMonSda = 1'b1;

  /* IIC */
  logic [IicCountSafe-1:0]  iicScl;
  logic [IicCountSafe-1:0]  iicSclTristate;
  logic [IicCountSafe-1:0]  iicSda;
  logic [IicCountSafe-1:0]  iicSdaTristate;
  if (IicCount) begin
    IOBUF uIOBUF_I2C_SCL_IO (.IO(I2C_SCL_IO), .I(1'b0), .T(iicSclTristate[0]),  .O(iicScl[0]) );
    IOBUF uIOBUF_I2C_SDA_IO (.IO(I2C_SDA_IO), .I(1'b0), .T(iicSdaTristate[0]),  .O(iicSda[0]) );
  end

  /* LED */
  logic [LedCountSafe-1:0]  ledOut;
  if (LedCount) begin
    OBUF uIOBUF_LED_A (.O(LED_A), .I(ledOut[0]) );
    OBUF uIOBUF_LED_B (.O(LED_B), .I(ledOut[1]) );
    OBUF uIOBUF_LED_C (.O(LED_C), .I(ledOut[2]) );
    OBUF uIOBUF_LED_D (.O(LED_D), .I(ledOut[3]) );
    OBUF uIOBUF_LED_RGB_R (.O(LED_RGB_R), .I(ledOut[4]) );
    OBUF uIOBUF_LED_RGB_G (.O(LED_RGB_G), .I(ledOut[5]) );
    OBUF uIOBUF_LED_RGB_B (.O(LED_RGB_B), .I(ledOut[6]) );
  end

  /* FAN */
  logic [FanCountSafe-1:0]  fanPwm, fanSense;
  if (FanCount) begin
    OBUF uIOBUF_fan_ctl (.O(fan_ctl), .I(fanPwm[0]) );
    IBUF uIBUF_fan_sense (.O(fanSense[0]), .I(fan_sense));
  end

  /* UARTS */
  (* dont_touch = "yes" *)
  logic [UartCount-1:0]     vio_uart;
  logic [UartCount-1:0]     uartRx, uartTx;

  IBUF uIBUF_FPGA_UART0_RXD (.O(uartRx[0]), .I(BMC_UART_TXD_IN));
  OBUF uOBUF_FPGA_UART0_TXD (.O(BMC_UART_RXD_OUT), .I(uartTx[0] && !vio_uart[0]));

  /* MISC */
  logic                     thermalWarning;
  logic                     thermalError;
  //OBUF uOBUF_HBM_CATTRIP_LS (.O(HBM_CATTRIP_LS), .I(thermalError));

  (* dont_touch = "yes" *)
  logic                     vio_reset;

  logic [7:0]               debug;

  /* GENERIC FPGA CORE */
  oc_top #(.Seed(Seed),
           // RefClocks
           .RefClockCount(RefClockCount),
           .RefClockHz(RefClockHz),
           .RefClockTop(RefClockTop),
           // PLLs
           .PllCount(PllCount),
           .PllClockRef(PllClockRef),
           .PllAbbcEnable(PllAbbcEnable),
           .PllMeasureEnable(PllMeasureEnable),
           .PllClocksMax(PllClocksMax),
           .PllClocks(PllClocks),
           .PllClockHz(PllClockHz),
           // ChipMons
           .ChipMonCount(ChipMonCount),
           .ChipMonAbbcEnable(ChipMonAbbcEnable),
           .ChipMonI2CEnable(ChipMonI2CEnable),
           // Protection
           .ProtectCount(ProtectCount),
           // Iic
           .IicCount(IicCount),
           .IicOffloadEnable(IicOffloadEnable),
           // LEDs
           .LedCount(LedCount),
           // FANs
           .FanCount(FanCount),
           // UARTs
           .UartCount(UartCount),
           .UartManagerChannel(UartManagerChannel),
           .UartBaud(UartBaud),
           // HBM
           .HbmCount(HbmCount),
           .HbmRefClock('{HbmRefClock}))
  uTOP (
        .clockRef(clockRef),
        .resetPin(vio_reset),
        .uartRx(uartRx), .uartTx(uartTx),
        .chipMonScl(chipMonScl), .chipMonSclTristate(chipMonSclTristate),
        .chipMonSda(chipMonSda), .chipMonSdaTristate(chipMonSdaTristate),
        .iicScl(iicScl), .iicSclTristate(iicSclTristate),
        .iicSda(iicSda), .iicSdaTristate(iicSdaTristate),
        .ledOut(ledOut),
        .fanPwm(fanPwm), .fanSense(fanSense),
        .thermalWarning(thermalWarning), .thermalError(thermalError), .debug(debug)
        );

  /* DEBUG CONTROLS */
`ifdef TARGET_VIO_DEBUG
  logic [27:0]            clockRef0Divide = '0;
  logic [27:0]            clockRef1Divide = '0;
  always_ff @(posedge clockRef[0]) clockRef0Divide <= (clockRef0Divide + 'd1);
  always_ff @(posedge clockRef[1]) clockRef1Divide <= (clockRef1Divide + 'd1);

  logic [31:8+1+UartCount+1+2+UartCount+UartCount+1]  vio_dummy_in = '0;
  logic [31:UartCount+1]  vio_dummy_out;

  `ifndef SIMULATION
  vio_0 uVIO (.clk(clockRef[RefClockTop]),
              .probe_in0({vio_dummy_in,
                          debug, vio_reset, vio_uart,
                          reset, clockRef1Divide[27], clockRef0Divide[27],
                          uartRx, uartTx, thermalError, thermalWarning}),
              .probe_out0({vio_dummy_out,
                           vio_uart, vio_reset}));
  `else // SIMULATION
  assign vio_reset = 1'b0;
  assign vio_uart = '0;
  `endif
  `else // !TARGET_VIO_DEBUG
  assign vio_reset = 1'b0;
  assign vio_uart = '0;
  `endif

endmodule // chip_top
