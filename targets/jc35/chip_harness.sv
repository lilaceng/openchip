
// SPDX-License-Identifier: MPL-2.0

// chip_harness.sv -- this maps a specific target's chip_top pins (which match the vendor FPGA/board definition) such
//                    that testbenches can be device-independent.  refclocks (which are board specific) are also
//                    driven from here

`define TB_UART_0_BAUD 12000000 // speedup for sim

module chip_harness ();

  // CLOCK / RESET

  localparam RefClockHz = 200000000;

  logic              clockRef;
  sim_clock #(.ClockHz(RefClockHz)) uCLOCKREF (.clock(clockRef));

  logic              resetPin;
  sim_reset #(.StartupResetCycles(30)) uRESETPIN (.clock(clockRef), .reset(resetPin));

  // SIM_UART

  logic              masterUartToDut;
  logic              masterUartFromDut;
  logic [15:0]       masterUartError;

  sim_uart #(.ClockHz(RefClockHz), .Baud(`TB_UART_0_BAUD))
  uUART (.clock(clockRef), .reset(resetPin),
         .in(masterUartFromDut), .out(masterUartToDut),
         .uartError(masterUartError));

  sim_master uMASTER();

  // CHIP_TOP
  logic              thermalError;

  chip_top #(
             .UartBaud( '{ `TB_UART_0_BAUD } )
             )
  uCHIP (
         .SYSCLK0_200_P(clockRef), .SYSCLK0_200_N(~clockRef),
    //     .HBM_CATTRIP_LS(thermalError),
         .BMC_UART_RXD_OUT(masterUartFromDut), .BMC_UART_TXD_IN(masterUartToDut)
         );

  assign thermalError = 1'b0;

endmodule // chip_harness
