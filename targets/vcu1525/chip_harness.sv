
// SPDX-License-Identifier: MPL-2.0

// chip_harness.sv -- this maps a specific target's chip_top pins (which match the vendor FPGA/board definition) such
//                    that testbenches can be device-independent.  refclocks (which are board specific) are also
//                    driven from here

`define TB_UART_0_BAUD 12000000 // speedup for sim
`define TB_UART_1_BAUD 115200

module chip_harness ();

  // CLOCK / RESET

  localparam RefClockHz = 156250000;

  logic              clockRef;
  sim_clock #(.ClockHz(RefClockHz)) uCLOCKREF (.clock(clockRef));

  localparam DiffRefClockHz = 161132812;

  logic              clockDiffRef;
  sim_clock #(.ClockHz(DiffRefClockHz)) uCLOCKDIFFREF (.clock(clockDiffRef));

  logic              resetPin;
  sim_reset #(.StartupResetCycles(10)) uRESETPIN (.clock(clockRef), .reset(resetPin));

  // SIM_UART

  logic              masterUartToDut;
  logic              masterUartFromDut;
  logic [15:0]       masterUartError;

  sim_uart #(.ClockHz(RefClockHz), .Baud(`TB_UART_0_BAUD))
  uUART (.clock(clockRef), .reset(resetPin),
         .in(masterUartFromDut), .out(masterUartToDut),
         .uartError(masterUartError));

  sim_master uMASTER();

  // CMAC (loopback or cross-connect)

  logic              QSFP28_0_TX1_N, QSFP28_0_TX1_P;
  logic              QSFP28_0_TX2_N, QSFP28_0_TX2_P;
  logic              QSFP28_0_TX3_N, QSFP28_0_TX3_P;
  logic              QSFP28_0_TX4_N, QSFP28_0_TX4_P;
  logic              QSFP28_1_TX1_N, QSFP28_1_TX1_P;
  logic              QSFP28_1_TX2_N, QSFP28_1_TX2_P;
  logic              QSFP28_1_TX3_N, QSFP28_1_TX3_P;
  logic              QSFP28_1_TX4_N, QSFP28_1_TX4_P;
  logic              QSFP28_0_RX1_N, QSFP28_0_RX1_P;
  logic              QSFP28_0_RX2_N, QSFP28_0_RX2_P;
  logic              QSFP28_0_RX3_N, QSFP28_0_RX3_P;
  logic              QSFP28_0_RX4_N, QSFP28_0_RX4_P;
  logic              QSFP28_1_RX1_N, QSFP28_1_RX1_P;
  logic              QSFP28_1_RX2_N, QSFP28_1_RX2_P;
  logic              QSFP28_1_RX3_N, QSFP28_1_RX3_P;
  logic              QSFP28_1_RX4_N, QSFP28_1_RX4_P;

`ifdef TEST_CMAC_CROSS_CONNECT
  assign {QSFP28_0_RX1_N, QSFP28_0_RX1_P} = {QSFP28_1_TX1_N, QSFP28_1_TX1_P};
  assign {QSFP28_0_RX2_N, QSFP28_0_RX2_P} = {QSFP28_1_TX2_N, QSFP28_1_TX2_P};
  assign {QSFP28_0_RX3_N, QSFP28_0_RX3_P} = {QSFP28_1_TX3_N, QSFP28_1_TX3_P};
  assign {QSFP28_0_RX4_N, QSFP28_0_RX4_P} = {QSFP28_1_TX4_N, QSFP28_1_TX4_P};
  assign {QSFP28_1_RX1_N, QSFP28_1_RX1_P} = {QSFP28_0_TX1_N, QSFP28_0_TX1_P};
  assign {QSFP28_1_RX2_N, QSFP28_1_RX2_P} = {QSFP28_0_TX2_N, QSFP28_0_TX2_P};
  assign {QSFP28_1_RX3_N, QSFP28_1_RX3_P} = {QSFP28_0_TX3_N, QSFP28_0_TX3_P};
  assign {QSFP28_1_RX4_N, QSFP28_1_RX4_P} = {QSFP28_0_TX4_N, QSFP28_0_TX4_P};
`else
  assign {QSFP28_0_RX1_N, QSFP28_0_RX1_P} = {QSFP28_0_TX1_N, QSFP28_0_TX1_P};
  assign {QSFP28_0_RX2_N, QSFP28_0_RX2_P} = {QSFP28_0_TX2_N, QSFP28_0_TX2_P};
  assign {QSFP28_0_RX3_N, QSFP28_0_RX3_P} = {QSFP28_0_TX3_N, QSFP28_0_TX3_P};
  assign {QSFP28_0_RX4_N, QSFP28_0_RX4_P} = {QSFP28_0_TX4_N, QSFP28_0_TX4_P};
  assign {QSFP28_1_RX1_N, QSFP28_1_RX1_P} = {QSFP28_1_TX1_N, QSFP28_1_TX1_P};
  assign {QSFP28_1_RX2_N, QSFP28_1_RX2_P} = {QSFP28_1_TX2_N, QSFP28_1_TX2_P};
  assign {QSFP28_1_RX3_N, QSFP28_1_RX3_P} = {QSFP28_1_TX3_N, QSFP28_1_TX3_P};
  assign {QSFP28_1_RX4_N, QSFP28_1_RX4_P} = {QSFP28_1_TX4_N, QSFP28_1_TX4_P};
`endif

  // CHIP_TOP

  chip_top #(
             .UartBaud( {`TB_UART_1_BAUD, `TB_UART_0_BAUD } )
             )
  uCHIP (
         .USER_SI570_CLOCK_P(clockRef), .USER_SI570_CLOCK_N(~clockRef),
         .QSFP0_CLOCK_P(clockDiffRef), .QSFP0_CLOCK_N(~clockDiffRef),
         .QSFP1_CLOCK_P(clockDiffRef), .QSFP1_CLOCK_N(~clockDiffRef),
         .USB_UART_RX(masterUartToDut), .USB_UART_TX(masterUartFromDut),
         .FPGA_RXD_MSP(1'b1), .FPGA_TXD_MSP(),
         .QSFP28_0_RX1_N(QSFP28_0_RX1_N), .QSFP28_0_RX1_P(QSFP28_0_RX1_P),
         .QSFP28_0_RX2_N(QSFP28_0_RX2_N), .QSFP28_0_RX2_P(QSFP28_0_RX2_P),
         .QSFP28_0_RX3_N(QSFP28_0_RX3_N), .QSFP28_0_RX3_P(QSFP28_0_RX3_P),
         .QSFP28_0_RX4_N(QSFP28_0_RX4_N), .QSFP28_0_RX4_P(QSFP28_0_RX4_P),
         .QSFP28_0_TX1_N(QSFP28_0_TX1_N), .QSFP28_0_TX1_P(QSFP28_0_TX1_P),
         .QSFP28_0_TX2_N(QSFP28_0_TX2_N), .QSFP28_0_TX2_P(QSFP28_0_TX2_P),
         .QSFP28_0_TX3_N(QSFP28_0_TX3_N), .QSFP28_0_TX3_P(QSFP28_0_TX3_P),
         .QSFP28_0_TX4_N(QSFP28_0_TX4_N), .QSFP28_0_TX4_P(QSFP28_0_TX4_P),
         .QSFP28_1_RX1_N(QSFP28_1_RX1_N), .QSFP28_1_RX1_P(QSFP28_1_RX1_P),
         .QSFP28_1_RX2_N(QSFP28_1_RX2_N), .QSFP28_1_RX2_P(QSFP28_1_RX2_P),
         .QSFP28_1_RX3_N(QSFP28_1_RX3_N), .QSFP28_1_RX3_P(QSFP28_1_RX3_P),
         .QSFP28_1_RX4_N(QSFP28_1_RX4_N), .QSFP28_1_RX4_P(QSFP28_1_RX4_P),
         .QSFP28_1_TX1_N(QSFP28_1_TX1_N), .QSFP28_1_TX1_P(QSFP28_1_TX1_P),
         .QSFP28_1_TX2_N(QSFP28_1_TX2_N), .QSFP28_1_TX2_P(QSFP28_1_TX2_P),
         .QSFP28_1_TX3_N(QSFP28_1_TX3_N), .QSFP28_1_TX3_P(QSFP28_1_TX3_P),
         .QSFP28_1_TX4_N(QSFP28_1_TX4_N), .QSFP28_1_TX4_P(QSFP28_1_TX4_P)
         );


endmodule // chip_harness
