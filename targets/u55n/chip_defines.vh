`ifndef __CHIP_DEFINES_VH
  `define __CHIP_DEFINES_VH

  `define TARGET_SEED 1
  `define TARGET_CHIPMON_COUNT 1
  `define TARGET_PROTECT_COUNT 1
  `define TARGET_HBM_COUNT 1
  `define TARGET_CMAC_COUNT 2

  `define TARGET_BITSTREAM_KEY 128'h44444444333333332222222211111111

  `define TARGET_VIO_DEBUG
  `define TARGET_TTY_MANAGER_ILA
  `define TARGET_HBM_AXI_PORT_ILA 32'h3

  //  `define TARGET_HBM_MODULE_NAME xip_hbm_no_switch // define this no treat each HBM port as separate memory
  //  `define TARGET_HBM_SEPARATE_MEMORY_SPACES // define this with the above, to have sim model act the same

  `define USER_ABBCS 4 // need 1 (always) + 1 (if HBM) + CMAC_COUNT
  `define USER_CLOCKS 1
  `define USER_AXI_MEMORY_PORTS 32

  `define SIM_SPEED_UP

//  `define USER_MEMBIST_ILA

`endif
