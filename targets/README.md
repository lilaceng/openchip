Targets
=======

OpenChip is delivered with one target folder per supported board.  This target is configured to load the default OpenChip userspace (**<OC_ROOT>/top/oc_user**) which is a self test. 

Loading Userspace App
---------------------

See **<OC_ROOT>/user/README.md** for instructions on loading userspace application into your preferred target.  
The tl;dr version is to copy the target dir (i.e. u55n -> u55n_myapp, leaving the project files inside unchanged), open the project, load the OpenChip TCL library (source ../../bin/oc_vivado.tcl), and run "oc_load_userspace <myapp>" to link the userspace source into the target.

Adding New Targets
------------------

This is the recommended method for adding support for a new target: 

- Copy the closest available target (1)  (i.e. u55n -> u200)
- Copy the (hopefully vendor-provided) constraint file (i.e. u200.xdc) into the new target dir
- Adjust pin names to match between chip_top.sv and the constraint file (2)
- Adjust target settings in chip_top.sv to match board (number of ref clocks, HBMs, DDR4, QSFP, etc)
- Open the project in the new target dir (i.e. open u55n.xpr within u200)
- File -> Project -> Save As...
- Name new project (i.e. u200), and ensure following are NOT selected: "Create Project Subdirectory", "Include Run Results", "Import all files to the new project"
- Tools -> Settings -> General -> "Project Device" (3) selecting your board/FPGA 
- Reports -> Report IP Status (i.e. check that IPs are compatible) 
- Click "Upgrade Selected" (4) if any IPs require it (usually they will in case of FPGA change)
- Remove the original pin definition / constraint file (u55n.xdc) and add the constraints for the new board (u200.xdc)
- Close Vivado (this is required before the next step, as it tends to hold various files open from the original project)
- Delete the original project file (u55n.xpr) and any project directories (u55n.srcs, u55n.sims, u55n.runs, u55n.cache, u55n.ip_user_files, u55n.gen, u55n.hw)


(1) "closest" has a few meanings here.  Ideally -- same FPGA, same pin namings in vendor-provided pinlist, same FPGA architecture, etc.
Today, until this issue is solved, the biggest concern is changing FPGA pin names, as the userspace may come with a chip level testbench that instantiates chip_top and therefore has assumptions about pin naming, which so far has been driver by u55n pin names. 

(2) this is a bit of an art.  On the one hand, one would like to not change the vendor file.  However this is often impossible, as the vendor isn't consistent about pin naming or whether to setup clocks and other constraints.
For openchip, we want input reference clocks defined in the constraint file -- either vendor provides this already, or vendor file is edited, or another constraint file is added to declare clocks. 
As far as pin naming, however, a judgement must be made whether to edit chip_top.sv to match the constraints, or the other way around.  TODAY, it's easier to rename pins in the constraint file, mainly because userspace apps can have top level TBs which will instantiate chip_top and expect certain names.
In FUTURE, the plan is to either create a wrapper around the chip (housing various TB components, ref clocks, etc) which the user test would instantiate, OR, to setup target-based defines that provide target-specific info like pin names.  

(3) Xilinx has a habit of making "special" versions of FPGAs for some boards (like Alveo) and then being too selective of which boards are available in the GUI.  For example, U200 is no longer appearing in Vivado 2021.1 GUI.  
In this case, use the TCL Console to set the device, ie :  "set_property part xcu200-fsgd2104-2-e [current_project]" for the U200

(4) If you get a dialog asking about "Core Containers", select "Continue with core containers disabled"
