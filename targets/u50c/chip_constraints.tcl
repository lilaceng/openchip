
# SPDX-License-Identifier: MPL-2.0

puts "CHIP_CONSTRAINTS.TCL: Start"

puts "CHIP_CONSTRAINTS.TCL: Setting up managed clocks from board"
oc_add_clock SYSCLK2 ; # clock is defined in u50c.xdc
oc_add_clock SYSCLK3 ; # clock is defined in u55n.xdc

puts "CHIP_CONSTRAINTS.TCL: Running OC_AUTO_SCOPED_XDC"
oc_auto_scoped_xdc

puts "CHIP_CONSTRAINTS.TCL: Running OC_AUTO_ATTR_CONSTRAINTS"
oc_auto_attr_constraints

puts "CHIP_CONSTRAINTS.TCL: Running OC_AUTO_MAX_DELAY"
oc_auto_max_delay

# this is a good place to put any pblocks that owuld likely apply to forks of this target
# TODO: make it such that IP pblocks are declared only if the IP is there, i.e. search for cells named uTOP/uBLAH...
if { ! [oc_is_run synth] } {
#    startgroup
#    create_pblock pblock_uBLAH
#    resize_pblock pblock_uBLAH -add CLOCKREGION_X0Y0:CLOCKREGION_X0Y3
#    add_cells_to_pblock pblock_uBLAH [get_cells [list uTOP/uBLAH]]
#    endgroup
}

# this kind of setup will often be in the vendor provided XDC (u55n.xdc from Xilinx in this dir) but sometimes not...
puts "CHIP_CONSTRAINTS.TCL: Setting up CONFIG/BITSTREAM"
set_property CONFIG_VOLTAGE 1.8 [current_design]
set_property BITSTREAM.CONFIG.CONFIGFALLBACK Enable [current_design]               ;# Golden image is the fall back image if  new bitstream is corrupted.
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 63.8 [current_design]
set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN disable [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN Pullup [current_design]                    ;# Choices are pullnone, pulldown, and pullup.
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR Yes [current_design]

# this can only be done during implementation phase
if { [oc_is_run impl] } {
    puts "CHIP_CONSTRAINTS.TCL: Setting up DBG_HUB"
    set_property C_CLK_INPUT_FREQ_HZ 100000000 [get_debug_cores dbg_hub]
    set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
    set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
    connect_debug_port dbg_hub/clk [ get_nets clockRef[0] ]
}

puts "CHIP_CONSTRAINTS.TCL: Done"
