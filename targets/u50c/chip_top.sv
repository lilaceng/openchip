
module chip_top
  #(
    // Misc
    parameter integer Seed = `OC_FROM_DEFINE_ELSE(TARGET_SEED, 0), // seed to generate varying implementation results
    // RefClocks
    parameter integer RefClockCount = 2,
    parameter integer RefClockHz [RefClockCount-1:0] = { 100000000, 100000000 },
    parameter integer RefClockTop = 0,
    // PLLs
    parameter integer PllCount = 1,
    parameter integer PllClockRef [PllCount-1:0] = { 0 }, // which reference clock, per PLL
    parameter bit     PllAbbcEnable [PllCount-1:0] = { 1 }, // whether to include CSRs, per PLL
    parameter bit     PllMeasureEnable [PllCount-1:0] = { 1 }, // whether to include clock measure logic, per PLL
    parameter integer PllClocksMax = 4, // max number of clocks per PLL
    parameter integer PllClocks [PllCount-1:0] = { 1 }, // number of clocks, per PLL
    parameter integer PllClockHz [PllCount-1:0] [PllClocksMax-1:0] = // frequency, per clock, per PLL
                      '{ '{ `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK3_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK2_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK1_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK0_HZ,450000000) } }, // 450MHz default for Pll[0]/Clock[0]
    // ChipMons
    parameter integer ChipMonCount = `OC_FROM_DEFINE_ELSE(TARGET_CHIPMON_COUNT,1),
    `OC_CREATE_SAFE_WIDTH(ChipMonCount),
    parameter bit     ChipMonAbbcEnable [ChipMonCountSafe-1:0] = { 1 },
    parameter bit     ChipMonI2CEnable [ChipMonCountSafe-1:0] = { 0 }, // U55N doesn't connect FPGA SYSMON I2C to PCIe
    // Protection
    parameter integer ProtectCount = `OC_FROM_DEFINE_ELSE(TARGET_PROTECT_COUNT,0),
    // UARTs
    parameter integer UartCount = 4,
    parameter integer UartManagerChannel = 0,
    parameter integer UartBaud [UartCount-1:0] = { `OC_FROM_DEFINE_ELSE(TARGET_UART3_BAUD,115200),
                                                   `OC_FROM_DEFINE_ELSE(TARGET_UART2_BAUD,115200),
                                                   `OC_FROM_DEFINE_ELSE(TARGET_UART1_BAUD,115200),
                                                   `OC_FROM_DEFINE_ELSE(TARGET_UART0_BAUD,460800) },
    // HBM
    parameter integer HbmCount = `OC_FROM_DEFINE_ELSE(TARGET_HBM_COUNT,0)
    )
  (
   input  SYSCLK2_P,
   input  SYSCLK2_N,
   input  SYSCLK3_P,
   input  SYSCLK3_N,
   output HBM_CATTRIP_LS,
   input  FPGA_UART0_RXD,
   output FPGA_UART0_TXD,
   input  FPGA_UART1_RXD,
   output FPGA_UART1_TXD,
   input  FPGA_UART2_RXD,
   output FPGA_UART2_TXD,
   input  FPGA_RXD_MSP_65,
   output FPGA_TXD_MSP_65
   );

  /* REFCLKS */
  (* dont_touch = "yes" *)
  logic [RefClockCount-1:0] clockRef;

  IBUFDS uIBUF_SYSCLK2 (.O(clockRef[0]), .I(SYSCLK2_P), .IB(SYSCLK2_N));
  IBUFDS uIBUF_SYSCLK3 (.O(clockRef[1]), .I(SYSCLK3_P), .IB(SYSCLK3_N));

  /* CHIPMON */
  logic [ChipMonCountSafe-1:0]  chipMonScl;
  logic [ChipMonCountSafe-1:0]  chipMonSclTristate;
  logic [ChipMonCountSafe-1:0]  chipMonSda;
  logic [ChipMonCountSafe-1:0]  chipMonSdaTristate;
  assign chipMonScl = 1'b1;
  assign chipMonSda = 1'b1;

  /* UARTS */
  (* dont_touch = "yes" *)
  logic [UartCount-1:0]     vio_uart;
  logic [UartCount-1:0]     uartRx, uartTx;

  IBUF uIBUF_FPGA_UART0_RXD (.O(uartRx[0]), .I(FPGA_UART0_RXD));
  OBUF uOBUF_FPGA_UART0_TXD (.O(FPGA_UART0_TXD), .I(uartTx[0] && !vio_uart[0]));
  IBUF uIBUF_FPGA_UART1_RXD (.O(uartRx[1]), .I(FPGA_UART1_RXD));
  OBUF uOBUF_FPGA_UART1_TXD (.O(FPGA_UART1_TXD), .I(uartTx[1] && !vio_uart[1]));
  IBUF uIBUF_FPGA_UART2_RXD (.O(uartRx[2]), .I(FPGA_UART2_RXD));
  OBUF uOBUF_FPGA_UART2_TXD (.O(FPGA_UART2_TXD), .I(uartTx[2] && !vio_uart[2]));
  IBUF uIBUF_FPGA_RXD_MSP_65 (.O(uartRx[3]), .I(FPGA_RXD_MSP_65));
  OBUF uOBUF_FPGA_TXD_MSP_65 (.O(FPGA_TXD_MSP_65), .I(uartTx[3] && !vio_uart[3]));

  /* MISC */
  logic                     thermalWarning;
  logic                     thermalError;
  OBUF uOBUF_HBM_CATTRIP_LS (.O(HBM_CATTRIP_LS), .I(thermalError));

  (* dont_touch = "yes" *)
  logic                     vio_reset;

  logic [7:0]               debug;

  /* GENERIC FPGA CORE */
  oc_top #(.Seed(Seed),
           // RefClocks
           .RefClockCount(RefClockCount),
           .RefClockHz(RefClockHz),
           .RefClockTop(RefClockTop),
           // PLLs
           .PllCount(PllCount),
           .PllClockRef(PllClockRef),
           .PllAbbcEnable(PllAbbcEnable),
           .PllMeasureEnable(PllMeasureEnable),
           .PllClocksMax(PllClocksMax),
           .PllClocks(PllClocks),
           .PllClockHz(PllClockHz),
           // ChipMons
           .ChipMonCount(ChipMonCount),
           .ChipMonAbbcEnable(ChipMonAbbcEnable),
           .ChipMonI2CEnable(ChipMonI2CEnable),
           // Protection
           .ProtectCount(ProtectCount),
           // UARTs
           .UartCount(UartCount),
           .UartManagerChannel(UartManagerChannel),
           .UartBaud(UartBaud),
           // HBM
           .HbmCount(HbmCount))
  uTOP (
        .clockRef(clockRef),
        .resetPin(vio_reset),
        .uartRx(uartRx), .uartTx(uartTx),
        .chipMonScl(chipMonScl), .chipMonSclTristate(chipMonSclTristate),
        .chipMonSda(chipMonSda), .chipMonSdaTristate(chipMonSdaTristate),
        .thermalWarning(thermalWarning), .thermalError(thermalError), .debug(debug)
        );

  /* DEBUG CONTROLS */
`ifdef TARGET_VIO_DEBUG
  logic [27:0]            clockRef0Divide = '0;
  logic [27:0]            clockRef1Divide = '0;
  always_ff @(posedge clockRef[0]) clockRef0Divide <= (clockRef0Divide + 'd1);
  always_ff @(posedge clockRef[1]) clockRef1Divide <= (clockRef1Divide + 'd1);

  logic [31:8+1+UartCount+1+2+UartCount+UartCount+1]  vio_dummy_in = '0;
  logic [31:UartCount+1]  vio_dummy_out;

  `ifndef SIMULATION
  vio_0 uVIO (.clk(clockRef[RefClockTop]),
              .probe_in0({vio_dummy_in,
                          debug, vio_reset, vio_uart,
                          reset, clockRef1Divide[27], clockRef0Divide[27],
                          uartRx, uartTx, thermalError, thermalWarning}),
              .probe_out0({vio_dummy_out,
                           vio_uart, vio_reset}));
  `else // SIMULATION
  assign vio_reset = 1'b0;
  assign vio_uart = '0;
  `endif
  `else // !TARGET_VIO_DEBUG
  assign vio_reset = 1'b0;
  assign vio_uart = '0;
  `endif

endmodule // chip_top
