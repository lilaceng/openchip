# Basic U50C constraints

set_property -dict { IOSTANDARD LVDS PACKAGE_PIN BC18 }	[get_ports SYSCLK3_N]
set_property -dict { IOSTANDARD LVDS PACKAGE_PIN BB18 }	[get_ports SYSCLK3_P]

set_property -dict { IOSTANDARD LVDS PACKAGE_PIN G16 }	[get_ports SYSCLK2_N]
set_property -dict { IOSTANDARD LVDS PACKAGE_PIN G17 }	[get_ports SYSCLK2_P]

create_clock -period 10.000 -name SYSCLK3	[get_ports SYSCLK3_P]
create_clock -period 10.000 -name SYSCLK2	[get_ports SYSCLK2_P]

set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN J18 }	[get_ports HBM_CATTRIP_LS]

set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN BF26 }	[get_ports FPGA_UART0_RXD]
set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN BE26 } [get_ports FPGA_UART0_TXD]

set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN A18 }	[get_ports FPGA_UART2_RXD]
set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN A19 }	[get_ports FPGA_UART2_TXD]

set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN B15 }	[get_ports FPGA_UART1_RXD]
set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN A17 }	[get_ports FPGA_UART1_TXD]

set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN BB26 }	[get_ports FPGA_RXD_MSP_65]
set_property -dict { IOSTANDARD LVCMOS18 PACKAGE_PIN BB25 }	[get_ports FPGA_TXD_MSP_65]
