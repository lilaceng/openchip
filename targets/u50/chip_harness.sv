
// SPDX-License-Identifier: MPL-2.0

// chip_harness.sv -- this maps a specific target's chip_top pins (which match the vendor FPGA/board definition) such
//                    that testbenches can be device-independent.  refclocks (which are board specific) are also
//                    driven from here

`define TB_UART_0_BAUD 12000000 // speedup for sim
`define TB_UART_1_BAUD 115200
`define TB_UART_2_BAUD 115200
`define TB_UART_3_BAUD 115200

module chip_harness ();

  // CLOCK / RESET

  localparam RefClockHz = 100000000;

  logic              clockRef;
  sim_clock #(.ClockHz(RefClockHz)) uCLOCKREF (.clock(clockRef));

  localparam DiffRefClockHz = 161132812;

  logic              clockDiffRef;
  sim_clock #(.ClockHz(DiffRefClockHz)) uCLOCKDIFFREF (.clock(clockDiffRef));

  logic              resetPin;
  sim_reset #(.StartupResetCycles(10)) uRESETPIN (.clock(clockRef), .reset(resetPin));

  // SIM_UART

  logic              masterUartToDut;
  logic              masterUartFromDut;
  logic [15:0]       masterUartError;

  sim_uart #(.ClockHz(RefClockHz), .Baud(`TB_UART_0_BAUD))
  uUART (.clock(clockRef), .reset(resetPin),
         .in(masterUartFromDut), .out(masterUartToDut),
         .uartError(masterUartError));

  sim_master uMASTER();

  // CMAC (loopback or cross-connect)

  logic              QSFP28_0_TX1_N, QSFP28_0_TX1_P;
  logic              QSFP28_0_TX2_N, QSFP28_0_TX2_P;
  logic              QSFP28_0_TX3_N, QSFP28_0_TX3_P;
  logic              QSFP28_0_TX4_N, QSFP28_0_TX4_P;
  logic              QSFP28_1_TX1_N, QSFP28_1_TX1_P;
  logic              QSFP28_1_TX2_N, QSFP28_1_TX2_P;
  logic              QSFP28_1_TX3_N, QSFP28_1_TX3_P;
  logic              QSFP28_1_TX4_N, QSFP28_1_TX4_P;
  logic              QSFP28_0_RX1_N, QSFP28_0_RX1_P;
  logic              QSFP28_0_RX2_N, QSFP28_0_RX2_P;
  logic              QSFP28_0_RX3_N, QSFP28_0_RX3_P;
  logic              QSFP28_0_RX4_N, QSFP28_0_RX4_P;

  assign {QSFP28_0_RX1_N, QSFP28_0_RX1_P} = {QSFP28_0_TX1_N, QSFP28_0_TX1_P};
  assign {QSFP28_0_RX2_N, QSFP28_0_RX2_P} = {QSFP28_0_TX2_N, QSFP28_0_TX2_P};
  assign {QSFP28_0_RX3_N, QSFP28_0_RX3_P} = {QSFP28_0_TX3_N, QSFP28_0_TX3_P};
  assign {QSFP28_0_RX4_N, QSFP28_0_RX4_P} = {QSFP28_0_TX4_N, QSFP28_0_TX4_P};

  // CHIP_TOP
  logic              thermalError;

  chip_top #(
             .UartBaud( {`TB_UART_3_BAUD, `TB_UART_2_BAUD, `TB_UART_1_BAUD, `TB_UART_0_BAUD } )
             )
  uCHIP (
         .SYSCLK2_P(clockRef), .SYSCLK2_N(~clockRef),
         .SYSCLK3_P(clockRef), .SYSCLK3_N(~clockRef),
         .SYNCE_CLK_P(clockDiffRef), .SYNCE_CLK_N(~clockDiffRef),
         .HBM_CATTRIP(thermalError),
         .FPGA_UART0_RXD(masterUartToDut), .FPGA_UART0_TXD(masterUartFromDut),
         .FPGA_UART1_RXD('b1), .FPGA_UART1_TXD(),
         .FPGA_UART2_RXD('b1), .FPGA_UART2_TXD(),
         .FPGA_RXD_MSP(1'b1), .FPGA_TXD_MSP(),
         .QSFP28_0_RX1_N(QSFP28_0_RX1_N), .QSFP28_0_RX1_P(QSFP28_0_RX1_P),
         .QSFP28_0_RX2_N(QSFP28_0_RX2_N), .QSFP28_0_RX2_P(QSFP28_0_RX2_P),
         .QSFP28_0_RX3_N(QSFP28_0_RX3_N), .QSFP28_0_RX3_P(QSFP28_0_RX3_P),
         .QSFP28_0_RX4_N(QSFP28_0_RX4_N), .QSFP28_0_RX4_P(QSFP28_0_RX4_P),
         .QSFP28_0_TX1_N(QSFP28_0_TX1_N), .QSFP28_0_TX1_P(QSFP28_0_TX1_P),
         .QSFP28_0_TX2_N(QSFP28_0_TX2_N), .QSFP28_0_TX2_P(QSFP28_0_TX2_P),
         .QSFP28_0_TX3_N(QSFP28_0_TX3_N), .QSFP28_0_TX3_P(QSFP28_0_TX3_P),
         .QSFP28_0_TX4_N(QSFP28_0_TX4_N), .QSFP28_0_TX4_P(QSFP28_0_TX4_P)
         );


endmodule // chip_harness
