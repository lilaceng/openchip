
U50 TARGET
==========

BoardRepo
---------

The U50 target provides an example of how to use the "board repo" functionality of Vivado.  The U50 has a board definition (kept in targets/u50/board).  The u50.xpr file sets an option called BoardPartRepoPaths to "$PPRDIR/board" which causes Vivado to look for board definitions in this directory.  So far, this is the best known way to handle boards (versus copying them into a shared common area like /opt/Xilinx as is usually recommended) -- keeping them within the target avoids system-wide config changes, and enables proper version control of the board definitions.
