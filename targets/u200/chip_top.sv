
module chip_top
  #(
    // Misc
    parameter integer Seed = `OC_FROM_DEFINE_ELSE(TARGET_SEED, 0), // seed to generate varying implementation results
    // RefClocks
    parameter integer RefClockCount = 1,
    parameter integer RefClockHz [RefClockCount-1:0] = { 156250000 },
    parameter integer RefClockTop = 0,
    parameter integer DiffRefClockCount = 2,
    parameter integer DiffRefClockHz [DiffRefClockCount-1:0] = '{DiffRefClockCount{161132812}}, // freq, per DiffRefClock
    // PLLs
    parameter integer PllCount = 1,
    parameter integer PllClockRef [PllCount-1:0] = { 0 }, // which reference clock, per PLL
    parameter bit     PllAbbcEnable [PllCount-1:0] = { 1 }, // whether to include CSRs, per PLL
    parameter bit     PllMeasureEnable [PllCount-1:0] = { 1 }, // whether to include clock measure logic, per PLL
    parameter integer PllClocksMax = 4, // max number of clocks per PLL
    parameter integer PllClocks [PllCount-1:0] = { 1 }, // number of clocks, per PLL
    parameter integer PllClockHz [PllCount-1:0] [PllClocksMax-1:0] = // frequency, per clock, per PLL
                      '{ '{ `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK3_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK2_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK1_HZ,0),
                            `OC_FROM_DEFINE_ELSE(TARGET_PLL0_CLK0_HZ,450000000) } }, // 450MHz default for Pll[0]/Clock[0]
    // ChipMons
    parameter integer ChipMonCount = `OC_FROM_DEFINE_ELSE(TARGET_CHIPMON_COUNT,1),
    `OC_CREATE_SAFE_WIDTH(ChipMonCount),
    parameter bit     ChipMonAbbcEnable [ChipMonCountSafe-1:0] = { 1 },
    parameter bit     ChipMonI2CEnable [ChipMonCountSafe-1:0] = { 0 }, // U200 doesn't connect FPGA SYSMON I2C to PCIe... i think
    // Protection
    parameter integer ProtectCount = `OC_FROM_DEFINE_ELSE(TARGET_PROTECT_COUNT,0),
    // IIC
    parameter integer IicCount = `OC_FROM_DEFINE_ELSE(TARGET_IIC_COUNT,1),
    `OC_CREATE_SAFE_WIDTH(IicCount),
    parameter integer IicOffloadEnable = `OC_FROM_DEFINE_ELSE(TARGET_IIC_OFFLOAD,1),
    // LED
    parameter integer LedCount = `OC_FROM_DEFINE_ELSE(TARGET_LED_COUNT,3),
    `OC_CREATE_SAFE_WIDTH(LedCount),
    // GPIO
    parameter integer GpioCount = `OC_FROM_DEFINE_ELSE(TARGET_GPIO_COUNT,17),
    `OC_CREATE_SAFE_WIDTH(GpioCount),
    // UARTs
    parameter integer UartCount = 2,
    parameter integer UartManagerChannel = 0,
    parameter integer UartBaud [UartCount-1:0] = { `OC_FROM_DEFINE_ELSE(TARGET_UART1_BAUD,115200),
                                                   `OC_FROM_DEFINE_ELSE(TARGET_UART0_BAUD,460800) },
    // CMAC
    parameter integer CmacCount = `OC_FROM_DEFINE_ELSE(TARGET_CMAC_COUNT,0),
    `OC_CREATE_SAFE_WIDTH(CmacCount),
    parameter integer CmacCountMax = 2, // to be able to assign good defaults below, the next params are always max width
    parameter integer CmacDiffRefClock [CmacCountMax-1:0] = '{ 1, 0 },
    parameter bit     CmacAbbcEnable [CmacCountMax-1:0] = '{ CmacCountMax { 1 } }
    )
  (
   input  USER_SI570_CLOCK_P, USER_SI570_CLOCK_N,
   input  QSFP0_CLOCK_P, QSFP0_CLOCK_N,
   input  QSFP1_CLOCK_P, QSFP1_CLOCK_N,
   output  USB_UART_RX,
   input USB_UART_TX,
   input  FPGA_RXD_MSP,
   output FPGA_TXD_MSP,
   inout I2C_FPGA_SCL,
   inout I2C_FPGA_SDA,
   output STATUS_LED0_FPGA, // red, bottom
   output STATUS_LED1_FPGA, // yellow, middle
   output STATUS_LED2_FPGA,  // green, top
   inout QSFP0_RESETL, QSFP0_MODPRSL, QSFP0_INTL, QSFP0_LPMODE, QSFP0_MODSELL, QSFP0_REFCLK_RESET,
   inout [1:0] QSFP0_FS,
   inout QSFP1_RESETL, QSFP1_MODPRSL, QSFP1_INTL, QSFP1_LPMODE, QSFP1_MODSELL, QSFP1_REFCLK_RESET,
   inout [1:0] QSFP1_FS,
   inout I2C_MAIN_RESETN,
   input QSFP28_0_RX1_N, QSFP28_0_RX1_P,
   input QSFP28_0_RX2_N, QSFP28_0_RX2_P,
   input QSFP28_0_RX3_N, QSFP28_0_RX3_P,
   input QSFP28_0_RX4_N, QSFP28_0_RX4_P,
   output QSFP28_0_TX1_N, QSFP28_0_TX1_P,
   output QSFP28_0_TX2_N, QSFP28_0_TX2_P,
   output QSFP28_0_TX3_N, QSFP28_0_TX3_P,
   output QSFP28_0_TX4_N, QSFP28_0_TX4_P,
   input QSFP28_1_RX1_N, QSFP28_1_RX1_P,
   input QSFP28_1_RX2_N, QSFP28_1_RX2_P,
   input QSFP28_1_RX3_N, QSFP28_1_RX3_P,
   input QSFP28_1_RX4_N, QSFP28_1_RX4_P,
   output QSFP28_1_TX1_N, QSFP28_1_TX1_P,
   output QSFP28_1_TX2_N, QSFP28_1_TX2_P,
   output QSFP28_1_TX3_N, QSFP28_1_TX3_P,
   output QSFP28_1_TX4_N, QSFP28_1_TX4_P
);

  /* REFCLKS */
  (* dont_touch = "yes" *)
  logic [RefClockCount-1:0] clockRef;

  IBUFDS uIBUF_USER_SI570_CLOCK (.O(clockRef[0]), .I(USER_SI570_CLOCK_P), .IB(USER_SI570_CLOCK_N));

  logic [DiffRefClockCount-1:0] clockDiffRefP;
  logic [DiffRefClockCount-1:0] clockDiffRefN;

  assign clockDiffRefP[0] = QSFP0_CLOCK_P;
  assign clockDiffRefN[0] = QSFP0_CLOCK_N;
  assign clockDiffRefP[1] = QSFP1_CLOCK_P;
  assign clockDiffRefN[1] = QSFP1_CLOCK_N;

  /* CHIPMON */
  logic [ChipMonCountSafe-1:0]  chipMonScl;
  logic [ChipMonCountSafe-1:0]  chipMonSclTristate;
  logic [ChipMonCountSafe-1:0]  chipMonSda;
  logic [ChipMonCountSafe-1:0]  chipMonSdaTristate;
  assign chipMonScl = 1'b1;
  assign chipMonSda = 1'b1;

  /* IIC */
  logic [IicCountSafe-1:0]  iicScl;
  logic [IicCountSafe-1:0]  iicSclTristate;
  logic [IicCountSafe-1:0]  iicSda;
  logic [IicCountSafe-1:0]  iicSdaTristate;
  if (IicCount) begin
    IOBUF uIOBUF_I2C_FPGA_SCL (.IO(I2C_FPGA_SCL), .I(1'b0), .T(iicSclTristate[0]),  .O(iicScl[0]) );
    IOBUF uIOBUF_I2C_FPGA_SDA (.IO(I2C_FPGA_SDA), .I(1'b0), .T(iicSdaTristate[0]),  .O(iicSda[0]) );
  end

  /* LED */
  logic [LedCountSafe-1:0]  ledOut;
  if (LedCount) begin
    OBUF uIOBUF_STATUS_LED0_FPGA (.O(STATUS_LED0_FPGA), .I(ledOut[0]) );
    OBUF uIOBUF_STATUS_LED1_FPGA (.O(STATUS_LED1_FPGA), .I(ledOut[1]) );
    OBUF uIOBUF_STATUS_LED2_FPGA (.O(STATUS_LED2_FPGA), .I(ledOut[2]) );
  end

  /* GPIO */
  logic [GpioCountSafe-1:0]  gpioOut;
  logic [GpioCountSafe-1:0]  gpioTristate;
  logic [GpioCountSafe-1:0]  gpioIn;
  if (GpioCount) begin
    IOBUF uIOBUF_QSFP0_RESETL       (.IO(QSFP0_RESETL),       .I(gpioOut[ 0]), .T(gpioTristate[ 0]), .O(gpioIn[ 0]));
    IOBUF uIOBUF_QSFP0_MODPRSL      (.IO(QSFP0_MODPRSL),      .I(gpioOut[ 1]), .T(gpioTristate[ 1]), .O(gpioIn[ 1]));
    IOBUF uIOBUF_QSFP0_INTL         (.IO(QSFP0_INTL),         .I(gpioOut[ 2]), .T(gpioTristate[ 2]), .O(gpioIn[ 2]));
    IOBUF uIOBUF_QSFP0_LPMODE       (.IO(QSFP0_LPMODE),       .I(gpioOut[ 3]), .T(gpioTristate[ 3]), .O(gpioIn[ 3]));
    IOBUF uIOBUF_QSFP0_MODSELL      (.IO(QSFP0_MODSELL),      .I(gpioOut[ 4]), .T(gpioTristate[ 4]), .O(gpioIn[ 4]));
    IOBUF uIOBUF_QSFP0_FS_0         (.IO(QSFP0_FS[0]),        .I(gpioOut[ 5]), .T(gpioTristate[ 5]), .O(gpioIn[ 5]));
    IOBUF uIOBUF_QSFP0_FS_1         (.IO(QSFP0_FS[1]),        .I(gpioOut[ 6]), .T(gpioTristate[ 6]), .O(gpioIn[ 6]));
    IOBUF uIOBUF_QSFP0_REFCLK_RESET (.IO(QSFP0_REFCLK_RESET), .I(gpioOut[ 7]), .T(gpioTristate[ 7]), .O(gpioIn[ 7]));
    IOBUF uIOBUF_QSFP1_RESETL       (.IO(QSFP1_RESETL),       .I(gpioOut[ 8]), .T(gpioTristate[ 8]), .O(gpioIn[ 8]));
    IOBUF uIOBUF_QSFP1_MODPRSL      (.IO(QSFP1_MODPRSL),      .I(gpioOut[ 9]), .T(gpioTristate[ 9]), .O(gpioIn[ 9]));
    IOBUF uIOBUF_QSFP1_INTL         (.IO(QSFP1_INTL),         .I(gpioOut[10]), .T(gpioTristate[10]), .O(gpioIn[10]));
    IOBUF uIOBUF_QSFP1_LPMODE       (.IO(QSFP1_LPMODE),       .I(gpioOut[11]), .T(gpioTristate[11]), .O(gpioIn[11]));
    IOBUF uIOBUF_QSFP1_MODSELL      (.IO(QSFP1_MODSELL),      .I(gpioOut[12]), .T(gpioTristate[12]), .O(gpioIn[12]));
    IOBUF uIOBUF_QSFP1_FS_0         (.IO(QSFP1_FS[0]),        .I(gpioOut[13]), .T(gpioTristate[13]), .O(gpioIn[13]));
    IOBUF uIOBUF_QSFP1_FS_1         (.IO(QSFP1_FS[1]),        .I(gpioOut[14]), .T(gpioTristate[14]), .O(gpioIn[14]));
    IOBUF uIOBUF_QSFP1_REFCLK_RESET (.IO(QSFP1_REFCLK_RESET), .I(gpioOut[15]), .T(gpioTristate[15]), .O(gpioIn[15]));
    IOBUF uIOBUF_I2C_MAIN_RESETN    (.IO(I2C_MAIN_RESETN),    .I(gpioOut[16]), .T(gpioTristate[16]), .O(gpioIn[16]));
  end

  /* UARTS */
  (* dont_touch = "yes" *)
  logic [UartCount-1:0]     vio_uart;
  logic [UartCount-1:0]     uartRx, uartTx;

  IBUF uIBUF_USB_UART_TX (.O(uartRx[0]), .I(USB_UART_TX));
  OBUF uOBUF_USB_UART_RX (.O(USB_UART_RX), .I(uartTx[0] && !vio_uart[0]));
  IBUF uIBUF_FPGA_RXD_MSP (.O(uartRx[1]), .I(FPGA_RXD_MSP));
  OBUF uOBUF_FPGA_TXD_MSP (.O(FPGA_TXD_MSP), .I(uartTx[1] && !vio_uart[1]));

  /* CMACS */
  logic [CmacCountSafe-1:0] [3:0] cmacTxP;
  logic [CmacCountSafe-1:0] [3:0] cmacTxN;
  logic [CmacCountSafe-1:0] [3:0] cmacRxP;
  logic [CmacCountSafe-1:0] [3:0] cmacRxN;

  if (CmacCount>0) begin
    assign cmacRxP[0] = { QSFP28_0_RX4_P, QSFP28_0_RX3_P, QSFP28_0_RX2_P, QSFP28_0_RX1_P };
    assign cmacRxN[0] = { QSFP28_0_RX4_N, QSFP28_0_RX3_N, QSFP28_0_RX2_N, QSFP28_0_RX1_N };
    assign { QSFP28_0_TX4_P, QSFP28_0_TX3_P, QSFP28_0_TX2_P, QSFP28_0_TX1_P } = cmacTxP[0];
    assign { QSFP28_0_TX4_N, QSFP28_0_TX3_N, QSFP28_0_TX2_N, QSFP28_0_TX1_N } = cmacTxN[0];
  end
  if (CmacCount>1) begin
    assign cmacRxP[1] = { QSFP28_1_RX4_P, QSFP28_1_RX3_P, QSFP28_1_RX2_P, QSFP28_1_RX1_P };
    assign cmacRxN[1] = { QSFP28_1_RX4_N, QSFP28_1_RX3_N, QSFP28_1_RX2_N, QSFP28_1_RX1_N };
    assign { QSFP28_1_TX4_P, QSFP28_1_TX3_P, QSFP28_1_TX2_P, QSFP28_1_TX1_P } = cmacTxP[1];
    assign { QSFP28_1_TX4_N, QSFP28_1_TX3_N, QSFP28_1_TX2_N, QSFP28_1_TX1_N } = cmacTxN[1];
  end

  /* MISC */
  logic                     thermalWarning;
  logic                     thermalError;

  (* dont_touch = "yes" *)
  logic                     vio_reset;

  logic [7:0]               debug;

  /* GENERIC FPGA CORE */
  oc_top #(.Seed(Seed),
           // RefClocks
           .RefClockCount(RefClockCount),
           .RefClockHz(RefClockHz),
           .RefClockTop(RefClockTop),
           .DiffRefClockCount(DiffRefClockCount),
           .DiffRefClockHz(DiffRefClockHz),
           // PLLs
           .PllCount(PllCount),
           .PllClockRef(PllClockRef),
           .PllAbbcEnable(PllAbbcEnable),
           .PllMeasureEnable(PllMeasureEnable),
           .PllClocksMax(PllClocksMax),
           .PllClocks(PllClocks),
           .PllClockHz(PllClockHz),
           // ChipMons
           .ChipMonCount(ChipMonCount),
           .ChipMonAbbcEnable(ChipMonAbbcEnable),
           .ChipMonI2CEnable(ChipMonI2CEnable),
           // Protection
           .ProtectCount(ProtectCount),
           // IIC
           .IicCount(IicCount),
           .IicOffloadEnable(IicOffloadEnable),
           // LEDs
           .LedCount(LedCount),
           // GPIOs
           .GpioCount(GpioCount),
           // UARTs
           .UartCount(UartCount),
           .UartManagerChannel(UartManagerChannel),
           .UartBaud(UartBaud),
           // CMAC
           .CmacCount(CmacCount),
           .CmacDiffRefClock(CmacDiffRefClock[CmacCountSafe-1:0]), // only pass CmacCountSafe values
           .CmacAbbcEnable(CmacAbbcEnable[CmacCountSafe-1:0]) // only pass CmacCountSafe values
           )
  uTOP (
        .clockRef(clockRef),
        .clockDiffRefP(clockDiffRefP), .clockDiffRefN(clockDiffRefN),
        .resetPin(vio_reset),
        .uartRx(uartRx), .uartTx(uartTx),
        .cmacTxP(cmacTxP), .cmacTxN(cmacTxN), .cmacRxP(cmacRxP), .cmacRxN(cmacRxN),
        .chipMonScl(chipMonScl), .chipMonSclTristate(chipMonSclTristate),
        .chipMonSda(chipMonSda), .chipMonSdaTristate(chipMonSdaTristate),
        .iicScl(iicScl), .iicSclTristate(iicSclTristate),
        .iicSda(iicSda), .iicSdaTristate(iicSdaTristate),
        .ledOut(ledOut),
        .gpioOut(gpioOut), .gpioTristate(gpioTristate), .gpioIn(gpioIn),
        .thermalWarning(thermalWarning), .thermalError(thermalError), .debug(debug)
        );

  /* DEBUG CONTROLS */
`ifdef TARGET_VIO_DEBUG
  logic [27:0]            clockRef0Divide = '0;
  always_ff @(posedge clockRef[0]) clockRef0Divide <= (clockRef0Divide + 'd1);

  logic [31:8+1+UartCount+1+1+UartCount+UartCount+1+1]  vio_dummy_in = '0;
  logic [31:UartCount+1]  vio_dummy_out;

  `ifndef SIMULATION
  vio_0 uVIO (.clk(clockRef[RefClockTop]),
              .probe_in0({vio_dummy_in,
                          debug, vio_reset, vio_uart,
                          reset, clockRef0Divide[27],
                          uartRx, uartTx, thermalError, thermalWarning}),
              .probe_out0({vio_dummy_out,
                           vio_uart, vio_reset}));
  `else // SIMULATION
  assign vio_reset = 1'b0;
  assign vio_uart = '0;
  `endif
`else // !TARGET_VIO_DEBUG
  assign vio_reset = 1'b0;
  assign vio_uart = '0;
`endif

endmodule // chip_top
