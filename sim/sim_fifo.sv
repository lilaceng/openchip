
// SPDX-License-Identifier: MPL-2.0

module sim_fifo #(parameter integer Width = 32,
                  parameter integer Depth = 32,
                  localparam integer AddressWidth = $clog2(Depth))
  (
   input                    clock,
   input                    reset,
   input [AddressWidth-1:0] almostFullThreshold = (Depth/2),
   output logic             almostFull,
   input [Width-1:0]        inData,
   input                    inValid,
   output logic             inReady,
   output logic [Width-1:0] outData,
   output logic             outValid,
   input                    outReady
   );

  logic [Width-1:0]          mem [Depth-1:0];
  logic                      write;
  logic                      read;
  logic                      full;
  logic                      empty;
  logic [AddressWidth-1:0]   readPointer;
  logic                      readPointerZero;

  assign outData = mem[readPointer];
  assign outValid = ~empty;
  assign inReady = ~full;

  assign readPointerZero = (readPointer == 0);
  assign full = (readPointer == (Depth-1));
  assign write = (inValid && inReady);
  assign read = (outValid && outReady);

  always @(posedge clock) begin
    // specific coding style to infer SRL
    if (write) begin
      for (int i=0; i<(Depth-1); i++) begin
        mem[i+1] <= mem[i];
      end
      mem[0] <= inData;
    end
  end

  always @(posedge clock) begin
    empty <= (reset ? 1'b1 :
              (empty && write) ? 1'b0 :
              (readPointerZero && read && ~write) ? 1'b1 :
              empty);
    readPointer <= (reset ? '0 :
                    (write && ~read && ~full && ~empty) ? (readPointer+1) :
                    (~write && read && ~readPointerZero) ? (readPointer-1) :
                    readPointer);
    almostFull <= (reset ? 1'b0 :
                   (readPointer >= almostFullThreshold));
  end

endmodule // sim_fifo

