
// SPDX-License-Identifier: MPL-2.0

// sim_clock.sv -- generate a clock for simulation

module sim_clock #(
                   parameter integer ClockHz = 100000000,
                   parameter time ClockPeriod = (1s / ClockHz)
                   )
  (
   output logic clock
   );

  time          currentPeriod = ClockPeriod;

  always begin
    #(currentPeriod/2); clock = 1'b0;
    #(currentPeriod - (currentPeriod/2)); clock = 1'b1; // take care of rounding
  end

  task SetHz (input integer hz);
    currentPeriod = (1s / hz);
    $display("%t %m: Set Hz to %0d (period = %t)", $realtime, hz, currentPeriod);
  endtask // SetHz

  task SetPeriod (input time period);
    currentPeriod = period;
    $display("%t %m: Set Period to %0d", $realtime, currentPeriod);
  endtask // SetPeriod

endmodule
