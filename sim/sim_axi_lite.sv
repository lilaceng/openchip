
// SPDX-License-Identifier: MPL-2.0

module sim_axi_lite #(
                      parameter Debug = 0,
                      parameter Stress =  `OC_FROM_DEFINE_ELSE(SIM_STRESS, 0)
                      )
  (
   input  clock,
   input  reset,
   output oclib_pkg::axi_lite_s axil,
   input  oclib_pkg::axi_lite_fb_s axilFb
   );

  logic   stress = Stress;
  int     debug = Debug;

  always @(posedge clock) begin
    if (reset) axil <= 0;
  end

  task CsrWrite (input [31:0]  address,
                 input [31:0] data);
    logic                   running;
    if (debug) $display("%t %m: %x -> [%x] (starting)", $realtime, data, address);
    running = 1;
    @(posedge clock);
    fork
      begin
        while (running) begin
          axil.bready <= (stress ? `OC_RAND_PERCENT(20) : 1'b1);
          @(posedge clock);
        end
      end
      begin
        fork
          begin
            if (stress) while (`OC_RAND_PERCENT(80)) @(posedge clock);
            axil.awvalid <= 1;
            axil.awaddr <= address;
            @(posedge clock);
            while (axilFb.awready == 0) @(posedge clock);
            axil.awvalid <= 0;
          end
          begin
            if (stress)  while (`OC_RAND_PERCENT(80)) @(posedge clock);
            axil.wvalid <= 1;
            axil.wdata <= data;
            @(posedge clock);
            while (axilFb.wready == 0) @(posedge clock);
            axil.wvalid <= 0;
          end
        join
        while ((axilFb.bvalid && axil.bready) == 0) @(posedge clock);
        running = 0;
      end
    join
    axil.bready <= 0;
    @(posedge clock);
    if (debug) $display("%t %m: %x -> [%x]", $realtime, data, address);
  endtask // CsrWrite

  task CsrRead (input [31:0]        address,
                output logic [31:0] data,
                input               check = 0);
    if (debug) $display("%t %m%0s:          <- [%08x] (start)", $realtime,  check ? "Check" : "", address);
    if (stress)  while (`OC_RAND_PERCENT(50)) @(posedge clock);
    @(posedge clock);
    axil.araddr  <= address;
    axil.arvalid <= 1;
    axil.rready <= (stress ? `OC_RAND_PERCENT(20) : 1'b1);
    @(posedge clock);
    while (axilFb.arready == 0) begin
      axil.rready <= (stress ? `OC_RAND_PERCENT(20) : 1'b1);
      @(posedge clock);
    end
    axil.arvalid <= 0;
    while ((axilFb.rvalid && axil.rready) == 0) begin
      axil.rready <= (stress ? `OC_RAND_PERCENT(20) : 1'b1);
      @(posedge clock);
    end
    axil.rready <= 0;
    data = axilFb.rdata;
    @(posedge clock);
    if (debug) $display("%t %m%0s: %08x <- [%08x] (done)", $realtime,  check ? "Check" : "", data, address);
  endtask // CsrRead

  task CsrReadCheck (input [31:0]  address,
                     input [31:0]  data,
                     input [31:0]  mask = 32'hffffffff);
    logic [31:0]                   readData;
    CsrRead(address, readData, 1);
    if (readData !== data) begin
      // read data mismatch
      $display("%t %m ERROR: readData (%08x) !== expected (%08x)", $realtime, readData, data);
      $finish;
    end
  endtask // CsrReadCheck

endmodule // sim_axi_lite
