
// SPDX-License-Identifier: MPL-2.0

module sim_uart #(
                  parameter ClockHz = 100000000,
                  parameter Baud = 115200,
                  parameter Debug = 2
                  )
  (
   input clock,
   input reset,
   input in,
   output logic out,
   output logic [15:0] uartError
   );

  logic [7:0] dutTxData, dutRxData;
  logic       dutTxValid, dutRxValid;
  logic       dutTxReady, dutRxReady;
  oclib_pkg::bbc_s bbcToUart, bbcFromUart;

//  oclib_uart #(.ClockHz(ClockHz * 0.95), // run 5% fast to stress the DUT
  oclib_uart #(.ClockHz(ClockHz),
               .Baud(Baud),
               .TxFifoDepth(0))
  uUART (.clock(clock), .reset(reset),
         .rx(in), .tx(out),
         .bbcIn(bbcToUart), .bbcOut(bbcFromUart),
         .error(uartError));

  assign bbcToUart.data = dutTxData;
  assign bbcToUart.valid = dutTxValid;
  assign bbcToUart.ready = dutRxReady;
  assign dutRxData = bbcFromUart.data;
  assign dutRxValid = bbcFromUart.valid;
  assign dutTxReady = bbcFromUart.ready;

  logic       accelerate;
  logic       stopOnError;
  int         debug;

  initial begin
    accelerate = 0;
    dutTxValid = 0;
    dutRxReady = 0;
    stopOnError = 1;
    debug = Debug;
  end

  // SERIAL PORT SUPPORT FUNCTIONS

  localparam BaudCycles = (ClockHz / Baud);
  localparam ByteCycles = BaudCycles * 10;
  localparam TimeoutCycles = (ByteCycles * 2);

  function DebugRaw();
    return (debug >= 10);
  endfunction

  task TxByteRaw (input [7:0] data, input [31:0] maxCycles = TimeoutCycles);
    int cycles;
    int errorSignalled;
    if (DebugRaw()) $display("%t %m: Start sending 0x%02x \"%s\"", $realtime, data, char2string(data));
    if (accelerate) begin
      repeat (18) @(negedge uHARNESS.uCHIP.uTOP.uMANAGER.clock);
      uHARNESS.uCHIP.uTOP.uMANAGER.commandRxDataSelect = data;
      uHARNESS.uCHIP.uTOP.uMANAGER.commandRxValidSelect = 1'b1;
      @(negedge uHARNESS.uCHIP.uTOP.uMANAGER.clock);
      uHARNESS.uCHIP.uTOP.uMANAGER.commandRxValidSelect = 1'b0;
    end
    else begin
      @(posedge clock);
      dutTxData <= data;
      dutTxValid <= 1'b1;
      @(posedge clock);
      cycles = 0;
      errorSignalled = 0;
      while (dutTxReady == 1'b0) begin
        @(posedge clock);
        cycles++;
        if ((cycles > maxCycles) && !errorSignalled) begin
          $display("%t %m: ERROR: Unable to send for %0d cycles", $realtime, maxCycles);
          if (stopOnError) $finish;
          errorSignalled = 1;
        end
      end
      dutTxValid <= 1'b0;
    end
    if (DebugRaw()) $display("%t %m: Done sending 0x%02x \"%s\"", $realtime, data, char2string(data));
  endtask // TxByteRaw

  task RxByteRaw (output logic [7:0] data, input [31:0] maxCycles = TimeoutCycles);
    int cycles;
    int errorSignalled;
    if (DebugRaw()) $display("%t %m: Waiting for byte...", $realtime);
    cycles = 0;
    errorSignalled = 0;
    if (accelerate) begin
      while (uHARNESS.uCHIP.uTOP.uMANAGER.commandTxValid == 0) begin
        @(negedge uHARNESS.uCHIP.uTOP.uMANAGER.clock);
        cycles++;
        if ((cycles > maxCycles) && !errorSignalled) begin
          $display("%t %m: ERROR: Didn't get expected byte for %0d cycles", $realtime, maxCycles);
          if (stopOnError) $finish;
          errorSignalled = 1;
        end
      end
      data = uHARNESS.uCHIP.uTOP.uMANAGER.commandTxData;
      uHARNESS.uCHIP.uTOP.uMANAGER.commandTxValid = 0;
      uHARNESS.uCHIP.uTOP.uMANAGER.commandTxReady = 1;
      @(negedge uHARNESS.uCHIP.uTOP.uMANAGER.clock);
    end
    else begin
      @(posedge clock);
      while (!(dutRxValid && !dutRxReady)) begin
        @(posedge clock);
        cycles++;
        if ((cycles > maxCycles) && !errorSignalled) begin
          $display("%t %m: ERROR: Didn't get expected byte for %0d cycles", $realtime, maxCycles);
          if (stopOnError) $finish;
          errorSignalled = 1;
        end
      end
      data = dutRxData; // note this is transferred right away (not delayed by <=)
      dutRxReady <= 1'b1;
      @(posedge clock);
      dutRxReady <= 1'b0;
    end
    if (DebugRaw()) $display("%t %m: Received 0x%02x \"%s\"", $realtime, data, char2string(data));
  endtask // RxByteRaw

  task RxByteRawCheck (input [7:0] data);
    logic [7:0] rxData;
    RxByteRaw(rxData);
    if (rxData !== data) begin
      $display("%t %m: ERROR: rxData (%02x) !== expected (%02x) errormask=%02x", $realtime,
               rxData, data, rxData ^ data);
      if (stopOnError) $finish;
    end
    else begin
      if (DebugRaw()) $display("%t %m: rxData (%02x \"%s\") as expected", $realtime, data, char2string(data));
    end
  endtask // RxByteRawCheck

  // monitor rx bytes to make sure TB is expecting them
  integer cycles = 0;
  integer dutAutoRxSink = 0;
  always @(posedge clock) begin : dutRxValidSink
    if (dutRxValid && !dutRxReady) begin
      cycles++;
      if (dutAutoRxSink) dutRxReady <= 1'b1;
      if (cycles > 50000) begin
        $display("%t %m: Received 0x%02x \"%s\" (unclaimed)", $realtime, dutRxData, char2string(dutRxData));
        if (stopOnError) $finish;
      end
    end
    else begin
      cycles = 0;
      if (dutAutoRxSink) dutRxReady <= 1'b0;
    end
  end

  // BYTE/WORD TX/RX/RX_CHECK VIA HEX/BINARY L2 LAYER

  logic dutEcho = 1'b1;
  logic dutHex = 1'b0;
  function string dut_hex_string();
    return (dutHex ? " (translated to hex)" : "");
  endfunction

  function DebugL2();
    return (debug >= 5);
  endfunction

  function logic [3:0] ascii2hex (input [7:0] data);
    if ((data >= "a") && (data <= "f")) return (10 + data - "a");
    if ((data >= "A") && (data <= "F")) return (10 + data - "A");
    if ((data >= "0") && (data <= "9")) return (data - "0");
    $display("%t %m ERROR: expecting ASCII hex digit, got 0x%02x", $realtime, data);
    if (stopOnError) $finish;
    return 0;
  endfunction

  function logic [7:0] hex2ascii (input [3:0] data);
    if (data > 9) return "a" + (data-10);
    else return "0" + data;
  endfunction

  function string char2string (input [7:0] data);
    if ((^data) === 1'bX) return "<XX>";
    if ((^data) === 1'bZ) return "<ZZ>";
    return ((((data >= "a") && (data <= "z")) || ((data >= "A") && (data <= "Z")) || ((data >= "0") && (data <= "9")) ||
             (data == " ") || (data == "-") || (data == ".") || (data == ",") || (data == " ") ||
             (data == "\"") || (data == "'") || (data == "!") || (data == "?") || (data == "\\") ||
             (data == "-") || (data == ".") || (data == ",") || (data == ">") || (data == "<")) ? $sformatf("%c", data) :
            (data == 'h0d) ? "<0d \\r cr>" : (data == 'h0a) ? "<0a \\n lf>" : "<?>");
  endfunction // char2string

  task TxByte (input [7:0] data, input quiet = 0);
    if (DebugL2()) $display("%t %m: Sending 0x%02x \"%s\"%s...", $realtime, data, char2string(data), dut_hex_string());
    if (dutHex) begin
      TxByteRaw(hex2ascii(data >> 4));
      if (dutEcho) RxByteRawCheck(hex2ascii(data >> 4));
      TxByteRaw(hex2ascii(data & 'hf));
      if (dutEcho) RxByteRawCheck(hex2ascii(data & 'hf));
    end
    else begin
      TxByteRaw(data);
      if (dutEcho) RxByteRawCheck(data);
    end
    if (DebugL2()) $display("%t %m: Done%s", $realtime, dut_hex_string());
  endtask // TxByte

  task RxByte (output logic [7:0] data, input quiet = 0);
    logic [7:0] rxData;
    if (DebugL2()) $display("%t %m: Waiting for byte%s...", $realtime, dut_hex_string());
    if (dutHex) begin
      RxByteRaw(rxData);
      data = ascii2hex(rxData) << 4;
      RxByteRaw(rxData);
      data |= ascii2hex(rxData);
    end
    else RxByteRaw(data);
    if (DebugL2()) $display("%t %m: Received 0x%02x \"%s\"%s", $realtime, data, char2string(data), dut_hex_string());
  endtask // RxByte

  task RxByteCheck (input [7:0] data, input quiet = 0);
    logic [7:0] rxData;
    RxByte(rxData, .quiet(quiet));
    if (rxData !== data) begin
      $display("%t %m: ERROR: rxData (%02x) !== expected (%02x) errormask=%02x", $realtime,
               rxData, data, rxData ^ data);
      if (stopOnError) $finish;
    end
    else begin
      if (DebugL2()) $display("%t %m: rxData (%02x \"%s\") as expected", $realtime, data, char2string(data));
    end
  endtask // RxByteCheck

  task TxWord32 (input [31:0] data, input quiet = 0);
    if (DebugL2()) $display("%t %m: Sending 0x%08x%s...", $realtime, data, dut_hex_string());
    for (int i=0; i<4; i++) TxByte((data >> (24 - (i*8))) & 'hff, .quiet(1));
    if (DebugL2()) $display("%t %m: Done%s", $realtime, dut_hex_string());
  endtask // TxWord32

  task RxWord32 (output logic [31:0] data, input quiet = 0);
    logic [7:0] temp8;
    if (DebugL2()) $display("%t %m: Waiting for 32-bit word%s...", $realtime, dut_hex_string());
    data = '0;
    for (int i=0; i<4; i++) begin
      RxByte(temp8, .quiet(1));
      data |= (temp8 << (24 - (i*8)));
    end
    if (DebugL2()) $display("%t %m: Received 0x%08x \"%s\"%s", $realtime, data, char2string(data), dut_hex_string());
  endtask // RxWord32

  task RxWord32Check (input [31:0] data, input quiet = 0);
    logic [31:0] rxData;
    RxWord32(rxData, .quiet(quiet));
    if (rxData !== data) begin
      $display("%t %m: ERROR: rxData (%08x) !== expected (%08x) errormask=%08x", $realtime,
               rxData, data, rxData ^ data);
      if (stopOnError) $finish;
    end
    else begin
      if (DebugL2()) $display("%t %m: rxData (%08x) as expected", $realtime, data);
    end
  endtask // RxWord32Check

  // MISC HELPERS TO SHIFT JUNK DATA, READ STRINGS, ETC

  integer            shiftCount;
  logic [31:0]       shiftReadData;
  logic [0:15] [7:0] shiftReadString;

  task ShiftInit();
    shiftCount = 0;
  endtask // ShiftInit

  task ShiftReadData (input int n);
    logic [7:0]  readData;
    for (int i=0; i<n; i++) begin
      RxByte(readData);
      shiftReadData = {shiftReadData, readData};
      shiftCount++;
    end
  endtask

  task ReadString (input int n);
    logic [7:0]  readData;
    for (int i=0; i<n; i++) begin
      RxByte(readData);
      shiftReadString[i] = readData;
      shiftCount++;
    end
    for (int i=n; i<16; i++) begin
      shiftReadString[i] = 0;
    end
  endtask

  task ShiftUntil (input int n);
    logic [7:0]  readData;
    while (shiftCount < n) begin
      RxByte(readData);
      shiftCount++;
    end
  endtask // ShiftUntil


  // CONNECTION PROTOCOL LAYER

  function DebugConn();
    return (debug >= 3);
  endfunction

  logic connected = 1'b0;
  logic connectedAscii = 1'b0;
  function string connection_ascii_string();
    return (connectedAscii ? " (ASCII)" : " (BINARY)");
  endfunction

  task RxPrompt();
    RxByteCheck('h0d); // 0x0d <CR>
    RxByteCheck("\n"); // 0x0a <LF>
    RxByteCheck(">");
    $display("%t %m: Received TTY prompt", $realtime);
  endtask

  task Connect (input [7:0] channel, input [7:0] txBytes, input [7:0] rxBytes, input ascii);
    if (connected) begin
      $display("%t %m: ERROR: Got Connect() but already connected", $realtime);
      if (stopOnError) $finish;
    end
    connectedAscii = ascii;
    if (DebugConn()) $display("%t %m: ESTABLISH CONNECTION%s", $realtime, connection_ascii_string());
    if (ascii) begin
      dutHex = 0;
      dutEcho = 1;
      TxByte("c"); // ascii connect
      dutHex = 1; // translate this to hex
      TxByte(channel);
      dutHex = 0;
      TxByte("\n"); // execute ascii connect
      dutHex = 1; // remaining context is hex translated until disconnect
    end
    else begin
      dutHex = 0;
      dutEcho = 1;
      TxByte("C"); // binary connect
      // binary mode transfers, leave dutHex off
      dutEcho = 0;
      TxByte(channel);
      TxByte(txBytes);
      TxByte(rxBytes);
    end
    connected = 1;
    if (DebugConn()) $display("%t %m: CONNECTED%s", $realtime, connection_ascii_string());
  endtask // Connect

  task Flush ();
    if (!connected) begin
      $display("%t %m: ERROR: Got Flush() but not connected", $realtime);
      if (stopOnError) $finish;
    end
    if (DebugConn()) $display("%t %m: FLUSHING%s", $realtime, connection_ascii_string());
    if (connectedAscii) begin
      dutHex = 0;
      TxByte("\n");
      dutHex = 1;
    end
    if (DebugConn()) $display("%t %m: FLUSHED%s", $realtime, connection_ascii_string());
  endtask // Flush

  task Disconnect ();
    if (!connected) begin
      $display("%t %m: ERROR: Got Disconnect() but not connected", $realtime);
      if (stopOnError) $finish;
    end
    if (DebugConn()) $display("%t %m: DISCONNECTING%s", $realtime, connection_ascii_string());
    dutHex = 0;
    if (connectedAscii) begin
      dutEcho = 0;
      TxByte("~");
      RxPrompt();
    end
    connected = 0;
    dutEcho = 1;
    if (DebugConn()) $display("%t %m: DISCONNECTED%s", $realtime, connection_ascii_string());
  endtask // Disconnect

  // CSR PROTOCOL LAYER

  task CsrWrite (input [7:0] channel, input [3:0] space, input [31:0] address, input [31:0] data, input ascii = 0);
    $display("%t %m: %08x -> [%02x][%01x][%08x] (start)%s", $realtime, data, channel, space, address, connection_ascii_string());
    Connect(channel, .txBytes(oclib_pkg::WordToCsrBytes), .rxBytes(0), .ascii(ascii));
    TxByte({oclib_pkg::CsrCommandWrite, space});
    TxWord32(address);
    TxWord32(data);
    Flush();
    Disconnect();
    $display("%t %m: %08x -> [%02x][%01x][%08x] (done)%s", $realtime, data, channel, space, address, connection_ascii_string());
  endtask

  task CsrRead (input [7:0] channel, input [3:0] space, input [31:0] address, output logic [7:0] status, output logic [31:0] data,
                input ascii = 0, input check = 0);
    logic [7:0] temp8;
    $display("%t %m%0s: -------- <- [%02x][%01x][%08x] (start)%s", $realtime, check ? "Check" : "",
             channel, space, address, connection_ascii_string());
    Connect(channel, .txBytes(oclib_pkg::WordToCsrBytes), .rxBytes(oclib_pkg::WordFromCsrBytes), .ascii(ascii));
    TxByte({oclib_pkg::CsrCommandRead, space});
    TxWord32(address);
    TxWord32('0); // dummy bytes because the CSR word size is always the same
    Flush();
    RxByte(status);
    RxWord32(data);
    Disconnect();
    $display("%t %m%0s: %08x <- [%02x][%01x][%08x] (status: %02x) (done)%s", $realtime, check ? "Check" : "",
             data, channel, space, address, status,
             connection_ascii_string());
  endtask

  task CsrReadCheck (input [7:0] channel, input [3:0] space, input [31:0] address,
                     input [7:0] status, input [31:0] data, input [31:0] mask = 32'hffffffff,
                     input       ascii = 0);
    logic [7:0]                  readStatus;
    logic [31:0]                 readData;
    CsrRead(channel, space, address, readStatus, readData, ascii, 1);
    if (readStatus !== status) begin
      $display("%t %m: ERROR: readStatus (%02x) !== expected (%02x) errormask=%02x", $realtime,
               readStatus, status, readStatus ^ status);
      if (stopOnError) $finish;
    end
    if ((readData&mask) !== (data&mask)) begin
      $display("%t %m: ERROR: readData (%08x) !== expected (%08x) mask=%08x errormask=%08x", $realtime,
               readData, data, mask, (readData ^ data) & mask);
      if (stopOnError) $finish;
    end
  endtask // CsrReadCheck

  // WORD TRANSFER LAYER
  // TODO: should CSR layer be above (and using) word transfer layer?
  // TODO: should we rename "word" to "vector" everywhere?

  task WordWrite (input [7:0] channel, input [oclib_pkg::MaxWordBytes-1:0] [7:0] data, input integer length, input ascii = 0);
    $display("%t %m: %0d byte word -> channel [%02x] (start)%s", $realtime, length, channel,
             connection_ascii_string());
    Connect(channel, .txBytes(length), .rxBytes(0), .ascii(ascii));
    for (int i=0; i<length; i++) begin
      TxByte(data[i]);
    end
    Disconnect();
    $display("%t %m: %0d byte word -> channel [%02x] (done)%s", $realtime, length, channel,
             connection_ascii_string());
  endtask // WordWrite

  task WordRead (input [7:0] channel, output logic [oclib_pkg::MaxWordBytes-1:0] [7:0] data, input integer length, input ascii = 0);
    $display("%t %m: %0d byte word <- channel [%02x] (start)%s", $realtime, length, channel,
             connection_ascii_string());
    Connect(channel, .txBytes(0), .rxBytes(length), .ascii(ascii));
    for (int i=0; i<length; i++) begin
      RxByte(data[i]);
    end
    Disconnect();
    $display("%t %m: %0d byte word <- channel [%02x] (done)%s", $realtime, length, channel,
             connection_ascii_string());
  endtask // WordRead

  task WordReadCheck (input [7:0] channel, input [oclib_pkg::MaxWordBytes-1:0] [7:0] data, input integer length,
                      input ascii = 0);
    logic [oclib_pkg::MaxWordBytes-1:0] [7:0] readData;
    WordRead(channel, readData, length, ascii);
    if (readData !== data) begin
      $display("%t %m: ERROR: readData (%x) !== expected (%x) errormask=%x", $realtime,
               readData, data, readData ^ data);
      if (stopOnError) $finish;
    end
  endtask // WordReadCheck


endmodule // sim_uart
