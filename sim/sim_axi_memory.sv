
// SPDX-License-Identifier: MPL-2.0

module sim_axi_memory #(parameter integer AxiPorts = 1,
                        parameter bit     SeparateMemorySpaces = 0,
                        parameter integer MemoryBytes = 65536 * AxiPorts, // our sim convention is to use 64K per HBM port
                        parameter integer InputFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_INPUT_FIFO_DEPTH,64),
                        parameter integer OutputFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_OUTPUT_FIFO_DEPTH,64),
                        parameter integer ARFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_AR_FIFO_DEPTH,InputFifoDepth),
                        parameter integer AWFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_AW_FIFO_DEPTH,InputFifoDepth),
                        parameter integer WFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_W_FIFO_DEPTH,InputFifoDepth),
                        parameter integer BFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_B_FIFO_DEPTH,OutputFifoDepth),
                        parameter integer RFifoDepth = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_R_FIFO_DEPTH,OutputFifoDepth),
                        parameter integer ReadLatencyCycles = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_READ_LATENCY,32),
                        parameter integer WriteLatencyCycles = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_WRITE_LATENCY,ReadLatencyCycles),
                        parameter integer DutyCycle = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_READ_DUTY_CYCLE,100),
                        parameter integer ReadDutyCycle = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_READ_DUTY_CYCLE,DutyCycle),
                        parameter integer WriteDutyCycle = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_WRITE_DUTY_CYCLE,DutyCycle),
                        parameter integer GapCycles = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_GAP_CYCLES,0),
                        parameter integer ReadGapCycles = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_READ_GAP_CYCLES,GapCycles),
                        parameter integer WriteGapCycles = `OC_FROM_DEFINE_ELSE(SIM_AXI_MEMORY_WRITE_GAP_CYCLES,GapCycles))
  (
   input [AxiPorts-1:0] clockAxi,
   input [AxiPorts-1:0] resetAxi,
   input oclib_pkg::axi3_s [AxiPorts-1:0] axi,
   output oclib_pkg::axi3_fb_s [AxiPorts-1:0] axiFb
   );

  localparam integer MemWidth = $bits(axi[0].w.data);
  localparam integer MemDepth = (MemoryBytes / oclib_pkg::Axi3DataBytes);
  logic [MemWidth-1:0] mem [MemDepth-1:0];
`ifdef SIM_AXI_MEMORY_INIT
  initial for (int i=0; i<MemDepth; i++)
  `ifdef SIM_AXI_MEMORY_INIT_ZERO
    mem[i] = '0;
  `else
    mem[i] = MemWidth'(i);
  `endif
`endif

  logic [AxiPorts-1:0] blockRead = '0; 
  logic [AxiPorts-1:0] blockWrite = '0; 

  for (genvar port=0; port<AxiPorts; port++) begin : axiPort
    // really simplistic axi3 slave for now
    // * no support for byte masks
    // * no support for creating errors
    // * no support for creating reordering

    integer readDutyCycle = ReadDutyCycle;
    integer writeDutyCycle = WriteDutyCycle;

    // *** aw channel
    oclib_pkg::axi3_a_s                                    aw;
    logic                                                  awready;
    logic                                                  awvalid;
    logic                                                  aw_almost_full;
    localparam AWFifoAddressWidth = $clog2(AWFifoDepth);

    sim_fifo #(.Width($bits(oclib_pkg::axi3_a_s)), .Depth(AWFifoDepth))
    uAW_FIFO (clockAxi[port], resetAxi[port], AWFifoAddressWidth'(AWFifoDepth/2),
              aw_almost_full, axi[port].aw, axi[port].awvalid, axiFb[port].awready, aw, awvalid, awready);

    // *** ar channel
    oclib_pkg::axi3_a_s                                    ar;
    logic                                                  arready;
    logic                                                  arvalid;
    logic                                                  ar_almost_full;
    localparam ARFifoAddressWidth = $clog2(ARFifoDepth);

    sim_fifo #(.Width($bits(oclib_pkg::axi3_a_s)), .Depth(ARFifoDepth))
    uAR_FIFO (clockAxi[port], resetAxi[port], ARFifoAddressWidth'(ARFifoDepth/2),
              ar_almost_full, axi[port].ar, axi[port].arvalid, axiFb[port].arready, ar, arvalid, arready);

    // *** w channel
    oclib_pkg::axi3_w_s                                    w;
    logic                                                  wvalid;
    logic                                                  wready;
    logic                                                  w_almost_full;
    localparam WFifoAddressWidth = $clog2(WFifoDepth);

    sim_fifo #(.Width($bits(oclib_pkg::axi3_w_s)), .Depth(WFifoDepth))
    uW_FIFO (clockAxi[port], resetAxi[port], WFifoAddressWidth'(WFifoDepth/2),
             w_almost_full, axi[port].w, axi[port].wvalid, axiFb[port].wready, w, wvalid, wready);

    // *** r channel
    oclib_pkg::axi3_r_s                                    r;
    logic                                                  rvalid;
    logic                                                  rready;
    logic                                                  r_almost_full;
    oclib_pkg::axi3_r_s                                    rDelay;
    logic                                                  rvalidDelay;
    localparam RFifoAddressWidth = $clog2(RFifoDepth);

    oclib_pipeline #(.Width($bits(oclib_pkg::axi3_r_s)+1), .Length(ReadLatencyCycles))
    uR_PIPE (clockAxi[port], {rvalid,r}, {rvalidDelay,rDelay});

    sim_fifo #(.Width($bits(oclib_pkg::axi3_r_s)), .Depth(RFifoDepth))
    uR_FIFO (clockAxi[port], resetAxi[port], RFifoAddressWidth'(RFifoDepth-2-ReadLatencyCycles),
             r_almost_full, rDelay, rvalidDelay, rready, axiFb[port].r, axiFb[port].rvalid, axi[port].rready);

    // *** b channel
    oclib_pkg::axi3_b_s                                    b;
    logic                                                  bvalid;
    logic                                                  bready;
    logic                                                  b_almost_full;
    oclib_pkg::axi3_b_s                                    bDelay;
    logic                                                  bvalidDelay;
    localparam BFifoAddressWidth = $clog2(BFifoDepth);

    oclib_pipeline #(.Width($bits(oclib_pkg::axi3_b_s)+1), .Length(WriteLatencyCycles))
    uB_PIPE (clockAxi[port], {bvalid,b}, {bvalidDelay,bDelay});

    sim_fifo #(.Width($bits(oclib_pkg::axi3_b_s)), .Depth(BFifoDepth))
    uB_FIFO (clockAxi[port], resetAxi[port], BFifoAddressWidth'(BFifoDepth-2-WriteLatencyCycles),
             b_almost_full, bDelay, bvalidDelay, bready, axiFb[port].b, axiFb[port].bvalid, axi[port].bready);

    localparam integer PortAddressOffset = (SeparateMemorySpaces ? (port * (MemoryBytes/AxiPorts)) : 0);
    localparam integer PortAddressMask = (SeparateMemorySpaces ? ((MemoryBytes/AxiPorts)-1) : 'hffffffff);
    integer            readGapCount;
    integer            writeGapCount;
    integer            extraWriteWords;
    integer            extraWriteCount;
    integer            extraReadWords;
    integer            extraReadCount;

    logic              doingWrite;
    logic              doingRead;

    assign wready = (awready || (extraWriteWords));
    assign doingWrite = (awvalid && wvalid && awready && (extraWriteWords==0));
    assign doingRead = (arvalid && arready && (extraReadWords==0));

    function int physical_address( input int address, input int burstWord );
      return (((((address + (burstWord*oclib_pkg::Axi3DataBytes)) & PortAddressMask) + PortAddressOffset) /
               oclib_pkg::Axi3DataBytes) % MemDepth);
    endfunction

    always @(posedge clockAxi[port]) begin
      if (resetAxi[port]) begin
        awready <= 1'b0;
        arready <= 1'b0;
        bvalid <= 1'b0;
        rvalid <= 1'b0;
        readGapCount <= 0;
        writeGapCount <= 0;
        extraWriteWords <= '0;
        extraWriteCount <= '0;
        extraReadWords <= '0;
        extraReadCount <= '0;
      end
      else begin
        awready <= 1'b0;
        arready <= 1'b0;
        bvalid <= 1'b0;
        rvalid <= 1'b0;
        writeGapCount <= ((writeGapCount>0) ? (writeGapCount-1) : writeGapCount);
        readGapCount <= ((readGapCount>0) ? (readGapCount-1) : readGapCount);
        awready <= (!b_almost_full && `OC_RAND_PERCENT(writeDutyCycle) && (writeGapCount==0) &&
                    !(doingWrite && aw.len) && !(extraWriteWords>1) && !blockWrite[port]);
        if (extraWriteWords) begin
          writeGapCount <= WriteGapCycles;
          extraWriteCount <= (extraWriteCount+1);
          extraWriteWords <= (extraWriteWords-1);
          mem[physical_address(aw.addr, extraWriteCount)] <= w.data;
          `ifdef SIM_AXI_MEMORY_DEBUG
          $display("%t %m: DEBUG: writing %x to %x on port %0d (model addr %x, burst word %0d)", $realtime,
                   w.data, (aw.addr + (extraWriteCount*oclib_pkg::Axi3DataBytes)), port,
                   physical_address(aw.addr, extraWriteCount), extraWriteCount);
          `endif
          if (extraWriteWords == 1) begin
            bvalid <= 1'b1;
          end
        end
        else if (doingWrite) begin
          writeGapCount <= WriteGapCycles;
          extraWriteWords <= aw.len;
          extraWriteCount <= 1;
          b.id <= aw.id;
          b.resp <= 2'd0;
          bvalid <= (aw.len == 0);
          mem[physical_address(aw.addr, 0)] <= w.data;
          `ifdef SIM_AXI_MEMORY_DEBUG
          $display("%t %m: DEBUG: writing %x to %x on port %0d (model addr %x)", $realtime,
                   w.data, aw.addr, port, physical_address(aw.addr, 0));
          `endif
        end
        arready <= (!r_almost_full && `OC_RAND_PERCENT(readDutyCycle) && (readGapCount==0) &&
                   !(doingRead && ar.len) && !(extraReadWords>1) && !blockRead[port]);
        if (extraReadWords) begin
          readGapCount <= ReadGapCycles;
          extraReadCount <= (extraReadCount+1);
          extraReadWords <= (extraReadWords-1);
          rvalid <= 1'b1;
          r.data <= mem[physical_address(ar.addr, extraReadCount)];
          `ifdef SIM_AXI_MEMORY_DEBUG
          $display("%t %m: DEBUG: reading %x from %x on port %0d (model addr %x, burst word %0d)", $realtime,
                   mem[physical_address(ar.addr, extraReadCount)], (ar.addr + (extraReadCount*oclib_pkg::Axi3DataBytes)), port,
                   physical_address(ar.addr, extraReadCount), extraReadCount);
          `endif
          if (extraReadWords == 1) begin
            r.last <= 1'b1;
          end
        end
        else if (doingRead) begin
          readGapCount <= ReadGapCycles;
          extraReadWords <= ar.len;
          extraReadCount <= 1;
          rvalid <= 1'b1;
          r.id <= ar.id;
          r.last <= (ar.len==0);
          r.resp <= 2'd0;
          r.data <= mem[physical_address(ar.addr, 0)];
          `ifdef SIM_AXI_MEMORY_DEBUG
          $display("%t %m: DEBUG: reading %x from %x on port %0d (model addr %x)", $realtime,
                   mem[physical_address(ar.addr, 0)], ar.addr, port, physical_address(ar.addr, 0));
          `endif
        end
      end
    end

  end // block: axiPort

endmodule // sim_axi_memory
