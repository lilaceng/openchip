
// SPDX-License-Identifier: MPL-2.0

// sim_master.sv -- this accepts CSR transactions that are to be routed to the DUT via appropriate paths
//                  (UART, PCIe, etc).  It also supports accelerating CSR accesses.  This module has no
//                  I/O itself, it talks directly to other parts of the TB or pokes directly into design.


`define OC_SIM_MASTER_CSR_WRITE(PATH) \
  begin \
   `ifdef PATH \
      @(negedge `PATH.clock); \
      `PATH.csrSelect <= (1<<space); \
      @(negedge `PATH.clock); \
      `PATH.csrSelect <= (1<<space); \
      `PATH.csr.write <= 1'b1; \
      `PATH.csr.read <= 1'b0; \
      `PATH.csr.address <= address; \
      `PATH.csr.wdata <= data; \
      tempcount = 0; \
      while (`PATH.csrFb[0].ready == 1'b0) begin \
        @(negedge `PATH.clock); \
        `PATH.csrSelect <= (1<<space); \
        tempcount++; \
        if (tempcount == 100) begin \
          $display("%t %m: ERROR: timeout writing to [%02x][%01x][%08x] (fast)", $realtime, channel, space, address); \
          $finish; \
        end \
      end \
      `PATH.csr.write <= 1'b0; \
      @(negedge `PATH.clock); \
      `PATH.csrSelect <= 1'b0; \
      @(negedge `PATH.clock); \
      if (debug) $display("%t %m: %08x -> [%02x][%01x][%08x] (fast)", $realtime, data, channel, space, address); \
   `else \
      $display("%t %m: INTERNAL ERROR: shouldn't be here", $realtime); \
   `endif \
  end

`define OC_SIM_MASTER_CSR_READ(PATH) \
  begin \
   `ifdef PATH \
      @(negedge `PATH.clock); \
      `PATH.csrSelect <= (1<<space); \
      @(negedge `PATH.clock); \
      `PATH.csrSelect <= (1<<space); \
      `PATH.csr.write <= 1'b0; \
      `PATH.csr.read <= 1'b1; \
      `PATH.csr.address <= address; \
      `PATH.csr.wdata <= '0; \
      tempcount = 0; \
      while (`PATH.csrFb[0].ready == 1'b0) begin \
        @(negedge `PATH.clock); \
        `PATH.csrSelect <= (1<<space); \
        tempcount++; \
        if (tempcount == 100) begin \
          $display("%t %m: ERROR: timeout reading from [%02x][%01x][%08x] (fast)", $realtime, channel, space, address); \
          $finish; \
        end \
      end \
      status = {7'd0, `PATH.csrFb[0].error}; \
      data = `PATH.csrFb[0].rdata; \
      `PATH.csr.read <= 1'b0; \
      @(negedge `PATH.clock); \
      `PATH.csrSelect <= 1'b0; \
      @(negedge `PATH.clock); \
      if (debug) $display("%t %m%0s: %08x <- [%02x][%01x][%08x] (status: %02x) (fast)", $realtime, check ? "Check" : "", \
                          data, channel, space, address, status); \
   `endif \
  end

module sim_master;

  int         debug;
  initial debug = 0;
  logic       accelerate;
  initial accelerate = 0;
  int         tempcount;
  initial tempcount = 0;

  task CsrWrite (input [7:0] channel, input [3:0] space, input [31:0] address, input [31:0] data, input ascii = 0);
    if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_A_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_A_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_B_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_B_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_C_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_C_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_D_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_D_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_E_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_E_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_F_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_F_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_G_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_G_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_H_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_WRITE(SIM_ACCEL_CSR_H_PATH)
    // fall back to UART
    else uHARNESS.uUART.CsrWrite(channel, space, address, data, ascii);
  endtask // CsrWrite

  task CsrRead (input [7:0] channel, input [3:0] space, input [31:0] address, output logic [7:0] status, output logic [31:0] data,
                input ascii = 0, input check = 0);
    if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_A_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_A_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_B_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_B_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_C_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_C_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_D_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_D_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_E_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_E_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_F_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_F_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_G_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_G_PATH)
    else if (accelerate && (channel==`OC_FROM_DEFINE_ELSE(SIM_ACCEL_CSR_H_CHANNEL,-1)))
      `OC_SIM_MASTER_CSR_READ(SIM_ACCEL_CSR_H_PATH)
    else uHARNESS.uUART.CsrRead(channel, space, address, status, data, ascii, check);
  endtask // CsrRead

  task CsrReadCheck (input [7:0] channel, input [3:0] space, input [31:0] address,
                     input [7:0] status, input [31:0] data, input [31:0] mask = 32'hffffffff,
                     input       ascii = 0);
    logic [7:0]                  readStatus;
    logic [31:0]                 readData;
    CsrRead(channel, space, address, readStatus, readData, ascii, 1);
    if (readStatus !== status) begin
      $display("%t %m: ERROR: readStatus (%02x) !== expected (%02x) errormask=%02x", $realtime,
               readStatus, status, readStatus ^ status);
      $finish;
    end
    if ((readData&mask) !== (data&mask)) begin
      $display("%t %m: ERROR: readData (%08x) !== expected (%08x) mask=%08x errormask=%08x", $realtime,
               readData, data, mask, (readData ^ data) & mask);
      $finish;
    end
  endtask // CsrReadCheck

endmodule // sim_master
