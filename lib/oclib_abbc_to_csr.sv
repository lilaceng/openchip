
// SPDX-License-Identifier: MPL-2.0

module oclib_abbc_to_csr #(
                         parameter AddressSpaces = 1,
                         parameter ResetSync = 0
                         )
  (
   input                            clock,
   input                            reset,
   input                            oclib_pkg::abbc_s abbcIn,
   output                           oclib_pkg::abbc_s abbcOut,
   output logic [AddressSpaces-1:0] csrSelect,
   output                           oclib_pkg::csr_s csr,
   input                            oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb
   );

  logic                             resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  localparam WordToCsrW = oclib_pkg::WordToCsrW;
  localparam WordFromCsrW = oclib_pkg::WordFromCsrW;

  logic [WordFromCsrW-1:0]          wordFromCsrData;
  logic                             wordFromCsrValid;
  logic                             wordFromCsrReady;
  logic [WordToCsrW-1:0]            wordToCsrData;
  logic                             wordToCsrValid;
  logic                             wordToCsrReady;

  oclib_abbc_to_word #(.WordFromAbbcW(WordToCsrW), .WordToAbbcW(WordFromCsrW))
  uABBC_TO_WORD (.clock(clock), .reset(resetQ), .abbcIn(abbcIn), .abbcOut(abbcOut),
                 .wordFromAbbcData(wordToCsrData), .wordFromAbbcValid(wordToCsrValid), .wordFromAbbcReady(wordToCsrReady),
                 .wordToAbbcData(wordFromCsrData), .wordToAbbcValid(wordFromCsrValid), .wordToAbbcReady(wordFromCsrReady));

  oclib_word_to_csr #(.AddressSpaces(AddressSpaces))
  uWORD_TO_CSR (.clock(clock), .reset(resetQ),
                .wordToCsrData(wordToCsrData), .wordToCsrValid(wordToCsrValid), .wordToCsrReady(wordToCsrReady),
                .wordFromCsrData(wordFromCsrData), .wordFromCsrValid(wordFromCsrValid), .wordFromCsrReady(wordFromCsrReady),
                .csrSelect(csrSelect), .csr(csr), .csrFb(csrFb));

endmodule // oclib_abbc_to_csr
