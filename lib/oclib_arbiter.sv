
// SPDX-License-Identifier: MPL-2.0

module oclib_arbiter #(
                       parameter integer Inputs = 2
                       )
  (
   input [Inputs-1:0]        request,
   input [Inputs-1:0]        next,
   output logic [Inputs-1:0] grant
   );

  logic [(Inputs*2)-1:0]     requestDouble;
  logic [(Inputs*2)-1:0]     grantDouble;
  assign requestDouble = {request,request};
  assign grantDouble = (requestDouble & ~(requestDouble-next));
  assign grant = (grantDouble[Inputs-1:0] | grantDouble[(2*Inputs)-1:Inputs]);

endmodule // oclib_arbiter
