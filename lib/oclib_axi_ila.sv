
// SPDX-License-Identifier: MPL-2.0

module oclib_axi_ila (
                      input clock,
                      input oclib_pkg::axi3_s axi,
                      input oclib_pkg::axi3_fb_s axiFb
                      );
  `ifndef SIMULATION
  ila_0 uILA (
              .clk(clock),
//              .trig_in(1'b0),
//              .trig_out_ack(1'b0),
              .probe0({axi.w.data[15:0],
                       axiFb.r.data[15:0],
                       axi.aw.addr,
                       axi.ar.addr }),
              .probe1({axi.awvalid,
                       axiFb.awready,
                       axi.aw.id,
                       axi.aw.len,
                       axi.aw.size,
                       axi.aw.burst,
                       axi.wvalid,
                       axiFb.wready,
                       axi.w.last,
                       axi.arvalid,
                       axiFb.arready,
                       axi.ar.id,
                       axi.ar.len,
                       axi.ar.size,
                       axi.ar.burst,
                       axiFb.bvalid,
                       axi.bready,
                       axiFb.b.id,
                       axiFb.b.resp,
                       axiFb.rvalid,
                       axi.rready,
                       axiFb.r.id,
                       axiFb.r.last,
                       axiFb.r.resp
                       })
              );
  `endif

endmodule // oclib_axi_ila
