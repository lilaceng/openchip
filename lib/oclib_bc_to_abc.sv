
// SPDX-License-Identifier: MPL-2.0

module oclib_bc_to_abc #(
                         parameter ResetSync = 0,
                         parameter MaxSkewNS = 4
                       )
  (
   input  clock,
   input  reset,
   input  oclib_pkg::bc_s bc,
   output oclib_pkg::bc_fb_s bcFb,
   (* OC_MAX_SKEW_NS = MaxSkewNS *)
   output oclib_pkg::abc_s abc,
   input  oclib_pkg::abc_fb_s abcFb
   );

  logic                            resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  // synchronize the incoming async signals
  oclib_pkg::abc_fb_s abcFbSync;
  oclib_synchronizer #(.Width($bits(abcFb))) uABC_FB_SYNC (clock, abcFb, abcFbSync);

  enum                             logic [1:0] { StIdle, StReq, StWait } state;

  always_ff @(posedge clock) begin
    if (resetQ) begin
      bcFb.ready <= 1'b0;
      abc.req <= 1'b0;
      abc.data <= '0;
      state <= StIdle;
    end
    else begin
      bcFb.ready <= 1'b0;
      case (state)
        StIdle : begin
          if (bc.valid) begin
            abc.data <= bc.data;
            abc.req <= 1'b1;
            bcFb.ready <= 1'b1;
            state <= StReq;
          end
        end
        StReq : begin
          if (abcFbSync.ack) begin
            abc.req <= 1'b0;
            state <= StWait;
          end
        end
        StWait : begin
          if (!abcFbSync.ack) begin
            state <= StIdle;
          end
        end
      endcase
    end
  end

endmodule // oclib_bc_to_abc
