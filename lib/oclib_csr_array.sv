
// SPDX-License-Identifier: MPL-2.0

module oclib_csr_array #(
                         parameter integer                      DataW = 32,
                         parameter integer                      NumCsr = 8,
                         parameter integer                      ResetSync = 0,
                         parameter integer                      ResetFlops = 0,

                         // These bits allow setting the default for all CSRs, which can be tuned per-CSR below
                         parameter bit                          InputAsync = 0,
                         parameter bit                          OutputAsync = 0,
                         parameter integer                      InputFlops = 0,
                         parameter integer                      OutputFlops = 0,

                         // From here down we are configuring per-CSR operation
                         parameter bit [NumCsr-1:0] [DataW-1:0] CsrFixedBits = '0, // set are 1 in csrConfig and readData
                         parameter bit [NumCsr-1:0] [DataW-1:0] CsrInitBits = '0, // reset to 1 in csrConfig
                         parameter bit [NumCsr-1:0] [DataW-1:0] CsrRwBits = '0, // writable and appear in readData
                         parameter bit [NumCsr-1:0] [DataW-1:0] CsrRoBits = '0, // read-only, readData from csrStatus
                         parameter bit [NumCsr-1:0] [DataW-1:0] CsrWoBits = '0, // write-only, go to csrConfig but not readData

                         // The following per-CSR config is about synch/pipelining per CSR, defaulting to global setting above
                         parameter integer                      CsrInputFlops [NumCsr-1:0] = '{ NumCsr { InputFlops }},
                         parameter integer                      CsrOutputFlops [NumCsr-1:0] = '{ NumCsr { OutputFlops }},
                         parameter bit [NumCsr-1:0]             CsrInputAsync = { NumCsr { InputAsync }},
                         parameter bit [NumCsr-1:0]             CsrOutputAsync = { NumCsr { OutputAsync }} // requires clockAsync
                         )
  (
   input                                 clock,
   input                                 reset,
   input                                 csrSelect,
   input                                 oclib_pkg::csr_s csr,
   output                                oclib_pkg::csr_fb_s csrFb,
   output logic [NumCsr-1:0] [DataW-1:0] csrConfig,
   input [NumCsr-1:0] [DataW-1:0]        csrStatus,
   output logic [NumCsr-1:0]             csrRead,
   output logic [NumCsr-1:0]             csrWrite,
   input                                 clockAsync = 1'b0
   );

  // synch then pipe the incoming reset, as per ResetSync and ResetFlops
  logic                                  resetSync, resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetSync);
  oclib_pipeline #(.Length(ResetFlops), .DontTouch(1), .NoShiftRegister(1))
  uRESET_FLOPS (.clock(clock), .in(resetSync), .out(resetQ));

  // synch then pipe the incoming csrStatus, as per CsrInputAsync and CsrInputFlops
  logic [NumCsr-1:0] [DataW-1:0]         csrStatusSync;
  logic [NumCsr-1:0] [DataW-1:0]         csrStatusInternal;

  for (genvar i=0; i<NumCsr; i++) begin : status_pipe

    oclib_synchronizer #(.Width(DataW), .Enable(CsrInputAsync[i]))
    uSYNC (.clock(clock), .in(csrStatus[i]), .out(csrStatusSync[i]));

    oclib_pipeline #(.Width(DataW), .Length(CsrInputFlops[i]), .NoShiftRegister(1))
    uPIPE (.clock(clock), .in(csrStatusSync[i]), .out(csrStatusInternal[i]));

  end

  // pipe then synch the outgoing csrConfig, as per CsrOutputAsync and CsrOutputFlops
  logic [NumCsr-1:0] [DataW-1:0]         csrConfigInternal;
  logic [NumCsr-1:0] [DataW-1:0]         csrConfigPipe;
  for (genvar i=0; i<NumCsr; i++) begin : config_pipe

    oclib_pipeline #(.Width(DataW), .Length(CsrOutputFlops[i]), .NoShiftRegister(1))
    uPIPE (.clock(clock), .in(csrConfigInternal[i]), .out(csrConfigPipe[i]));

    oclib_synchronizer #(.Width(DataW), .Enable(CsrOutputAsync[i]))
    uSYNC (.clock(clockAsync), .in(csrConfigPipe[i]), .out(csrConfig[i]));

  end

  // These strobes aren't handled very well in async case, in future may want to revisit this
  // so that (for example) logic in another clock domain gets a read strobe on the exact
  // cycle that it's data is captured, so that it can implement a clear-on-read counter.  Right
  // now we just output pulses in main clock domain, if they are needed by async client they
  // can be synchronized but the exact alignment to the input/output data busses will be undefined
  logic [NumCsr-1:0]                     csrWriteInternal;
  logic [NumCsr-1:0]                     csrReadInternal;
  oclib_pipeline #(.Width(2*NumCsr), .Length(OutputFlops), .NoShiftRegister(1))
  uOUTPUT_FLOPS (.clock(clock), .in({csrWriteInternal,csrReadInternal}),
                 .out({csrWrite, csrRead}));

  // Compute read values for all CSRs, this logic is unrolled and mostly optimizes away
  logic [NumCsr-1:0] [DataW-1:0]         csrReadValue;
  always_comb begin
    for (int i=0; i<NumCsr; i++) begin
      csrReadValue[i] = ((csrConfigInternal[i] & CsrRwBits[i] & ~CsrWoBits[i] & ~CsrRoBits[i] & ~CsrFixedBits[i]) |
                         (csrStatusInternal[i] & CsrRoBits[i] & ~CsrFixedBits[i]) |
                         (CsrInitBits[i] & CsrFixedBits[i]));
    end
  end

  // Implement the CSR flops.  Fairly simple, also heavily reliant on synthesis optimizing unused logic (a common theme in OpenChip)
  always_ff @(posedge clock) begin
    csrFb.rdata <= csrReadValue[csr.address];
    if (resetQ) begin
      for (int i=0; i<NumCsr; i++) begin
        csrConfigInternal[i] <= CsrInitBits[i];
      end
      csrFb.ready <= 1'b0;
      csrFb.error <= 1'b0;
      csrReadInternal <= '0;
      csrWriteInternal <= '0;
    end
    else begin
      csrReadInternal <= '0;
      csrWriteInternal <= '0;
      csrFb.ready <= (csrSelect && (csr.read || csr.write) && !csrFb.ready);
      csrFb.error <= (csrFb.error ? csrSelect :
                      (csrSelect && (csr.read || csr.write) && (csr.address >= NumCsr)));
      for (int i=0; i<NumCsr; i++) begin
        if (i==csr.address) begin
          if (csrSelect && csr.write && !csrFb.ready) begin
            csrConfigInternal[i] <= ((CsrFixedBits[i] & CsrInitBits[i]) |
                                     (csr.wdata & (CsrRwBits[i] | CsrWoBits[i]) & ~CsrFixedBits[i]));
            csrWriteInternal[i] <= 1'b1;
          end
          if (csrSelect && csr.read && !csrFb.ready) begin
            csrReadInternal[i] <= 1'b1;
          end
        end
      end
    end
  end

endmodule // oclib_csr_array
