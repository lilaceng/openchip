
// SPDX-License-Identifier: MPL-2.0

module oclib_reorder_fifo #(parameter integer Width = 32,
                            parameter integer  Depth = 32,
                            localparam integer AddressWidth = $clog2(Depth))
  (
   input                         clock,
   input                         reset,
   output logic                  init,
   output logic [AddressWidth:0] slot,
   output logic                  slotValid,
   input                         slotReady,
   input [Width-1:0]             inData,
   input [AddressWidth:0]        inSlot,
   input                         inValid,
   output logic [Width-1:0]      outData,
   output logic                  outValid,
   input                         outReady
   );

  logic [AddressWidth:0]         readAddress; // MSB indicates which phase
  logic [AddressWidth:0]         readAddressD;
  logic [AddressWidth:0]         slotD;

  oclib_ram1r1w #(.Width(Width), .Depth(Depth))
  uDATA_RAM (.clock(clock),
             .write(inValid),
             .writeAddress(inSlot[AddressWidth-1:0]),
             .writeData(inData),
             .read(1'b1),
             .readAddress(readAddress[AddressWidth-1:0] + (outValid && outReady)),
             .readData(outData)
             );

  logic                          readValid;
  oclib_ram1r1w #(.Width(1), .Depth(Depth))
  uVALID_RAM (.clock(clock),
              .write(init ? 1'b1 : inValid),
              .writeAddress(init ? readAddress[AddressWidth-1:0] : inSlot[AddressWidth-1:0]),
              .writeData(init ? 1'b1 : inSlot[AddressWidth]),
              .read(1'b1),
              .readAddress(readAddressD[AddressWidth-1:0]),
              .readData(readValid)
              );

  assign outValid = ((readValid ^ ~readAddress[AddressWidth]) && ~init);
  assign readAddressD = readAddress + (outValid && outReady);
  assign slotD = slot + (slotValid && slotReady);

  logic                          initInternal;
  always_ff @(posedge clock) begin
    if (reset) begin
      initInternal <= 1'b1;
      init <= 1'b1;
      readAddress <= {1'b1, {AddressWidth{1'b0}}};
      slot <= '0;
      slotValid <= 1'b0;
    end
    else begin
      if (initInternal) begin
        readAddress <= (readAddress + 'd1);
        init <= 1'b1;
        initInternal <= (readAddress[AddressWidth-1:0] != (Depth-1));
      end
      else begin
        readAddress <= readAddressD;
        slot <= slotD;
        slotValid <= ((readAddressD[AddressWidth-1:0] != slotD[AddressWidth-1:0]) ||
                      (readAddressD[AddressWidth] == slotD[AddressWidth]));
        init <= 1'b0;
      end
    end
  end

endmodule // oclib_reorder_fifo

