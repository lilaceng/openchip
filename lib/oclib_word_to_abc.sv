
// SPDX-License-Identifier: MPL-2.0

module oclib_word_to_abc #(
                         parameter WordW = 64,
                         parameter ShiftFanout = 16,
                         parameter ResetSync = 0
                         )
  (
   input             clock,
   input             reset,
   input [WordW-1:0] wordData,
   input             wordValid,
   output logic      wordReady,
   output            oclib_pkg::abc_s abc,
   input             oclib_pkg::abc_fb_s abcFb
   );

  logic                     resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  oclib_pkg::bc_s bc;
  oclib_pkg::bc_fb_s bcFb;
  oclib_word_to_bc #(.WordW(WordW), .ShiftFanout(ShiftFanout))
  uWORD_TO_BC (.clock(clock), .reset(resetQ),
               .wordData(wordData), .wordValid(wordValid), .wordReady(wordReady),
               .bc(bc), .bcFb(bcFb));

  oclib_bc_to_abc uABC_TO_BC (.clock(clock), .reset(resetQ),
                            .bc(bc), .bcFb(bcFb),
                            .abc(abc), .abcFb(abcFb));

endmodule // oclib_word_to_abc
