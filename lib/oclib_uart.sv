
// SPDX-License-Identifier: MPL-2.0

module oclib_uart #(
                    parameter integer ClockHz = 100000000,
                    parameter integer Baud = 115200,
                    parameter integer BaudCycles = (ClockHz / Baud),
                    parameter integer DebounceCycles = (BaudCycles / 16),
                    parameter bit ResetSync = 0,
                    parameter integer TxFifoDepth = 32,
                    parameter integer RxFifoDepth = 0
                  )
  (
   input         clock,
   input         reset,
   input         rx,
   output        tx,
   input         oclib_pkg::bbc_s bbcIn,
   output        oclib_pkg::bbc_s bbcOut,
   output [15:0] error
   );

  wire                             resetQ;
  oclib_synchronizer  #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  oclib_uart_tx #(ClockHz, Baud, BaudCycles, TxFifoDepth)
  uTX (clock, resetQ, bbcIn.data, bbcIn.valid, bbcOut.ready, tx);

  oclib_uart_rx #(ClockHz, Baud, BaudCycles, DebounceCycles, RxFifoDepth)
  uRX (clock, resetQ,  bbcOut.data, bbcOut.valid, bbcIn.ready, rx, error);

endmodule // oclib_uart
