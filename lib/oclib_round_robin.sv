
// SPDX-License-Identifier: MPL-2.0

module oclib_round_robin #(
                           parameter integer Inputs = 2,
                           localparam PointerWidth = $clog2(Inputs)
                           )
  (
   input                     clock,
   input                     reset,
   input [Inputs-1:0]        request,
   output logic [Inputs-1:0] grant
   );

  logic [Inputs-1:0]         next;

  always_ff @(posedge clock) begin
    next <= (reset ? 'd1 :
             ((|request) ? {grant[Inputs-2:0],grant[Inputs-1]} :
              next));
  end

  oclib_arbiter #(.Inputs(Inputs))
  uARB (.request(request), .next(next), .grant(grant));

endmodule // oclib_round_robin

