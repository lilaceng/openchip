
// SPDX-License-Identifier: MPL-2.0


module oclib_csr_synchronizer #(CsrSelectBits = 1)
  (
   input                            resetIn,
   input                            clockIn,
   input [CsrSelectBits-1:0]        csrSelectIn,
   input                            oclib_pkg::csr_s csrIn,
   output                           oclib_pkg::csr_fb_s csrInFb,
   input                            resetOut,
   input                            clockOut,
   output logic [CsrSelectBits-1:0] csrSelectOut,
   output                           oclib_pkg::csr_s csrOut,
   input                            oclib_pkg::csr_fb_s csrOutFb
   );

  // Synchronize the csr input bus.  This is easy because all the signals are level, not pulse.  We
  // will still have to take care to not assume all signals arrive at far side on the same clock (as
  // always one clock skew is tolerated on async crossing)

  logic [CsrSelectBits-1:0]   csrSelectInSync;
  oclib_pkg::csr_s            csrInSync;

  oclib_synchronizer #(.Width(CsrSelectBits + $bits(csrIn)))
  uREQ_SYNC (.clock(clockOut), .in({csrSelectIn, csrIn}), .out({csrSelectInSync, csrInSync}));

  // Look at synchronized version of csrIn and generate csrOut.  The csrOutFb has two pulsed signals
  // (ready, error) which we convert to toggle form for clock crossing.

  logic                       csrOutWrite;
  logic                       csrOutRead;
  enum                        logic [1:0] { StOutIdle, StOutStart, StOutWait, StOutFinish } csrOutState;
  logic                       csrToggleReady;
  logic                       csrToggleError;

  always_ff @(posedge clockOut) begin
    if (resetOut) begin
      csrOutWrite <= 1'b0;
      csrOutRead <= 1'b0;
      csrSelectOut <= '0;
      csrOutState <= StOutIdle;
      csrToggleReady <= 1'b0;
      csrToggleError <= 1'b0;
    end
    else begin
      case (csrOutState)
        StOutIdle : begin
          if (csrInSync.read || csrInSync.write) begin
            csrOutState <= StOutStart;
          end
        end
        StOutStart : begin
          csrSelectOut <= csrSelectInSync;
          csrOutWrite <= csrInSync.write;
          csrOutRead <= csrInSync.read;
          csrOutState <= StOutWait;
        end
        StOutWait : begin
          if (csrOutFb.ready || csrOutFb.error) begin
            csrSelectOut <= '0;
            csrOutWrite <= 1'b0;
            csrOutRead <= 1'b0;
            csrToggleReady <= (csrToggleReady ^ csrOutFb.ready);
            csrToggleError <= (csrToggleError ^ csrOutFb.error);
            csrOutState <= StOutFinish;
          end
        end
        StOutFinish : begin
          if (!(csrInSync.read || csrInSync.write)) begin
            csrOutState <= StOutIdle;
          end
        end
      endcase
    end
  end

  assign csrOut.address = csrInSync.address;
  assign csrOut.write = csrOutWrite;
  assign csrOut.read = csrOutRead;
  assign csrOut.wdata = csrInSync.wdata;

  // Monitor the ready/error toggle strobes, wait a cycle for all signals to come across, then
  // drive csrInFb

  logic csrToggleErrorSync;
  logic csrToggleReadySync;
  logic [31:0] csrReadDataSync;

  oclib_synchronizer #(.Width(2 + 32))
  uRESP_SYNC (.clock(clockOut), .in({csrToggleError, csrToggleReady, csrOutFb.rdata}),
              .out({csrToggleErrorSync, csrToggleReadySync, csrReadDataSync}));

  enum                        logic { StInIdle, StInGo } csrInState;
  logic csrToggleErrorIn;
  logic csrToggleReadyIn;

  always_ff @(posedge clockIn) begin
    if (resetIn) begin
      csrInState <= StInIdle;
      csrInFb <= '0;
      csrToggleReadyIn <= csrToggleReadySync;
      csrToggleErrorIn <= csrToggleErrorSync;
    end
    else begin
      case (csrInState)
        StInIdle : begin
          csrInFb.ready <= 1'b0;
          csrInFb.error <= 1'b0;
          if ((csrToggleErrorIn ^ csrToggleErrorSync) || (csrToggleReadyIn ^ csrToggleReadySync)) begin
            csrInState <= StInGo;
          end
        end
        StInGo : begin
          csrInFb.ready <= (csrToggleReadyIn ^ csrToggleReadySync);
          csrInFb.error <= (csrToggleErrorIn ^ csrToggleErrorSync);
          csrInFb.rdata <= csrReadDataSync;
          csrToggleReadyIn <= csrToggleReadySync;
          csrToggleErrorIn <= csrToggleErrorSync;
          csrInState <= StInIdle;
        end
      endcase // case (csrInState)
    end
  end

endmodule // oclib_csr_synchronizer
