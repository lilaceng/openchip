
// SPDX-License-Identifier: MPL-2.0

module oclib_uart_rx #(
                       parameter integer ClockHz = 100000000,
                       parameter integer Baud = 115200,
                       parameter integer BaudCycles = (ClockHz / Baud), // i.e. 868
                       parameter integer DebounceCycles = (BaudCycles / 16), // i.e. 54
                       parameter integer FifoDepth = 0
                     )
  (
   input              clock,
   input              reset,
   output logic [7:0] rxData,
   output logic       rxValid,
   input              rxReady,
   input              rx,
   output logic [15:0] error
   );

  /* Design Philosophy

   1) We debounce the input fairly aggressively, delaying transitions by 1/16th of a baud in order
   to filter noise esp during switching.
   ---X---XXX---X_XX-X______X__________XX-X_X-X-------------
   becomes
   ------------------------_________________________--------

   2) When we see a start bit, we delay by half a baud, then restart the baud timer (*).
   -------------------X_________START_________X-----------D0----------X-----D1----
   State:  StIdle      |   StStart  |  StData
   BaudCtr:   0  0   0 / 1 + 0.5B->(*) 0  1 +  +  + 1.0B->(*) 1 +  +  +
   BaudCtrTC:                                              *
   BaudCtrHalfTC:                   *          *                      *
   BitCtr:    0  0   0  0   0  0    0    0   0   0    0   0  /  1   1   1

   3) BitCtr indicates the next bit to be latched.  We stay in data state bit until we
   latch bit 7, then transition to capturing the STOP bit, then transition to idle.
   --------D6---------X___________D7_________X-----------STOP----------XXXXXXXXXXXXXX
   State:  StData                   |  StStop                | StIdle  |? StStart?
   BaudCtr: (*) 0  1 +   + 1.0B->(*) 0 1 +  +  +  + 1.0B->(*) 0 1 +  +  +
   BaudCtrTC:                                              *
   BaudCtrHalfTC:     *                       *                      *
   BitCtr:  /  7    7     7    7   /   0   0     0    0    0     0   0    0   0

   3) We assert rxValid as soon as we've latched D7, and we assert it when we see
   rxReady. We are tolerant with rxReady taking a while, and only consider it an
   error if we need to reassert it.  This enables back-to-back connection to a
   oclib_uart_tx which will take one less baud time to give ready.
   --------D6---------X___________D7_________X-----------STOP----------XXXXXXXXXXXXXX
   State:  StData                   |  StStop                | StIdle  |? StStart?
   BitCtr:  /  7    7     7    7   /   0   0     0    0    0     0   0    0   0
   RxValid _________________________/---------------------------------------\_____ (A)
   RxReady _____________________________________________________________/------\__ (A)
   RxValid -------------\___________/--------------------------------------------- (B)
   RxReady _________/------\______________________________________________________ (B)

   error[0] : runt stop bit
   error[1] : no stop bit
   error[2] : rxReady not seen
   */

  wire                            rxInt;
  oclib_debounce #(DebounceCycles) uINPUT_DEBOUNCE (clock, reset, rx, rxInt);

  localparam BaudCounterW = $clog2(BaudCycles);
  logic [BaudCounterW-1:0] baudCounter;
  logic                    baudCounterTC;
  logic                    baudCounterHalfTC;
  enum                     logic [1:0] { StIdle, StStart, StData, StStop } state;
  logic [2:0]              bitCounter;
  logic [7:0]              shiftData;

  always @(posedge clock) begin
    if (reset) begin
      rxValid <= 1'b0;
      rxData <= '0;
      shiftData <= '0;
      error <= '0;
      baudCounter <= '0;
      bitCounter <= '0;
      baudCounterTC <= 1'b0;
      baudCounterHalfTC <= 1'b0;
      state <= StIdle;
    end
    else begin
      baudCounter <= (baudCounterTC ? '0 : (baudCounter + 'd1));
      baudCounterTC <= (baudCounter == (BaudCycles-2));
      baudCounterHalfTC <= (baudCounter == ((BaudCycles/2)-2));
      rxValid <= (rxValid && ~rxReady);
      case (state)
        StIdle : begin
          bitCounter <= '0;
          baudCounter <= '0;
          if (rxInt == '0) begin
            state <= StStart;
          end
        end
        StStart : begin
          bitCounter <= '0;
          if (baudCounterHalfTC) begin
            baudCounter <= '0;
            state <= StData;
            if (rxInt != 1'b0) begin
              error[0] <= 1'b1;
            end
          end
        end
        StData : begin
          if (baudCounterTC) begin
            shiftData <= { rxInt, shiftData[7:1] };
            bitCounter <= (bitCounter + 'd1);
            if (bitCounter == 'd7) begin
              state <= StStop;
              rxValid <= 1'b1;
              rxData <= { rxInt, shiftData[7:1] };
              if (rxValid) begin
                error[2] <= 1'b1;
              end
            end
          end
        end
        StStop : begin
          if (baudCounterTC) begin
            if (rxInt != 1'b1) begin
              error[1] <= 1'b1;
            end
            state <= StIdle;
          end
        end
      endcase // case (state)
    end
  end

endmodule // oclib_uart_rx
