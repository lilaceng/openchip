
// SPDX-License-Identifier: MPL-2.0

module oclib_abc_to_bc #(
                       parameter ResetSync = 0
                       )
  (
   input  clock,
   input  reset,
   input  oclib_pkg::abc_s abc,
   output oclib_pkg::abc_fb_s abcFb,
   output oclib_pkg::bc_s bc,
   input  oclib_pkg::bc_fb_s bcFb
   );

  // optional reset synchronizer
  logic                            resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  // synchronize the incoming async signals
  oclib_pkg::abc_s abcSync;
  oclib_synchronizer #($bits(abc)) uABC_SYNC (clock, abc, abcSync);

  always_ff @(posedge clock) begin
    if (resetQ) begin
      bc.valid <= 1'b0;
      bc.data <= '0;
      abcFb.ack <= 1'b0;
    end
    else begin
      bc.valid <= (bc.valid && !bcFb.ready);
      abcFb.ack <= (abcFb.ack ? abcSync.req : // if ACK already 1, lower when REQ lowers
                    (abcSync.req && ~bc.valid)); // else, raise if REQ=1 and VALID=0 (i.e. not busy)
      if (abcFb.ack && !abcSync.req) begin
        bc.data <= abcSync.data;
        bc.valid <= 1'b1;
      end
    end
  end

endmodule // oclib_abc_to_bc
