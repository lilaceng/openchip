
// SPDX-License-Identifier: MPL-2.0

`ifndef __OCLIB_VH
  `define __OCLIB_VH

  `define ANNOUNCE_MODULE(m) $display("%m: ANNOUNCE module %-20s", `STRINGIGY(m));
  `define ANNOUNCE_PARAM_INTEGER(p) $display("%m: ANNOUNCE param %-20s = %0d", `STRINGIGY(p), p);
  `define ANNOUNCE_PARAM_BIT(p) $display("%m: ANNOUNCE param %-20s = %x", `STRINGIGY(p), p);
  `define ANNOUNCE_PARAM_REAL(p) $display("%m: ANNOUNCE param %-20s = %.3f", `STRINGIGY(p), p);


`endif
