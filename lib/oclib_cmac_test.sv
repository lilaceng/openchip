
// SPDX-License-Identifier: MPL-2.0

module oclib_cmac_test #(parameter integer Instance = 0,
                         parameter integer ResetSync = 1)
  (
   input  clock,
   input  reset,
   input  clockCmac,
   input  oclib_pkg::abbc_s abbcIn,
   output oclib_pkg::abbc_s abbcOut,
   output oclib_pkg::axi4st_512_s axiCmacTx,
   input  oclib_pkg::axi4st_512_fb_s axiCmacTxFb,
   input  oclib_pkg::axi4st_512_s axiCmacRx,
   output oclib_pkg::axi4st_512_fb_s axiCmacRxFb
   );

  // **************
  // Reset / CSR
  // **************

  logic                         resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  logic                         resetCmac;
  oclib_synchronizer #(.Width(1)) uRESETCMAC_SYNC (.clock(clockCmac), .in(reset), .out(resetCmac));

  logic                         csrSelect;
  oclib_pkg::csr_s              csr;
  oclib_pkg::csr_fb_s           csrFb;

  oclib_abbc_to_csr #(.AddressSpaces(1))
  uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);


  oclib_pkg::csr_s              csrCmac;
  oclib_pkg::csr_fb_s           csrCmacFb;
  logic                         csrSelectCmac;

  oclib_csr_synchronizer #(.CsrSelectBits(1))
  uCSR_SYNC (.clockIn(clock), .resetIn(resetQ),
             .csrIn(csr), .csrInFb(csrFb), .csrSelectIn(csrSelect),
             .clockOut(clockCmac), .resetOut(resetCmac),
             .csrOut(csrCmac), .csrOutFb(csrCmacFb), .csrSelectOut(csrSelectCmac));

  localparam integer NumCsr = 21;
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  oclib_csr_array #(.NumCsr(NumCsr),
                    .CsrRwBits   ({ {3{32'h00000000}}, {16{32'hffffffff}}, 32'hffffffff, 32'hffff000f }),
                    .CsrRoBits   ({ {3{32'hffffffff}}, {16{32'h00000000}}, 32'h00000000, 32'h00000100 }))
  uCSR (clock, resetQ, csrSelectCmac, csrCmac, csrCmacFb, csrConfig, csrStatus);

  logic         genReset;
  logic         genEnable;
  logic [15:0]  genBytes;
  logic [31:0]  genIpg;
  logic [15:0] [31:0] genFirstWord;
  logic [31:0]  genPacketCount;

  assign genReset = csrConfig[0][0];
  assign genEnable = csrConfig[0][1];
  assign genBytes = csrConfig[0][31:16];
  assign genIpg = csrConfig[1][31:0];
  for (genvar i=0; i<16; i++) begin
    assign genFirstWord[i] = csrConfig[2+i][31:0];
  end
  assign csrStatus[18] = genPacketCount;

  logic        chkReset;
  logic        chkEnable;
  logic        chkErrorSeen;
  logic [31:0] chkPacketCount;
  logic [31:0] chkByteCount;

  assign chkReset = csrConfig[0][2];
  assign chkEnable = csrConfig[0][3];
  assign csrStatus[19] = chkPacketCount;
  assign csrStatus[20] = chkByteCount;
  assign csrStatus[0][8] = chkErrorSeen;

  // **************
  // Packet Check
  // **************

  logic         resetChk;
  oclib_pkg::axi4st_512_s axiCmacRxQ;
  logic         axiCmacRxValidQQ;
  logic         axiCmacRxLastQQ;
  logic         axiCmacRxErrorQQ;
  logic [6:0]   axiCmacRxBytesQ;
  logic [6:0]   axiCmacRxBytesQQ;

  always_comb begin
    axiCmacRxBytesQ = '0;
    for (int i=0; i<64; i++) begin
      axiCmacRxBytesQ = (axiCmacRxBytesQ + axiCmacRxQ.tkeep[i]);
    end
  end

  always_ff @(posedge clockCmac) begin
    if (resetChk) begin
      chkPacketCount <= '0;
      chkByteCount <= '0;
      chkErrorSeen <= 1'b0;
    end
    else begin
      chkPacketCount <= (chkPacketCount + ((axiCmacRxValidQQ && axiCmacRxLastQQ) ? 1 : 0));
      chkByteCount <= (chkByteCount + (axiCmacRxValidQQ ? axiCmacRxBytesQQ : 0));
      chkErrorSeen <= (chkErrorSeen || (axiCmacRxValidQQ && axiCmacRxErrorQQ));
    end
    resetChk <= (resetCmac || axiCmacRx.reset || chkReset); // top reset || CMAC TX side reset || CSR soft reset
    axiCmacRxQ <= axiCmacRx;
    axiCmacRxValidQQ <= (axiCmacRxQ.tvalid && chkEnable);
    axiCmacRxLastQQ <= axiCmacRxQ.tlast;
    axiCmacRxErrorQQ <= axiCmacRxQ.tuser;
    axiCmacRxBytesQQ <= axiCmacRxBytesQ;
  end

  assign axiCmacRxFb.tready = 1'b1;

  // **************
  // Packet Gen
  // **************

  logic [511:0] txFifoWriteData;
  logic         txFifoWriteLast;
  logic [63:0]  txFifoWriteKeep;
  logic         txFifoWriteValid;
  logic         txFifoWriteReady;
  logic         txFifoAlmostFull;

  logic         resetGen;
  logic [31:0]  genCountIpg;
  logic [15:0]  genRemaining;
  logic         genFirst;
  logic         genLast;
  logic [5:0]   genEmpty;
  logic         genRunning;
  logic         txFifoNotFull;

  always_ff @(posedge clockCmac) begin
    if (resetGen) begin
      txFifoWriteValid <= 1'b0;
      genCountIpg <= '0;
      genRemaining <= '0;
      genFirst <= 1'b0;
      genLast <= 1'b0;
      genEmpty <= '0;
      genRunning <= 1'b0;
      genPacketCount <= '0;
    end
    else begin
      if (genCountIpg) genCountIpg <= (genCountIpg - 1);
      if (genEnable && (genCountIpg == '0) && (!genRunning || genLast) && txFifoNotFull) begin
        // we are writing first word of packet
        txFifoWriteValid <= 1'b1;
        genCountIpg <= genIpg;
        genRemaining <= ((genBytes <= 64) ? '0 : (genBytes - 64));
        genFirst <= 1'b1;
        genLast <= (genBytes <= 64);
        genEmpty <= ((genBytes <= 64) ? (64 - genBytes) : '0);
        genRunning <= 1'b1;
        genPacketCount <= (genPacketCount + 1);
      end
      else if (genRunning && !genLast && txFifoNotFull) begin
        // we are writing non-first-word
        txFifoWriteValid <= 1'b1;
        genRemaining <= ((genRemaining <= 64) ? '0 : (genRemaining - 64));
        genFirst <= 1'b0;
        genLast <= (genRemaining <= 64);
        genEmpty <= ((genRemaining <= 64) ? (64 - genRemaining) : '0);
        genRunning <= 1'b1;
      end
      else begin
        // we aren't writing for one of many reasons
        txFifoWriteValid <= 1'b0;
        // we have finished
        if (genRunning && genLast) begin
          genRunning <= 1'b0;
        end
      end
    end
    resetGen <= (resetCmac || axiCmacTxFb.reset || genReset); // top reset || CMAC TX side reset || CSR soft reset
    txFifoNotFull <= !txFifoAlmostFull;
  end

  logic [7:0] [63:0] genRandom;
  for (genvar i=0; i<8; i++) begin
    oclib_random #(.Seed(32'h55555555 + Instance + (i << 8)), .OutWidth(64))
    uGEN_RANDOM (.clock(clockCmac), .reset(resetCmac), .out(genRandom[i]));
  end

  always_comb begin
    txFifoWriteData = (genFirst ? genFirstWord : genRandom);
    txFifoWriteKeep = ({64{1'b1}} >> genEmpty);
    txFifoWriteLast = genLast;
  end

  // output FIFO

  logic [511:0] txFifoReadData;
  logic         txFifoReadLast;
  logic [63:0]  txFifoReadKeep;
  logic         txFifoReadValid;
  logic         txFifoReadReady;

  oclib_fifo #(.Width(512+1+64), .Depth(32), .AlmostFull(16))
  uTX_FIFO (.clock(clockCmac), .reset(resetGen), .almostFull(txFifoAlmostFull),
            .inData({txFifoWriteLast, txFifoWriteKeep, txFifoWriteData}),
            .inValid(txFifoWriteValid), .inReady(txFifoWriteReady),
            .outData({txFifoReadLast, txFifoReadKeep, txFifoReadData}),
            .outValid(txFifoReadValid), .outReady(txFifoReadReady));

  assign axiCmacTx.tvalid = txFifoReadValid;
  assign axiCmacTx.tdata = txFifoReadData;
  assign axiCmacTx.tlast = txFifoReadLast;
  assign axiCmacTx.tkeep = txFifoReadKeep;
  assign axiCmacTx.tuser = 1'b0;
  assign axiCmacTx.reset = 1'b0;
  assign txFifoReadReady = axiCmacTxFb.tready;

endmodule
