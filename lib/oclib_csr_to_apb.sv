
// SPDX-License-Identifier: MPL-2.0

module oclib_csr_to_apb #(
                          parameter DataW = 32,
                          parameter AddressW = 16,
                          parameter ApbSlaveError = 1,
                          parameter ResetSync = 0
                          )
  (
   input  clock,
   input  reset,
   input  csrSelect,
   input  oclib_pkg::csr_s csr,
   output oclib_pkg::csr_fb_s csrFb,
   output oclib_pkg::apb_s apb,
   input  oclib_pkg::apb_fb_s apbFb
   );

  logic          resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  enum           logic [2:0] { StIdle, StWrite1, StWrite2, StRead1, StRead2, StDone } state;

  always_ff @(posedge clock) begin
    if (resetQ) begin
      apb <= '0;
      csrFb <= '0;
      state <= StIdle;
    end
    else begin
      case (state)
        StIdle : begin
          if (csr.write && csrSelect) begin
            apb.select <= 1'b1;
            apb.write <= 1'b1;
            apb.address <= csr.address[AddressW-1:0];
            apb.wdata <= csr.wdata[DataW-1:0];
            state <= StWrite1;
          end
          else if (csr.read && csrSelect) begin
            apb.select <= 1'b1;
            apb.write <= 1'b0;
            apb.address <= csr.address[AddressW-1:0];
            state <= StRead1;
          end
        end
        StWrite1 : begin
          apb.enable <= 1'b1;
          state <= StWrite2;
        end
        StWrite2 : begin
          if (apbFb.ready) begin
            csrFb.ready <= 1'b1;
            csrFb.error <= apbFb.error;
            apb.select <= 1'b0;
            apb.enable <= 1'b0;
            apb.write <= 1'b0;
            state <= StDone;
          end
        end
        StRead1 : begin
          apb.enable <= 1'b1;
          state <= StRead2;
        end
        StRead2 : begin
          if (apbFb.ready) begin
            csrFb.ready <= 1'b1;
            csrFb.error <= apbFb.error;
            csrFb.rdata <= apbFb.rdata;
            apb.select <= 1'b0;
            apb.enable <= 1'b0;
            state <= StDone;
          end
        end
        StDone : begin
          csrFb.ready <= 1'b0;
          if ((!csr.read) && (!csr.write)) begin
            state <= StIdle;
          end
        end
      endcase
    end
  end

endmodule // oclib_csr_to_apb
