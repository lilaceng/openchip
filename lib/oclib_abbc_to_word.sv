
// SPDX-License-Identifier: MPL-2.0

module oclib_abbc_to_word #(
                          parameter WordFromAbbcW = 64,
                          parameter WordToAbbcW = 64,
                          parameter ResetSync = 0
                          )
  (
   input                            clock,
   input                            reset,
   input                            oclib_pkg::abbc_s abbcIn,
   output                           oclib_pkg::abbc_s abbcOut,
   output logic [WordFromAbbcW-1:0] wordFromAbbcData,
   output logic                     wordFromAbbcValid,
   input                            wordFromAbbcReady,
   input [WordToAbbcW-1:0]          wordToAbbcData,
   input                            wordToAbbcValid,
   output logic                     wordToAbbcReady
   );

  logic                        resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  oclib_pkg::abc_s abcFromAbbc;
  oclib_pkg::abc_fb_s abcFromAbbcFb;
  oclib_pkg::abc_s abcToAbbc;
  oclib_pkg::abc_fb_s abcToAbbcFb;

  oclib_abbc_to_abc uABBC_TO_ABC (.abbcIn(abbcIn), .abbcOut(abbcOut),
                                  .abcFromAbbc(abcFromAbbc), .abcFromAbbcFb(abcFromAbbcFb),
                                  .abcToAbbc(abcToAbbc), .abcToAbbcFb(abcToAbbcFb));

  oclib_abc_to_word #(.WordW(WordFromAbbcW))
  uABC_TO_WORD (.clock(clock), .reset(resetQ),
                .abc(abcFromAbbc), .abcFb(abcFromAbbcFb),
                .wordData(wordFromAbbcData), .wordValid(wordFromAbbcValid), .wordReady(wordFromAbbcReady));

  oclib_word_to_abc #(.WordW(WordToAbbcW))
  uWORD_TO_ABC (.clock(clock), .reset(resetQ),
                .wordData(wordToAbbcData), .wordValid(wordToAbbcValid), .wordReady(wordToAbbcReady),
                .abc(abcToAbbc), .abcFb(abcToAbbcFb));

endmodule // oclib_abbc_to_word
