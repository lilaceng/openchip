
// SPDX-License-Identifier: MPL-2.0

//`define OCLIB_WAVE_PIPELINE_SWITCH_CORE
//`define OCLIB_SWITCH_CORE_FLOP_STRIDE_X 4
//`define OCLIB_SWITCH_CORE_FLOP_STRIDE_Y 4

module oclib_switch_core #(
                           parameter integer  Ports = 2,
                           parameter integer  InputPorts = Ports,
                           parameter integer  OutputPorts = Ports,
                           parameter integer  PortsPerSlice = 2,
                           parameter integer  DataWidth = 32,
                           localparam integer SwitchPorts = ((InputPorts >= OutputPorts) ?
                                              InputPorts : OutputPorts),
                           localparam integer Stages = ((SwitchPorts <= PortsPerSlice) ? 1 :
                                              (SwitchPorts <= (PortsPerSlice**2)) ? 2 :
                                              (SwitchPorts <= (PortsPerSlice**3)) ? 3 :
                                              (SwitchPorts <= (PortsPerSlice**4)) ? 4 :
                                              (SwitchPorts <= (PortsPerSlice**5)) ? 5 :
                                              (SwitchPorts <= (PortsPerSlice**6)) ? 6 :
                                              (SwitchPorts <= (PortsPerSlice**7)) ? 7 :
                                              (SwitchPorts <= (PortsPerSlice**8)) ? 8 :
                                              -1),
                           localparam integer SwitchPortW = $clog2(SwitchPorts),
                           parameter logic    OutputFifo = 1,
                           parameter integer  OutputFifoDepth = 8,
                           parameter integer  FlopStrideX = `OC_FROM_DEFINE_ELSE(OCLIB_SWITCH_CORE_FLOP_STRIDE_X, 1),
                           parameter integer  FlopStrideY = `OC_FROM_DEFINE_ELSE(OCLIB_SWITCH_CORE_FLOP_STRIDE_Y, 1)
                           )
  (
   input                                            clock,
   input                                            reset,
   input [InputPorts-1:0] [DataWidth-1:0]           inData,
   input [InputPorts-1:0] [SwitchPortW-1:0]         inPort,
   input [InputPorts-1:0]                           inValid,
   output logic [InputPorts-1:0]                    inReady,
   output logic [OutputPorts-1:0] [DataWidth-1:0]   outData,
   output logic [OutputPorts-1:0] [SwitchPortW-1:0] outPort,
   output logic [OutputPorts-1:0]                   outValid,
   input [OutputPorts-1:0]                          outReady
   );

`ifdef OCLIB_PERFECT_SWITCH_CORE

  /*
   When OCLIB_PERFECT_SWITCH_CORE is defined, the oclib_switch_core implementation is a behavioral (simulation-only)
   ideal ("perfect") switch.  It maintains output FIFOs (which will be 64 deep, unless OutputFifoDepth param is
   larger, in which case it will use the larger).  Inputs are accepted (inReady) when they are requesting an output
   that isn't full.

   The fundamental difference between this switch and a "real" switch is that there is infinite bandwidth INTO each
   output FIFO.  There is no blocking unless that output FIFO becomes full.  For example, 32 input ports could all
   request the same output, and all can be granted in the same cycle.  This is not practical in a real switch.

   This mode is included for development (faster simulation) as well as determining whether the switch is a bottleneck.
   If your design performs a lot better in this mode, then clearly the "real" implementation is a bottleneck.
   */

  localparam PerfectOutputFifoDepth = ((OutputFifoDepth > 64) ? OutputFifoDepth : 64);

  // per output queues

  logic [DataWidth-1:0]            dataQ [OutputPorts] [$];
  logic [SwitchPortW-1:0]          portQ [OutputPorts] [$];

  for (genvar i=0; i<InputPorts; i++) begin
    // generate ready if we are requesting a queue that isn't full
    assign inReady[i] = (inValid[i] && (dataQ[inPort[i]].size() < OutputFifoDepth));
    // if ready&&valid, push entry into output queue
    always_ff @(posedge clock) begin
      if (inValid[i] && inReady[i]) begin
        dataQ[inPort[i]].push_back(inData[i]);
        portQ[inPort[i]].push_back(i);
      end
    end
  end

  for (genvar o=0; o<OutputPorts; o++) begin
    always_ff @(posedge clock) begin
      if (reset) begin
        dataQ[o].delete();
        portQ[o].delete();
        outValid[o] <= 1'b0;
      end
      else begin
        if (dataQ[o].size() && !(outValid[o] && !outReady[o])) begin
          outValid[o] <= 1'b1;
          outData[o] <= dataQ[o].pop_front();
          outPort[o] <= portQ[o].pop_front();
        end
        else if (outValid[o] && outReady[o]) begin
          outValid[o] <= 1'b0;
        end
      end
    end
  end

`elsif OCLIB_WAVE_PIPELINE_SWITCH_CORE

  /*

   When OCLIB_WAVE_PIPELINE_SWITCH_CORE is defined, the oclib_switch_core implementation is the wave-pipelined switch.
   The philosophy of this switch is to achieve high scale, high frequency, and minimal area.  It acheives this by avoiding
   long timing paths.  All decisions are made "locally", which in this case means "pertaining to one input and/or output".

   ---- S0 ----

   S0 is a retiming stage, to decouple the timing of the fabric from the logic feeding it. S0 presents a direct flopped
   set of signals to S1.

   TODO: options for skipping the input decoupling and/or increasing it (making it a deeper FIFO).

   ---- S1 ----

   S1 is the grant pipeline.  This is where the arbitration decisions are made.  At this stage, each input port is requesting
   a single output port.  A decision is made whether to grant the input access to it's requested output.  This is done via
   looking at a per-output vector indicating whether the output is blocked.  If an input is granted access to the output, the
   blocked vector bit associated with that output is set.  The blocked vector is then flopped and passed down to the next input.
   The nice thing is that each decision is made using local information (request info is flopped from S0, and blocked info is
   flopped from the previous input in S1).
   The not-so-nice thing is that the arbitration is strict priority (lower input ports always get priority).  In some use cases
   like network switches, this will be undesireable.  However in many use cases, like computation workloads, the system will tend
   to "self-balance".  During congestion, lower ports will "get ahead" but then will run out of work to do, allowing the upper
   ports to "catch up".  Without congestion, say each input going 1:1 to an associated output, there is no blocking and 100%
   utilization will be achieved.
   The last thing to keep in mind is that the decisions are skewed in time.  For example if input 0 is granted output Z on
   cycle 0, input N will see blocked[Z] assert on cycle N, at which time port 0 has already fed the data into the S2 matrix.

   In S1, each input cell "i" looks like:
                                            vv one hot bitmap
                                            blockS1[i-1][o]  (or zeros for i==0)
                                                 |
                                                 |-- NOT-\
                                                 |       |
   portS0[i] --demux-> requestS1_d[i][o] -------------- AND --> grantS1_d[i][o] ---FLOP--> grantS1[i][o]
   ^^^ binary          ^^^ one hot bitmap   |    |              ^^^ one hot bitmap
                                            \---OR
                                                 |
   dataS0[i] ----------------------------------------------------------------------FLOP--> dataS1[i]
                                                 |
                                                FLOP
                                                 |
                                                 v
                                            blockS1[i][o]

   The complete S1 has one of the above for each input, hence looks like:

                         zero
                           |
                  /-----------------\
   portS0[0] -->  |----see above----|  --> grantS1[0][n:0]   (where n is highest output port)
   dataS0[0] -->  |                 |  --> dataS1[0]
                  \-----------------/
                           |
                  /-----------------\
   portS0[1] -->  |----see above----|  --> grantS1[1][n:0]
   dataS0[1] -->  |                 |  --> dataS1[1]
                  \-----------------/
                           |
                       ...etc...

   ---- S2 ----

   S2 is the pipelined crossbar fabric.  It's job is to get the data from the input port to the output port, given a
   vector per {input port, output port} indicating that a particular input has been granted an output.  This is done
   with a matrix of cells, the height being the number of inputs, the width being the number of outputs.

   In S2, each matrix cell "i,j" looks like:

                        outValidS2[i-1][o]   outDataS2[i-1][o]  (or zeros for i=0)
                                  |               |
   grantInS2[i][o-1][n:0]-----+---------------+-----------------------FLOP--> grantS2[i][o+1][n:0]
   ^^^ one hot                |   |           |   |
   dataInS2[i][o-1]---------------------------------------------------FLOP--> dataS2[i][o+1]
   (grant/dataS1 if o=0)      |   |           |   |
                              \--OR           \--AND
                                  |               |
                                 FLOP            FLOP
                                  |               |
                          outValidS2[i][o]    outDataS2[i][o]

   The complete S2 has one of the above for each input, hence looks like:

                            zero   zero              zero   zero
                              |     |                  |     |
                        /-----------------\      /-----------------\
   grantS1[0][n:0] -->  |----see above----|  --> |----see above----|  --> ... etc ...
   dataS1[0]       -->  |                 |  --> |                 |  -->
                        \-----------------/      \-----------------/
                              |     |                  |     | <- outDataS2[0][1]
                        /-----------------\      /-----------------\
   grantS1[0][n:0] -->  |----see above----|  --> |----see above----|  --> ... etc ...
   dataS1[0]       -->  |                 |  --> |                 |  -->
                        \-----------------/      \-----------------/
                              |     |                  |     |
                             ...etc...                ...etc...

   Some interesting things to note:
   - lower input ports have higher latency through the fabric
   - higher output ports have higher latency through the fabric
   - as data flows "down", it stays in sync with the pipelined arbitration done in S1, for example, input[1] in S1
     sees "blocked" from input[0] with one cycle delay, and likewise, it sees the data in S2 with one cycle delay,
     so they are aligned
   - multiple data from the same input may exit the fabric at the same time, for example, if input[0] sends data to
     outputs 3, 2, 1, 0 (in that order), they will all exit the fabric on the same cycle, due to the additional
     latency of the higher numbered ports
   - an ideal floorplan would have inputs on one axis (say Y axis), and outputs on another (say X axis), in which
     case the floorplan matches this diagram.  It's common however for inputs to be "at the top" and outputs "at the
     bottom" in which case the ideal floorplan is basically 45 degree rotated (OR, of course, the inputs or outputs
     could be  pipelined to conform to the ideal switch floorplan)

   Some ideas to improve timing:
   - obviously reducing logic isn't going to help much, as there's almost no logic
   - maybe adding some retiming stages to lower numbered inputs (and/or outputs) would help balance things.  In a
     16x16 switch, there are ~32 flops on the worst case datapath, but ~1 flop on the best case.  This could cause
     floorplan trauma as it tries to stretch input[0]->output[0] path to actually get from input logic to output logic?
   - maybe there's too many unnecessary flops... none of the flops are required for functionality, they are all just
     there to ease timing, but at some point a large number of flops becomes it's own timing headache.  Approaches
     could include flopping for every 2 or 4 rows (and/or columns).  Just need to take care to match the latencies
     of the S1 and S2 paths.

   */

  // s0: retime inputs

  logic [InputPorts-1:0]                            resetS0;
  logic [InputPorts-1:0] [DataWidth-1:0]            dataS0;
  logic [InputPorts-1:0] [SwitchPortW-1:0]          portS0;
  logic [InputPorts-1:0]                            validS0;
  logic [InputPorts-1:0]                            readyS0;

  logic                                             resetIn;
  oclib_pipeline #(.Width(1), .Length(3), .DontTouch(1), .NoShiftRegister(1))
  uRESETIN (.clock(clock), .in(reset), .out(resetIn));

  for (genvar i=0; i<InputPorts; i++) begin : s0

    always_ff @(posedge clock) resetS0[i] <= ((i==0) ? resetIn : resetS0[i-1]);

    oclib_ready_valid_retime #(.Width(DataWidth+SwitchPortW))
    uINPIPE (.clock(clock), .reset(reset),
             .inData({inPort[i], inData[i]}), .inValid(inValid[i]), .inReady(inReady[i]),
             .outData({portS0[i], dataS0[i]}), .outValid(validS0[i]), .outReady(readyS0[i]) );
  end

  logic                                             phaseS0;
  always_ff @(posedge clock) phaseS0 <= (resetIn ? 1'b0 : ~phaseS0);

  logic [InputPorts-1:0]                            validS00;

`ifdef SIMULATION_XXX
  logic [InputPorts-1:0]                            mask;
  integer                                           maxMaskPercent;
  integer                                           lastMaskPort;

  initial begin
    maxMaskPercent = 50;
    lastMaskPort = 16;
  end
  for (genvar i=0; i<InputPorts; i++) begin
    always_ff @(posedge clock) begin
      mask[i] <= ((i > lastMaskPort) ? 1'b0 : `OC_RAND_PERCENT((maxMaskPercent * (lastMaskPort-i)) / 16));
    end
  end

  assign validS00 = (validS0 & ~mask);
`else
  assign validS00 = validS0;
`endif

  // s1: grant pipeline

  logic [InputPorts-1:0] [DataWidth-1:0]            dataS1;
  logic [InputPorts-1:0] [OutputPorts-1:0]          requestS1_d;
  logic [InputPorts-1:0] [OutputPorts-1:0]          grantS1;
  logic [InputPorts-1:0] [OutputPorts-1:0]          grantS1_d;
  logic [InputPorts-1:0] [OutputPorts-1:0]          blockS1;
  logic [InputPorts-1:0] [OutputPorts-1:0]          blockS1_d;
  logic [OutputPorts-1:0]                           outFifoAlmostFull;

  for (genvar i=0; i<InputPorts; i++) begin

    always_ff @(posedge clock) begin
      dataS1[i] <= dataS0[i];
      grantS1[i] <= grantS1_d[i];
    end

    if ((i%FlopStrideY)==0) begin
      always_ff @(posedge clock) begin
        blockS1[i] <= blockS1_d[i];
      end
    end
    else begin
      always_comb begin
        blockS1[i] = blockS1_d[i];
      end
    end

    for (genvar o=0; o<OutputPorts; o++) begin
      assign requestS1_d[i][o] = (validS00[i] && (portS0[i] == o) && (!outFifoAlmostFull[o]));
      assign grantS1_d[i][o] = (requestS1_d[i][o] && ((i==0) ? 1'b1 : (!blockS1[i-1][o])));
      assign blockS1_d[i][o] = (requestS1_d[i][o] || ((i==0) ? 1'b0 : blockS1[i-1][o]));
    end

    assign readyS0[i] = |grantS1_d[i]; // accept the S0 data if we grant this input to any output

  end

  // s2: wave pipelined crossbar fabric

  // these are just some wires to make the flop calculations simpler
  logic [InputPorts-1:0] [OutputPorts-1:0] [DataWidth-1:0]   dataInS2;
  logic [InputPorts-1:0] [OutputPorts-1:0] [OutputPorts-1:0] grantInS2;
  // these are actual flops at each input*output cell
  logic [InputPorts-1:0] [OutputPorts-1:0] [DataWidth-1:0]   dataS2;
  logic [InputPorts-1:0] [OutputPorts-1:0] [OutputPorts-1:0] grantS2;
  logic [InputPorts-1:0] [OutputPorts-1:0] [DataWidth-1:0]   outDataS2;
  logic [InputPorts-1:0] [OutputPorts-1:0] [SwitchPortW-1:0] outPortS2;
  logic [InputPorts-1:0] [OutputPorts-1:0]                   outValidS2;

  for (genvar i=0; i<InputPorts; i++) begin
    for (genvar o=0; o<OutputPorts; o++) begin
      assign dataInS2[i][o] = ((o==0) ? dataS1[i] : dataS2[i][o-1]);
      assign grantInS2[i][o] = ((o==0) ? grantS1[i] : grantS2[i][o-1]);
      if ((o%FlopStrideX)==0) begin
        always_ff @(posedge clock) begin
          dataS2[i][o] <= dataInS2[i][o];
          grantS2[i][o] <= {1'b0,grantInS2[i][o][OutputPorts-1:1]};
        end
      end
      else begin
        always_comb begin
          dataS2[i][o] = dataInS2[i][o];
          grantS2[i][o] = {1'b0,grantInS2[i][o][OutputPorts-1:1]};
        end
      end
      if ((i%FlopStrideY)==0) begin
        always_ff @(posedge clock) begin
          outDataS2[i][o] <= ((grantInS2[i][o][0] ? (dataInS2[i][o]) : '0) | // we've been told to insert our data
                              ((i==0) ? '0 : outDataS2[i-1][o])); // input above us in this column has priority
          outPortS2[i][o] <= ((grantInS2[i][o][0] ? i : '0) |
                              ((i==0) ? '0 : outPortS2[i-1][o]));
          outValidS2[i][o] <= (grantInS2[i][o][0] |
                               ((i==0) ? '0 : outValidS2[i-1][o]));
        end
      end
      else begin
        always_comb begin
          outDataS2[i][o] = ((grantInS2[i][o][0] ? (dataInS2[i][o]) : '0) | // we've been told to insert our data
                             ((i==0) ? '0 : outDataS2[i-1][o])); // input above us in this column has priority
          outPortS2[i][o] = ((grantInS2[i][o][0] ? i : '0) |
                             ((i==0) ? '0 : outPortS2[i-1][o]));
          outValidS2[i][o] = (grantInS2[i][o][0] |
                              ((i==0) ? '0 : outValidS2[i-1][o]));
        end
      end
    end
  end

  // s3 : optional output FIFO
  if (OutputFifo) begin : s3

    logic                                             resetOut;
    oclib_pipeline #(.Width(1), .Length(3), .DontTouch(1), .NoShiftRegister(1))
    uRESETOUT (.clock(clock), .in(reset), .out(resetOut));

    logic [OutputPorts-1:0]                           resetS3;
    logic [OutputPorts-1:0]                           outFifoInReady;
    logic [OutputPorts-1:0]                           outFifoAlmostFullRaw;
    for (genvar o=0; o<OutputPorts; o++) begin
      always_ff @(posedge clock) resetS3[o] <= ((o==0) ? resetOut : resetS3[o-1]);
      oclib_fifo #(.Width(DataWidth + SwitchPortW), .Depth(64), .AlmostFull(64-InputPorts-3))
      uOUTFIFO (.clock(clock), .reset(resetS3[o]), .almostFull(outFifoAlmostFullRaw[o]),
                .inData({outDataS2[InputPorts-1][o], outPortS2[InputPorts-1][o]}),
                .inValid(outValidS2[InputPorts-1][o]), .inReady(outFifoInReady[o]),
                .outData({outData[o], outPort[o]}), .outValid(outValid[o]), .outReady(outReady[o]));
      always_ff @(posedge clock) `OC_ASSERT(outFifoInReady[o]);
    end

    oclib_pipeline #(.Width(OutputPorts), .Length(4), .DontTouch(1), .NoShiftRegister(1))
    uAFULL (.clock(clock), .in(outFifoAlmostFullRaw), .out(outFifoAlmostFull));

  end
  else begin
    // when not including output FIFO, we expect all outputs to be ready always
    always_ff @(posedge clock) `OC_ASSERT(outReady == {OutputPorts{1'b1}});
    for (genvar o=0; o<OutputPorts; o++) begin
      assign outData[o] = outDataS2[InputPorts-1][o];
      assign outPort[o] = outPortS2[InputPorts-1][o];
      assign outValid[o] = outValidS2[InputPorts-1][o];
      assign outFifoAlmostFull[o] = 1'b0;
    end
  end

`else // !`elsif OCLIB_WAVE_PIPELINE_SWITCH_CORE

  localparam integer                                 Slices = ($clog2(PortsPerSlice) << (Stages-1));
  localparam integer                                 CellPorts = (Slices * PortsPerSlice);

  // check that everything fits properly
  `OC_STATIC_ASSERT(CellPorts>=InputPorts);
  `OC_STATIC_ASSERT(SwitchPorts>=InputPorts);
  `OC_STATIC_ASSERT(CellPorts>=OutputPorts);
  `OC_STATIC_ASSERT(SwitchPorts>=OutputPorts);

  logic [Stages-1:0] [CellPorts-1:0] [DataWidth-1:0]                           cellInData;
  logic [Stages-1:0] [CellPorts-1:0] [SwitchPortW-1:0]                         cellInPort;
  logic [Stages-1:0] [CellPorts-1:0]                                           cellInValid;
  logic [Stages-1:0] [CellPorts-1:0]                                           cellInReady;
  logic [Stages-1:0] [CellPorts-1:0] [DataWidth-1:0]                           cellOutData;
  logic [Stages-1:0] [CellPorts-1:0] [SwitchPortW-1:0]                         cellOutPort;
  logic [Stages-1:0] [CellPorts-1:0]                                           cellOutValid;
  logic [Stages-1:0] [CellPorts-1:0]                                           cellOutReady;

  for (genvar s=0; s<Stages; s++) begin : stage

    localparam SlicePortW = $clog2(PortsPerSlice);
    localparam PortLsb = ((Stages-s-1) * SlicePortW);
    localparam PortMsb = (PortLsb + SlicePortW - 1);

    for (genvar sl=0; sl<Slices; sl++) begin : slice

      localparam SliceFirstPort = (sl*PortsPerSlice);
      localparam SliceLastPort = (((sl+1)*PortsPerSlice)-1);

      logic [PortsPerSlice-1:0] [PortsPerSlice-1:0] [DataWidth-1:0]              arbInData;
      logic [PortsPerSlice-1:0] [PortsPerSlice-1:0] [SwitchPortW-1:0]            arbInPort;
      logic [PortsPerSlice-1:0] [PortsPerSlice-1:0]                              arbInValid;
      logic [PortsPerSlice-1:0] [PortsPerSlice-1:0]                              arbInReady;
      logic [PortsPerSlice-1:0] [PortsPerSlice-1:0] [SwitchPortW+DataWidth-1:0]  arbInCombinedData;
      logic [PortsPerSlice-1:0] [DataWidth-1:0]                                  arbOutData;
      logic [PortsPerSlice-1:0] [SwitchPortW-1:0]                                arbOutPort;
      logic [PortsPerSlice-1:0]                                                  arbOutValid;
      logic [PortsPerSlice-1:0]                                                  arbOutReady;

      for (genvar sp=0; sp<PortsPerSlice; sp++) begin : port

        // get from cellOut to cellIn
        localparam p = (SliceFirstPort + sp);
        localparam StagePortMask = ((1<<((s+1)*SlicePortW))-1); // 0011
        localparam ip = ((p & ~StagePortMask) | // the MSBs stage the same, i.e. we stay within the stage's port group
                         ((p & StagePortMask) >> SlicePortW) ^ (p[SlicePortW-1:0] << (s*SlicePortW)));

        always_comb begin
          if (s == 0) begin
            // connect up to block input ports if this is the first stage
            if (p < InputPorts) begin
              cellInData[s][p] = inData[p];
              cellInValid[s][p] = inValid[p];
              cellInPort[s][p] = inPort[p];
              inReady[p] = cellInReady[s][p];
            end else begin
              cellInData[s][p] = '0;
              cellInValid[s][p] = 1'b0;
              cellInPort[s][p] = '0;
            end
          end // if (s == 0)...
          else begin
            // else connect to the outputs of the previous stage (TBD: pipelining)
            cellInData[s][p] = cellOutData[s-1][ip];
            cellInValid[s][p] = cellOutValid[s-1][ip];
            cellInPort[s][p] = cellOutPort[s-1][ip];
            cellOutReady[s-1][ip] = cellInReady[s][p];
          end
        end // always_comb

        // get from cellIn to arbIn
        always_comb begin
          // iterate over the inputs to this switch cell
          for (int i=0; i<PortsPerSlice; i++) begin
            arbInData[sp][i] = cellInData[s][SliceFirstPort+i];
            arbInPort[sp][i] = {cellInPort[s][SliceFirstPort+i], {SlicePortW{1'b0}}};
            arbInPort[sp][i][SlicePortW-1:0]  = i;
            arbInCombinedData[sp][i] = {arbInPort[sp][i],arbInData[sp][i]};
            arbInValid[sp][i] = (cellInValid[s][SliceFirstPort+i] &&
                                 (cellInPort[s][SliceFirstPort+i][SwitchPortW-1:SwitchPortW-SlicePortW] == sp));
          end
        end // always_comb

        // generate the feedback to cellIn
        assign cellInReady[s][p] = arbInReady[cellInPort[s][p][SwitchPortW-1:SwitchPortW-SlicePortW]][sp];

        // instantiate the arb mux
        oclib_arb_mux #(.Inputs(PortsPerSlice), .Width(DataWidth+SwitchPortW))
        uMUX (.clock(clock), .reset(reset),
              .inData(arbInCombinedData[sp][PortsPerSlice-1:0]),
              .inValid(arbInValid[sp][PortsPerSlice-1:0]),
              .inReady(arbInReady[sp][PortsPerSlice-1:0]),
              .outData({arbOutPort[sp],arbOutData[sp]}),
              .outValid(arbOutValid[sp]),
              .outReady(arbOutReady[sp]));

        // get from arbOut to cellOut
        always_comb begin
          cellOutData[s][p] = arbOutData[sp];
          cellOutPort[s][p] = arbOutPort[sp];
          cellOutValid[s][p] = arbOutValid[sp];
          arbOutReady[sp] = cellOutReady[s][p];
        end

        // connect to block output ports if this is the final stage
        if (s==(Stages-1)) begin
          if (p<OutputPorts) begin
            always_comb begin
              outData[p] = cellOutData[s][p];
              // the address was handled in a very tricky way through the fabric, which we undo here
              // at each stage, the cell looks at the MSBs of the incoming address to determine
              // which output port to use.  It then shifts the address left (so the next stage also
              // looks at the MSBs).  When shifting, the LSBs are set to the cell input port that
              // the data came in on.  Therefore, at the final stage cell output, the address has
              // the address of the first stage subport in the MSBs, but they should be in the LSBs
              // to specify the 'switch input port', so we have to swap.  This isn't a simple bit
              // reversal because it depends on how many bits are consumed in each stage.
              for (int si=0; si<Stages; si++) begin
                for (int i=0; i<SlicePortW; i++) begin
                  outPort[p][(si*SlicePortW)+i] = cellOutPort[s][p][((Stages-si-1)*SlicePortW)+i];
                end
              end
              outValid[p] = cellOutValid[s][p];
              cellOutReady[s][p] = outReady[p];
            end // always_comb
          end // if (p<OutputPorts) ...
        end // if (s==(Stages-1))...

      end // block: port
    end // block: slice
  end // stage

`endif

endmodule // oclib_switch_core
