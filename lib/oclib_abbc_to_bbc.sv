
// SPDX-License-Identifier: MPL-2.0

module oclib_abbc_to_bbc #(
                       parameter ResetSync = 0
                       )
  (
   input              clock,
   input              reset,
   input              oclib_pkg::abbc_s abbcIn,
   output             oclib_pkg::abbc_s abbcOut,
   input              oclib_pkg::bbc_s bbcIn,
   output             oclib_pkg::bbc_s bbcOut
   );

  // optional reset synchronizer
  logic                            resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  // rewire the structs
  oclib_pkg::abc_s abcIn, abcOut;
  oclib_pkg::abc_fb_s abcInFb, abcOutFb;
  oclib_pkg::bc_s bcIn, bcOut;
  oclib_pkg::bc_fb_s bcInFb, bcOutFb;

  assign abcIn.data = abbcIn.data;
  assign abcIn.req = abbcIn.req;
  assign abbcOut.ack = abcInFb.ack;

  assign abbcOut.data = abcOut.data;
  assign abbcOut.req = abcOut.req;
  assign abcOutFb.ack = abbcIn.ack;

  assign bcIn.data = bbcIn.data;
  assign bcIn.valid = bbcIn.valid;
  assign bbcOut.ready = bcInFb.ready;

  assign bbcOut.data = bcOut.data;
  assign bbcOut.valid = bcOut.valid;
  assign bcOutFb.ready = bbcIn.ready;

  // instantiate the unidirectional converters
  oclib_abc_to_bc uABC_TO_BC (.clock(clock), .reset(resetQ),
                            .abc(abcIn), .abcFb(abcInFb), .bc(bcOut), .bcFb(bcOutFb));
  oclib_bc_to_abc uBC_TO_ABC (.clock(clock), .reset(resetQ),
                            .bc(bcIn), .bcFb(bcInFb), .abc(abcOut), .abcFb(abcOutFb));

endmodule // oclib_abbc_to_bbc
