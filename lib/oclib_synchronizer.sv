
// SPDX-License-Identifier: MPL-2.0

module oclib_synchronizer #(parameter integer Width = 1,
                            parameter integer Enable = 1,
                            parameter integer Cycles = 3 )
  (
   input                    clock,
   input [Width-1:0]        in,
   output logic [Width-1:0] out
   );

  if ((Enable == 0) || (Cycles==0)) begin
    assign out = in;
  end
  else begin
`ifdef SIMULATION
    logic [Width-1:0]         pipe [Cycles-1:0];
    always @(posedge clock) begin
      for (int i=0; i<Cycles; i=i+1) pipe[i] <= ((i == 0) ? in : pipe[i-1]);
    end
    // assign output bus from the end of the data pipe
    assign out = pipe[Cycles-1];
`elsif OC_LIBRARY_ULTRASCALE_PLUS
   `OC_STATIC_ASSERT(Cycles<=10);
   `OC_STATIC_ASSERT(Cycles>=2);
    xpm_cdc_array_single #(.DEST_SYNC_FF(Cycles), .INIT_SYNC_FF(0), .SIM_ASSERT_CHK(0),
                           .SRC_INPUT_REG(0), .WIDTH(Width) )
    uXPM (.dest_clk(clock), .dest_out(out), .src_clk(1'b0), .src_in(in) );
`else
    `OC_STATIC_ERROR("Cannot synthesize without a supported vendor library!");
`endif
  end

endmodule // oclib_synchronizer
