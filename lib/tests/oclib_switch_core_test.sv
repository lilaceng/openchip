
// SPDX-License-Identifier: MPL-2.0

module oclib_switch_core_test
  #(
    parameter integer  Ports = 4,
    parameter integer  InputPorts = Ports,
    parameter integer  OutputPorts = Ports,
    parameter integer  PortsPerSlice = 2,
    parameter integer  DataWidth = 32,
    localparam integer SwitchPorts = ((InputPorts >= OutputPorts) ?
                       InputPorts : OutputPorts),
    localparam integer Stages = ((SwitchPorts <= PortsPerSlice) ? 1 :
                       (SwitchPorts <= (PortsPerSlice**2)) ? 2 :
                       (SwitchPorts <= (PortsPerSlice**3)) ? 3 :
                       (SwitchPorts <= (PortsPerSlice**4)) ? 4 :
                       (SwitchPorts <= (PortsPerSlice**5)) ? 5 :
                       (SwitchPorts <= (PortsPerSlice**6)) ? 6 :
                       (SwitchPorts <= (PortsPerSlice**7)) ? 7 :
                       (SwitchPorts <= (PortsPerSlice**8)) ? 8 :
                       -1),
    localparam integer SwitchPortW = $clog2(SwitchPorts)
    )
  ();

  localparam integer   Slices = ($clog2(PortsPerSlice) << (Stages-1));

  logic                                      clock = 1;
  logic                                      reset = 1;
  sim_clock uCLOCK (clock);
  sim_reset #(.CyclesAfterReset(50)) uRESET (clock, reset);

  logic [InputPorts-1:0] [DataWidth-1:0]     inData;
  logic [InputPorts-1:0] [SwitchPortW-1:0]   inPort;
  logic [InputPorts-1:0]                     inValid;
  logic [InputPorts-1:0]                     inReady;
  logic [OutputPorts-1:0] [DataWidth-1:0]    outData;
  logic [OutputPorts-1:0] [SwitchPortW-1:0]  outPort;
  logic [OutputPorts-1:0]                    outValid;
  logic [OutputPorts-1:0]                    outReady;

  oclib_switch_core #(.InputPorts(InputPorts), .OutputPorts(OutputPorts), .DataWidth(DataWidth),
                      .PortsPerSlice(PortsPerSlice) )
  uDUT (.clock(clock), .reset(reset),
        .inData(inData), .inPort(inPort), .inValid(inValid), .inReady(inReady),
        .outData(outData), .outPort(outPort), .outValid(outValid), .outReady(outReady));

  integer                              pushPercent;
  integer                              popPercent;
  logic                                debug = 0;

  typedef struct packed {
    logic [SwitchPortW-1:0] port;
    logic [DataWidth-1:0]    data;
  } entry_s;

  entry_s pushQueue [InputPorts-1:0] [$];
  entry_s checkQueue [OutputPorts-1:0] [InputPorts-1:0] [$];

  logic                      statRunning = 0;
  integer                    statCycle;
  integer                    statCycles;
  integer                    statPushesPerPort [InputPorts-1:0];
  integer                    statPopsPerPort [OutputPorts-1:0];

  always @(posedge clock) if (statRunning) statCycle += 1;

  for (genvar i=0; i<InputPorts; i++) begin : push
    entry_s temp;
    initial begin
      inData[i] = 0;
      inPort[i] = 0;
      inValid[i] = 1'b0;
      repeat (5) @(posedge clock);
      while (1) begin
        if (pushQueue[i].size() && !(inValid[i] && !inReady[i]) && `OC_RAND_PERCENT(pushPercent)) begin
          temp = pushQueue[i].pop_front();
          inData[i] <= temp.data;
          inPort[i] <= temp.port;
          inValid[i] <= 1'b1;
          statPushesPerPort[i] ++;
          if (debug) $display("%t %m: pushed entry in[%0d] out[%0d] data=%0x", $realtime, i, temp.port, temp.data);
        end
        else if (inReady[i]) begin
          inValid[i] <= 1'b0;
        end
        @(posedge clock);
      end
    end
  end

  for (genvar i=0; i<OutputPorts; i++) begin : check
    entry_s temp;
    initial begin
      outReady[i] = 1'b0;
      repeat (5) @(posedge clock);
      while (1) begin
        outReady[i] <= `OC_RAND_PERCENT(popPercent);
        if (outValid[i] && outReady[i]) begin
          if (checkQueue[i][outPort[i]].size() == 0) begin
            $display("%t %m ERROR: got unexpected entry on port[%0d] from port[%0d]", $realtime, i, outPort[i]);
            $finish;
          end
          // find the entry at the head of one of the input queues
          temp = checkQueue[i][outPort[i]].pop_front();
          if (debug) $display("%t %m: checking on out[%0d], expecting entry in[%0d] data=%0x", $realtime, i,
                              temp.port, temp.data);
          `OC_ASSERT(temp.port == outPort[i]);
          `OC_ASSERT(temp.data == outData[i]);
          statPopsPerPort[i] ++;
          statRunning <= 1; // we start the stat cycle counter on first output, i.e. start/stop on first/last output
          statCycles = statCycle + 1; // we update the length of the stat period on each output, to avoid post-finish idles
        end
        @(posedge clock);
      end
    end
  end

`ifndef OCLIB_WAVE_PIPELINE_SWITCH_CORE
  for (genvar s=0; s<Stages; s++) begin : stage
    localparam SlicePortW = $clog2(PortsPerSlice);
    localparam PortLsb = ((Stages-s-1) * SlicePortW);
    localparam PortMsb = (PortLsb + SlicePortW - 1);
    for (genvar sl=0; sl<Slices; sl++) begin : slice
      localparam SliceFirstPort = (sl*PortsPerSlice);
      localparam SliceLastPort = (((sl+1)*PortsPerSlice)-1);
      for (genvar sp=0; sp<PortsPerSlice; sp++) begin : sliceport
        localparam p = (SliceFirstPort + sp);
        always @(posedge clock) begin
          if (debug && !reset) begin
            if (uDUT.cellInValid[s][p])
              $display("%t %m: cellIn[%0d][%0d] (port=%x, data=%x)", $realtime,
                       s, p, uDUT.cellInPort[s][p], uDUT.cellInData[s][p]);
            if (uDUT.cellOutValid[s][p])
              $display("%t %m: cellOut[%0d][%0d] (port=%x, data=%x)", $realtime,
                       s, p, uDUT.cellOutPort[s][p], uDUT.cellOutData[s][p]);
          end
        end
      end
    end
  end
  `endif

  initial begin
    $display("%t %m: ================== START ==================", $realtime);
    $display("%t %m: === Ports:     %4d  PortsPerSlice:%4d ===", $realtime, SwitchPorts, PortsPerSlice);
    $display("%t %m: === InputPorts:%4d  OutputPorts  :%4d ===", $realtime, InputPorts, OutputPorts);
    $display("%t %m: === Stages:    %4d  Slices       :%4d ===", $realtime, Stages, Slices);
    $display("%t %m: ===========================================", $realtime);
    pushPercent = 100;
    popPercent = 100;
    SanityTest();
    pushPercent = 50;
    popPercent = 50;
    SanityTest();
    pushPercent = 100;
    popPercent = 100;
    StressTest();
    pushPercent = 50;
    popPercent = 50;
    StressTest();
    $display("%t %m: =============== DONE (PASSED) ===============", $realtime);
    $finish;
  end // initial begin

  task StressTest(integer cycles = 1000, integer maxDepth = 100);
    integer outPort;
    $display("%t %m: =============== START (Push@%0d%% Pop@%0d%%) ===============", $realtime, pushPercent, popPercent);
    uRESET.Reset();
    ResetStats();
    // just go nuts enqueueing one thing into every input port every clock unless any of the
    // queues in the path get too full
    for (int i=0; i<cycles; i++) begin
      @(posedge clock);
      for (int p=0; p<InputPorts; p++) begin
        outPort = ({$random} % OutputPorts);
        if ((pushQueue[p].size() < maxDepth) &&
            (checkQueue[outPort][p].size() < maxDepth)) begin
          PostEntry(p, outPort, $random);
        end
      end
    end
    WaitForIdle();
    ReportStats();
    $display("%t %m: =============== DONE (Push@%0d%% Pop@%0d%%) ===============", $realtime, pushPercent, popPercent);
  endtask

  task SanityTest();
    $display("%t %m: =============== START (Push@%0d%% Pop@%0d%%) ===============", $realtime, pushPercent, popPercent);
    uRESET.Reset();
    debug = 1;
    $display("%t %m: *** Single entry 1:1", $realtime);
    for (int in_port = 0; in_port < InputPorts; in_port++) begin
      repeat (10) @(posedge clock);
      PostEntry(in_port, (in_port % OutputPorts), $random);
      WaitForIdle();
    end
    $display("%t %m: *** Single entry matrix", $realtime);
    for (int in_port = 0; in_port < InputPorts; in_port++) begin
      for (int out_port = 0; out_port < OutputPorts; out_port++) begin
        repeat (10) @(posedge clock);
        PostEntry(in_port, out_port, $random);
        WaitForIdle();
      end
    end
    debug = 0;
    $display("%t %m: *** Burst 1:1", $realtime);
    for (int in_port = 0; in_port < InputPorts; in_port++) begin
      repeat (100) @(posedge clock);
      repeat (50) PostEntry(in_port, (in_port % OutputPorts), $random);
      WaitForIdle();
    end
    $display("%t %m: *** Burst matrix", $realtime);
    for (int in_port = 0; in_port < InputPorts; in_port++) begin
      for (int out_port = 0; out_port < OutputPorts; out_port++) begin
        repeat (100) @(posedge clock);
        repeat (50) PostEntry(in_port, out_port, $random);
        WaitForIdle();
      end
    end
    repeat (100) @(posedge clock);
    $display("%t %m: *** All 1:1 (Push@%0d%% Pop@%0d%%)", $realtime, pushPercent, popPercent);
    ResetStats();
    repeat (100) begin
      @(posedge clock);
      for (int in_port = 0; in_port < InputPorts; in_port++) begin
        PostEntry(in_port, (in_port % OutputPorts), $random);
      end
    end
    WaitForIdle();
    ReportStats();
    repeat (100) @(posedge clock);
    $display("%t %m: *** All-to-all matrix (Push@%0d%% Pop@%0d%%)", $realtime, pushPercent, popPercent);
    ResetStats();
    repeat (50) begin
      @(posedge clock);
      for (int in_port = 0; in_port < InputPorts; in_port++) begin
        for (int out_port = 0; out_port < OutputPorts; out_port++) begin
          PostEntry(in_port, out_port, $random);
        end
      end
    end
    WaitForIdle();
    ReportStats();
    $display("%t %m: =============== DONE (Push@%0d%% Pop@%0d%%) ===============", $realtime, pushPercent, popPercent);
  endtask

  task ResetStats();
    statRunning = 0;
    statCycle = 0;
    statCycles = 0;
    for (int i=0; i<InputPorts; i++) statPushesPerPort[i] = 0;
    for (int i=0; i<OutputPorts; i++) statPopsPerPort[i] = 0;
  endtask // ResetStats

  task ReportStats();
    integer statPushes;
    integer statPops;
    real entPerClock;
    real loss;
    integer i;
    statPushes = 0;
    statPops = 0;
    for (i=0; i<InputPorts; i++) statPushes += statPushesPerPort[i];
    for (i=0; i<OutputPorts; i++) statPops += statPopsPerPort[i];
    entPerClock = real'(statPops / (1.0 * Ports * statCycles));
    loss = real'(100.0 * (1.0 - entPerClock));
    $display("%t %m: **************************************************", $realtime);
    $display("%t %m: **  Sent %8d entries in %8d cycles    **", $realtime, statPops, statCycles);
    $display("%t %m: **  == %5.3f entries/port-clocks (%5.2f%% loss)  **", $realtime, entPerClock, loss);
    $display("%t %m: **  Port        In     (%%)        Out   (%%)     **", $realtime);
    for (i=0; i<SwitchPorts; i++) begin
      if ((i < InputPorts) && (i < OutputPorts))
          $display("%t %m: **  %4d   %8d   %5.2f  %8d   %5.2f   **", $realtime, i,
                   statPushesPerPort[i], (100.0 * (real'(statPushesPerPort[i]) / real'(statPushes))),
                   statPopsPerPort[i], (100.0 * (real'(statPopsPerPort[i]) / real'(statPops))));
      else if (i < InputPorts)
          $display("%t %m: **  %4d   %8d   %5.2f                     **", $realtime, i,
                   statPushesPerPort[i], (100.0 * (real'(statPushesPerPort[i]) / real'(statPushes))));
      else if (i < OutputPorts)
          $display("%t %m: **  %4d                     %8d   %5.2f   **", $realtime, i,
                   statPopsPerPort[i], (100.0 * (real'(statPopsPerPort[i]) / real'(statPops))));
    end
    $display("%t %m: **************************************************", $realtime);
  endtask // ReportStats

  task PostEntry (input logic [SwitchPortW-1:0] inPort,
                  input logic [SwitchPortW-1:0] outPort,
                  input logic [DataWidth-1:0]   data);
    entry_s pushEntry;
    entry_s checkEntry;
    pushEntry.data = data;
    pushEntry.port = outPort;
    checkEntry.data = pushEntry.data;
    checkEntry.port = inPort;
    pushQueue[inPort].push_back(pushEntry);
    checkQueue[outPort][inPort].push_back(checkEntry);
    if (debug) $display("%t %m: enqueueing entry in[%0d] out[%0d] data=%0x", $realtime, inPort, outPort, data);
  endtask // PostEntry

  task WaitForIdle();
    integer lastQueueDepth;
    integer thisQueueDepth;
    logic   running;
    if (debug > 1) $display("%t %m: Start", $realtime);
    lastQueueDepth = -1;
    running = 1;
    while (running) begin
      thisQueueDepth = 0;
      for (int i=0; i<InputPorts; i++) begin
        thisQueueDepth += pushQueue[i].size();
        for (int p=0; p<OutputPorts; p++) begin
          thisQueueDepth += checkQueue[p][i].size();
        end
      end
      if (debug > 1) $display("%t %m: queue depth = %0d", $realtime, thisQueueDepth);
      if (thisQueueDepth == 0) begin
        running = 0;
      end
      else begin
        if (lastQueueDepth == thisQueueDepth) begin
          $display("%t %m: ERROR: stuck while waiting for idle", $realtime);
          for (int i=0; i<Ports; i++) begin
            $display("%t %m: ERROR: port[%3d] pushQueue=%0d", $realtime,
                     i, pushQueue[i].size());
            for (int p=0; p<Ports; p++) begin
              $display("%t %m: ERROR: port[%3d] checkQueue from[%3d]=%3d", $realtime,
                       i, p, checkQueue[i][p].size());
            end
          end
          $finish;
        end
        lastQueueDepth = thisQueueDepth;
        repeat (50) @(posedge clock);
      end
    end
    if (debug > 1) $display("%t %m: Done", $realtime);
  endtask

endmodule // oclib_switch_core_test
