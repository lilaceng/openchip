
// SPDX-License-Identifier: MPL-2.0

module oclib_reorder_fifo_test
  #(parameter integer  Width = 32,
    parameter integer  Depth = 32,
    localparam integer AddressWidth = $clog2(Depth)) ();

  logic                        clock;
  logic                        reset;
  sim_clock uCLOCK (clock);
  sim_reset uRESET (clock, reset);

  logic [AddressWidth:0]       slot;
  logic                        slotValid;
  logic                        slotReady;
  logic [Width-1:0]            inData;
  logic [AddressWidth:0]       inSlot;
  logic                        inValid;
  logic [Width-1:0]            outData;
  logic                        outValid;
  logic                        outReady;
  logic                        init;

  oclib_reorder_fifo #(.Width(Width), .Depth(Depth))
  uDUT (.clock(clock), .reset(reset), .init(init),
        .slot(slot), .slotValid(slotValid), .slotReady(slotReady),
        .inData(inData), .inSlot(inSlot), .inValid(inValid),
        .outData(outData), .outValid(outValid), .outReady(outReady));

  integer                      idx;

  initial begin
    $display("%t %m: START", $realtime);
    SanityTest();
    StressTest();
    $display("%t %m: DONE (PASSED)", $realtime);
    $finish;
  end // initial begin

  typedef struct packed {
    logic             valid;
    logic [Width-1:0] data;
  } entry_s;
  integer             returnEntries;

  localparam integer  ReturnDepth = (Depth*2); // since slots come from a space 2X the depth
  entry_s returnTable [ReturnDepth-1:0];
  entry_s checkTable [ReturnDepth-1:0];
  logic               running;
  integer             slotPercent;
  integer             returnPercent;
  integer             consumePercent;
  integer             checkPointer;
  logic [31:0]        temp;
  logic               biasOutOfOrder;

  task StressTest();
    $display("%t %m: START", $realtime);
    uRESET.Reset();
    for (int i=0; i<ReturnDepth; i++) returnTable[i] = '0;
    for (int i=0; i<ReturnDepth; i++) checkTable[i] = '0;
    returnEntries = 0;
    running = 1;
    fork
      begin // get slots and populate the returnTable
        while (running) begin
          @(posedge clock);
          slotReady <= `OC_RAND_PERCENT(slotPercent);
          if (slotReady && slotValid) begin
            `OC_ASSERT(returnTable[slot].valid == 0);
            temp = $random;
            returnTable[slot].valid = 1;
            returnTable[slot].data = temp;
            checkTable[slot].valid = 1;
            checkTable[slot].data = temp;
            returnEntries++;
          end
        end
        slotReady <= 1'b0;
      end
      begin // select entries from the returnTable and return them
        while (running) begin
          @(posedge clock);
          if (returnEntries && `OC_RAND_PERCENT(returnPercent)) begin
            // select a random return entry
            temp = ({$random} % ReturnDepth);
            while (returnTable[temp].valid == 0) begin
              temp = ((biasOutOfOrder ? (temp-1) : (temp+1)) % ReturnDepth);
            end
            inValid <= 1'b1;
            inData <= returnTable[temp].data;
            inSlot <= temp;
            returnTable[temp].valid = 0;
            returnTable[temp].data = 'X;
            returnEntries--;
          end
          else begin
            inValid <= 1'b0;
          end
        end
        inValid <= 1'b0;
      end
      begin // consume output and verify it's in order
        checkPointer = 0;
        while (running) begin
          @(posedge clock);
          outReady <= `OC_RAND_PERCENT(consumePercent);
          if (outValid && outReady) begin
            `OC_ASSERT(checkTable[checkPointer].valid);
            `OC_ASSERT(outData == checkTable[checkPointer].data);
            checkTable[checkPointer].valid = 0;
            checkTable[checkPointer].data = 'X;
            checkPointer <= ((checkPointer+1) % ReturnDepth);
          end
        end
        outReady <= 1'b0;
      end
      begin // test control
        for (slotPercent = 0; slotPercent <= 100; slotPercent += 20) begin
          for (returnPercent = 0; returnPercent <= 100; returnPercent += 20) begin
            for (consumePercent = 0; consumePercent <= 100; consumePercent += 20) begin
              biasOutOfOrder = 0;
              repeat (1000) @(posedge clock);
              biasOutOfOrder = 1;
              repeat (1000) @(posedge clock);
            end
          end
        end
        running = 0; // we are done
      end
    join
    repeat (100) @(posedge clock);
    $display("%t %m: DONE", $realtime);
  endtask

  task SanityTest();
    $display("%t %m: START", $realtime);
    uRESET.Reset();
    inValid = 0;
    slotReady = 0;
    outReady = 0;
    idx = 0;
    repeat (50+Depth) @(posedge clock);
    slotReady <= 1;
    repeat (5) @(posedge clock);
    slotReady <= 0;
    repeat (5) @(posedge clock);
    inSlot <= 0;
    inData <= 0;
    inValid <= 1;
    @(posedge clock);
    inValid <= 0;
    repeat (10) @(posedge clock);
    outReady <= 1'b1;
    repeat (10) @(posedge clock);
    outReady <= 1'b0;
    inSlot <= 3;
    inData <= 3;
    inValid <= 1;
    @(posedge clock);
    inSlot <= 2;
    inData <= 2;
    @(posedge clock);
    inSlot <= 1;
    inData <= 1;
    @(posedge clock);
    inValid <= 0;
    repeat (10) @(posedge clock);
    outReady <= 1'b1;
    repeat (10) @(posedge clock);
    outReady <= 1'b0;
    repeat (10) @(posedge clock);
    slotReady <= 1'b1;
    repeat (Depth+10) @(posedge clock);
    slotReady <= 1'b0;
    repeat (10) @(posedge clock);
    $display("%t %m: DONE", $realtime);
  endtask

endmodule // oclib_reorder_fifo_test
