
// SPDX-License-Identifier: MPL-2.0

module oclib_abc_to_word #(
                         parameter WordW = 64,
                         parameter ShiftFanout = 16,
                         parameter ResetSync = 0
                         )
  (
   input                    clock,
   input                    reset,
   input                    oclib_pkg::abc_s abc,
   output                   oclib_pkg::abc_fb_s abcFb,
   output logic [WordW-1:0] wordData,
   output logic             wordValid,
   input                    wordReady
   );

  logic                     resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  oclib_pkg::bc_s bc;
  oclib_pkg::bc_fb_s bcFb;
  oclib_abc_to_bc uABC_TO_BC (.clock(clock), .reset(resetQ),
                            .abc(abc), .abcFb(abcFb),
                            .bc(bc), .bcFb(bcFb));

  oclib_bc_to_word #(.WordW(WordW), .ShiftFanout(ShiftFanout))
  uBC_TO_WORD (.clock(clock), .reset(resetQ),
               .bc(bc), .bcFb(bcFb),
               .wordData(wordData), .wordValid(wordValid), .wordReady(wordReady));

endmodule // oclib_abc_to_word
