
// SPDX-License-Identifier: MPL-2.0

package oclib_pkg;

  // ***********************************************************************************************
  // Utility functions
  // ***********************************************************************************************

  function real DiffReal (input real a, input real b);
     if (a > b) return (a-b);
     else       return (b-a);
  endfunction

  function real ComputeVcoFrequency (input real vcoMin, input real vcoMax,
                                     input real refFreq, input real targetFreq,
                                     input real refMultMin, input real refMultMax, input real refMultStep,
                                     input real outDivMin, input real outDivMax, input real outDivStep);
    real                                        vcoFreq, outFreq, result, error;
    result = vcoMax;  // by default, we just run VCO at max speed and then calculate dividers to be as close as possible to output
    error = vcoMax * 1000; // large error by default, we want to replace this with something better
    // we start with high mult, preferring high VCO freq...
    for (real refMult = refMultMax; refMult >= refMultMin; refMult -= refMultStep) begin
      vcoFreq = refMult * refFreq;
      if ((vcoFreq >= vcoMin) && (vcoFreq <= vcoMax)) begin // this is a legal VCO frequency
        for (real outDiv = outDivMin; outDiv <= outDivMax; outDiv += outDivStep) begin
          outFreq = (vcoFreq / outDiv);
          if (DiffReal(outFreq, targetFreq) < error) begin // we have a better result
            result = vcoFreq;
            error = DiffReal(outFreq, targetFreq);
            // $display("Chose new VCO: %8.3f (out=%8.3f, target=%8.3f, error=%8.3f)", result, outFreq, targetFreq, error);
          end
        end
      end
    end
    return result;
  endfunction

  // ***********************************************************************************************
  // Enum to identify top level IPs via ABBC space 0, address 0 ID register
  // ***********************************************************************************************

  localparam integer AbbcIdPll = 16'd1;
  localparam integer AbbcIdChipmon = 16'd2;
  localparam integer AbbcIdProtect = 16'd3;
  localparam integer AbbcIdHbm = 16'd4;
  localparam integer AbbcIdIic = 16'd5;
  localparam integer AbbcIdLed = 16'd6;
  localparam integer AbbcIdGpio = 16'd7;
  localparam integer AbbcIdFan = 16'd8;
  localparam integer AbbcIdCmac = 16'd9;

  localparam integer TargetVendorCount = 2;
  function string TargetVendorString (input int n);
    if (n==1) return "Xilinx";
    return "unknown";
  endfunction

  localparam integer TargetBoardCount = 6;
  function string TargetBoardString (input int n);
    if (n==1) return "vcu1525";
    if (n==2) return "u200";
    if (n==3) return "u50";
    if (n==4) return "u55n";
    if (n==5) return "u50c";
    if (n==6) return "jc35";
    return "unknown";
  endfunction

  localparam integer TargetLibraryCount = 2;
  function string TargetLibraryString (input int n);
    if (n==1) return "Ultrascale+";
    return "unknown";
  endfunction

  // ***********************************************************************************************
  // ABC protocol (and related ABBC, BC, BBC)
  // ***********************************************************************************************

  // Master puts DATA on bus, asserts REQ
  // Slave raises ACK (doesn't sample data yet!)
  // Master sees ACK, de-asserts REQ
  // Slave sees REQ de-assert, samples data, de-asserts ACK
  // Master sees ACK de-assert, and knows (a) slave got the data, (b) it can start a new transaction

  typedef struct packed {
    logic [7:0]  data;
    logic        req;
  } abc_s;

  typedef struct packed {
    logic        ack;
  } abc_fb_s;

  // ABBC is a bidirectional version of ABC.  It's symmetric, so same struct in both directions.
  typedef struct packed {
    logic [7:0]  data;
    logic        req;
    logic        ack;
  } abbc_s;

  // BC is a non-async version of the bus.  Blocks should internally use BC, and instantiate an
  // adapter to convert to ABC.  This enables the ABC to change if needed, and allows direct
  // connections of multiple BC-based blocks without the gates/latency of async conversion.
  // Standard ready/valid interface is used.

  typedef struct packed {
    logic [7:0]  data;
    logic        valid;
  } bc_s;

  typedef struct packed {
    logic        ready;
  } bc_fb_s;

  typedef struct packed {
    logic [7:0]  data;
    logic        valid;
    logic        ready;
  } bbc_s;

  // ***********************************************************************************************
  // WORD PROTOCOL
  // ***********************************************************************************************

  parameter MaxWordBytes = 1024;
  parameter MaxWordW = (MaxWordBytes * 8);

  // ***********************************************************************************************
  // CSR PROTOCOL
  // ***********************************************************************************************

  parameter CsrCommandW = 4;
  parameter CsrSpaceW = 4;
  parameter CsrAddressW = 32;
  parameter CsrDataW = 32;
  parameter CsrStatusW = 8;

  parameter CsrCommandRead = 4'h1;
  parameter CsrCommandWrite = 4'h2;
  parameter CsrCommandStatus = 4'he;
  parameter CsrCommandClear = 4'hf;

  typedef struct packed {
    logic                write;
    logic                read;
    logic [CsrAddressW-1:0] address;
    logic [CsrDataW-1:0]    wdata;
  } csr_s;

  typedef struct packed {
    logic [CsrDataW-1:0] rdata;
    logic                ready;
    logic                error;
  } csr_fb_s;

  // used when serializing the CSR transactions into words
  parameter WordToCsrW = (CsrCommandW + CsrSpaceW + CsrAddressW + CsrDataW);
  parameter WordFromCsrW = (CsrStatusW + CsrDataW);

  parameter WordToCsrBytes = (WordToCsrW+7)/8;
  parameter WordFromCsrBytes = (WordFromCsrW+7)/8;

  // ***********************************************************************************************
  // DRP PROTOCOL
  // ***********************************************************************************************

  typedef struct packed {
    logic        enable;
    logic [15:0] address;
    logic        write;
    logic [15:0] wdata;
  } drp_s;

  typedef struct packed {
    logic [15:0] rdata;
    logic        ready;
  } drp_fb_s;

  // ***********************************************************************************************
  // APB PROTOCOL
  // ***********************************************************************************************

  typedef struct packed {
    logic        select;
    logic        enable;
    logic [31:0] address;
    logic        write;
    logic [31:0] wdata;
  } apb_s;

  typedef struct packed {
    logic [31:0] rdata;
    logic        ready;
    logic        error;
  } apb_fb_s;

  // ***********************************************************************************************
  // AXI-LITE 32-BIT PROTOCOL
  // ***********************************************************************************************

  typedef struct packed {
    logic [31:0] awaddr;
    logic        awvalid;
    logic [31:0] araddr;
    logic        arvalid;
    logic [31:0] wdata;
    logic [3:0]  wstrb;
    logic        wvalid;
    logic        rready;
    logic        bready;
  } axi_lite_s;

  typedef struct packed {
    logic [31:0] rdata;
    logic [1:0]  rresp;
    logic        rvalid;
    logic [1:0]  bresp;
    logic        bvalid;
    logic        awready;
    logic        arready;
    logic        wready;
  } axi_lite_fb_s;

  parameter AxiLiteDataWidth = 32;

  // ***********************************************************************************************
  // AXI3 PROTOCOL (typically used for memory controllers)
  // ***********************************************************************************************

  localparam Axi3IdWidth = 6;
  localparam Axi3AddressWidth = 33;
  localparam Axi3DataWidth = 256;
  localparam Axi3DataBytes = (Axi3DataWidth/8);

  typedef struct packed {
    logic [Axi3AddressWidth-1:0] addr;
    logic [Axi3IdWidth-1:0]      id;
    logic [1:0]                  burst;
    logic [2:0]                  size;
    logic [3:0]                  len;
  } axi3_a_s;

  typedef struct packed {
    logic [Axi3DataBytes-1:0] [7:0] data;
    logic [Axi3DataBytes-1:0]       strb;
    logic                           last;
  } axi3_w_s;

  typedef struct packed {
    logic [Axi3IdWidth-1:0]         id;
    logic [Axi3DataBytes-1:0] [7:0] data;
    logic [1:0]                     resp;
    logic                           last;
  } axi3_r_s;

  typedef struct packed {
    logic [Axi3IdWidth-1:0] id;
    logic [1:0]             resp;
  } axi3_b_s;

  typedef struct packed {
    axi3_a_s aw;
    logic    awvalid;
    axi3_a_s ar;
    logic    arvalid;
    axi3_w_s w;
    logic    wvalid;
    logic    rready;
    logic    bready;
  } axi3_s;

  typedef struct packed {
    axi3_r_s r;
    logic    rvalid;
    axi3_b_s b;
    logic    bvalid;
    logic    awready;
    logic    arready;
    logic    wready;
  } axi3_fb_s;

  // ***********************************************************************************************
  // AXI-STREAM PROTOCOL (typically used for Ethernet MAC)
  // ***********************************************************************************************

  typedef struct packed {
    logic         tvalid;
    logic [511:0] tdata;
    logic         tlast;
    logic [63:0]  tkeep;
    logic         tuser;
    logic         reset; // this will be driven by MAC in RX usage, will be ignored in TX usage
  } axi4st_512_s;

  typedef struct packed {
    logic         tready;
    logic         reset; // this will be driven by MAC in TX usage, will be ignored in RX usage
  } axi4st_512_fb_s;

  // ***********************************************************************************************
  // CHIP STATUS
  // ***********************************************************************************************

  typedef struct packed {
    logic        [7:0] unlocked; // support for unlocking 8 separate functions
    logic        thermalError;
    logic        thermalWarning;
  } ring_status_s;

  typedef struct packed {
    logic        error;
    logic        done;
  } user_status_s;

endpackage
