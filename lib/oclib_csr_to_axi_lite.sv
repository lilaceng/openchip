
// SPDX-License-Identifier: MPL-2.0

module oclib_csr_to_axi_lite #(
                               parameter DataW = 32,
                               parameter AddressW = 32,
                               parameter ResetSync = 0
                               )
  (
   input  clock,
   input  reset,
   input  csrSelect,
   input  oclib_pkg::csr_s csr,
   output oclib_pkg::csr_fb_s csrFb,
   output oclib_pkg::axi_lite_s axil,
   input  oclib_pkg::axi_lite_fb_s axilFb
   );

  logic          resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  enum           logic [2:0] { StIdle, StWrite1, StWrite2, StRead1, StRead2, StDone } state;

  always_ff @(posedge clock) begin
    if (resetQ) begin
      axil <= '0;
      csrFb <= '0;
      state <= StIdle;
    end
    else begin
      case (state)
        StIdle : begin
          if (csr.write && csrSelect) begin
            axil.awvalid <= 1'b1;
            axil.wvalid <= 1'b1;
            axil.awaddr <= csr.address[AddressW-1:0];
            axil.wdata <= csr.wdata[DataW-1:0];
            axil.wstrb <= {DataW/8{1'b1}};
            state <= StWrite1;
          end
          else if (csr.read && csrSelect) begin
            axil.arvalid <= 1'b1;
            axil.araddr <= csr.address[AddressW-1:0];
            state <= StRead1;
          end
        end
        StWrite1 : begin
          if (axilFb.awready) axil.awvalid <= 1'b0;
          if (axilFb.wready) axil.wvalid <= 1'b0;
          if ((!axil.awvalid) && (!axil.wvalid)) begin
            axil.bready <= 1'b1;
            state <= StWrite2;
          end
        end
        StWrite2 : begin
          if (axilFb.bvalid) begin
            axil.bready <= 1'b0;
            csrFb.ready <= 1'b1;
            csrFb.error <= (axilFb.bresp != 2'd0);
            state <= StDone;
          end
        end
        StRead1 : begin
          if (axilFb.arready) axil.arvalid <= 1'b0;
          if (!axil.arvalid) begin
            axil.rready <= 1'b1;
            state <= StRead2;
          end
        end
        StRead2 : begin
          if (axilFb.rvalid) begin
            axil.rready <= 1'b0;
            csrFb.ready <= 1'b1;
            csrFb.rdata <= axilFb.rdata;
            csrFb.error <= (axilFb.rresp != 2'd0);
            state <= StDone;
          end
        end
        StDone : begin
          csrFb.ready <= 1'b0;
          if ((!csr.read) && (!csr.write)) begin
            state <= StIdle;
          end
        end
      endcase
    end
  end

endmodule // oclib_csr_to_axi_lite
