
// SPDX-License-Identifier: MPL-2.0

`define OCLIB_MEMORY_TEST_VERSION 32'h70000000

package oclib_memory_test_pkg;

  parameter MaxAxi3 = 32;
  parameter Axi3AddressWidth = 34;
  parameter Axi3DataWidth = 256;
  parameter MaxAxi3Width = $clog2(MaxAxi3);

  typedef struct packed {
    logic                                go;
    logic [7:0]                          sts_port_select;
    logic [7:0]                          sts_csr_select;
    logic [5:0]                          address_port_shift;
    logic [7:0]                          write_mode;
    logic [7:0]                          read_mode;
    logic [31:0]                         op_count;
    logic [7:0]                          wait_states;
    logic [3:0]                          burst_length;
    logic [MaxAxi3-1:0]                  axi_enable;
    logic [7:0]                          read_max_id;
    logic [7:0]                          write_max_id;
    logic [Axi3AddressWidth-1:0]         address;
    logic [Axi3AddressWidth-1:0]         address_inc;
    logic [Axi3AddressWidth-1:0]         address_inc_mask;
    logic [Axi3AddressWidth-1:0]         address_random_mask;
    logic [MaxAxi3Width-1:0]             address_port_mask;
    logic [(Axi3DataWidth/8)-1:0] [7:0]  data;
  } cfg_s;

  typedef struct packed {
    logic                                done;
    logic [31:0]                         signature;
    logic [31:0]                         error;
    logic [31:0]                         rdata;
    logic [(Axi3DataWidth/8)-1:0] [7:0]  data;
  } sts_s;

endpackage // oclib_memory_test_pkg



module oclib_memory_test #(
                           parameter integer CsrToPortFlops = 2,
                           parameter integer AxilClockFreq = 100000000, // used for the uptime counters
                           parameter integer AxiPorts = 4,
                           parameter integer AxiPortRetimers = 0, // add retiming stages between port logic and HBM port
                           parameter integer ConfigPipeStages = 0,
                           parameter integer StatusPipeStages = 0
                           )
  (
   // GLOBALS
   input  reset,

   // AXIL CSR
   input  clockAxil,
   input  oclib_pkg::axi_lite_s axil,
   output oclib_pkg::axi_lite_fb_s axilFb,

   // AXI3 MEMORY PORTS
   input  clockAxi,
   output oclib_pkg::axi3_s [AxiPorts-1:0] axi,
   input  oclib_pkg::axi3_fb_s [AxiPorts-1:0] axiFb
   );

  // *** synchronize reset into two domains

  logic                            resetAxil;
  logic                            resetAxi;
  logic [AxiPorts-1:0]             resetAxiPort;

  oclib_synchronizer uRESET_AXIL (.clock(clockAxil), .in(reset), .out(resetAxil));
  oclib_synchronizer uRESET_AXI (.clock(clockAxi), .in(reset), .out(resetAxi));

  for (genvar i=0; i<AxiPorts; i++) begin : axi_reset
    oclib_pipeline #(.Length(2), .DontTouch(1))
    uRESET_AXI_PORT (.clock(clockAxi), .in(resetAxi), .out(resetAxiPort[i]));
  end

  // *** lightweight AXI-Lite CSR logic

  oclib_memory_test_pkg::cfg_s     csr_cfg;
  oclib_memory_test_pkg::sts_s     csr_sts;

  oclib_memory_test_csrs #(AxilClockFreq)
  uCSRS (// AXIL
         clockAxil, resetAxil, axil, axilFb,
         // CONFIG/STATUS (AXI DOMAIN)
         clockAxi, resetAxi, csr_cfg, csr_sts
         );

  // *** pipeline CFG bus over to PORT[0] area

  oclib_memory_test_pkg::cfg_s     port_cfg [AxiPorts-1:0]; // port_cfg[0] is cfg going IN to port logic #0

  oclib_pipeline #(.Width($bits(csr_cfg)), .Length(CsrToPortFlops))
  uCFG_PIPE (.clock(clockAxi), .in(csr_cfg), .out(port_cfg[0]));

  // *** pipeline CFG bus across from PORT[0] to [N]

  if (AxiPorts>1) begin
    always_ff @(posedge clockAxi) begin
      for (int p=1; p<AxiPorts; p++) begin
        port_cfg[p] <= port_cfg[p-1];
      end
    end
  end

  // *** instantiate the PORT engines

  oclib_memory_test_pkg::sts_s     port_sts [AxiPorts-1:0]; // port_sts_local[0] is sts coming OUT engine 0

  for (genvar p=0; p<AxiPorts; p++) begin
    oclib_memory_test_axi #(p, AxiPortRetimers, ConfigPipeStages, StatusPipeStages)
    uAXI (clockAxi, resetAxiPort[p],
          port_cfg[p], port_sts[p],
          axi[p], axiFb[p] );
  end

  // *** pipeline STS bus back from PORT[N] .. [0]

  oclib_memory_test_pkg::sts_s     port_sts_pipe [AxiPorts-1:0]; // [0] is sts OUT from block 0 (incl port[0] + [1] ...)

  for (genvar p=0; p<AxiPorts; p++) begin

    oclib_memory_test_pkg::sts_s   port_sts_in; // port status coming from upstream
    oclib_memory_test_pkg::sts_s   port_sts_local; // port status from local engine
    oclib_memory_test_pkg::sts_s   port_sts_out; // port status going to downstream

    // combinationally combine the engine status with the upstream status, which both come from flops, to go into output flop
    always_comb begin
      port_sts_local = port_sts[p];
      if (p == (AxiPorts-1)) begin
        port_sts_in = '0; // default inputs to 0 for highest numbered port, there is no "from upstream"
        port_sts_in.done = 1'b1; // except for done bit, pretend that's asserted :)
      end
      else begin
        port_sts_in = port_sts_pipe[p+1]; // for all other ports, the input is driven from the next higher port
      end
      if (port_cfg[p].axi_enable[p]) begin
        // this port is enabled, combine our status with what's coming from upstream
        port_sts_out = (port_sts_in | port_sts_local); // we OR together our two status except for fields below
        port_sts_out.done = (port_sts_in.done & port_sts_local.done); // done when ALL are done
        port_sts_out.rdata = ((port_cfg[p].sts_port_select == p) ? port_sts_local.rdata : port_sts_in.rdata);
      end
      else begin
        port_sts_out = port_sts_in; // port is disabled, just pass on from upstream
      end
    end

    // send sts back along the pipeline
    always_ff @(posedge clockAxi) begin
      port_sts_pipe[p] <= port_sts_out;
    end

  end

  // *** pipeline STS bus over from PORT[0] back to CSR area

  oclib_memory_test_pkg::sts_s     csr_to_port_sts [CsrToPortFlops-1:0]; // pipe stages between CSR block and port 0

  oclib_pipeline #(.Width($bits(csr_sts)), .Length(CsrToPortFlops))
  uSTS_PIPE (.clock(clockAxi), .in(port_sts_pipe[0]), .out(csr_sts));

endmodule // oclib_metest




module oclib_memory_test_axi #(
                               parameter PortNumber = 0,
                               parameter integer AxiPortRetimers = 0, // add retiming stages between port logic and HBM port
                               parameter integer ConfigPipeStages = 0,
                               parameter integer StatusPipeStages = 0
                               )
  (
   // GLOBALS
   input  clockAxi,
   input  resetAxi,

   // CONFIG / STATUS
   input  oclib_memory_test_pkg::cfg_s cfg,
   output oclib_memory_test_pkg::sts_s sts,

   // AXI3 PORT
   output oclib_pkg::axi3_s axi,
   input  oclib_pkg::axi3_fb_s axiFb
   );

  // **********
  // optional retiming stages (if port engine is placed farther from HBM AXI3 port)
  // **********


  // *** aw channel
  oclib_pkg::axi3_a_s                       fifo_aw; // output of FIFO
  logic                                     fifo_awvalid, fifo_awready;

  oclib_ready_valid_pipeline #(.Width($bits(oclib_pkg::axi3_a_s)), .Length(AxiPortRetimers))
  uAW_BUF (clockAxi, resetAxi, fifo_aw, fifo_awvalid, fifo_awready, axi.aw, axi.awvalid, axiFb.awready);

  // *** ar channel
  oclib_pkg::axi3_a_s                       fifo_ar; // output of FIFO
  logic                                     fifo_arvalid, fifo_arready;

  oclib_ready_valid_pipeline #(.Width($bits(oclib_pkg::axi3_a_s)), .Length(AxiPortRetimers))
  uAR_BUF (clockAxi, resetAxi, fifo_ar, fifo_arvalid, fifo_arready, axi.ar, axi.arvalid, axiFb.arready);

  // *** w channel
  oclib_pkg::axi3_w_s                       fifo_w; // output of FIFO
  logic                                     fifo_wvalid, fifo_wready;

  oclib_ready_valid_pipeline #(.Width($bits(oclib_pkg::axi3_w_s)), .Length(AxiPortRetimers))
  uW_BUF (clockAxi, resetAxi, fifo_w, fifo_wvalid, fifo_wready, axi.w, axi.wvalid, axiFb.wready);

  // *** r channel
  oclib_pkg::axi3_r_s                       fifo_r; // input to FIFO
  logic                                     fifo_rvalid, fifo_rready;

  oclib_ready_valid_pipeline #(.Width($bits(oclib_pkg::axi3_r_s)), .Length(AxiPortRetimers))
  uR_BUF (clockAxi, resetAxi, axiFb.r, axiFb.rvalid, axi.rready, fifo_r, fifo_rvalid, fifo_rready);

  // *** b channel
  oclib_pkg::axi3_b_s                       fifo_b; // input to FIFO
  logic                                     fifo_bvalid, fifo_bready;

  oclib_ready_valid_pipeline #(.Width($bits(oclib_pkg::axi3_b_s)), .Length(AxiPortRetimers))
  uB_BUF (clockAxi, resetAxi, axiFb.b, axiFb.bvalid, axi.bready, fifo_b, fifo_bvalid, fifo_bready);

  // **********
  // *** mandatory FIFO stages (decouple HBM port from memory test logic, local to memtest port engine)
  // **********

  // *** aw channel
  oclib_pkg::axi3_a_s                                    aw;
  logic                                                  awready;
  logic                                                  awvalid;
  logic                                                  aw_almost_full;

  oclib_fifo #(.Width($bits(oclib_pkg::axi3_a_s)))
  uAW_FIFO (clockAxi, resetAxi, aw_almost_full, aw, awvalid, awready, fifo_aw, fifo_awvalid, fifo_awready);

  // *** ar channel
  oclib_pkg::axi3_a_s                                    ar;
  logic                                                  arready;
  logic                                                  arvalid;
  logic                                                  ar_almost_full;

  oclib_fifo #(.Width($bits(oclib_pkg::axi3_a_s)))
  uAR_FIFO (clockAxi, resetAxi, ar_almost_full, ar, arvalid, arready, fifo_ar, fifo_arvalid, fifo_arready);

  // *** w channel
  oclib_pkg::axi3_w_s                                    w;
  logic                                                  wvalid;
  logic                                                  wready;
  logic                                                  w_almost_full;

  oclib_fifo #(.Width($bits(oclib_pkg::axi3_w_s)))
  uW_FIFO (clockAxi, resetAxi, w_almost_full, w, wvalid, wready, fifo_w, fifo_wvalid, fifo_wready);

  // *** r channel
  oclib_pkg::axi3_r_s                                    r;
  logic                                                  rvalid;
  logic                                                  rready;
  logic                                                  r_almost_full;

  oclib_fifo #(.Width($bits(oclib_pkg::axi3_r_s)))
  uR_FIFO (clockAxi, resetAxi, r_almost_full, fifo_r, fifo_rvalid, fifo_rready, r, rvalid, rready);

  // *** b channel
  oclib_pkg::axi3_b_s                                    b;
  logic                                                  bvalid;
  logic                                                  bready;
  logic                                                  b_almost_full;

  oclib_fifo #(.Width($bits(oclib_pkg::axi3_b_s)))
  uB_FIFO (clockAxi, resetAxi, b_almost_full, fifo_b, fifo_bvalid, fifo_bready, b, bvalid, bready);

  // **********
  // *** optionally retime cfg/status signals
  // **********

  oclib_memory_test_pkg::cfg_s                           cfg_local;
  oclib_pipeline #(.Width($bits(cfg_local)), .Length(ConfigPipeStages))
  uCFG_PIPE (.clock(clockAxi), .in(cfg), .out(cfg_local));

  oclib_memory_test_pkg::sts_s                           sts_local;
  oclib_pipeline #(.Width($bits(sts_local)), .Length(StatusPipeStages))
  uSTS_PIPE (.clock(clockAxi), .in(sts_local), .out(sts));

  // **********
  // Generate shifted port address (kind of a per-port base address that is somewhat configurable)
  // **********

  logic [oclib_pkg::Axi3AddressWidth-1:0]                port_address;
  always_ff @(posedge clockAxi) begin
    port_address <= ((PortNumber & cfg_local.address_port_mask) << cfg_local.address_port_shift);
  end

  // **********
  // Generate WRITE transactions
  // **********

  logic [oclib_pkg::Axi3AddressWidth-1:0]                awaddr;
  logic [7:0]                                            awid;
  logic [3:0]                                            awlen;
  logic [oclib_pkg::Axi3DataWidth-1:0]                   wdata;
  logic                                                  wlast;

  assign aw.id = awid; // single source ID for now
  assign aw.addr = awaddr; // don't like to mix assign and flops into struct, buggy
  assign aw.burst = 'd1; // INCR
  assign aw.size = 'd5; // 2**5 = 32Byte
  assign aw.len = awlen;

  assign w.data = wdata;
  assign w.strb = {(oclib_pkg::Axi3DataWidth/8){1'b1}}; // all bytes writing
  assign w.last = wlast; // 1-word bursts for now

  enum logic [2:0] { WrIdle, WrBusy, WrBurst, WrWait, WrDone } write_state;
  logic [31:0] write_count;
  logic [15:0] write_wait_count;

  // cfg_write_mode
  // 0 : writing
  // 4 : randomize length
  // 5 : rotate write data
  // 6 : random write data

  logic [oclib_pkg::Axi3AddressWidth-1:0]                awaddr_next;
  logic [oclib_pkg::Axi3AddressWidth-1:0]                awaddr_next_next;
  logic [oclib_pkg::Axi3AddressWidth-1:0]                awaddr_random;

  logic                                                  reset_random;
  always_ff @(posedge clockAxi) begin
    reset_random <= (resetAxi || !cfg_local.go);
  end

  logic                                                  consume_aw_random;
  assign consume_aw_random = ((write_state == WrBusy) && !aw_almost_full && !w_almost_full);

  oclib_random #(.Seed(PortNumber+100), .OutWidth(oclib_pkg::Axi3AddressWidth))
  uAWADDR_RANDOM (.clock(clockAxi), .reset(reset_random), .enable(consume_aw_random), .out(awaddr_random));

  always_comb begin
    awaddr_next_next = (awaddr_next      + cfg_local.address_inc) & cfg_local.address_inc_mask;
    awaddr_next_next = (awaddr_next_next ^ (awaddr_random & cfg_local.address_random_mask));
    awaddr_next_next = (awaddr_next_next ^ port_address);
  end

  // compute next arlen
  logic [3:0]                                              awlen_next;
  logic [3:0]                                              awlen_random;

  oclib_random #(.Seed(PortNumber+101), .OutWidth(4), .LfsrWidth(33))
  uAWLEN_RANDOM (.clock(clockAxi), .reset(reset_random), .enable(consume_aw_random), .out(awlen_random));

  always_comb begin
    if (cfg_local.write_mode[4])  awlen_next = (awlen_random & cfg_local.burst_length);
    else                          awlen_next = cfg_local.burst_length;
  end

  logic [oclib_pkg::Axi3DataWidth-1:0]                   wdata_next;
  logic [32:0]                                           wdata_random; // we generate 33 random bits per cycle
  logic                                                  consume_w_random;
  assign consume_w_random = (((write_state == WrBusy) && !aw_almost_full && !w_almost_full) ||
                             ((write_state == WrBurst) && !w_almost_full));

  oclib_random #(.Seed(PortNumber+102), .OutWidth(33))
  uWDATA_RANDOM (.clock(clockAxi), .reset(reset_random), .enable(consume_w_random), .out(wdata_random));

  always_comb begin
    if (cfg_local.write_mode[6])       wdata_next = {wdata[oclib_pkg::Axi3DataWidth-34:0], wdata_random};
    else if (cfg_local.write_mode[5])  wdata_next = {wdata[oclib_pkg::Axi3DataWidth-2:0], wdata[oclib_pkg::Axi3DataWidth-1]};
    else                               wdata_next = wdata;
  end

  logic                                       cfg_go_delayed;
  always_ff @(posedge clockAxi) begin
    cfg_go_delayed <= cfg_local.go; // trigger one cycle later, so any values set same time as cfg_go can be computed before start
  end

  always_ff @(posedge clockAxi) begin
    if (resetAxi) begin
      awvalid <= 1'b0;
      awid <= '0;
      awlen <= '0;
      wvalid <= 1'b0;
      awaddr_next <= '0;
      wlast <= 1'b0;
      write_state <= WrIdle;
      write_count <= '0;
      write_wait_count <= '0;
      wdata <= '0;
    end
    else begin
      awvalid <= 1'b0;
      wvalid <= 1'b0;
      if (awvalid) awid <= ((awid == cfg_local.write_max_id) ? '0 : (awid + 'd1));
      case (write_state)
        WrIdle: begin
          if (cfg_go_delayed && cfg_local.axi_enable[PortNumber]) begin
            write_state <= (cfg_local.write_mode[0] ? WrBusy : WrDone);
          end
          awaddr_next <= cfg_local.address ^ port_address;
          write_count <= '0;
          awid <= '0;
          wdata <= cfg_local.data;
        end
        WrBusy: begin
          if (!cfg_local.go) begin
            write_state <= WrIdle;
          end
          else begin
            if (!w_almost_full && !aw_almost_full) begin
              // we are doing a write
              awvalid <= 1'b1;
              wvalid <= 1'b1;
              wlast <= 1'b1;
              wdata <= wdata_next;
              awaddr <= awaddr_next;
              awaddr_next <= awaddr_next_next;
              awlen <= awlen_next;
              write_count <= (write_count + 1);
              if (awlen_next) begin
                write_state <= WrBurst;
                wlast <= 1'b0;
              end
              else if (cfg_local.op_count <= write_count) begin
                write_state <= WrDone;
              end
              else if (cfg_local.wait_states) begin
                write_state <= WrWait;
              end
            end
          end
          write_wait_count <= 'd1;
        end
        WrBurst: begin
          if (!cfg_local.go) begin
            write_state <= WrIdle;
          end
          else if (!w_almost_full) begin
            wvalid <= 1'b1;
            wdata <= wdata_next;
            if (write_wait_count >= awlen) begin
              // we are done with this burst
              wlast <= 1'b1;
              if (cfg_local.op_count < write_count) begin
                write_state <= WrDone;
              end
              else if (cfg_local.wait_states) begin
                write_state <= WrWait;
              end
              else begin
                write_state <= WrBusy;
              end
              write_wait_count <= 'd1;
            end
            else begin
              write_wait_count <= (write_wait_count + 'd1);
            end
          end
        end
        WrWait: begin
          if (!cfg_local.go) begin
            write_state <= WrIdle;
          end
          else if (write_wait_count >= cfg_local.wait_states) begin
            write_state <= WrBusy;
          end
          write_wait_count <= (write_wait_count + 'd1);
        end
        WrDone: begin
          if (!cfg_local.go) begin
            write_state <= WrIdle;
          end
        end
      endcase
    end
  end

  // **********
  // Generate READ transactions
  // **********

  logic [oclib_pkg::Axi3AddressWidth-1:0]                araddr;
  logic [7:0]                                              arid;
  logic [3:0]                                              arlen;

  assign ar.id = arid; // single source ID for now
  assign ar.addr = araddr;
  assign ar.burst = 'd1; // INCR
  assign ar.size = 'd5; // 2**5 = 32Byte
  assign ar.len = arlen;

  enum logic [2:0] { RdIdle, RdBusy, RdWait, RdDone } read_state;
  logic [31:0] read_count;
  logic [15:0] read_wait_count;
  logic [47:0] read_expect_words;
  logic [47:0] read_receive_words;
  logic        reset_signature;

  // cfg_read_mode
  // 0 : reading
  // 4 : randomize length
  // 5 : allow read reordering (changes signature algorithm)

  // compute next araddr
  logic [oclib_pkg::Axi3AddressWidth-1:0]                araddr_next;
  logic [oclib_pkg::Axi3AddressWidth-1:0]                araddr_next_next;
  logic [oclib_pkg::Axi3AddressWidth-1:0]                araddr_random;

  logic                                                  consume_ar_random;
  assign consume_ar_random = ((read_state == RdBusy) && !ar_almost_full);
  oclib_random #(.Seed(PortNumber+200), .OutWidth(oclib_pkg::Axi3AddressWidth))
  uARADDR_RANDOM (.clock(clockAxi), .reset(reset_random), .enable(consume_ar_random), .out(araddr_random));

  logic                                                  hack_mode;
  logic [1:0]                                            prev_count;
  logic [1:0]                                            next_count;
  always_ff @(posedge clockAxi) begin
    hack_mode <= (cfg_local.address_inc_mask == 'hd00d); // in hack mode, we are going to rotate bank group bits 5 & 13
  end
  assign prev_count = {araddr_next[13], araddr_next[5]};
  assign next_count = (prev_count + 2'd1);

  always_comb begin
    araddr_next_next = (araddr_next      + cfg_local.address_inc) & cfg_local.address_inc_mask;
    araddr_next_next = (hack_mode ? {'0,next_count[1],7'd0,next_count[0],5'd0} : araddr_next_next);
    araddr_next_next = (araddr_next_next ^ (araddr_random & cfg_local.address_random_mask));
    araddr_next_next = (araddr_next_next ^ port_address);
  end

  // compute next arlen
  logic [3:0]                                              arlen_next;
  logic [3:0]                                              arlen_random;

  oclib_random #(.Seed(PortNumber+201), .OutWidth(4), .LfsrWidth(33))
  uARLEN_RANDOM (.clock(clockAxi), .reset(reset_random), .enable(consume_ar_random), .out(arlen_random));

  always_comb begin
    if (cfg_local.read_mode[4])  arlen_next = (arlen_random & cfg_local.burst_length);
    else                         arlen_next = cfg_local.burst_length;
  end

  always_ff @(posedge clockAxi) begin
    if (resetAxi) begin
      arvalid <= 1'b0;
      arid <= '0;
      arlen <= '0;
      araddr_next <= '0;
      read_state <= RdIdle;
      read_count <= '0;
      read_wait_count <= '0;
      read_expect_words <= '0;
      reset_signature <= 1'b1;
    end
    else begin
      arvalid <= 1'b0;
      reset_signature <= 1'b0;
      if (arvalid) arid <= ((arid == cfg_local.read_max_id) ? '0 : (arid + 'd1));
      case (read_state)
        RdIdle: begin
          if (cfg_go_delayed && cfg_local.axi_enable[PortNumber]) begin
            read_state <= (cfg_local.read_mode[0] ? RdBusy : RdDone);
            reset_signature <= 1'b1;
          end
          araddr_next <= cfg_local.address ^ port_address;
          read_count <= '0;
          read_expect_words <= '0;
          arid <= '0;
        end
        RdBusy: begin
          if (!cfg_local.go) begin
            read_state <= RdIdle;
          end
          else begin
            if (!ar_almost_full) begin
              // we are doing a read
              arvalid <= 1'b1;
              araddr <= araddr_next;
              arlen <= arlen_next;
              araddr_next <= araddr_next_next;
              read_count <= (read_count + 1);
              read_expect_words <= (read_expect_words + 1 + arlen_next);
              if (cfg_local.op_count <= read_count) begin
                read_state <= RdDone;
              end
              else if (cfg_local.wait_states) begin
                read_state <= RdWait;
              end
           end
          end
          read_wait_count <= 'd1;
        end
        RdWait: begin
          if (!cfg_local.go) begin
            read_state <= RdIdle;
          end
          else if (read_wait_count >= cfg_local.wait_states) begin
            read_state <= RdBusy;
          end
          read_wait_count <= (read_wait_count + 'd1);
        end
        RdDone: begin
          if (!cfg_local.go) begin
            read_state <= RdIdle;
          end
        end
      endcase
    end
  end

  // **********
  // Accept write responses
  // **********

  assign bready = 1'b1;
  logic                                 bresp_error;
  always_ff @(posedge clockAxi) begin
    bresp_error <= bvalid && (b.resp != '0);
  end

  // **********
  // Accept read responses
  // **********

  localparam DatapathWords = (oclib_pkg::Axi3DataWidth/32);

  logic                                 rvalid_q;
  logic                                 rlast_q;
  logic [DatapathWords-1:0] [31:0]      rdata_q;
  logic                                 rvalid_qq;
  logic                                 rlast_qq;
  logic [(DatapathWords/4)-1:0] [31:0]  rdata_qq; // we reduce 4:1
  logic                                 rvalid_qqq;
  logic [31:0]                          rdata_qqq; // we reduce the rest of the way
  logic                                 rresp_error;
  logic [31:0]                          rdata_combined;

  assign rready = 1'b1;

  always_comb begin
    rdata_combined = '0;
    for (int w=0; w<(DatapathWords/4); w++) begin
      rdata_combined += rdata_qq[w];
    end
  end

  logic       resetStats;
  logic [9:0] contexts;
  logic [9:0] contextsMax;
  logic [9:0] contextsAveragePre;
  logic [9:0] contextsAverage;
  logic [15:0] currentCycle;
  logic [15:0] startCycle;
  logic [16:0] latencyD;
  logic [15:0] latency;
  logic [15:0] latencyMin;
  logic [15:0] latencyMax;
  logic [15:0] latencyAverage;
  logic        statReqValid;
  logic        statRespValid;
  logic        statReqValidQ;
  logic        statRespValidQ;
  logic        statRespValidQQ;

  // avg with a time constant of 1K cycles in real world, 128 in sim
  oclib_averager #(.InWidth(10), .OutWidth(10), .TimeShift(`OC_IFDEF_A_ELSE_B(SIMULATION,7,10)))
  uCONTENT_AVG (.clock(clockAxi), .reset(resetStats),
                .in(contexts), .inValid(1'b1), .out(contextsAveragePre));

  oclib_fifo #(.Width(16), .Depth(1024))
  uLATENCY_FIFO (.clock(clockAxi), .reset(resetStats),
                 .inData(currentCycle), .inValid(statReqValidQ), .inReady(),
                 .outData(startCycle), .outValid(), .outReady(statRespValidQ));

  // avg with a time constant of 1K ops in real world, 128 in sim
  oclib_averager #(.InWidth(16), .OutWidth(16), .TimeShift(`OC_IFDEF_A_ELSE_B(SIMULATION,7,10)))
  uLATENCY_AVG (.clock(clockAxi), .reset(resetStats),
                .in(latency), .inValid(statRespValidQQ), .out(latencyAverage));

  assign latencyD = ({1'b1,currentCycle} - {1'b0,startCycle});

  always_ff @(posedge clockAxi) begin
    resetStats <= (resetAxi || reset_signature);
    statReqValid <= (axi.arvalid && axiFb.arready);
    statReqValidQ <= statReqValid;
    statRespValid <= (axiFb.rvalid && axiFb.r.last && axi.rready);
    statRespValidQ <= statRespValid;
    statRespValidQQ <= statRespValidQ;
    latency <= (statRespValidQ ? latencyD[15:0] : '0); // valid with statRespValidQQ
    if (resetStats) begin
      contexts <= '0;
      contextsMax <= '0;
      contextsAverage <= '0;
      currentCycle <= '0;
      latencyMin <= 16'hffff;
      latencyMax <= '0;
    end
    else begin
      contexts <= (contexts + {'0, statReqValidQ} - {'0, statRespValidQ});
      contextsMax <= ((contexts > contextsMax) ? contexts : contextsMax);
      contextsAverage <= contextsAveragePre;
      currentCycle <= (currentCycle + 1);
      if (statRespValidQQ) begin
        latencyMax <= ((latency > latencyMax) ? latency : latencyMax);
        latencyMin <= ((latency < latencyMin) ? latency : latencyMin);
      end
    end
  end

  always_ff @(posedge clockAxi) begin
    rvalid_q <= rvalid;
    rlast_q <= r.last;
    rdata_q <= r.data;
    rresp_error <= rvalid && (r.resp != '0);
    sts_local.data <= (rvalid_q ? rdata_q : sts_local.data); // latch the last word of read data for CPU
    rvalid_qq <= rvalid_q;
    rlast_qq <= rlast_q;
    for (int w=0; w<(DatapathWords/4); w++) begin
      rdata_qq[w] <= (rdata_q[w*4] + rdata_q[w*4+1] + rdata_q[w*4+2] + rdata_q[w*4+3]);
    end
    rvalid_qqq <= rvalid_qq;
    rdata_qqq <= rdata_combined;
  end

  always_ff @(posedge clockAxi) begin
    if (resetStats) begin
      sts_local.signature <= '0;
      read_receive_words <= '0;
    end
    else begin
      if (rvalid_qqq) begin
        sts_local.signature <= (rdata_qqq + (cfg_local.read_mode[5] ?
                                             sts_local.signature : // we just add read data without rotations, so order won't matter
                                             {sts_local.signature[30:0], sts_local.signature[31]}));
        read_receive_words <= (read_receive_words + 1);
      end
    end
  end

  // Generate DONE / ERROR status
  always_ff @(posedge clockAxi) begin
    sts_local.done <= ((write_state == WrDone) && (read_state == RdDone) && (read_receive_words >= read_expect_words));
    sts_local.error <= (resetAxi ? '0 :
                        (sts_local.error | { bresp_error, rresp_error }));
    sts_local.rdata <= ((cfg_local.sts_csr_select == 'h00) ? {6'd0,contextsMax,6'd0,contextsAverage} :
                        (cfg_local.sts_csr_select == 'h01) ? {latencyMax,latencyMin} :
                        (cfg_local.sts_csr_select == 'h02) ? {16'd0,latencyAverage} :
                        'h0bad0bad);
  end

endmodule // oclib_memory_test_axi





module oclib_memory_test_csrs #(
                                parameter AxilClockFreq = 100000000 // used for the uptime counters
                                )
  (
   // AXIL
   input                                            clockAxil,
   input                                            resetAxil,
   input                                            oclib_pkg::axi_lite_s axil,
   output                                           oclib_pkg::axi_lite_fb_s axilFb,

   // CONFIG/STATUS SIGNALS
   input                                            clockAxi,
   input                                            resetAxi,
   output                                           oclib_memory_test_pkg::cfg_s cfg,
   input                                            oclib_memory_test_pkg::sts_s sts
   );

  // Some timers to debug whether the bitfile is being reset or reloaded

  logic [7:0]                                       reload_stable = 8'h0;
  logic                                             reload_detect;
  logic                                             reload_uptime_pulse;
  logic [31:0]                                      reload_uptime_prescale;
  logic [31:0]                                      reload_uptime_seconds;
  logic                                             reset_uptime_pulse;
  logic [31:0]                                      reset_uptime_prescale;
  logic [31:0]                                      reset_uptime_seconds;
  logic [31:0]                                      cycles_under_reset;
  logic [31:0]                                      cycles_since_reset;

  // first we detect the reload event, essentially generating a "power on reset"
  always_ff @(posedge clockAxil) begin
    reload_stable <= { reload_stable[6:0], 1'b1 };
    reload_detect <= !reload_stable[7];
  end

  always_ff @(posedge clockAxil) begin
    if (reload_detect) begin
      reload_uptime_prescale <= '0;
      reload_uptime_seconds <= '0;
      cycles_under_reset <= '0;
    end
    else begin
      reload_uptime_prescale <= (reload_uptime_pulse ? '0 : (reload_uptime_prescale + 'd1));
      reload_uptime_seconds <= (reload_uptime_seconds + {'0, reload_uptime_pulse});
      cycles_under_reset <= (cycles_under_reset + {'0, resetAxil});
    end
    if (resetAxil) begin
      reset_uptime_prescale <= '0;
      reset_uptime_seconds <= '0;
      cycles_since_reset <= '0;
    end
    else begin
      reset_uptime_prescale <= (reset_uptime_pulse ? '0 : (reset_uptime_prescale + 'd1));
      reset_uptime_seconds <= (reset_uptime_seconds + {'0, reset_uptime_pulse});
      cycles_since_reset <= (cycles_since_reset + 'd1);
    end
    reload_uptime_pulse <= (reload_uptime_prescale == (AxilClockFreq-2));
    reset_uptime_pulse <= (reset_uptime_prescale == (AxilClockFreq-2));
  end

  // AXIL interface in AXIL domain

  logic [9:0]                                     axil_address;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]         axil_write_data;
  logic                                           axil_write;
  logic                                           axil_write_req_toggle;
  logic                                           axil_write_resp_toggle;
  logic                                           axil_write_resp_toggle_q;
  logic                                           axil_read;
  logic                                           axil_read_req_toggle;
  logic                                           axil_read_resp_toggle;
  logic                                           axil_read_resp_toggle_q;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]         axil_read_data;
  logic                                           axil_rvalid;
  logic                                           axil_bvalid;

  logic [31:0]                                    axil_write_count;
  logic [31:0]                                    axil_read_count;

  logic [oclib_pkg::AxiLiteDataWidth-1:0]         axil_local_read_data;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]         axi_read_data_sync;

  assign axilFb.awready = axil_write;
  assign axilFb.arready = axil_read;
  assign axilFb.wready = axil_write;
  assign axilFb.rresp = '0; // we always signal successful
  assign axilFb.rdata = axil_read_data;
  assign axilFb.rvalid = axil_rvalid;
  assign axilFb.bresp = '0; // we always signal successful
  assign axilFb.bvalid = axil_bvalid;

  always_ff @(posedge clockAxil) begin
    if (resetAxil) begin
      axil_write <= 1'b0;
      axil_read <= 1'b0;
      axil_write_req_toggle <= 1'b0;
      axil_read_req_toggle <= 1'b0;
      axil_rvalid <= 1'b0;
      axil_bvalid <= 1'b0;
      axil_write_count <= '0;
      axil_read_count <= '0;
    end
    else begin
      axil_write <= (axil.awvalid && axil.wvalid && !axil_write);
      axil_read <= (axil.arvalid && !axil_read);
      axil_write_req_toggle <= (axil_write_req_toggle ^ axil_write);
      axil_read_req_toggle <= (axil_read_req_toggle ^ axil_read);
      axil_rvalid <= (axil_rvalid ? ~axil.rready : (axil_read_resp_toggle ^ axil_read_resp_toggle_q));
      axil_bvalid <= (axil_bvalid ? ~axil.bready : (axil_write_resp_toggle ^ axil_write_resp_toggle_q));
      axil_write_count <= (axil_write_count + {31'd0, axil_write});
      axil_read_count <= (axil_read_count + {31'd0, axil_read});
    end
    axil_write_resp_toggle_q <= axil_write_resp_toggle;
    axil_read_resp_toggle_q <= axil_read_resp_toggle;
    // this is just paranoia, probably don't need to latch these, but spec says they are only valid during valid...
    axil_address <= (axil_write ? axil.awaddr :
                     axil_read ? axil.araddr :
                     axil_address);
    axil_write_data <= (axil_write ? axil.wdata :
                        axil_write_data);
    // mux in the read data, mostly from AXI domain but some local
    axil_read_data <= ((axil_address[9:8] == 2'd3) ? axil_local_read_data : axi_read_data_sync);
  end

  always_comb begin
    case (axil_address[4:2])
      3'b000 : axil_local_read_data = reload_uptime_seconds;
      3'b001 : axil_local_read_data = reset_uptime_seconds;
      3'b010 : axil_local_read_data = axil_write_count;
      3'b011 : axil_local_read_data = axil_read_count;
      3'b100 : axil_local_read_data = cycles_under_reset;
      3'b101 : axil_local_read_data = cycles_since_reset;
      3'b110 : axil_local_read_data = `OCLIB_MEMORY_TEST_VERSION;
      default : axil_local_read_data = 32'h1badbabe;
    endcase // case (axil_address[4:2])
  end

  // SYNC BETWEEN CLOCK DOMAINS

  logic [9:0]                                     axi_address;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]         axi_write_data;
  logic                                           axi_write_req_toggle;
  logic                                           axi_read_req_toggle;
  logic                                           axi_write_req_toggle_q;
  logic                                           axi_read_req_toggle_q;
  logic                                           axi_write_resp_toggle;
  logic                                           axi_read_resp_toggle;
  logic                                           axi_write;
  logic                                           axi_read;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]         axi_read_data;

  oclib_synchronizer #(.Width(10), .Cycles(3))
  uAXI_ADDRESS (clockAxi, axil_address, axi_address);

  oclib_synchronizer #(.Width(oclib_pkg::AxiLiteDataWidth), .Cycles(3))
  uAXI_WRITE_DATA (clockAxi, axil_write_data, axi_write_data);

  oclib_synchronizer #(.Width(2), .Cycles(5))
  uAXI_CONTROL (clockAxi, {axil_write_req_toggle, axil_read_req_toggle}, {axi_write_req_toggle, axi_read_req_toggle});

  always_ff @(posedge clockAxi) begin
    if (resetAxi) begin
      axi_write <= 1'b0;
      axi_read <= 1'b0;
      axi_write_resp_toggle <= 1'b0;
      axi_read_resp_toggle <= 1'b0;
    end
    else begin
      axi_write <= (axi_write_req_toggle ^ axi_write_req_toggle_q);
      axi_read <= (axi_read_req_toggle ^ axi_read_req_toggle_q);
      axi_write_resp_toggle <= (axi_write_resp_toggle ^ axi_write);
      axi_read_resp_toggle <= (axi_read_resp_toggle ^ axi_read);
    end
    axi_write_req_toggle_q <= axi_write_req_toggle;
    axi_read_req_toggle_q <= axi_read_req_toggle;
  end

  oclib_synchronizer #(.Width(oclib_pkg::AxiLiteDataWidth), .Cycles(3))
  uAXIL_READ_DATA (clockAxil, axi_read_data, axi_read_data_sync);

  oclib_synchronizer #(.Width(2), .Cycles(5))
  uAXIL_CONTROL (clockAxil, {axi_write_resp_toggle, axi_read_resp_toggle}, {axil_write_resp_toggle, axil_read_resp_toggle});

  // IMPLEMENT THE MAIN CSR ARRAY IN AXI DOMAIN

  // first some debug stuff
  logic [7:0]                                       axi_reload_stable = '0;
  logic                                             axi_reload_detect;
  always_ff @(posedge clockAxi) begin
    axi_reload_stable <= { axi_reload_stable[6:0], 1'b1 };
    axi_reload_detect <= !axi_reload_stable[7];
  end
                                                                                   // to check the AXI clock speed
  logic [31:0]                                      axi_cycles_under_reset = '0;
  logic [31:0]                                      axi_cycles_since_reset;
  always_ff @(posedge clockAxi) begin
    axi_cycles_under_reset <= (axi_reload_detect ? '0 : (axi_cycles_under_reset + {'0, resetAxi}));
    axi_cycles_since_reset <= (resetAxi ? '0 : (axi_cycles_since_reset + 'd1));
  end

  logic [31:0] axi_select; // we reserve space for N * 4Byte "individual" CSRs
  logic        axi_select_write_data; // these two indicate that address matches a range for larger CSRs
  logic        axi_select_read_data;

  // data buffer related, we reserve space for up to 256Byte of buffer
  localparam DataBufferWords = (oclib_pkg::Axi3DataWidth /
                                oclib_pkg::AxiLiteDataWidth); // assume data width is multiple of oclib_pkg::AxiLite...
  logic [DataBufferWords-1:0]                                   axi_word_select; // reserve space for N * 4Byte "individual" CSRs
  logic [oclib_pkg::AxiLiteDataWidth-1:0]                       muxed_write_data;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]                       muxed_write_data_q;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]                       muxed_read_data;
  logic [oclib_pkg::AxiLiteDataWidth-1:0]                       muxed_read_data_q;
  logic [DataBufferWords-1:0] [oclib_pkg::AxiLiteDataWidth-1:0] cfg_data_as_words;
  logic [DataBufferWords-1:0] [oclib_pkg::AxiLiteDataWidth-1:0] sts_data_as_words;

  logic                                              timer_run;
  logic [7:0]                                        prescale_counter;
  logic                                              prescale_pulse;
  logic [31:0]                                       sts_op_cycles;
  logic                                              cfg_prescale;

  assign cfg.data = cfg_data_as_words; // convert between byte-oriented and word-oriented formats
  assign sts_data_as_words = sts.data; // convert between byte-oriented and word-oriented formats

  always_ff @(posedge clockAxi) begin
    if (resetAxi) begin
      cfg_prescale <= 1'b0;
      cfg.go <= 1'b0;
      cfg.write_mode <= '0;
      cfg.read_mode <= '0;
      cfg.op_count <= '0;
      cfg.axi_enable <= '0;
      cfg.address <= '0;
      cfg.address_inc <= '0;
      cfg.address_inc_mask <= {oclib_pkg::Axi3AddressWidth{1'b1}};
      cfg.address_random_mask <= {oclib_pkg::Axi3AddressWidth{1'b0}};
      cfg.sts_port_select <= '0;
      cfg.sts_csr_select <= '0;
      cfg.address_port_shift <= '0;
      cfg.address_port_mask <= {oclib_memory_test_pkg::MaxAxi3Width{1'b1}};
      cfg.wait_states <= '0;
      cfg.burst_length <= '0;
      cfg.read_max_id <= '0;
      cfg.write_max_id <= '0;
      cfg_data_as_words <= '0;
    end
    else begin
      if (axi_write && axi_select[0]) begin
        cfg_prescale <= axi_write_data[1];
        cfg.go <= axi_write_data[0];
        cfg.write_mode <= axi_write_data[15:8];
        cfg.read_mode <= axi_write_data[23:16];
      end
      if (axi_write && axi_select[1]) begin
        cfg.op_count <= axi_write_data;
      end
      if (axi_write && axi_select[2]) begin
        cfg.axi_enable <= axi_write_data;
      end
      if (axi_write && axi_select[4]) begin
        cfg.address[31:0] <= axi_write_data;
      end
      if (axi_write && axi_select[5]) begin
        cfg.address[oclib_pkg::Axi3AddressWidth-1:32] <= axi_write_data;
      end
      if (axi_write && axi_select[6]) begin
        cfg.address_inc[31:0] <= axi_write_data;
      end
      if (axi_write && axi_select[7]) begin
        cfg.address_inc[oclib_pkg::Axi3AddressWidth-1:32] <= axi_write_data;
      end
      if (axi_write && axi_select[8]) begin
        cfg.address_inc_mask[31:0] <= axi_write_data;
      end
      if (axi_write && axi_select[9]) begin
        cfg.address_inc_mask[oclib_pkg::Axi3AddressWidth-1:32] <= axi_write_data;
      end
      if (axi_write && axi_select[16]) begin
        cfg.wait_states <= axi_write_data;
      end
      if (axi_write && axi_select[17]) begin
        cfg.burst_length <= axi_write_data;
      end
      if (axi_write && axi_select[18]) begin
        cfg.write_max_id <= axi_write_data[7:0];
        cfg.read_max_id <= axi_write_data[15:8];
      end
      if (axi_write && axi_select[19]) begin
        cfg.address_port_shift <= axi_write_data[oclib_memory_test_pkg::MaxAxi3Width-1:0];
        cfg.address_port_mask <= axi_write_data[oclib_memory_test_pkg::MaxAxi3Width+15:16];
      end
      if (axi_write && axi_select[20]) begin
        cfg.address_random_mask[31:0] <= axi_write_data;
      end
      if (axi_write && axi_select[21]) begin
        cfg.address_random_mask[oclib_pkg::Axi3AddressWidth-1:32] <= axi_write_data;
      end
      if (axi_write && axi_select[22]) begin
        cfg.sts_csr_select <= axi_write_data[7:0];
        cfg.sts_port_select <= axi_write_data[23:16];
      end
      if (axi_write && axi_select_write_data) begin
        cfg_data_as_words[axi_address[7:2]] <= axi_write_data;
      end
    end
    axi_select <= '0;
    if (axi_address[9:8] == 2'd0) begin
      axi_select[axi_address[7:2]] <= 1'b1; // set just the bit corresponding to address of a 32-bit (4 byte) CSR
    end
    axi_word_select <= '0;
    axi_word_select[axi_address[7:2]] <= 1'b1;
    axi_select_write_data <= (axi_address[9:8] == 2'd1); // addresses 256-511
    axi_select_read_data <= (axi_address[9:8] == 2'd2); // addresses 512-767

    axi_read_data <=
             (({oclib_pkg::AxiLiteDataWidth{axi_select[0]}} & { sts.done, 7'd0,
                                                                cfg.read_mode, cfg.write_mode,
                                                                6'd0, cfg_prescale, cfg.go}) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[1]}} & { '0, cfg.op_count }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[2]}} & { '0, cfg.axi_enable }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[4]}} & { cfg.address[31:0] }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[5]}} & { '0, cfg.address[oclib_pkg::Axi3AddressWidth-1:32] }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[6]}} & { cfg.address_inc[31:0] }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[7]}} & { '0, cfg.address_inc[oclib_pkg::Axi3AddressWidth-1:32] }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[8]}} & { cfg.address_inc_mask[31:0] }) | // 0x20
              ({oclib_pkg::AxiLiteDataWidth{axi_select[9]}} & { '0, cfg.address_inc_mask[oclib_pkg::Axi3AddressWidth-1:32] }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[10]}} & { sts.signature }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[11]}} & { sts.error }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[12]}} & { sts_op_cycles }) | // 0x30
              ({oclib_pkg::AxiLiteDataWidth{axi_select[13]}} & { 32'h4d454d54 }) | // MEMT
              ({oclib_pkg::AxiLiteDataWidth{axi_select[14]}} & { axi_cycles_under_reset }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[15]}} & { axi_cycles_since_reset }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[16]}} & { '0, cfg.wait_states }) | // 0x40
              ({oclib_pkg::AxiLiteDataWidth{axi_select[17]}} & { '0, cfg.burst_length }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[18]}} & { '0, cfg.read_max_id, cfg.write_max_id }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[19]}} & ( {cfg.address_port_mask,16'd0} | cfg.address_port_shift)) | // 0x4c
              ({oclib_pkg::AxiLiteDataWidth{axi_select[20]}} & { cfg.address_random_mask[31:0] }) | // 0x50
              ({oclib_pkg::AxiLiteDataWidth{axi_select[21]}} & { '0, cfg.address_random_mask[oclib_pkg::Axi3AddressWidth-1:32] }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[22]}} & { 8'd0, cfg.sts_port_select, 8'd0, cfg.sts_csr_select }) |
              ({oclib_pkg::AxiLiteDataWidth{axi_select[23]}} & { sts.rdata }) | // 0x5c
              ({oclib_pkg::AxiLiteDataWidth{axi_select_write_data}} & { muxed_write_data_q }) | // 0x100
              ({oclib_pkg::AxiLiteDataWidth{axi_select_read_data}} & { muxed_read_data_q }) | // 0x200
              {oclib_pkg::AxiLiteDataWidth{1'b0}}
              );

    muxed_write_data_q <= muxed_write_data;
    muxed_read_data_q <= muxed_read_data;
    timer_run <= (cfg.go && !sts.done);
    prescale_counter <= (timer_run ? (prescale_counter + 'd1) : '0);
    prescale_pulse <= timer_run && ((!cfg_prescale) || (&prescale_counter)); // "pulse" always if !cfg_prescale, else every 256
    sts_op_cycles <= (cfg.go ? (prescale_pulse ? (sts_op_cycles + 'd1) : sts_op_cycles) : '0);
  end // always_ff @

  always_comb begin
    muxed_write_data = '0;
    for (int word=0; word<DataBufferWords; word++) begin
      if (axi_word_select[word]) muxed_write_data = muxed_write_data | cfg_data_as_words[word];
    end
  end

  always_comb begin
    muxed_read_data = '0;
    for (int word=0; word<DataBufferWords; word++) begin
      if (axi_word_select[word]) muxed_read_data = muxed_read_data | sts_data_as_words[word];
    end
  end

`ifdef USER_MEMORY_TEST_ILA

  ila_0 csr_axil (
                  .clk(clockAxil), // input wire clk
                  .probe0({axil,
                           axilFb,
                           axil_address,
                           axil_read_data,
                           axil_write_data}), // input wire [511:0]  probe0
                  .probe1({axil_read_req_toggle,
                           axil_read_resp_toggle,
                           axil_read,
                           axil_write_req_toggle,
                           axil_write_resp_toggle,
                           axil_write}) // input wire [127:0]  probe1);
                  );

  ila_0 csr_axi (
                  .clk(clockAxi), // input wire clk
                  .probe0({cfg_data_as_words,
                           axi_write_select,
                           axi_write_address}), // input wire [511:0]  probe0
                  .probe1({axi_write_select_cfg_data,
                           axi_write,
                           resetAxi,
                           muxed_cfg_data,
                           axi_read_select_cfg_data,
                           axi_read_select_sts_data}) // input wire [127:0]  probe1);
  );
`endif

endmodule // oclib_memory_test_csrs
