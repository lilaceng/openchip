
// SPDX-License-Identifier: MPL-2.0

module oclib_word_to_bc #(
                        parameter WordW = 64,
                        parameter ShiftFanout = 16,
                        parameter ResetSync = 0
                        )
  (
   input             clock,
   input             reset,
   input [WordW-1:0] wordData,
   input             wordValid,
   output logic      wordReady,
   output            oclib_pkg::bc_s bc,
   input             oclib_pkg::bc_fb_s bcFb
   );

  logic              resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  localparam integer WordBytes = ((WordW+7)/8);
  localparam ByteCounterW = $clog2(WordBytes);

  // Datapath Logic
  logic [ByteCounterW-1:0] byteCounter;
  logic [7:0]              byteSelected;
  logic [0:WordBytes-1] [7:0] wordAsBytes; // note reversed order, we want byte 0 to be MSBs
  assign wordAsBytes = wordData;
  always_ff @(posedge clock) begin
    byteSelected <= wordAsBytes[byteCounter];
  end

  // Control Logic
  logic                     byteCounterTC;
  enum                      logic [1:0] { StIdle, StLoad, StNext, StDone } state;

  always_ff @(posedge clock) begin
    byteCounterTC <= (byteCounter == (WordBytes-1));
    if (resetQ) begin
      bc.data <= '0;
      bc.valid <= 1'b0;
      wordReady <= 1'b0;
      byteCounter <= '0;
      state <= StIdle;
    end
    else begin
      wordReady <= 1'b0;
      bc.valid <= (bc.valid && !bcFb.ready);
      case (state)
        StIdle : begin
          byteCounter <= '0;
          if (wordValid && ~bc.valid && ~wordReady) begin
            state <= StLoad;
          end
        end
        StLoad : begin
          bc.data <= byteSelected;
          bc.valid <= 1'b1;
          byteCounter <= (byteCounter + 'd1);
          state <= (byteCounterTC ? StDone : StNext);
        end
        StNext : begin // during this cycle, the byteCounter is used to latch next into byteSelected
          if (bcFb.ready) begin
            state <= StLoad;
          end
        end
        StDone : begin
          if (bcFb.ready) begin
            wordReady <= 1'b1;
            state <= StIdle;
          end
        end
      endcase // case (state)
    end // else: !if(resetQ)
  end // always_ff @ (posedge clock)

endmodule // oclib_word_to_bc
