
// SPDX-License-Identifier: MPL-2.0

module oclib_abbc_to_abc (
   input  oclib_pkg::abbc_s abbcIn,
   output oclib_pkg::abbc_s abbcOut,
   output oclib_pkg::abc_s abcFromAbbc,
   input  oclib_pkg::abc_fb_s abcFromAbbcFb,
   input  oclib_pkg::abc_s abcToAbbc,
   output oclib_pkg::abc_fb_s abcToAbbcFb
   );

  assign abcFromAbbc.data = abbcIn.data;
  assign abcFromAbbc.req = abbcIn.req;
  assign abbcOut.ack = abcFromAbbcFb.ack;

  assign abbcOut.data = abcToAbbc.data;
  assign abbcOut.req = abcToAbbc.req;
  assign abcToAbbcFb.ack = abbcIn.ack;

endmodule // oclib_abbc_to_abc
