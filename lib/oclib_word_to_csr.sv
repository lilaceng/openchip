
// SPDX-License-Identifier: MPL-2.0

module oclib_word_to_csr #(
                         parameter AddressSpaces = 1,
                         parameter CsrAddressW = oclib_pkg::CsrAddressW,
                         parameter CsrDataW = oclib_pkg::CsrDataW,
                         parameter CsrSpaceW = oclib_pkg::CsrSpaceW,
                         parameter CsrCommandW = oclib_pkg::CsrCommandW,
                         parameter CsrStatusW = oclib_pkg::CsrStatusW,
                         parameter WordToCsrW = oclib_pkg::WordToCsrW,
                         parameter WordFromCsrW = oclib_pkg::WordFromCsrW,
                         parameter ResetSync = 0
                         )
  (
   input                            clock,
   input                            reset,
   input [WordToCsrW-1:0]           wordToCsrData,
   input                            wordToCsrValid,
   output logic                     wordToCsrReady,
   output logic [WordFromCsrW-1:0]  wordFromCsrData,
   output logic                     wordFromCsrValid,
   input                            wordFromCsrReady,
   output logic [AddressSpaces-1:0] csrSelect,
   output                           oclib_pkg::csr_s csr,
   input                            oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb
   );

  logic                             resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  logic [CsrSpaceW-1:0]             csrSpace;
  logic [CsrCommandW-1:0]           csrCommand;
  logic [CsrDataW-1:0]              csrReadDataLatched;
  logic                             csrRead;
  logic                             csrWrite;
  logic [CsrStatusW-1:0]            csrStatus;

  assign csrCommand = wordToCsrData[CsrDataW+CsrAddressW+CsrSpaceW +: CsrCommandW];
  assign csrSpace = wordToCsrData[CsrDataW+CsrAddressW +: CsrSpaceW];
  assign csr.address = wordToCsrData[CsrDataW +:CsrAddressW];
  assign csr.wdata = wordToCsrData[0 +: CsrDataW];
  assign csr.write = csrWrite;
  assign csr.read = csrRead;

  assign wordFromCsrData[CsrDataW +: CsrStatusW] = csrStatus;
  assign wordFromCsrData[0 +: CsrDataW] = csrReadDataLatched;

  oclib_pkg::csr_fb_s                 csrFbSelect;
  assign csrFbSelect = csrFb[csrSpace];

  enum                      logic [1:0] { StIdle, StCommand, StResponse } state;

  always_ff @(posedge clock) begin
    csrReadDataLatched <= ((csrFbSelect.ready) ? csrFbSelect.rdata : csrReadDataLatched);
    if (resetQ) begin
      csrRead <= 1'b0;
      csrWrite <= 1'b0;
      csrSelect <= '0;
      csrStatus <= '0;
      wordToCsrReady <= 1'b0;
      wordFromCsrValid <= 1'b0;
      state <= StIdle;
    end
    else begin
      wordToCsrReady <= 1'b0;
      wordFromCsrValid <= 1'b0;
      case (state)
        StIdle : begin
          csrSelect <= '0;
          if (wordToCsrValid && ~wordToCsrReady) begin
            if (csrCommand == oclib_pkg::CsrCommandRead) begin
              csrRead <= 1'b1;
              csrSelect[csrSpace] <= 1'b1;
              state <= StCommand;
            end
            else if (csrCommand == oclib_pkg::CsrCommandWrite) begin
              csrWrite <= 1'b1;
              csrSelect[csrSpace] <= 1'b1;
              state <= StCommand;
            end
            else if (csrCommand == oclib_pkg::CsrCommandStatus) begin
              wordToCsrReady <= 1'b1;
              wordFromCsrValid <= 1'b1;
              state <= StResponse;
            end
            else if (csrCommand == oclib_pkg::CsrCommandClear) begin
              wordToCsrReady <= 1'b1;
              csrStatus <= '0;
            end
            else begin
              // invalid command error
              wordToCsrReady <= 1'b1;
              csrStatus[7] <= 1'b1;
            end
          end
        end
        StCommand : begin
          if (csrFbSelect.ready) begin
            csrRead <= 1'b0;
            csrWrite <= 1'b0;
            csrStatus[0] <= csrFbSelect.error;
            wordToCsrReady <= 1'b1;
            state <= (csrRead ? StResponse : StIdle);
            wordFromCsrValid <= csrRead;
          end
        end
        StResponse : begin
          if (wordFromCsrReady) begin
            wordFromCsrValid <= 1'b0;
            state <= StIdle;
          end
        end
      endcase // case (state)
    end
  end

endmodule
