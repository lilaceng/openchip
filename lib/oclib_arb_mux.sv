
// SPDX-License-Identifier: MPL-2.0

module oclib_arb_mux #(
                       parameter integer Inputs = 2,
                       parameter integer Width = 32
                       )
  (
   input                                 clock,
   input                                 reset,
   input [Inputs-1:0] [Width-1:0]        inData,
   input [Inputs-1:0]                    inValid,
   output logic [Inputs-1:0]             inReady,
   output logic [Width-1:0]              outData,
   output logic                          outValid,
   input                                 outReady
   );

  localparam PointerWidth = $clog2(Inputs);

  logic [Inputs-1:0]                     grant;
  logic [Width-1:0]                      arbData;
  logic                                  arbValid;
  logic                                  arbReady;

  oclib_round_robin #(.Inputs(Inputs))
  uROUND_ROBIN (.clock(clock), .reset(reset), .request(inValid), .grant(grant));

  always_comb begin
    arbData = '0;
    for (int i=0; i<Inputs; i++) begin
      arbData = (arbData | ({Width{grant[i]}} & inData[i]));
    end
    arbValid = (|inValid);
    inReady = ({Inputs{arbReady}} & grant);
  end

  oclib_ready_valid_retime #(.Width(Width))
  uRETIME (.clock(clock), .reset(reset),
           .inData(arbData), .inValid(arbValid), .inReady(arbReady),
           .outData(outData), .outValid(outValid), .outReady(outReady));

endmodule // oclib_arb_mux


