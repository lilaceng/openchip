
// SPDX-License-Identifier: MPL-2.0

module oclib_csr_to_drp #(
                        parameter DataW = 16,
                        parameter AddressW = 16,
                        parameter ResetSync = 0
                        )
  (
   input  clock,
   input  reset,
   input  csrSelect,
   input  oclib_pkg::csr_s csr,
   output oclib_pkg::csr_fb_s csrFb,
   output oclib_pkg::drp_s drp,
   input  oclib_pkg::drp_fb_s drpFb
   );

  logic          resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  enum           logic [1:0] { StIdle, StWrite, StRead, StDone } state;

  always_ff @(posedge clock) begin
    if (resetQ) begin
      drp <= '0;
      csrFb <= '0;
      state <= StIdle;
    end
    else begin
      case (state)
        StIdle : begin
          if (csr.write && csrSelect) begin
            drp.enable <= 1'b1;
            drp.write <= 1'b1;
            drp.address <= csr.address[AddressW-1:0];
            drp.wdata <= csr.wdata[DataW-1:0];
            state <= StWrite;
          end
          else if (csr.read && csrSelect) begin
            drp.enable <= 1'b1;
            drp.address <= csr.address[AddressW-1:0];
            state <= StRead;
          end
        end
        StWrite : begin
          drp <= '0;
          if (drpFb.ready) begin
            csrFb.ready <= 1'b1;
            state <= StDone;
          end
        end
        StRead : begin
          drp <= '0;
          if (drpFb.ready) begin
            csrFb.rdata <= drpFb.rdata;
            csrFb.ready <= 1'b1;
            state <= StDone;
          end
        end
        StDone : begin
          csrFb.ready <= 1'b0;
          if ((!csr.read) && (!csr.write)) begin
            state <= StIdle;
          end
        end
      endcase
    end
  end

endmodule // oclib_csr_to_drp
