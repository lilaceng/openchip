
// SPDX-License-Identifier: MPL-2.0

module oc_chipmon #(
                    // Use these parameters for more vendor-independent, automatic clocking
                    parameter real    ClockHz = 100000000,
                    parameter bit     InternalReference = 1,
                    parameter real    WarningTempHigh = 90.0, // when to assert warning condition (throttling)
                    parameter real    WarningTempLow = 85.0, // when to deassert warning condition
                    parameter real    ErrorTempHigh = 100.0, // when to assert error condition
                    parameter real    ErrorTempLow = 90.0, // when to deassert error condition
                    parameter real    WarningVccIntHigh = 0.880,
                    parameter real    WarningVccIntLow = 0.820,
                    parameter real    WarningVccAuxHigh = 1.850,
                    parameter real    WarningVccAuxLow = 1.750,
                    parameter real    WarningVccBramHigh = 0.880,
                    parameter real    WarningVccBramLow = 0.820,
                    parameter bit     AbbcEnable = 0,
                    parameter integer AbbcNumber = 0,
                    parameter bit     ResetSync = 0
                 )
  (
   input        clock,
   input        reset,
   input        scl = 1'b1,
   output logic sclTristate,
   input        sda = 1'b1,
   output logic sdaTristate,
   output logic thermalWarning,
   output logic thermalError,
   input        oclib_pkg::abbc_s abbcIn = '0,
   output       oclib_pkg::abbc_s abbcOut
   );

`ifdef OC_LIBRARY_ULTRASCALE_PLUS

  // See Xilinx UG580

  oclib_pkg::drp_s    drp;
  oclib_pkg::drp_fb_s drpFb;

  localparam integer NumCsr = 3;
  localparam integer CsrAddressControl = 1;
  localparam integer CsrAddressStatus = 2;
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdChipmon,
                                    15'd0, InternalReference };
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;
  logic                     alertTristate;

  if (AbbcEnable) begin : abbc

    logic                     resetQ;
    oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

    localparam AddressSpaces = 2;
    logic [AddressSpaces-1:0] csrSelect;
    oclib_pkg::csr_s                        csr;
    oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

    oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
    uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

    // Implement address space 0
    oclib_csr_array #(.NumCsr(NumCsr),
                      .CsrRwBits   ({ 32'h00000000, 32'h00000001, 32'h00000000 }),
                      .CsrRoBits   ({ 32'hffff0001, 32'h70000000, 32'h00000000 }),
                      .CsrFixedBits({ 32'h00000000, 32'h00000000, 32'hffffffff }),
                      .CsrInitBits ({ 32'h00000000, 32'h00000000, CsrId }))
    uCSR (clock, resetQ, csrSelect[0], csr, csrFb[0], csrConfig, csrStatus);

    // Implement address space 1
    oclib_csr_to_drp #(.DataW(16), .AddressW(8))
    uCSR_TO_DRP (clock, resetQ, csrSelect[1], csr, csrFb[1], drp, drpFb);

  end else begin
    assign drp.enable = 1'b0;
    assign drp.address = '0;
    assign drp.write = 1'b0;
    assign drp.wdata = '0;
    assign csrConfig = '0;
  end

  logic        softReset;
  logic [15:0] alarm;
  logic        jtagLocked;
  logic        jtagModified;
  logic        jtagBusy;

  // SYSMONE4 temp -> code
  function integer TempToCode (input real temp);
    if (InternalReference) return (((temp + 280.23087870) * 65536.0) / 507.5921310);
    else                   return (((temp + 279.42657680) * 65536.0) / 509.3140064);
  endfunction // TempToCode
  function integer VoltToCode (input real volt);
    return ((volt * 65536.0) / 3.0);
  endfunction // TempToCode

  localparam logic [15:0] AlarmTempHighCode = TempToCode(WarningTempHigh);
  localparam logic [15:0] AlarmTempLowCode = TempToCode(WarningTempLow) & 16'hfffe;
  localparam logic [15:0] OverTempHighCode = TempToCode(ErrorTempHigh);
  localparam logic [15:0] OverTempLowCode = TempToCode(ErrorTempLow) & 16'hfffe; // clear LSB to use hysteresis mode

  localparam logic [15:0] VccIntHighCode = VoltToCode(WarningVccIntHigh);
  localparam logic [15:0] VccIntLowCode = VoltToCode(WarningVccIntLow);
  localparam logic [15:0] VccAuxHighCode = VoltToCode(WarningVccAuxHigh);
  localparam logic [15:0] VccAuxLowCode = VoltToCode(WarningVccAuxLow);
  localparam logic [15:0] VccBramHighCode = VoltToCode(WarningVccBramHigh);
  localparam logic [15:0] VccBramLowCode = VoltToCode(WarningVccBramLow);

  localparam logic [7:0]  AdcClockDivider = (ClockHz / 4500000); // target 4.5MHz

  wire [4:0]              muxaddr;
  wire [5:0]              channel;
  wire                    eoc, eos, busy;
  wire [15:0]             adcdata;

  SYSMONE4 #(
             .INIT_40(16'h2000), // average over 64 samples
             .INIT_41(16'h2000), // Continuous Sequence
             .INIT_42({AdcClockDivider, 8'h00}),
             .INIT_43(16'h0000),
             .INIT_44(16'h0000),
             .INIT_48(16'h4701), // Temp, VCCint, VCCaux, VCCbram, calibration
             .INIT_4B(16'h4700), // Averaging Temp, VCCint, VCCaux, VCCbram
             .INIT_50(AlarmTempHighCode),
             .INIT_51(VccIntHighCode),
             .INIT_52(VccAuxHighCode),
             .INIT_53(OverTempHighCode),
             .INIT_54(AlarmTempLowCode),
             .INIT_55(VccIntLowCode),
             .INIT_56(VccAuxLowCode),
             .INIT_57(OverTempLowCode),
             .INIT_58(VccBramHighCode),
             .INIT_5C(VccBramLowCode),
             .SIM_MONITOR_FILE({`OC_ROOT, "/top/oc_chipmon.sim.txt"}),
             .SIM_DEVICE(string'(`OC_LIBRARY_STRING))
             )
  uMONITOR (
            .DO(drpFb.rdata),
            .DI(drp.wdata),
            .DADDR(drp.address[7:0]),
            .DEN(drp.enable),
            .DWE(drp.write),
            .DCLK(clock),
            .DRDY(drpFb.ready),
            .RESET(reset || softReset),
            .CONVST(1'b0),
            .CONVSTCLK(1'b0),
            .VP(),
            .VN(),
            .VAUXP(),
            .VAUXN(),
            .I2C_SCLK(scl), // for connection to I2C/SMBus
            .I2C_SCLK_TS(sclTristate),
            .I2C_SDA(sda), // for connection to I2C/SMBus
            .I2C_SDA_TS(sdaTristate),
            .SMBALERT_TS(alertTristate), // when low, indicates ALERT, for connection to SMBus (generally on PCIe)
            .ADC_DATA(adcdata),
            .ALM(alarm),
            .OT(thermalError),
            .MUXADDR(muxaddr),
            .CHANNEL(channel),
            .EOC(eoc),
            .EOS(eos),
            .BUSY(busy),
            .JTAGLOCKED(jtagLocked),
            .JTAGMODIFIED(jtagModified),
            .JTAGBUSY(jtagBusy)
            );

  assign softReset = csrConfig[CsrAddressControl][0];
  // these are actually available via DRP, so maybe CsrAddressStatus should go away?
  assign csrStatus[CsrAddressStatus][0] = thermalError;
  assign csrStatus[CsrAddressStatus][31:16] = alarm;
  assign csrStatus[CsrAddressControl][28] = jtagLocked;
  assign csrStatus[CsrAddressControl][29] = jtagModified;
  assign csrStatus[CsrAddressControl][30] = jtagBusy;
  assign thermalWarning = alarm[0];

  `ifdef OC_CHIPMON_DEBUG

  wire [511:0] dummy0;
  wire [255:0] dummy1;
  assign dummy0 = '0;
  assign dummy1 = '0;

  ila_0 uILA (
              .clk(clock),
//              .trig_in(1'b0),
//              .trig_out_ack(1'b0),
              .probe0(dummy0),
              .probe1({dummy1,
                       drp,
                       drpFb,
                       reset,
                       softReset,
                       scl,
                       sclTristate,
                       sda,
                       sdaTristate,
                       jtagLocked,
                       jtagModified,
                       jtagBusy,
                       adcdata,
                       muxaddr,
                       channel,
                       eoc,
                       eos,
                       busy,
                       thermalError,
                       thermalWarning,
                       alertTristate,
                       alarm
                       })
              );

  `endif

`else // !`ifdef OC_LIBRARY_ULTRASCALE_PLUS

  // BEHAVIORAL IMPLEMENTATION

  // Note this implementation provides only the most basic functionality (i.e. legal behavior at outputs). It doesn't
  // implement any CSRs.  The indended use for this mode is bringing up a design in a vendor-neutral fashion, without
  // requiring any vendor libraries (i.e. just generic SystemVerilog simulation).

  assign abcOut = '0;
  assign sclTristate = 1'b1;
  assign sdaTristate = 1'b1;
  assign alertTristate = 1'b1;
  assign thermalWarning = 1'b0;
  assign thermalError = 1'b0;

`endif // !`ifdef OC_LIBRARY_ULTRASCALE_PLUS

`ifdef SIM_ANNOUNCE
  `include "../lib/oclib_defines.vh"
  initial begin
    `OC_ANNOUNCE_MODULE(oc_chipmon);
    `OC_ANNOUNCE_PARAM_INTEGER(ClockHz);
    `OC_ANNOUNCE_PARAM_BIT(InternalReference);
    `OC_ANNOUNCE_PARAM_REAL(WarningTempHigh);
    `OC_ANNOUNCE_PARAM_REAL(WarningTempLow);
    `OC_ANNOUNCE_PARAM_REAL(ErrorTempHigh);
    `OC_ANNOUNCE_PARAM_REAL(ErrorTempLow);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_chipmon
