
// SPDX-License-Identifier: MPL-2.0

module oc_user #(
                 parameter integer Abbcs = 1,
                 parameter integer AbbcNumber = 0,
                 parameter integer Clocks = 1,
                 parameter integer AxiMemoryPorts = 0,
                 `OC_CREATE_SAFE_WIDTH(AxiMemoryPorts),
                 parameter integer CmacCount = 0,
                 `OC_CREATE_SAFE_WIDTH(CmacCount)
                 )
  (
   input                     clockTop,
   input                     resetTop,
   input [Clocks-1:0]        clock,
   input [Clocks-1:0]        reset,
`ifdef TARGET_CMAC_COUNT
   input [CmacCountSafe-1:0] clockCmac,
   output                    oclib_pkg::axi4st_512_s [CmacCountSafe-1:0] axiCmacTx,
   input                     oclib_pkg::axi4st_512_fb_s [CmacCountSafe-1:0] axiCmacTxFb,
   input                     oclib_pkg::axi4st_512_s [CmacCountSafe-1:0] axiCmacRx,
   output                    oclib_pkg::axi4st_512_fb_s [CmacCountSafe-1:0] axiCmacRxFb,
`endif
   output                    oclib_pkg::axi3_s [AxiMemoryPortsSafe-1:0] axiMemory,
   input                     oclib_pkg::axi3_fb_s [AxiMemoryPortsSafe-1:0] axiMemoryFb,
   input                     oclib_pkg::ring_status_s ringStatus,
   output                    oclib_pkg::user_status_s userStatus,
   input                     oclib_pkg::abbc_s abbcIn [Abbcs-1:0],
   output                    oclib_pkg::abbc_s abbcOut [Abbcs-1:0]
   );

  `OC_STATIC_ASSERT(Abbcs == (1 + (AxiMemoryPorts?1:0) + CmacCount));

  localparam InWordW = 256;
  localparam OutWordW = 72;

  logic [InWordW-1:0]  inWordData;
  logic                inWordValid;
  logic                inWordReady;
  logic [OutWordW-1:0] outWordData;
  logic                outWordValid;
  logic                outWordReady;

  oclib_abbc_to_word #(.WordFromAbbcW(InWordW),
                       .WordToAbbcW(OutWordW))
  uABBC_TO_WORD (.clock(clock[0]), .reset(reset[0]),
                 .abbcIn(abbcIn[0]), .abbcOut(abbcOut[0]),
                 .wordFromAbbcData(inWordData),
                 .wordFromAbbcValid(inWordValid),
                 .wordFromAbbcReady(inWordReady),
                 .wordToAbbcData(outWordData),
                 .wordToAbbcValid(outWordValid),
                 .wordToAbbcReady(outWordReady));

  // pretend to do some work
  logic [OutWordW-1:0] fifoWordData_d;
  always_comb begin
    fifoWordData_d = '0;
    for (int i=0; i<InWordW; i++) begin
      fifoWordData_d[i % OutWordW] ^= inWordData[i];
    end
  end

  // flop the pretend work, to go into FIFO (also flop FIFO's almostFull and turn it into inWordReady)
  logic [OutWordW-1:0] fifoWordData;
  logic                fifoWordValid;
  logic                fifoAlmostFull;
  always_ff @(posedge clock) begin
    fifoWordData <= fifoWordData_d;
    fifoWordValid <= inWordValid && inWordReady; // only accept if asserting ready, as per ready/valid protocol
    inWordReady <= !fifoAlmostFull;
  end

  // FIFO up the output
  oclib_fifo #(.Width(OutWordW), .Depth(32), .AlmostFull(28))
  uFIFO (.clock(clock), .reset(reset), .almostFull(fifoAlmostFull),
         .inData(fifoWordData), .inValid(fifoWordValid), .inReady(), // note not using ready/valid here
         .outData(outWordData), .outValid(outWordValid), .outReady(outWordReady));

  assign userStatus.error = 1'b0;
  assign userStatus.done = 1'b0;

  // Memory BIST engine for the axi memory ports
  if (AxiMemoryPorts) begin : memtest

    logic                                   csrSelect;
    oclib_pkg::csr_s                        csr;
    oclib_pkg::csr_fb_s                     csrFb;

    oclib_abbc_to_csr #(.AddressSpaces(1))
    uABBC_TO_CSR (clockTop, resetTop, abbcIn[1], abbcOut[1], csrSelect, csr, csrFb);

    oclib_pkg::axi_lite_s axil;
    oclib_pkg::axi_lite_fb_s axilFb;

    oclib_csr_to_axi_lite #(.AddressW(10))
    uCSR_TO_AXIL (clockTop, resetTop, csrSelect, csr, csrFb, axil, axilFb);

    oclib_memory_test #(.AxiPorts(AxiMemoryPorts))
    uMEMORY_TEST (.reset(reset), .clockAxil(clockTop), .clockAxi(clock),
                  .axil(axil), .axilFb(axilFb),
                  .axi(axiMemory), .axiFb(axiMemoryFb));
  end

`ifdef TARGET_CMAC_COUNT
  // Packet Gen/Chk for the CMAC ports
  for (genvar i=0; i<CmacCount; i++) begin : cmac
    oclib_cmac_test #(.Instance(i))
    uCMAC (
           .clock(clockTop), .reset(reset),
           .abbcIn(abbcIn[1+(AxiMemoryPorts?1:0)+i]), .abbcOut(abbcOut[1+(AxiMemoryPorts?1:0)+i]),
           .clockCmac(clockCmac[i]),
           .axiCmacTx(axiCmacTx[i]), .axiCmacTxFb(axiCmacTxFb[i]),
           .axiCmacRx(axiCmacRx[i]), .axiCmacRxFb(axiCmacRxFb[i])
           );
  end
`endif

`ifdef SIM_ANNOUNCE
  initial begin
    `OC_ANNOUNCE_MODULE(oc_user);
    `OC_ANNOUNCE_PARAM_INTEGER(Clocks);
    `OC_ANNOUNCE_PARAM_INTEGER(Abbcs);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_INTEGER(AxiMemoryPorts);
  end
`endif

endmodule // oc_user
