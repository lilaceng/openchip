
// SPDX-License-Identifier: MPL-2.0

module oc_cmac #(
                 parameter integer Instance = 0,
                 parameter bit     AbbcEnable = 0,
                 parameter integer AbbcNumber = 0,
                 parameter bit     ResetSync = 0
                 )
  (
   input        clock,
   input        reset,
   output [3:0] txP,
   output [3:0] txN,
   input [3:0]  rxP,
   input [3:0]  rxN,
   input        clockRefP,
   input        clockRefN,
   output       oclib_pkg::axi4st_512_s axiRx,
   input        oclib_pkg::axi4st_512_fb_s axiRxFb,
   input        oclib_pkg::axi4st_512_s axiTx,
   output       oclib_pkg::axi4st_512_fb_s axiTxFb,
   output logic clockAxi,
   input        oclib_pkg::abbc_s abbcIn = '0,
   output       oclib_pkg::abbc_s abbcOut
   );

  logic                         resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

`ifdef OC_LIBRARY_ULTRASCALE_PLUS

  // clock/reset
  logic                         gt_txusrclk2;
  assign clockAxi = gt_txusrclk2;

  // control signals
  logic                         soft_sys_reset;
  logic                         soft_drp_reset;
  logic                         soft_core_tx_reset;
  logic                         soft_core_rx_reset;
  logic                         soft_gt_tx_reset;
  logic                         soft_gt_rx_reset;
  logic                         gt_loopback_in;
  logic                         ctl_tx_enable;
  logic                         ctl_tx_send_idle;
  logic                         ctl_tx_send_rfi;
  logic                         ctl_tx_send_lfi;
  logic                         ctl_tx_test_pattern;
  logic                         ctl_tx_rsfec_enable;
  logic                         ctl_rx_enable;
  logic                         ctl_rx_force_resync;
  logic                         ctl_rx_test_pattern;
  logic                         ctl_rx_rsfec_enable;
  logic                         ctl_rx_rsfec_enable_correction;
  logic                         ctl_rx_rsfec_enable_indication;
  logic                         ctl_rsfec_ieee_error_indication_mode;

  // status signals
  logic [3:0]                   gt_powergoodout;
  logic                         stat_rx_status;
  logic                         stat_rx_aligned;
  logic                         stat_rx_misaligned;
  logic                         stat_rx_aligned_err;
  logic                         stat_rx_hi_ber;
  logic                         stat_rx_internal_local_fault;
  logic                         stat_rx_received_local_fault;
  logic                         stat_rx_local_fault;
  logic                         stat_rx_remote_fault;
  logic                         stat_rx_rsfec_lane_alignment_status;
  logic [19:0]                  stat_rx_block_lock;
  logic [19:0]                  stat_rx_synced;
  logic                         tx_ovfout;
  logic                         tx_unfout;

  oclib_pkg::drp_s              drp;
  oclib_pkg::drp_fb_s           drpFb;

   // Implement CSRs if they are enabled
  if (AbbcEnable) begin : abbc

    localparam AddressSpaces = 2;
    logic [AddressSpaces-1:0] csrSelect;
    oclib_pkg::csr_s                        csr;
    oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

    oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
    uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

    // Implement address space 0
    localparam integer            NumCsr = 6;
    localparam logic [31:0]       CsrId = { oclib_pkg::AbbcIdCmac,
                                            12'd0, 4'(Instance) };
    logic [NumCsr-1:0] [31:0]     csrConfig;
    logic [NumCsr-1:0] [31:0]     csrStatus;

    oclib_csr_array #(.NumCsr(NumCsr),
                      .CsrRwBits   ({ 32'h00000000, 32'h00000000, 32'h00000f07, 32'h0000011f, 32'h0000013f, 32'h00000000 }),
                      .CsrRoBits   ({ 32'h000fffff, 32'h000fffff, 32'h03ff0000, 32'h00030000, 32'h000f0000, 32'h00000000 }),
                      .CsrFixedBits({ 32'h00000000, 32'h00000000, 32'h00000000, 32'h00000000, 32'h00000000, 32'hffffffff }),
                      .CsrInitBits ({ 32'h00000000, 32'h00000000, 32'h00000000, 32'h00000000, 32'h00000000, CsrId        }),
                      .CsrInputAsync  (6'b111110), // will add input synchronizers on these CSRs
                      .CsrOutputAsync (6'b111100)) // will add output synchronizers on these CSRs, using the clockAsync input
    uCSR (.clock(clock), .reset(resetQ), .clockAsync(clockAxi),
          .csrSelect(csrSelect[0]), .csr(csr), .csrFb(csrFb[0]),
          .csrConfig(csrConfig), .csrStatus(csrStatus));

    // ADDR 1 - Reset
    // these outputs are not synchronized, because they are async inputs to the MAC, and resets will likely stop clockAxi, leading
    // to chicken and egg problem.  Other CSRs are declared async so they will get synchronized to/from clockAxi
    assign soft_sys_reset = csrConfig[1][0];
    assign soft_drp_reset = csrConfig[1][1];
    assign soft_core_tx_reset = csrConfig[1][2];
    assign soft_core_rx_reset = csrConfig[1][3];
    assign soft_gt_tx_reset = csrConfig[1][4];
    assign soft_gt_rx_reset = csrConfig[1][5];
    assign gt_loopback_in = csrConfig[1][6];
    // ADDR 2 - TxConfig
    assign ctl_tx_enable = csrConfig[2][0];
    assign ctl_tx_send_idle = csrConfig[2][1];
    assign ctl_tx_send_rfi = csrConfig[2][2];
    assign ctl_tx_send_lfi = csrConfig[2][3];
    assign ctl_tx_test_pattern = csrConfig[2][4];
    assign ctl_tx_rsfec_enable = csrConfig[2][5];
    // ADDR 3 - RxConfig
    assign ctl_rx_enable = csrConfig[3][0];
    assign ctl_rx_force_resync = csrConfig[3][1];
    assign ctl_rx_test_pattern = csrConfig[3][2];
    assign ctl_rx_rsfec_enable = csrConfig[3][3];
    assign ctl_rx_rsfec_enable_correction = csrConfig[3][4];
    assign ctl_rx_rsfec_enable_indication = csrConfig[3][5];
    assign ctl_rsfec_ieee_error_indication_mode = csrConfig[3][6];

    logic                     latched_tx_ovfout;
    logic                     latched_tx_unfout;
    always_ff @(posedge clockAxi) begin
      latched_tx_ovfout <= (axiTxFb.reset ? 1'b0 : (latched_tx_ovfout || tx_ovfout));
      latched_tx_unfout <= (axiTxFb.reset ? 1'b0 : (latched_tx_unfout || tx_unfout));
    end

    always_comb begin
      csrStatus = '0;
      csrStatus[1][19:16] = gt_powergoodout;
      csrStatus[2][16] = latched_tx_ovfout;
      csrStatus[2][17] = latched_tx_unfout;
      csrStatus[3][16] = stat_rx_status;
      csrStatus[3][17] = stat_rx_aligned;
      csrStatus[3][18] = stat_rx_misaligned;
      csrStatus[3][19] = stat_rx_aligned_err;
      csrStatus[3][20] = stat_rx_hi_ber;
      csrStatus[3][21] = stat_rx_internal_local_fault;
      csrStatus[3][22] = stat_rx_received_local_fault;
      csrStatus[3][23] = stat_rx_local_fault;
      csrStatus[3][24] = stat_rx_remote_fault;
      csrStatus[3][25] = stat_rx_rsfec_lane_alignment_status;
      csrStatus[4][19:0] = stat_rx_block_lock;
      csrStatus[5][19:0] = stat_rx_synced;
    end

    // Implement address space 1
    oclib_csr_to_drp #(.DataW(16), .AddressW(10))
    uCSR_TO_DRP (clock, resetQ, csrSelect[1], csr, csrFb[1], drp, drpFb);

  end else begin
    assign csrConfig = '0;
  end


// the chip_defines.vh can override this to use alternate IPs for CMAC (like xip_cmac_anlt)
`OC_DEFINE_IF_NOT_DEFINED(TARGET_CMAC_MODULE_BASENAME,xip_cmac)
// helper define macro, creates a module name out of the above basename, with an instance (xip_cmac -> xip_cmac_0)
`define OC_LOCAL_MODULE_NAME(base,ins) base``_``ins
// all connections will be the same regardless of module name, so we use a define to stamp out multiple copies
`define OC_LOCAL_MODULE_CONNECTIONS \
           .gt_txp_out(txP), \
           .gt_txn_out(txN), \
           .gt_rxp_in(rxP), \
           .gt_rxn_in(rxN), \
           .gt_txusrclk2(gt_txusrclk2), \
           .gt_loopback_in({12{gt_loopback_in}}), \
           .gt_ref_clk_out(), \
           .gt_rxrecclkout(), \
           .gt_powergoodout(gt_powergoodout), \
           .gtwiz_reset_tx_datapath(resetQ || soft_gt_tx_reset), \
           .gtwiz_reset_rx_datapath(resetQ || soft_gt_rx_reset), \
           .ctl_tx_rsfec_enable(ctl_tx_rsfec_enable), \
           .ctl_rx_rsfec_enable(ctl_rx_rsfec_enable), \
           .ctl_rsfec_ieee_error_indication_mode(ctl_rsfec_ieee_error_indication_mode), \
           .ctl_rx_rsfec_enable_correction(ctl_rx_rsfec_enable_correction), \
           .ctl_rx_rsfec_enable_indication(ctl_rx_rsfec_enable_indication), \
           .stat_rx_rsfec_am_lock0(), .stat_rx_rsfec_am_lock1(), .stat_rx_rsfec_am_lock2(), .stat_rx_rsfec_am_lock3(), \
           .stat_rx_rsfec_corrected_cw_inc(), \
           .stat_rx_rsfec_cw_inc(), \
           .stat_rx_rsfec_err_count0_inc(), .stat_rx_rsfec_err_count1_inc(), \
           .stat_rx_rsfec_err_count2_inc(), .stat_rx_rsfec_err_count3_inc(), \
           .stat_rx_rsfec_hi_ser(), \
           .stat_rx_rsfec_lane_alignment_status(stat_rx_rsfec_lane_alignment_status), \
           .stat_rx_rsfec_lane_fill_0(), .stat_rx_rsfec_lane_fill_1(), .stat_rx_rsfec_lane_fill_2(), .stat_rx_rsfec_lane_fill_3(), \
           .stat_rx_rsfec_lane_mapping(), \
           .stat_rx_rsfec_uncorrected_cw_inc(), \
           .sys_reset(resetQ || soft_sys_reset), \
           .gt_ref_clk_p(clockRefP), \
           .gt_ref_clk_n(clockRefN), \
           .init_clk(clock), \
           .rx_axis_tvalid(axiRx.tvalid), \
           .rx_axis_tdata(axiRx.tdata), \
           .rx_axis_tlast(axiRx.tlast), \
           .rx_axis_tkeep(axiRx.tkeep), \
           .rx_axis_tuser(axiRx.tuser), \
           .rx_otn_bip8_0(), .rx_otn_bip8_1(), .rx_otn_bip8_2(), .rx_otn_bip8_3(), .rx_otn_bip8_4(), \
           .rx_otn_data_0(), .rx_otn_data_1(), .rx_otn_data_2(), .rx_otn_data_3(), .rx_otn_data_4(), \
           .rx_otn_ena(), \
           .rx_otn_lane0(), \
           .rx_otn_vlmarker(), \
           .rx_preambleout(), \
           .usr_rx_reset(axiRx.reset), \
           .gt_rxusrclk2(), \
           .stat_rx_aligned(stat_rx_aligned), \
           .stat_rx_aligned_err(stat_rx_aligned_err), \
           .stat_rx_bad_code(), \
           .stat_rx_bad_fcs(), \
           .stat_rx_bad_preamble(), \
           .stat_rx_bad_sfd(), \
           .stat_rx_bip_err_0(), .stat_rx_bip_err_1(), .stat_rx_bip_err_2(), .stat_rx_bip_err_3(), \
           .stat_rx_bip_err_4(), .stat_rx_bip_err_5(), .stat_rx_bip_err_6(), .stat_rx_bip_err_7(), \
           .stat_rx_bip_err_8(), .stat_rx_bip_err_9(), .stat_rx_bip_err_10(), .stat_rx_bip_err_11(), \
           .stat_rx_bip_err_12(), .stat_rx_bip_err_13(), .stat_rx_bip_err_14(), .stat_rx_bip_err_15(), \
           .stat_rx_bip_err_16(), .stat_rx_bip_err_17(), .stat_rx_bip_err_18(), .stat_rx_bip_err_19(), \
           .stat_rx_block_lock(stat_rx_block_lock), \
           .stat_rx_broadcast(), \
           .stat_rx_fragment(), \
           .stat_rx_framing_err_0(), .stat_rx_framing_err_1(), .stat_rx_framing_err_2(), .stat_rx_framing_err_3(), \
           .stat_rx_framing_err_4(), .stat_rx_framing_err_5(), .stat_rx_framing_err_6(), .stat_rx_framing_err_7(), \
           .stat_rx_framing_err_8(), .stat_rx_framing_err_9(), .stat_rx_framing_err_10(), .stat_rx_framing_err_11(), \
           .stat_rx_framing_err_12(), .stat_rx_framing_err_13(), .stat_rx_framing_err_14(), .stat_rx_framing_err_15(), \
           .stat_rx_framing_err_16(), .stat_rx_framing_err_17(), .stat_rx_framing_err_18(), .stat_rx_framing_err_19(), \
           .stat_rx_framing_err_valid_0(), .stat_rx_framing_err_valid_1(), .stat_rx_framing_err_valid_2(), \
           .stat_rx_framing_err_valid_3(), .stat_rx_framing_err_valid_4(), .stat_rx_framing_err_valid_5(), \
           .stat_rx_framing_err_valid_6(), .stat_rx_framing_err_valid_7(), .stat_rx_framing_err_valid_8(), \
           .stat_rx_framing_err_valid_9(), .stat_rx_framing_err_valid_10(), .stat_rx_framing_err_valid_11(), \
           .stat_rx_framing_err_valid_12(), .stat_rx_framing_err_valid_13(), .stat_rx_framing_err_valid_14(), \
           .stat_rx_framing_err_valid_15(), .stat_rx_framing_err_valid_16(), .stat_rx_framing_err_valid_17(), \
           .stat_rx_framing_err_valid_18(), .stat_rx_framing_err_valid_19(), \
           .stat_rx_got_signal_os(), \
           .stat_rx_hi_ber(stat_rx_hi_ber), \
           .stat_rx_inrangeerr(), \
           .stat_rx_internal_local_fault(stat_rx_internal_local_fault), \
           .stat_rx_jabber(), \
           .stat_rx_local_fault(stat_rx_local_fault), \
           .stat_rx_mf_err(), \
           .stat_rx_mf_len_err(), \
           .stat_rx_mf_repeat_err(), \
           .stat_rx_misaligned(stat_rx_misaligned), \
           .stat_rx_multicast(), \
           .stat_rx_oversize(), \
           .stat_rx_packet_1024_1518_bytes(), \
           .stat_rx_packet_128_255_bytes(), \
           .stat_rx_packet_1519_1522_bytes(), \
           .stat_rx_packet_1523_1548_bytes(), \
           .stat_rx_packet_1549_2047_bytes(), \
           .stat_rx_packet_2048_4095_bytes(), \
           .stat_rx_packet_256_511_bytes(), \
           .stat_rx_packet_4096_8191_bytes(), \
           .stat_rx_packet_512_1023_bytes(), \
           .stat_rx_packet_64_bytes(), \
           .stat_rx_packet_65_127_bytes(), \
           .stat_rx_packet_8192_9215_bytes(), \
           .stat_rx_packet_bad_fcs(), \
           .stat_rx_packet_large(), \
           .stat_rx_packet_small(), \
           .ctl_rx_enable(ctl_rx_enable), \
           .ctl_rx_force_resync(ctl_rx_force_resync), \
           .ctl_rx_test_pattern(ctl_rx_test_pattern), \
           .core_rx_reset(resetQ || soft_core_rx_reset), \
           .rx_clk(clockAxi), \
           .stat_rx_received_local_fault(stat_rx_received_local_fault), \
           .stat_rx_remote_fault(stat_rx_remote_fault), \
           .stat_rx_status(stat_rx_status), \
           .stat_rx_stomped_fcs(), \
           .stat_rx_synced(stat_rx_synced), \
           .stat_rx_synced_err(), \
           .stat_rx_test_pattern_mismatch(), \
           .stat_rx_toolong(), \
           .stat_rx_total_bytes(), \
           .stat_rx_total_good_bytes(), \
           .stat_rx_total_good_packets(), \
           .stat_rx_total_packets(), \
           .stat_rx_truncated(), \
           .stat_rx_undersize(), \
           .stat_rx_unicast(), \
           .stat_rx_vlan(), \
           .stat_rx_pcsl_demuxed(), \
           .stat_rx_pcsl_number_0(), .stat_rx_pcsl_number_1(), .stat_rx_pcsl_number_2(), .stat_rx_pcsl_number_3(), \
           .stat_rx_pcsl_number_4(), .stat_rx_pcsl_number_5(), .stat_rx_pcsl_number_6(), .stat_rx_pcsl_number_7(), \
           .stat_rx_pcsl_number_8(), .stat_rx_pcsl_number_9(), .stat_rx_pcsl_number_10(), .stat_rx_pcsl_number_11(), \
           .stat_rx_pcsl_number_12(), .stat_rx_pcsl_number_13(), .stat_rx_pcsl_number_14(), .stat_rx_pcsl_number_15(), \
           .stat_rx_pcsl_number_16(), .stat_rx_pcsl_number_17(), .stat_rx_pcsl_number_18(), .stat_rx_pcsl_number_19(), \
           .stat_tx_bad_fcs(), \
           .stat_tx_broadcast(), \
           .stat_tx_frame_error(), \
           .stat_tx_local_fault(), \
           .stat_tx_multicast(), \
           .stat_tx_packet_1024_1518_bytes(), \
           .stat_tx_packet_128_255_bytes(), \
           .stat_tx_packet_1519_1522_bytes(), \
           .stat_tx_packet_1523_1548_bytes(), \
           .stat_tx_packet_1549_2047_bytes(), \
           .stat_tx_packet_2048_4095_bytes(), \
           .stat_tx_packet_256_511_bytes(), \
           .stat_tx_packet_4096_8191_bytes(), \
           .stat_tx_packet_512_1023_bytes(), \
           .stat_tx_packet_64_bytes(), \
           .stat_tx_packet_65_127_bytes(), \
           .stat_tx_packet_8192_9215_bytes(), \
           .stat_tx_packet_large(), \
           .stat_tx_packet_small(), \
           .stat_tx_total_bytes(), \
           .stat_tx_total_good_bytes(), \
           .stat_tx_total_good_packets(), \
           .stat_tx_total_packets(), \
           .stat_tx_unicast(), \
           .stat_tx_vlan(), \
           .ctl_tx_enable(ctl_tx_enable), \
           .ctl_tx_send_idle(ctl_tx_send_idle), \
           .ctl_tx_send_rfi(ctl_tx_send_rfi), \
           .ctl_tx_send_lfi(ctl_tx_send_lfi), \
           .ctl_tx_test_pattern(ctl_tx_test_pattern), \
           .core_tx_reset(resetQ || soft_core_tx_reset), \
           .tx_axis_tready(axiTxFb.tready), \
           .tx_axis_tvalid(axiTx.tvalid), \
           .tx_axis_tdata(axiTx.tdata), \
           .tx_axis_tlast(axiTx.tlast), \
           .tx_axis_tkeep(axiTx.tkeep), \
           .tx_axis_tuser(axiTx.tuser), \
           .tx_ovfout(tx_ovfout), \
           .tx_unfout(tx_unfout), \
           .tx_preamblein('0), \
           .usr_tx_reset(axiTxFb.reset), \
           .core_drp_reset(resetQ || soft_drp_reset), \
           .drp_clk(clock), \
           .drp_addr(drp.address[9:0]), \
           .drp_di(drp.wdata), \
           .drp_en(drp.enable), \
           .drp_do(drpFb.rdata), \
           .drp_rdy(drpFb.ready), \
           .drp_we(drp.write)

    // And finally, based on which Instance we are, we construct the module name and connectivity

    if (Instance==0) begin
    `OC_LOCAL_MODULE_NAME(`TARGET_CMAC_MODULE_BASENAME,0) uCMAC ( `OC_LOCAL_MODULE_CONNECTIONS );
    end else if (Instance==1) begin
    `OC_LOCAL_MODULE_NAME(`TARGET_CMAC_MODULE_BASENAME,1) uCMAC ( `OC_LOCAL_MODULE_CONNECTIONS );
    end else if (Instance==2) begin
    `OC_LOCAL_MODULE_NAME(`TARGET_CMAC_MODULE_BASENAME,2) uCMAC ( `OC_LOCAL_MODULE_CONNECTIONS );
    end else if (Instance==3) begin
    `OC_LOCAL_MODULE_NAME(`TARGET_CMAC_MODULE_BASENAME,3) uCMAC ( `OC_LOCAL_MODULE_CONNECTIONS );
    end else begin
    `OC_STATIC_ERROR("Only support up to 4 CMACs currently");
    end

  // cleanup the temp defines we declared in this section
  `undef OC_LOCAL_MODULE_NAME
  `undef OC_LOCAL_MODULE_CONNECTIONS

`endif // !`ifdef OC_LIBRARY_ULTRASCALE_PLUS

`ifdef SIM_ANNOUNCE
  initial begin
    `OC_ANNOUNCE_MODULE(oc_cmac);
    `OC_ANNOUNCE_PARAM_INTEGER(Instance);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_cmac
