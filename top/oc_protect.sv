
// SPDX-License-Identifier: MPL-2.0

module oc_protect #(
                    parameter logic [31:0]  BitstreamID = `OC_FROM_DEFINE_ELSE(TARGET_BITSTREAM_ID, 32'h89abcdef),
                    parameter logic [127:0] BitstreamKey = `OC_FROM_DEFINE_ELSE(TARGET_BITSTREAM_KEY,
                                            128'h44444444_33333333_22222222_11111111),
                    parameter bit           EnableSkeletonKey = 0,
                    parameter bit           EnableTimedLicense = 0,
                    parameter bit           EnableParanoia = 0,
                    parameter bit           AbbcEnable = 1'b1,
                    parameter integer       AbbcNumber = 0,
                    parameter bit           ResetSync = 0
                    )
 (
  input        clock,
  input        reset,
  output logic unlocked,
  input        oclib_pkg::abbc_s abbcIn = '0,
  output       oclib_pkg::abbc_s abbcOut
  );

  `OC_STATIC_ASSERT(AbbcEnable==1); // we need ABBC CSRs for protection to work

  logic        resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  // Get FPGA serial number
  localparam SerialBits = 32;
  logic [SerialBits-1:0] serial;
  oclib_fpga_serial #(.SerialBits(SerialBits)) uSERIAL (.clock(clock), .reset(resetQ), .serial(serial));

  // decryption block
  localparam integer Words = 2; // we always decrypt a 2-word chunk

  logic                    decryptGo;
  logic [Words-1:0] [31:0] ciphertext;
  logic                    decryptDone;
  logic [Words-1:0] [31:0] plaintext;

  oclib_xxtea uDEC (.clock(clock), .go(decryptGo), .in(ciphertext), .key(BitstreamKey),
                    .done(decryptDone), .out(plaintext));

  // check plaintext
  always_ff @(posedge clock) begin
    unlocked <= (resetQ ? 1'b0 :
                 decryptDone ? (plaintext == { BitstreamID, serial }) :
                 unlocked);
  end

  // CSR interface
  localparam integer NumCsr = 8; // 1 id, 1 control, 2 ciphertext, 1 bitstream, 1 serial, 2 plaintext
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdProtect,
                                    13'd0, EnableParanoia, EnableTimedLicense, EnableSkeletonKey };
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  logic                     csrSelect;
  oclib_pkg::csr_s          csr;
  oclib_pkg::csr_fb_s       csrFb;

  oclib_abbc_to_csr #(.AddressSpaces(1))
  uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

  oclib_csr_array #(.NumCsr(NumCsr),
                    .CsrRwBits   ({ {4{32'h00000000}}, {2{32'hffffffff}}, 32'h00000001, 32'h00000000 }),
                    .CsrRoBits   ({ {4{32'hffffffff}}, {2{32'h00000000}}, 32'hc0000000, 32'h00000000 }),
                    .CsrFixedBits({ {4{32'h00000000}}, {2{32'h00000000}}, 32'h00000000, 32'hffffffff }),
                    .CsrInitBits ({ {4{32'h00000000}}, {2{32'h00000000}}, 32'h00000000, CsrId        }))
  uCSR (clock, resetQ, csrSelect, csr, csrFb, csrConfig, csrStatus);

  assign decryptGo = csrConfig[1][0];
  assign ciphertext = {csrConfig[3], csrConfig[2]};
  assign csrStatus[1][31] = unlocked;
  assign csrStatus[1][30] = decryptDone;
  assign csrStatus[4] = serial;
  assign csrStatus[5] = BitstreamID;
  assign csrStatus[6] = plaintext[0];
  assign csrStatus[7] = plaintext[1];

`ifdef SIM_ANNOUNCE
  `include "../lib/oclib_defines.vh"
  initial begin
    `OC_ANNOUNCE_MODULE(oc_protect);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_protect
