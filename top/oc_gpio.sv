
// SPDX-License-Identifier: MPL-2.0

module oc_gpio #(
                parameter integer GpioCount = 1,
                parameter bit     AbbcEnable = 1'b1,
                parameter integer AbbcNumber = 0,
                parameter bit     ResetSync = 1'b0
                )
 (
  input                        clock,
  input                        reset,
  output logic [GpioCount-1:0] gpioOut,
  output logic [GpioCount-1:0] gpioTristate,
  input [GpioCount-1:0]        gpioIn,
  input                        oclib_pkg::abbc_s abbcIn = '0,
  output                       oclib_pkg::abbc_s abbcOut
  );

  `OC_STATIC_ASSERT(GpioCount>=1); // we shouldn't be instantiating this block if we don't have GPIOs
  `OC_STATIC_ASSERT(AbbcEnable==1); // we need ABBC CSRs for GPIO to work

  logic        resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  localparam AddressSpaces = 1;
  logic [AddressSpaces-1:0] csrSelect;
  oclib_pkg::csr_s                        csr;
  oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

  oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
  uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

  // Implement address space 0

  // 0 : CSR ID
  // 1->(GpioCount+1) : Gpio
  //   [0]  out
  //   [4]  drive
  //   [8]  in

  localparam integer NumCsr = 1 + GpioCount;
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdGpio, 8'd0, 8'(GpioCount)};
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  oclib_csr_array #(.NumCsr(NumCsr),
                    .CsrRwBits   ({ {GpioCount{32'h00000011}}, 32'h00000000 }),
                    .CsrRoBits   ({ {GpioCount{32'h00000100}}, 32'h00000000 }),
                    .CsrFixedBits({ {GpioCount{32'h00000000}}, 32'hffffffff }),
                    .CsrInitBits ({ {GpioCount{32'h00000000}}, CsrId        }))
  uCSR (clock, resetQ, csrSelect[0], csr, csrFb[0], csrConfig, csrStatus);

  logic [GpioCount-1:0]     gpioInSync;
  oclib_synchronizer #(.Width(GpioCount)) uSYNC (.clock(clock), .in(gpioIn), .out(gpioInSync));

  for (genvar i=0; i<GpioCount; i++) begin
    assign gpioOut[i] = csrConfig[1+i][0];
    assign gpioTristate[i] = !csrConfig[1+i][4];
    assign csrStatus[1+i][8] = gpioInSync[i];
  end

`ifdef SIM_ANNOUNCE
  `include "../lib/oclib_defines.vh"
  initial begin
    `OC_ANNOUNCE_MODULE(oc_gpio);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
    `OC_ANNOUNCE_PARAM_INTEGER(GpioCount);
  end
`endif

endmodule // oc_gpio
