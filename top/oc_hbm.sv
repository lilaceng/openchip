
// SPDX-License-Identifier: MPL-2.0

module oc_hbm #(
                parameter integer  RefClockHz = 100000000,
                parameter integer  AxiPorts = 1,
                parameter integer  AxiClocks = 1,
                parameter integer  AxiPortClockSelect [AxiPorts-1:0] = '{ AxiPorts { 0 } },
                parameter bit      [AxiPorts-1:0] AxiPortIla = { AxiPorts { 1'b0 } },
                parameter integer  Stacks = 2,
                parameter integer  HbmRefClocks = 1,
                parameter integer  HbmRefClockHz = `OC_FROM_DEFINE_ELSE(TARGET_HBM_REF_CLK_HZ, RefClockHz),
                localparam integer StackRefClocks = Stacks, // must match IP down below, not be set by preference above
                parameter integer  StackRefClockSelect [Stacks-1:0] = '{ Stacks { 0 } }, // which clockRefHbm to each clockRefStack
                parameter bit      SeparateFabricClocks = 0,
                parameter integer  FabricClocks = 0,
                localparam integer StackFabricClocks = Stacks, // must match IP down below, not be set by preference above
                localparam integer StackFabricClockHbmAxiSelect [StackFabricClocks-1:0] = '{ 24, 7 }, // must match IP...
                parameter integer  StackFabricClockSelect [StackFabricClocks-1:0] = '{ 0, 0 }, // which clockFabric to each stack
                localparam integer HbmAxiPorts = ((Stacks == 2) ? 32 : (Stacks == 1) ? 16 : -1),
                localparam bit     HbmAxiPortsOneToOne = (AxiPorts == HbmAxiPorts),
                parameter integer  HbmAxiPortSelect [HbmAxiPorts-1:0] = // which hbm axi into each user axi
                                   ((Stacks==2) ?
                                   // for dual stack we pick odd then even in center, then work outwards
                                   '{31, 0, 30, 1, 29, 2, 28, 3, 27, 4, 26, 5, 25, 6, 24, 7,
                                   23, 8, 22, 9, 21, 10, 20, 11, 19, 12, 18, 13, 17, 14, 16, 15} :
                                   // for single stack we pick even then odd in center, then work outwards, using input 7 last
                                   '{ 7, 0, 15, 14, 1, 2, 13, 12, 3, 4, 11, 10, 5, 6, 9, 8 } ),
                parameter bit      AbbcEnable = 0,
                parameter integer  AbbcNumber = 0,
                parameter bit      ResetSync = 0,
                                   `OC_CREATE_SAFE_WIDTH(FabricClocks) // legal input must be 1 bit even if unused
                )
  (
   input                        clock,
   input                        reset,
   output logic                 thermalError,
   input [HbmRefClocks-1:0]     clockRefHbm,
   input [AxiClocks-1:0]        clockAxi,
   input [FabricClocksSafe-1:0] clockFabric = { FabricClocksSafe { 1'b0 }},
   input                        oclib_pkg::axi3_s [AxiPorts-1:0] axi,
   output                       oclib_pkg::axi3_fb_s [AxiPorts-1:0] axiFb,
   input                        oclib_pkg::abbc_s abbcIn = '0,
   output                       oclib_pkg::abbc_s abbcOut
   );

  logic                         resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

`ifdef OC_LIBRARY_ULTRASCALE_PLUS

  oclib_pkg::apb_s    apb [Stacks-1:0];
  oclib_pkg::apb_fb_s apbFb [Stacks-1:0];

  localparam integer NumCsr = 2; // 1 id, 1 control/status
  localparam integer CsrAddressControl = 1;
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdHbm,
                                    4'd0, 8'(AxiPorts), 4'(Stacks) };
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  // Implement CSRs if they are enabled
  if (AbbcEnable) begin : abbc

    localparam AddressSpaces = 1+Stacks;
    logic [AddressSpaces-1:0] csrSelect;
    oclib_pkg::csr_s                        csr;
    oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

    oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
    uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

    // Implement address space 0
    // ADDR 1: Config/Status
    // [0]: resetAxi
    // [1]: resetApb
    // [14:8]: stack0temp
    // [21:15]: stack1temp
    // [24]: apb0ready
    // [25]: apb1ready
    // [30]: stack0thermalError
    // [31]: stack1thermalError
    oclib_csr_array #(.NumCsr(NumCsr),
                      .CsrRwBits   ({ 32'h00000003, 32'h00000000 }),
                      .CsrRoBits   ({ 32'hc37f7f00, 32'h00000000 }),
                      .CsrFixedBits({ 32'h00000000, 32'hffffffff }),
                      .CsrInitBits ({ 32'h00000000, CsrId        }))
    uCSR (clock, resetQ, csrSelect[0], csr, csrFb[0], csrConfig, csrStatus);

    // Implement address space per stack
    for (genvar stack=0; stack<Stacks; stack++) begin
      oclib_csr_to_apb #(.DataW(32), .AddressW(22))
      uCSR_TO_DRP (clock, resetQ, csrSelect[1+stack], csr, csrFb[1+stack], apb[stack], apbFb[stack]);
    end

  end else begin
    for (genvar stack=0; stack<Stacks; stack++) begin
      assign apb[stack] = '0;
    end
    assign csrConfig = '0;
  end

  // Per-reference clock PLL if needed
  logic    clockRefHbmUse [HbmRefClocks-1:0];
  for (genvar clk=0; clk<HbmRefClocks; clk++) begin
    if (RefClockHz == HbmRefClockHz) begin
      assign clockRefHbmUse[clk] = clockRefHbm[StackRefClockSelect[clk]];
    end
    else begin
      // we are going to instantiate a PLL to generate the desired HBM stack ref clock
      oc_pll #(.RefClockHz(RefClockHz), .Out0MHz(HbmRefClockHz/1000000))
      uHBMREFPLL (.clock(clock), .reset(resetQ),
                  .clockRef(clockRefHbm[clk]), .clockOut(clockRefHbmUse[clk]));
    end
  end

  // Per-Stack reference clocks
  logic    clockRefStack [StackRefClocks-1:0];
  for (genvar clk=0; clk<StackRefClocks; clk++) begin
    assign clockRefStack[clk] = clockRefHbmUse[StackRefClockSelect[clk]];
  end

  // Per-Stack APB reset
  logic    resetApbN [Stacks-1:0];
  for (genvar stack=0; stack<Stacks; stack++) begin
    oclib_synchronizer uRESET_SYNC (.clock(clock), .in(!(resetQ || csrConfig[CsrAddressControl][1])), .out(resetApbN[stack]));
  end

  // Wire up each AXI port
  logic [HbmAxiPorts-1:0] axiHbmClock;
  logic [HbmAxiPorts-1:0] axiHbmResetN;
  oclib_pkg::axi3_s [HbmAxiPorts-1:0] axiHbm;
  oclib_pkg::axi3_fb_s [HbmAxiPorts-1:0] axiHbmFb;

  // Helper function, figures out what we are doing with a particular port, returning:
  // 0 to (AxiPorts-1)     = connects to one of AxiPorts inputs AXI[connection]
  // -1 to -(FabricClocks) = connects to HBM special fabric clock/reset
  // -998                  = not connecting to a user AXI port, but this is a fabric clock, drive in AXI clock
  // -999                  = connects to nothing (default)
  function integer HbmConnection(input integer hbm);
    // look for this hbm port in HbmPortMap
    if (HbmAxiPortsOneToOne) return hbm;  // we have the same number of HBM and AXI ports, map them 1:1
    for (integer a=0; a<AxiPorts; a++) if (HbmAxiPortSelect[a]==hbm) return a;  // otherwise, intelligent (overridable) defaults
    // this HBM port is not connected to a user AXI port
    // look for this hbm port in StackFabricClockHbmAxiSelect, i.e. if this is one of the special ports the IP uses for clocking
    for (integer clk=0; clk<StackFabricClocks; clk++) begin
      if (StackFabricClockHbmAxiSelect[clk]==hbm) begin
        if (SeparateFabricClocks) begin
          // this is a special fabric clock port, and we are using separate fabric clocks
          return -(1+StackFabricClockSelect[clk]);
        end
        // this is a special fabric clock port, not using separate fabric clocks, just make sure it gets AXI clock
        else return -998;
      end
    end
    // this HBM port is connected to nothing, tie it off
    return -999;
  endfunction

  for (genvar hbm=0; hbm<HbmAxiPorts; hbm++) begin : uHBM_AXI
    if (HbmConnection(hbm) == -999) begin
      // This port is tied off.
      assign axiHbm[hbm] = '0;
      assign axiHbmClock[hbm] = 1'b0;
      assign axiHbmResetN[hbm] = 1'b1;
    end
    else if (HbmConnection(hbm) == -998) begin
      // This port is tied off.
      assign axiHbm[hbm] = '0;
      assign axiHbmClock[hbm] = clockAxi[0];
      oclib_synchronizer uRESET_SYNC (.clock(axiHbmClock[hbm]), .in(!(resetQ || csrConfig[CsrAddressControl][0])),
                                      .out(axiHbmResetN[hbm]));
    end
    else if (HbmConnection(hbm) <= -1) begin
      // This port is being used to supply a fabric clock.  We tie it off, connect fabric clock, and sync reset.
      assign axiHbm[hbm] = '0;
      assign axiHbmClock[hbm] = clockFabric[-HbmConnection(hbm)-1]; // tricky, tricky
      oclib_synchronizer uRESET_SYNC (.clock(axiHbmClock[hbm]), .in(!(resetQ || csrConfig[CsrAddressControl][0])),
                                      .out(axiHbmResetN[hbm]));
    end
    else begin
      // This port is being connected to user AXI port
      assign axiHbm[hbm] = axi[HbmConnection(hbm)];
      assign axiFb[HbmConnection(hbm)] = axiHbmFb[hbm];
      assign axiHbmClock[hbm] = clockAxi[AxiPortClockSelect[HbmConnection(hbm)]];
      oclib_synchronizer uRESET_SYNC (.clock(axiHbmClock[hbm]), .in(!(resetQ || csrConfig[CsrAddressControl][0])),
                                      .out(axiHbmResetN[hbm]));
    end
  end

  // Instantiate AXI ILAs if configured
  for (genvar i=0; i<AxiPorts; i++) begin : uAXI_ILA
    if (AxiPortIla[i]) begin
      oclib_axi_ila uILA (.clock(clockAxi[AxiPortClockSelect[i]]), .axi(axi[i]), .axiFb(axiFb[i]));
    end
  end

  // Instantiate the HBM itself

  logic [Stacks-1:0] stackApbReady;
  logic [Stacks-1:0] stackThermalError;
  logic [Stacks-1:0] [6:0] stackTemperature;

  `ifndef SIMULATION

  `OC_DEFINE_IF_NOT_DEFINED(TARGET_HBM_MODULE_NAME,xip_hbm)
  `TARGET_HBM_MODULE_NAME uHBM (
                .HBM_REF_CLK_0(clockRefStack[0]),           // input wire HBM_REF_CLK_0
                .HBM_REF_CLK_1(clockRefStack[1]),           // input wire HBM_REF_CLK_1
                .AXI_00_ACLK(axiHbmClock[0]),              // input wire AXI_00_ACLK
                .AXI_00_ARESET_N(axiHbmResetN[0]),         // input wire AXI_00_ARESET_N
                .AXI_00_ARADDR(axiHbm[0].ar.addr),         // input wire [32 : 0] AXI_00_ARADDR
                .AXI_00_ARBURST(axiHbm[0].ar.burst),       // input wire [1 : 0] AXI_00_ARBURST
                .AXI_00_ARID(axiHbm[0].ar.id),             // input wire [5 : 0] AXI_00_ARID
                .AXI_00_ARLEN(axiHbm[0].ar.len),           // input wire [3 : 0] AXI_00_ARLEN
                .AXI_00_ARSIZE(axiHbm[0].ar.size),         // input wire [2 : 0] AXI_00_ARSIZE
                .AXI_00_ARVALID(axiHbm[0].arvalid),        // input wire AXI_00_ARVALID
                .AXI_00_AWADDR(axiHbm[0].aw.addr),         // input wire [32 : 0] AXI_00_AWADDR
                .AXI_00_AWBURST(axiHbm[0].aw.burst),       // input wire [1 : 0] AXI_00_AWBURST
                .AXI_00_AWID(axiHbm[0].aw.id),             // input wire [5 : 0] AXI_00_AWID
                .AXI_00_AWLEN(axiHbm[0].aw.len),           // input wire [3 : 0] AXI_00_AWLEN
                .AXI_00_AWSIZE(axiHbm[0].aw.size),         // input wire [2 : 0] AXI_00_AWSIZE
                .AXI_00_AWVALID(axiHbm[0].awvalid),        // input wire AXI_00_AWVALID
                .AXI_00_RREADY(axiHbm[0].rready),          // input wire AXI_00_RREADY
                .AXI_00_BREADY(axiHbm[0].bready),          // input wire AXI_00_BREADY
                .AXI_00_WDATA(axiHbm[0].w.data),           // input wire [255 : 0] AXI_00_WDATA
                .AXI_00_WLAST(axiHbm[0].w.last),           // input wire AXI_00_WLAST
                .AXI_00_WSTRB(axiHbm[0].w.strb),           // input wire [31 : 0] AXI_00_WSTRB
                .AXI_00_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_00_WDATA_PARITY
                .AXI_00_WVALID(axiHbm[0].wvalid),          // input wire AXI_00_WVALID
                .AXI_01_ACLK(axiHbmClock[1]),              // input wire AXI_01_ACLK
                .AXI_01_ARESET_N(axiHbmResetN[1]),         // input wire AXI_01_ARESET_N
                .AXI_01_ARADDR(axiHbm[1].ar.addr),         // input wire [32 : 0] AXI_01_ARADDR
                .AXI_01_ARBURST(axiHbm[1].ar.burst),       // input wire [1 : 0] AXI_01_ARBURST
                .AXI_01_ARID(axiHbm[1].ar.id),             // input wire [5 : 0] AXI_01_ARID
                .AXI_01_ARLEN(axiHbm[1].ar.len),           // input wire [3 : 0] AXI_01_ARLEN
                .AXI_01_ARSIZE(axiHbm[1].ar.size),         // input wire [2 : 0] AXI_01_ARSIZE
                .AXI_01_ARVALID(axiHbm[1].arvalid),        // input wire AXI_01_ARVALID
                .AXI_01_AWADDR(axiHbm[1].aw.addr),         // input wire [32 : 0] AXI_01_AWADDR
                .AXI_01_AWBURST(axiHbm[1].aw.burst),       // input wire [1 : 0] AXI_01_AWBURST
                .AXI_01_AWID(axiHbm[1].aw.id),             // input wire [5 : 0] AXI_01_AWID
                .AXI_01_AWLEN(axiHbm[1].aw.len),           // input wire [3 : 0] AXI_01_AWLEN
                .AXI_01_AWSIZE(axiHbm[1].aw.size),         // input wire [2 : 0] AXI_01_AWSIZE
                .AXI_01_AWVALID(axiHbm[1].awvalid),        // input wire AXI_01_AWVALID
                .AXI_01_RREADY(axiHbm[1].rready),          // input wire AXI_01_RREADY
                .AXI_01_BREADY(axiHbm[1].bready),          // input wire AXI_01_BREADY
                .AXI_01_WDATA(axiHbm[1].w.data),           // input wire [255 : 0] AXI_01_WDATA
                .AXI_01_WLAST(axiHbm[1].w.last),           // input wire AXI_01_WLAST
                .AXI_01_WSTRB(axiHbm[1].w.strb),           // input wire [31 : 0] AXI_01_WSTRB
                .AXI_01_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_01_WDATA_PARITY
                .AXI_01_WVALID(axiHbm[1].wvalid),          // input wire AXI_01_WVALID
                .AXI_02_ACLK(axiHbmClock[2]),              // input wire AXI_02_ACLK
                .AXI_02_ARESET_N(axiHbmResetN[2]),         // input wire AXI_02_ARESET_N
                .AXI_02_ARADDR(axiHbm[2].ar.addr),         // input wire [32 : 0] AXI_02_ARADDR
                .AXI_02_ARBURST(axiHbm[2].ar.burst),       // input wire [1 : 0] AXI_02_ARBURST
                .AXI_02_ARID(axiHbm[2].ar.id),             // input wire [5 : 0] AXI_02_ARID
                .AXI_02_ARLEN(axiHbm[2].ar.len),           // input wire [3 : 0] AXI_02_ARLEN
                .AXI_02_ARSIZE(axiHbm[2].ar.size),         // input wire [2 : 0] AXI_02_ARSIZE
                .AXI_02_ARVALID(axiHbm[2].arvalid),        // input wire AXI_02_ARVALID
                .AXI_02_AWADDR(axiHbm[2].aw.addr),         // input wire [32 : 0] AXI_02_AWADDR
                .AXI_02_AWBURST(axiHbm[2].aw.burst),       // input wire [1 : 0] AXI_02_AWBURST
                .AXI_02_AWID(axiHbm[2].aw.id),             // input wire [5 : 0] AXI_02_AWID
                .AXI_02_AWLEN(axiHbm[2].aw.len),           // input wire [3 : 0] AXI_02_AWLEN
                .AXI_02_AWSIZE(axiHbm[2].aw.size),         // input wire [2 : 0] AXI_02_AWSIZE
                .AXI_02_AWVALID(axiHbm[2].awvalid),        // input wire AXI_02_AWVALID
                .AXI_02_RREADY(axiHbm[2].rready),          // input wire AXI_02_RREADY
                .AXI_02_BREADY(axiHbm[2].bready),          // input wire AXI_02_BREADY
                .AXI_02_WDATA(axiHbm[2].w.data),           // input wire [255 : 0] AXI_02_WDATA
                .AXI_02_WLAST(axiHbm[2].w.last),           // input wire AXI_02_WLAST
                .AXI_02_WSTRB(axiHbm[2].w.strb),           // input wire [31 : 0] AXI_02_WSTRB
                .AXI_02_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_02_WDATA_PARITY
                .AXI_02_WVALID(axiHbm[2].wvalid),          // input wire AXI_02_WVALID
                .AXI_03_ACLK(axiHbmClock[3]),              // input wire AXI_03_ACLK
                .AXI_03_ARESET_N(axiHbmResetN[3]),         // input wire AXI_03_ARESET_N
                .AXI_03_ARADDR(axiHbm[3].ar.addr),         // input wire [32 : 0] AXI_03_ARADDR
                .AXI_03_ARBURST(axiHbm[3].ar.burst),       // input wire [1 : 0] AXI_03_ARBURST
                .AXI_03_ARID(axiHbm[3].ar.id),             // input wire [5 : 0] AXI_03_ARID
                .AXI_03_ARLEN(axiHbm[3].ar.len),           // input wire [3 : 0] AXI_03_ARLEN
                .AXI_03_ARSIZE(axiHbm[3].ar.size),         // input wire [2 : 0] AXI_03_ARSIZE
                .AXI_03_ARVALID(axiHbm[3].arvalid),        // input wire AXI_03_ARVALID
                .AXI_03_AWADDR(axiHbm[3].aw.addr),         // input wire [32 : 0] AXI_03_AWADDR
                .AXI_03_AWBURST(axiHbm[3].aw.burst),       // input wire [1 : 0] AXI_03_AWBURST
                .AXI_03_AWID(axiHbm[3].aw.id),             // input wire [5 : 0] AXI_03_AWID
                .AXI_03_AWLEN(axiHbm[3].aw.len),           // input wire [3 : 0] AXI_03_AWLEN
                .AXI_03_AWSIZE(axiHbm[3].aw.size),         // input wire [2 : 0] AXI_03_AWSIZE
                .AXI_03_AWVALID(axiHbm[3].awvalid),        // input wire AXI_03_AWVALID
                .AXI_03_RREADY(axiHbm[3].rready),          // input wire AXI_03_RREADY
                .AXI_03_BREADY(axiHbm[3].bready),          // input wire AXI_03_BREADY
                .AXI_03_WDATA(axiHbm[3].w.data),           // input wire [255 : 0] AXI_03_WDATA
                .AXI_03_WLAST(axiHbm[3].w.last),           // input wire AXI_03_WLAST
                .AXI_03_WSTRB(axiHbm[3].w.strb),           // input wire [31 : 0] AXI_03_WSTRB
                .AXI_03_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_03_WDATA_PARITY
                .AXI_03_WVALID(axiHbm[3].wvalid),          // input wire AXI_03_WVALID
                .AXI_04_ACLK(axiHbmClock[4]),              // input wire AXI_04_ACLK
                .AXI_04_ARESET_N(axiHbmResetN[4]),         // input wire AXI_04_ARESET_N
                .AXI_04_ARADDR(axiHbm[4].ar.addr),         // input wire [32 : 0] AXI_04_ARADDR
                .AXI_04_ARBURST(axiHbm[4].ar.burst),       // input wire [1 : 0] AXI_04_ARBURST
                .AXI_04_ARID(axiHbm[4].ar.id),             // input wire [5 : 0] AXI_04_ARID
                .AXI_04_ARLEN(axiHbm[4].ar.len),           // input wire [3 : 0] AXI_04_ARLEN
                .AXI_04_ARSIZE(axiHbm[4].ar.size),         // input wire [2 : 0] AXI_04_ARSIZE
                .AXI_04_ARVALID(axiHbm[4].arvalid),        // input wire AXI_04_ARVALID
                .AXI_04_AWADDR(axiHbm[4].aw.addr),         // input wire [32 : 0] AXI_04_AWADDR
                .AXI_04_AWBURST(axiHbm[4].aw.burst),       // input wire [1 : 0] AXI_04_AWBURST
                .AXI_04_AWID(axiHbm[4].aw.id),             // input wire [5 : 0] AXI_04_AWID
                .AXI_04_AWLEN(axiHbm[4].aw.len),           // input wire [3 : 0] AXI_04_AWLEN
                .AXI_04_AWSIZE(axiHbm[4].aw.size),         // input wire [2 : 0] AXI_04_AWSIZE
                .AXI_04_AWVALID(axiHbm[4].awvalid),        // input wire AXI_04_AWVALID
                .AXI_04_RREADY(axiHbm[4].rready),          // input wire AXI_04_RREADY
                .AXI_04_BREADY(axiHbm[4].bready),          // input wire AXI_04_BREADY
                .AXI_04_WDATA(axiHbm[4].w.data),           // input wire [255 : 0] AXI_04_WDATA
                .AXI_04_WLAST(axiHbm[4].w.last),           // input wire AXI_04_WLAST
                .AXI_04_WSTRB(axiHbm[4].w.strb),           // input wire [31 : 0] AXI_04_WSTRB
                .AXI_04_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_04_WDATA_PARITY
                .AXI_04_WVALID(axiHbm[4].wvalid),          // input wire AXI_04_WVALID
                .AXI_05_ACLK(axiHbmClock[5]),              // input wire AXI_05_ACLK
                .AXI_05_ARESET_N(axiHbmResetN[5]),         // input wire AXI_05_ARESET_N
                .AXI_05_ARADDR(axiHbm[5].ar.addr),         // input wire [32 : 0] AXI_05_ARADDR
                .AXI_05_ARBURST(axiHbm[5].ar.burst),       // input wire [1 : 0] AXI_05_ARBURST
                .AXI_05_ARID(axiHbm[5].ar.id),             // input wire [5 : 0] AXI_05_ARID
                .AXI_05_ARLEN(axiHbm[5].ar.len),           // input wire [3 : 0] AXI_05_ARLEN
                .AXI_05_ARSIZE(axiHbm[5].ar.size),         // input wire [2 : 0] AXI_05_ARSIZE
                .AXI_05_ARVALID(axiHbm[5].arvalid),        // input wire AXI_05_ARVALID
                .AXI_05_AWADDR(axiHbm[5].aw.addr),         // input wire [32 : 0] AXI_05_AWADDR
                .AXI_05_AWBURST(axiHbm[5].aw.burst),       // input wire [1 : 0] AXI_05_AWBURST
                .AXI_05_AWID(axiHbm[5].aw.id),             // input wire [5 : 0] AXI_05_AWID
                .AXI_05_AWLEN(axiHbm[5].aw.len),           // input wire [3 : 0] AXI_05_AWLEN
                .AXI_05_AWSIZE(axiHbm[5].aw.size),         // input wire [2 : 0] AXI_05_AWSIZE
                .AXI_05_AWVALID(axiHbm[5].awvalid),        // input wire AXI_05_AWVALID
                .AXI_05_RREADY(axiHbm[5].rready),          // input wire AXI_05_RREADY
                .AXI_05_BREADY(axiHbm[5].bready),          // input wire AXI_05_BREADY
                .AXI_05_WDATA(axiHbm[5].w.data),           // input wire [255 : 0] AXI_05_WDATA
                .AXI_05_WLAST(axiHbm[5].w.last),           // input wire AXI_05_WLAST
                .AXI_05_WSTRB(axiHbm[5].w.strb),           // input wire [31 : 0] AXI_05_WSTRB
                .AXI_05_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_05_WDATA_PARITY
                .AXI_05_WVALID(axiHbm[5].wvalid),          // input wire AXI_05_WVALID
                .AXI_06_ACLK(axiHbmClock[6]),              // input wire AXI_06_ACLK
                .AXI_06_ARESET_N(axiHbmResetN[6]),         // input wire AXI_06_ARESET_N
                .AXI_06_ARADDR(axiHbm[6].ar.addr),         // input wire [32 : 0] AXI_06_ARADDR
                .AXI_06_ARBURST(axiHbm[6].ar.burst),       // input wire [1 : 0] AXI_06_ARBURST
                .AXI_06_ARID(axiHbm[6].ar.id),             // input wire [5 : 0] AXI_06_ARID
                .AXI_06_ARLEN(axiHbm[6].ar.len),           // input wire [3 : 0] AXI_06_ARLEN
                .AXI_06_ARSIZE(axiHbm[6].ar.size),         // input wire [2 : 0] AXI_06_ARSIZE
                .AXI_06_ARVALID(axiHbm[6].arvalid),        // input wire AXI_06_ARVALID
                .AXI_06_AWADDR(axiHbm[6].aw.addr),         // input wire [32 : 0] AXI_06_AWADDR
                .AXI_06_AWBURST(axiHbm[6].aw.burst),       // input wire [1 : 0] AXI_06_AWBURST
                .AXI_06_AWID(axiHbm[6].aw.id),             // input wire [5 : 0] AXI_06_AWID
                .AXI_06_AWLEN(axiHbm[6].aw.len),           // input wire [3 : 0] AXI_06_AWLEN
                .AXI_06_AWSIZE(axiHbm[6].aw.size),         // input wire [2 : 0] AXI_06_AWSIZE
                .AXI_06_AWVALID(axiHbm[6].awvalid),        // input wire AXI_06_AWVALID
                .AXI_06_RREADY(axiHbm[6].rready),          // input wire AXI_06_RREADY
                .AXI_06_BREADY(axiHbm[6].bready),          // input wire AXI_06_BREADY
                .AXI_06_WDATA(axiHbm[6].w.data),           // input wire [255 : 0] AXI_06_WDATA
                .AXI_06_WLAST(axiHbm[6].w.last),           // input wire AXI_06_WLAST
                .AXI_06_WSTRB(axiHbm[6].w.strb),           // input wire [31 : 0] AXI_06_WSTRB
                .AXI_06_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_06_WDATA_PARITY
                .AXI_06_WVALID(axiHbm[6].wvalid),          // input wire AXI_06_WVALID
                .AXI_07_ACLK(axiHbmClock[7]),              // input wire AXI_07_ACLK
                .AXI_07_ARESET_N(axiHbmResetN[7]),         // input wire AXI_07_ARESET_N
                .AXI_07_ARADDR(axiHbm[7].ar.addr),         // input wire [32 : 0] AXI_07_ARADDR
                .AXI_07_ARBURST(axiHbm[7].ar.burst),       // input wire [1 : 0] AXI_07_ARBURST
                .AXI_07_ARID(axiHbm[7].ar.id),             // input wire [5 : 0] AXI_07_ARID
                .AXI_07_ARLEN(axiHbm[7].ar.len),           // input wire [3 : 0] AXI_07_ARLEN
                .AXI_07_ARSIZE(axiHbm[7].ar.size),         // input wire [2 : 0] AXI_07_ARSIZE
                .AXI_07_ARVALID(axiHbm[7].arvalid),        // input wire AXI_07_ARVALID
                .AXI_07_AWADDR(axiHbm[7].aw.addr),         // input wire [32 : 0] AXI_07_AWADDR
                .AXI_07_AWBURST(axiHbm[7].aw.burst),       // input wire [1 : 0] AXI_07_AWBURST
                .AXI_07_AWID(axiHbm[7].aw.id),             // input wire [5 : 0] AXI_07_AWID
                .AXI_07_AWLEN(axiHbm[7].aw.len),           // input wire [3 : 0] AXI_07_AWLEN
                .AXI_07_AWSIZE(axiHbm[7].aw.size),         // input wire [2 : 0] AXI_07_AWSIZE
                .AXI_07_AWVALID(axiHbm[7].awvalid),        // input wire AXI_07_AWVALID
                .AXI_07_RREADY(axiHbm[7].rready),          // input wire AXI_07_RREADY
                .AXI_07_BREADY(axiHbm[7].bready),          // input wire AXI_07_BREADY
                .AXI_07_WDATA(axiHbm[7].w.data),           // input wire [255 : 0] AXI_07_WDATA
                .AXI_07_WLAST(axiHbm[7].w.last),           // input wire AXI_07_WLAST
                .AXI_07_WSTRB(axiHbm[7].w.strb),           // input wire [31 : 0] AXI_07_WSTRB
                .AXI_07_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_07_WDATA_PARITY
                .AXI_07_WVALID(axiHbm[7].wvalid),          // input wire AXI_07_WVALID
                .AXI_08_ACLK(axiHbmClock[8]),              // input wire AXI_08_ACLK
                .AXI_08_ARESET_N(axiHbmResetN[8]),         // input wire AXI_08_ARESET_N
                .AXI_08_ARADDR(axiHbm[8].ar.addr),         // input wire [32 : 0] AXI_08_ARADDR
                .AXI_08_ARBURST(axiHbm[8].ar.burst),       // input wire [1 : 0] AXI_08_ARBURST
                .AXI_08_ARID(axiHbm[8].ar.id),             // input wire [5 : 0] AXI_08_ARID
                .AXI_08_ARLEN(axiHbm[8].ar.len),           // input wire [3 : 0] AXI_08_ARLEN
                .AXI_08_ARSIZE(axiHbm[8].ar.size),         // input wire [2 : 0] AXI_08_ARSIZE
                .AXI_08_ARVALID(axiHbm[8].arvalid),        // input wire AXI_08_ARVALID
                .AXI_08_AWADDR(axiHbm[8].aw.addr),         // input wire [32 : 0] AXI_08_AWADDR
                .AXI_08_AWBURST(axiHbm[8].aw.burst),       // input wire [1 : 0] AXI_08_AWBURST
                .AXI_08_AWID(axiHbm[8].aw.id),             // input wire [5 : 0] AXI_08_AWID
                .AXI_08_AWLEN(axiHbm[8].aw.len),           // input wire [3 : 0] AXI_08_AWLEN
                .AXI_08_AWSIZE(axiHbm[8].aw.size),         // input wire [2 : 0] AXI_08_AWSIZE
                .AXI_08_AWVALID(axiHbm[8].awvalid),        // input wire AXI_08_AWVALID
                .AXI_08_RREADY(axiHbm[8].rready),          // input wire AXI_08_RREADY
                .AXI_08_BREADY(axiHbm[8].bready),          // input wire AXI_08_BREADY
                .AXI_08_WDATA(axiHbm[8].w.data),           // input wire [255 : 0] AXI_08_WDATA
                .AXI_08_WLAST(axiHbm[8].w.last),           // input wire AXI_08_WLAST
                .AXI_08_WSTRB(axiHbm[8].w.strb),           // input wire [31 : 0] AXI_08_WSTRB
                .AXI_08_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_08_WDATA_PARITY
                .AXI_08_WVALID(axiHbm[8].wvalid),          // input wire AXI_08_WVALID
                .AXI_09_ACLK(axiHbmClock[9]),              // input wire AXI_09_ACLK
                .AXI_09_ARESET_N(axiHbmResetN[9]),         // input wire AXI_09_ARESET_N
                .AXI_09_ARADDR(axiHbm[9].ar.addr),         // input wire [32 : 0] AXI_09_ARADDR
                .AXI_09_ARBURST(axiHbm[9].ar.burst),       // input wire [1 : 0] AXI_09_ARBURST
                .AXI_09_ARID(axiHbm[9].ar.id),             // input wire [5 : 0] AXI_09_ARID
                .AXI_09_ARLEN(axiHbm[9].ar.len),           // input wire [3 : 0] AXI_09_ARLEN
                .AXI_09_ARSIZE(axiHbm[9].ar.size),         // input wire [2 : 0] AXI_09_ARSIZE
                .AXI_09_ARVALID(axiHbm[9].arvalid),        // input wire AXI_09_ARVALID
                .AXI_09_AWADDR(axiHbm[9].aw.addr),         // input wire [32 : 0] AXI_09_AWADDR
                .AXI_09_AWBURST(axiHbm[9].aw.burst),       // input wire [1 : 0] AXI_09_AWBURST
                .AXI_09_AWID(axiHbm[9].aw.id),             // input wire [5 : 0] AXI_09_AWID
                .AXI_09_AWLEN(axiHbm[9].aw.len),           // input wire [3 : 0] AXI_09_AWLEN
                .AXI_09_AWSIZE(axiHbm[9].aw.size),         // input wire [2 : 0] AXI_09_AWSIZE
                .AXI_09_AWVALID(axiHbm[9].awvalid),        // input wire AXI_09_AWVALID
                .AXI_09_RREADY(axiHbm[9].rready),          // input wire AXI_09_RREADY
                .AXI_09_BREADY(axiHbm[9].bready),          // input wire AXI_09_BREADY
                .AXI_09_WDATA(axiHbm[9].w.data),           // input wire [255 : 0] AXI_09_WDATA
                .AXI_09_WLAST(axiHbm[9].w.last),           // input wire AXI_09_WLAST
                .AXI_09_WSTRB(axiHbm[9].w.strb),           // input wire [31 : 0] AXI_09_WSTRB
                .AXI_09_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_09_WDATA_PARITY
                .AXI_09_WVALID(axiHbm[9].wvalid),          // input wire AXI_09_WVALID
                .AXI_10_ACLK(axiHbmClock[10]),              // input wire AXI_10_ACLK
                .AXI_10_ARESET_N(axiHbmResetN[10]),         // input wire AXI_10_ARESET_N
                .AXI_10_ARADDR(axiHbm[10].ar.addr),         // input wire [32 : 0] AXI_10_ARADDR
                .AXI_10_ARBURST(axiHbm[10].ar.burst),       // input wire [1 : 0] AXI_10_ARBURST
                .AXI_10_ARID(axiHbm[10].ar.id),             // input wire [5 : 0] AXI_10_ARID
                .AXI_10_ARLEN(axiHbm[10].ar.len),           // input wire [3 : 0] AXI_10_ARLEN
                .AXI_10_ARSIZE(axiHbm[10].ar.size),         // input wire [2 : 0] AXI_10_ARSIZE
                .AXI_10_ARVALID(axiHbm[10].arvalid),        // input wire AXI_10_ARVALID
                .AXI_10_AWADDR(axiHbm[10].aw.addr),         // input wire [32 : 0] AXI_10_AWADDR
                .AXI_10_AWBURST(axiHbm[10].aw.burst),       // input wire [1 : 0] AXI_10_AWBURST
                .AXI_10_AWID(axiHbm[10].aw.id),             // input wire [5 : 0] AXI_10_AWID
                .AXI_10_AWLEN(axiHbm[10].aw.len),           // input wire [3 : 0] AXI_10_AWLEN
                .AXI_10_AWSIZE(axiHbm[10].aw.size),         // input wire [2 : 0] AXI_10_AWSIZE
                .AXI_10_AWVALID(axiHbm[10].awvalid),        // input wire AXI_10_AWVALID
                .AXI_10_RREADY(axiHbm[10].rready),          // input wire AXI_10_RREADY
                .AXI_10_BREADY(axiHbm[10].bready),          // input wire AXI_10_BREADY
                .AXI_10_WDATA(axiHbm[10].w.data),           // input wire [255 : 0] AXI_10_WDATA
                .AXI_10_WLAST(axiHbm[10].w.last),           // input wire AXI_10_WLAST
                .AXI_10_WSTRB(axiHbm[10].w.strb),           // input wire [31 : 0] AXI_10_WSTRB
                .AXI_10_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_10_WDATA_PARITY
                .AXI_10_WVALID(axiHbm[10].wvalid),          // input wire AXI_10_WVALID
                .AXI_11_ACLK(axiHbmClock[11]),              // input wire AXI_11_ACLK
                .AXI_11_ARESET_N(axiHbmResetN[11]),         // input wire AXI_11_ARESET_N
                .AXI_11_ARADDR(axiHbm[11].ar.addr),         // input wire [32 : 0] AXI_11_ARADDR
                .AXI_11_ARBURST(axiHbm[11].ar.burst),       // input wire [1 : 0] AXI_11_ARBURST
                .AXI_11_ARID(axiHbm[11].ar.id),             // input wire [5 : 0] AXI_11_ARID
                .AXI_11_ARLEN(axiHbm[11].ar.len),           // input wire [3 : 0] AXI_11_ARLEN
                .AXI_11_ARSIZE(axiHbm[11].ar.size),         // input wire [2 : 0] AXI_11_ARSIZE
                .AXI_11_ARVALID(axiHbm[11].arvalid),        // input wire AXI_11_ARVALID
                .AXI_11_AWADDR(axiHbm[11].aw.addr),         // input wire [32 : 0] AXI_11_AWADDR
                .AXI_11_AWBURST(axiHbm[11].aw.burst),       // input wire [1 : 0] AXI_11_AWBURST
                .AXI_11_AWID(axiHbm[11].aw.id),             // input wire [5 : 0] AXI_11_AWID
                .AXI_11_AWLEN(axiHbm[11].aw.len),           // input wire [3 : 0] AXI_11_AWLEN
                .AXI_11_AWSIZE(axiHbm[11].aw.size),         // input wire [2 : 0] AXI_11_AWSIZE
                .AXI_11_AWVALID(axiHbm[11].awvalid),        // input wire AXI_11_AWVALID
                .AXI_11_RREADY(axiHbm[11].rready),          // input wire AXI_11_RREADY
                .AXI_11_BREADY(axiHbm[11].bready),          // input wire AXI_11_BREADY
                .AXI_11_WDATA(axiHbm[11].w.data),           // input wire [255 : 0] AXI_11_WDATA
                .AXI_11_WLAST(axiHbm[11].w.last),           // input wire AXI_11_WLAST
                .AXI_11_WSTRB(axiHbm[11].w.strb),           // input wire [31 : 0] AXI_11_WSTRB
                .AXI_11_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_11_WDATA_PARITY
                .AXI_11_WVALID(axiHbm[11].wvalid),          // input wire AXI_11_WVALID
                .AXI_12_ACLK(axiHbmClock[12]),              // input wire AXI_12_ACLK
                .AXI_12_ARESET_N(axiHbmResetN[12]),         // input wire AXI_12_ARESET_N
                .AXI_12_ARADDR(axiHbm[12].ar.addr),         // input wire [32 : 0] AXI_12_ARADDR
                .AXI_12_ARBURST(axiHbm[12].ar.burst),       // input wire [1 : 0] AXI_12_ARBURST
                .AXI_12_ARID(axiHbm[12].ar.id),             // input wire [5 : 0] AXI_12_ARID
                .AXI_12_ARLEN(axiHbm[12].ar.len),           // input wire [3 : 0] AXI_12_ARLEN
                .AXI_12_ARSIZE(axiHbm[12].ar.size),         // input wire [2 : 0] AXI_12_ARSIZE
                .AXI_12_ARVALID(axiHbm[12].arvalid),        // input wire AXI_12_ARVALID
                .AXI_12_AWADDR(axiHbm[12].aw.addr),         // input wire [32 : 0] AXI_12_AWADDR
                .AXI_12_AWBURST(axiHbm[12].aw.burst),       // input wire [1 : 0] AXI_12_AWBURST
                .AXI_12_AWID(axiHbm[12].aw.id),             // input wire [5 : 0] AXI_12_AWID
                .AXI_12_AWLEN(axiHbm[12].aw.len),           // input wire [3 : 0] AXI_12_AWLEN
                .AXI_12_AWSIZE(axiHbm[12].aw.size),         // input wire [2 : 0] AXI_12_AWSIZE
                .AXI_12_AWVALID(axiHbm[12].awvalid),        // input wire AXI_12_AWVALID
                .AXI_12_RREADY(axiHbm[12].rready),          // input wire AXI_12_RREADY
                .AXI_12_BREADY(axiHbm[12].bready),          // input wire AXI_12_BREADY
                .AXI_12_WDATA(axiHbm[12].w.data),           // input wire [255 : 0] AXI_12_WDATA
                .AXI_12_WLAST(axiHbm[12].w.last),           // input wire AXI_12_WLAST
                .AXI_12_WSTRB(axiHbm[12].w.strb),           // input wire [31 : 0] AXI_12_WSTRB
                .AXI_12_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_12_WDATA_PARITY
                .AXI_12_WVALID(axiHbm[12].wvalid),          // input wire AXI_12_WVALID
                .AXI_13_ACLK(axiHbmClock[13]),              // input wire AXI_13_ACLK
                .AXI_13_ARESET_N(axiHbmResetN[13]),         // input wire AXI_13_ARESET_N
                .AXI_13_ARADDR(axiHbm[13].ar.addr),         // input wire [32 : 0] AXI_13_ARADDR
                .AXI_13_ARBURST(axiHbm[13].ar.burst),       // input wire [1 : 0] AXI_13_ARBURST
                .AXI_13_ARID(axiHbm[13].ar.id),             // input wire [5 : 0] AXI_13_ARID
                .AXI_13_ARLEN(axiHbm[13].ar.len),           // input wire [3 : 0] AXI_13_ARLEN
                .AXI_13_ARSIZE(axiHbm[13].ar.size),         // input wire [2 : 0] AXI_13_ARSIZE
                .AXI_13_ARVALID(axiHbm[13].arvalid),        // input wire AXI_13_ARVALID
                .AXI_13_AWADDR(axiHbm[13].aw.addr),         // input wire [32 : 0] AXI_13_AWADDR
                .AXI_13_AWBURST(axiHbm[13].aw.burst),       // input wire [1 : 0] AXI_13_AWBURST
                .AXI_13_AWID(axiHbm[13].aw.id),             // input wire [5 : 0] AXI_13_AWID
                .AXI_13_AWLEN(axiHbm[13].aw.len),           // input wire [3 : 0] AXI_13_AWLEN
                .AXI_13_AWSIZE(axiHbm[13].aw.size),         // input wire [2 : 0] AXI_13_AWSIZE
                .AXI_13_AWVALID(axiHbm[13].awvalid),        // input wire AXI_13_AWVALID
                .AXI_13_RREADY(axiHbm[13].rready),          // input wire AXI_13_RREADY
                .AXI_13_BREADY(axiHbm[13].bready),          // input wire AXI_13_BREADY
                .AXI_13_WDATA(axiHbm[13].w.data),           // input wire [255 : 0] AXI_13_WDATA
                .AXI_13_WLAST(axiHbm[13].w.last),           // input wire AXI_13_WLAST
                .AXI_13_WSTRB(axiHbm[13].w.strb),           // input wire [31 : 0] AXI_13_WSTRB
                .AXI_13_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_13_WDATA_PARITY
                .AXI_13_WVALID(axiHbm[13].wvalid),          // input wire AXI_13_WVALID
                .AXI_14_ACLK(axiHbmClock[14]),              // input wire AXI_14_ACLK
                .AXI_14_ARESET_N(axiHbmResetN[14]),         // input wire AXI_14_ARESET_N
                .AXI_14_ARADDR(axiHbm[14].ar.addr),         // input wire [32 : 0] AXI_14_ARADDR
                .AXI_14_ARBURST(axiHbm[14].ar.burst),       // input wire [1 : 0] AXI_14_ARBURST
                .AXI_14_ARID(axiHbm[14].ar.id),             // input wire [5 : 0] AXI_14_ARID
                .AXI_14_ARLEN(axiHbm[14].ar.len),           // input wire [3 : 0] AXI_14_ARLEN
                .AXI_14_ARSIZE(axiHbm[14].ar.size),         // input wire [2 : 0] AXI_14_ARSIZE
                .AXI_14_ARVALID(axiHbm[14].arvalid),        // input wire AXI_14_ARVALID
                .AXI_14_AWADDR(axiHbm[14].aw.addr),         // input wire [32 : 0] AXI_14_AWADDR
                .AXI_14_AWBURST(axiHbm[14].aw.burst),       // input wire [1 : 0] AXI_14_AWBURST
                .AXI_14_AWID(axiHbm[14].aw.id),             // input wire [5 : 0] AXI_14_AWID
                .AXI_14_AWLEN(axiHbm[14].aw.len),           // input wire [3 : 0] AXI_14_AWLEN
                .AXI_14_AWSIZE(axiHbm[14].aw.size),         // input wire [2 : 0] AXI_14_AWSIZE
                .AXI_14_AWVALID(axiHbm[14].awvalid),        // input wire AXI_14_AWVALID
                .AXI_14_RREADY(axiHbm[14].rready),          // input wire AXI_14_RREADY
                .AXI_14_BREADY(axiHbm[14].bready),          // input wire AXI_14_BREADY
                .AXI_14_WDATA(axiHbm[14].w.data),           // input wire [255 : 0] AXI_14_WDATA
                .AXI_14_WLAST(axiHbm[14].w.last),           // input wire AXI_14_WLAST
                .AXI_14_WSTRB(axiHbm[14].w.strb),           // input wire [31 : 0] AXI_14_WSTRB
                .AXI_14_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_14_WDATA_PARITY
                .AXI_14_WVALID(axiHbm[14].wvalid),          // input wire AXI_14_WVALID
                .AXI_15_ACLK(axiHbmClock[15]),              // input wire AXI_15_ACLK
                .AXI_15_ARESET_N(axiHbmResetN[15]),         // input wire AXI_15_ARESET_N
                .AXI_15_ARADDR(axiHbm[15].ar.addr),         // input wire [32 : 0] AXI_15_ARADDR
                .AXI_15_ARBURST(axiHbm[15].ar.burst),       // input wire [1 : 0] AXI_15_ARBURST
                .AXI_15_ARID(axiHbm[15].ar.id),             // input wire [5 : 0] AXI_15_ARID
                .AXI_15_ARLEN(axiHbm[15].ar.len),           // input wire [3 : 0] AXI_15_ARLEN
                .AXI_15_ARSIZE(axiHbm[15].ar.size),         // input wire [2 : 0] AXI_15_ARSIZE
                .AXI_15_ARVALID(axiHbm[15].arvalid),        // input wire AXI_15_ARVALID
                .AXI_15_AWADDR(axiHbm[15].aw.addr),         // input wire [32 : 0] AXI_15_AWADDR
                .AXI_15_AWBURST(axiHbm[15].aw.burst),       // input wire [1 : 0] AXI_15_AWBURST
                .AXI_15_AWID(axiHbm[15].aw.id),             // input wire [5 : 0] AXI_15_AWID
                .AXI_15_AWLEN(axiHbm[15].aw.len),           // input wire [3 : 0] AXI_15_AWLEN
                .AXI_15_AWSIZE(axiHbm[15].aw.size),         // input wire [2 : 0] AXI_15_AWSIZE
                .AXI_15_AWVALID(axiHbm[15].awvalid),        // input wire AXI_15_AWVALID
                .AXI_15_RREADY(axiHbm[15].rready),          // input wire AXI_15_RREADY
                .AXI_15_BREADY(axiHbm[15].bready),          // input wire AXI_15_BREADY
                .AXI_15_WDATA(axiHbm[15].w.data),           // input wire [255 : 0] AXI_15_WDATA
                .AXI_15_WLAST(axiHbm[15].w.last),           // input wire AXI_15_WLAST
                .AXI_15_WSTRB(axiHbm[15].w.strb),           // input wire [31 : 0] AXI_15_WSTRB
                .AXI_15_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_15_WDATA_PARITY
                .AXI_15_WVALID(axiHbm[15].wvalid),          // input wire AXI_15_WVALID
                .AXI_16_ACLK(axiHbmClock[16]),              // input wire AXI_16_ACLK
                .AXI_16_ARESET_N(axiHbmResetN[16]),         // input wire AXI_16_ARESET_N
                .AXI_16_ARADDR(axiHbm[16].ar.addr),         // input wire [32 : 0] AXI_16_ARADDR
                .AXI_16_ARBURST(axiHbm[16].ar.burst),       // input wire [1 : 0] AXI_16_ARBURST
                .AXI_16_ARID(axiHbm[16].ar.id),             // input wire [5 : 0] AXI_16_ARID
                .AXI_16_ARLEN(axiHbm[16].ar.len),           // input wire [3 : 0] AXI_16_ARLEN
                .AXI_16_ARSIZE(axiHbm[16].ar.size),         // input wire [2 : 0] AXI_16_ARSIZE
                .AXI_16_ARVALID(axiHbm[16].arvalid),        // input wire AXI_16_ARVALID
                .AXI_16_AWADDR(axiHbm[16].aw.addr),         // input wire [32 : 0] AXI_16_AWADDR
                .AXI_16_AWBURST(axiHbm[16].aw.burst),       // input wire [1 : 0] AXI_16_AWBURST
                .AXI_16_AWID(axiHbm[16].aw.id),             // input wire [5 : 0] AXI_16_AWID
                .AXI_16_AWLEN(axiHbm[16].aw.len),           // input wire [3 : 0] AXI_16_AWLEN
                .AXI_16_AWSIZE(axiHbm[16].aw.size),         // input wire [2 : 0] AXI_16_AWSIZE
                .AXI_16_AWVALID(axiHbm[16].awvalid),        // input wire AXI_16_AWVALID
                .AXI_16_RREADY(axiHbm[16].rready),          // input wire AXI_16_RREADY
                .AXI_16_BREADY(axiHbm[16].bready),          // input wire AXI_16_BREADY
                .AXI_16_WDATA(axiHbm[16].w.data),           // input wire [255 : 0] AXI_16_WDATA
                .AXI_16_WLAST(axiHbm[16].w.last),           // input wire AXI_16_WLAST
                .AXI_16_WSTRB(axiHbm[16].w.strb),           // input wire [31 : 0] AXI_16_WSTRB
                .AXI_16_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_16_WDATA_PARITY
                .AXI_16_WVALID(axiHbm[16].wvalid),          // input wire AXI_16_WVALID
                .AXI_17_ACLK(axiHbmClock[17]),              // input wire AXI_17_ACLK
                .AXI_17_ARESET_N(axiHbmResetN[17]),         // input wire AXI_17_ARESET_N
                .AXI_17_ARADDR(axiHbm[17].ar.addr),         // input wire [32 : 0] AXI_17_ARADDR
                .AXI_17_ARBURST(axiHbm[17].ar.burst),       // input wire [1 : 0] AXI_17_ARBURST
                .AXI_17_ARID(axiHbm[17].ar.id),             // input wire [5 : 0] AXI_17_ARID
                .AXI_17_ARLEN(axiHbm[17].ar.len),           // input wire [3 : 0] AXI_17_ARLEN
                .AXI_17_ARSIZE(axiHbm[17].ar.size),         // input wire [2 : 0] AXI_17_ARSIZE
                .AXI_17_ARVALID(axiHbm[17].arvalid),        // input wire AXI_17_ARVALID
                .AXI_17_AWADDR(axiHbm[17].aw.addr),         // input wire [32 : 0] AXI_17_AWADDR
                .AXI_17_AWBURST(axiHbm[17].aw.burst),       // input wire [1 : 0] AXI_17_AWBURST
                .AXI_17_AWID(axiHbm[17].aw.id),             // input wire [5 : 0] AXI_17_AWID
                .AXI_17_AWLEN(axiHbm[17].aw.len),           // input wire [3 : 0] AXI_17_AWLEN
                .AXI_17_AWSIZE(axiHbm[17].aw.size),         // input wire [2 : 0] AXI_17_AWSIZE
                .AXI_17_AWVALID(axiHbm[17].awvalid),        // input wire AXI_17_AWVALID
                .AXI_17_RREADY(axiHbm[17].rready),          // input wire AXI_17_RREADY
                .AXI_17_BREADY(axiHbm[17].bready),          // input wire AXI_17_BREADY
                .AXI_17_WDATA(axiHbm[17].w.data),           // input wire [255 : 0] AXI_17_WDATA
                .AXI_17_WLAST(axiHbm[17].w.last),           // input wire AXI_17_WLAST
                .AXI_17_WSTRB(axiHbm[17].w.strb),           // input wire [31 : 0] AXI_17_WSTRB
                .AXI_17_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_17_WDATA_PARITY
                .AXI_17_WVALID(axiHbm[17].wvalid),          // input wire AXI_17_WVALID
                .AXI_18_ACLK(axiHbmClock[18]),              // input wire AXI_18_ACLK
                .AXI_18_ARESET_N(axiHbmResetN[18]),         // input wire AXI_18_ARESET_N
                .AXI_18_ARADDR(axiHbm[18].ar.addr),         // input wire [32 : 0] AXI_18_ARADDR
                .AXI_18_ARBURST(axiHbm[18].ar.burst),       // input wire [1 : 0] AXI_18_ARBURST
                .AXI_18_ARID(axiHbm[18].ar.id),             // input wire [5 : 0] AXI_18_ARID
                .AXI_18_ARLEN(axiHbm[18].ar.len),           // input wire [3 : 0] AXI_18_ARLEN
                .AXI_18_ARSIZE(axiHbm[18].ar.size),         // input wire [2 : 0] AXI_18_ARSIZE
                .AXI_18_ARVALID(axiHbm[18].arvalid),        // input wire AXI_18_ARVALID
                .AXI_18_AWADDR(axiHbm[18].aw.addr),         // input wire [32 : 0] AXI_18_AWADDR
                .AXI_18_AWBURST(axiHbm[18].aw.burst),       // input wire [1 : 0] AXI_18_AWBURST
                .AXI_18_AWID(axiHbm[18].aw.id),             // input wire [5 : 0] AXI_18_AWID
                .AXI_18_AWLEN(axiHbm[18].aw.len),           // input wire [3 : 0] AXI_18_AWLEN
                .AXI_18_AWSIZE(axiHbm[18].aw.size),         // input wire [2 : 0] AXI_18_AWSIZE
                .AXI_18_AWVALID(axiHbm[18].awvalid),        // input wire AXI_18_AWVALID
                .AXI_18_RREADY(axiHbm[18].rready),          // input wire AXI_18_RREADY
                .AXI_18_BREADY(axiHbm[18].bready),          // input wire AXI_18_BREADY
                .AXI_18_WDATA(axiHbm[18].w.data),           // input wire [255 : 0] AXI_18_WDATA
                .AXI_18_WLAST(axiHbm[18].w.last),           // input wire AXI_18_WLAST
                .AXI_18_WSTRB(axiHbm[18].w.strb),           // input wire [31 : 0] AXI_18_WSTRB
                .AXI_18_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_18_WDATA_PARITY
                .AXI_18_WVALID(axiHbm[18].wvalid),          // input wire AXI_18_WVALID
                .AXI_19_ACLK(axiHbmClock[19]),              // input wire AXI_19_ACLK
                .AXI_19_ARESET_N(axiHbmResetN[19]),         // input wire AXI_19_ARESET_N
                .AXI_19_ARADDR(axiHbm[19].ar.addr),         // input wire [32 : 0] AXI_19_ARADDR
                .AXI_19_ARBURST(axiHbm[19].ar.burst),       // input wire [1 : 0] AXI_19_ARBURST
                .AXI_19_ARID(axiHbm[19].ar.id),             // input wire [5 : 0] AXI_19_ARID
                .AXI_19_ARLEN(axiHbm[19].ar.len),           // input wire [3 : 0] AXI_19_ARLEN
                .AXI_19_ARSIZE(axiHbm[19].ar.size),         // input wire [2 : 0] AXI_19_ARSIZE
                .AXI_19_ARVALID(axiHbm[19].arvalid),        // input wire AXI_19_ARVALID
                .AXI_19_AWADDR(axiHbm[19].aw.addr),         // input wire [32 : 0] AXI_19_AWADDR
                .AXI_19_AWBURST(axiHbm[19].aw.burst),       // input wire [1 : 0] AXI_19_AWBURST
                .AXI_19_AWID(axiHbm[19].aw.id),             // input wire [5 : 0] AXI_19_AWID
                .AXI_19_AWLEN(axiHbm[19].aw.len),           // input wire [3 : 0] AXI_19_AWLEN
                .AXI_19_AWSIZE(axiHbm[19].aw.size),         // input wire [2 : 0] AXI_19_AWSIZE
                .AXI_19_AWVALID(axiHbm[19].awvalid),        // input wire AXI_19_AWVALID
                .AXI_19_RREADY(axiHbm[19].rready),          // input wire AXI_19_RREADY
                .AXI_19_BREADY(axiHbm[19].bready),          // input wire AXI_19_BREADY
                .AXI_19_WDATA(axiHbm[19].w.data),           // input wire [255 : 0] AXI_19_WDATA
                .AXI_19_WLAST(axiHbm[19].w.last),           // input wire AXI_19_WLAST
                .AXI_19_WSTRB(axiHbm[19].w.strb),           // input wire [31 : 0] AXI_19_WSTRB
                .AXI_19_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_19_WDATA_PARITY
                .AXI_19_WVALID(axiHbm[19].wvalid),          // input wire AXI_19_WVALID
                .AXI_20_ACLK(axiHbmClock[20]),              // input wire AXI_20_ACLK
                .AXI_20_ARESET_N(axiHbmResetN[20]),         // input wire AXI_20_ARESET_N
                .AXI_20_ARADDR(axiHbm[20].ar.addr),         // input wire [32 : 0] AXI_20_ARADDR
                .AXI_20_ARBURST(axiHbm[20].ar.burst),       // input wire [1 : 0] AXI_20_ARBURST
                .AXI_20_ARID(axiHbm[20].ar.id),             // input wire [5 : 0] AXI_20_ARID
                .AXI_20_ARLEN(axiHbm[20].ar.len),           // input wire [3 : 0] AXI_20_ARLEN
                .AXI_20_ARSIZE(axiHbm[20].ar.size),         // input wire [2 : 0] AXI_20_ARSIZE
                .AXI_20_ARVALID(axiHbm[20].arvalid),        // input wire AXI_20_ARVALID
                .AXI_20_AWADDR(axiHbm[20].aw.addr),         // input wire [32 : 0] AXI_20_AWADDR
                .AXI_20_AWBURST(axiHbm[20].aw.burst),       // input wire [1 : 0] AXI_20_AWBURST
                .AXI_20_AWID(axiHbm[20].aw.id),             // input wire [5 : 0] AXI_20_AWID
                .AXI_20_AWLEN(axiHbm[20].aw.len),           // input wire [3 : 0] AXI_20_AWLEN
                .AXI_20_AWSIZE(axiHbm[20].aw.size),         // input wire [2 : 0] AXI_20_AWSIZE
                .AXI_20_AWVALID(axiHbm[20].awvalid),        // input wire AXI_20_AWVALID
                .AXI_20_RREADY(axiHbm[20].rready),          // input wire AXI_20_RREADY
                .AXI_20_BREADY(axiHbm[20].bready),          // input wire AXI_20_BREADY
                .AXI_20_WDATA(axiHbm[20].w.data),           // input wire [255 : 0] AXI_20_WDATA
                .AXI_20_WLAST(axiHbm[20].w.last),           // input wire AXI_20_WLAST
                .AXI_20_WSTRB(axiHbm[20].w.strb),           // input wire [31 : 0] AXI_20_WSTRB
                .AXI_20_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_20_WDATA_PARITY
                .AXI_20_WVALID(axiHbm[20].wvalid),          // input wire AXI_20_WVALID
                .AXI_21_ACLK(axiHbmClock[21]),              // input wire AXI_21_ACLK
                .AXI_21_ARESET_N(axiHbmResetN[21]),         // input wire AXI_21_ARESET_N
                .AXI_21_ARADDR(axiHbm[21].ar.addr),         // input wire [32 : 0] AXI_21_ARADDR
                .AXI_21_ARBURST(axiHbm[21].ar.burst),       // input wire [1 : 0] AXI_21_ARBURST
                .AXI_21_ARID(axiHbm[21].ar.id),             // input wire [5 : 0] AXI_21_ARID
                .AXI_21_ARLEN(axiHbm[21].ar.len),           // input wire [3 : 0] AXI_21_ARLEN
                .AXI_21_ARSIZE(axiHbm[21].ar.size),         // input wire [2 : 0] AXI_21_ARSIZE
                .AXI_21_ARVALID(axiHbm[21].arvalid),        // input wire AXI_21_ARVALID
                .AXI_21_AWADDR(axiHbm[21].aw.addr),         // input wire [32 : 0] AXI_21_AWADDR
                .AXI_21_AWBURST(axiHbm[21].aw.burst),       // input wire [1 : 0] AXI_21_AWBURST
                .AXI_21_AWID(axiHbm[21].aw.id),             // input wire [5 : 0] AXI_21_AWID
                .AXI_21_AWLEN(axiHbm[21].aw.len),           // input wire [3 : 0] AXI_21_AWLEN
                .AXI_21_AWSIZE(axiHbm[21].aw.size),         // input wire [2 : 0] AXI_21_AWSIZE
                .AXI_21_AWVALID(axiHbm[21].awvalid),        // input wire AXI_21_AWVALID
                .AXI_21_RREADY(axiHbm[21].rready),          // input wire AXI_21_RREADY
                .AXI_21_BREADY(axiHbm[21].bready),          // input wire AXI_21_BREADY
                .AXI_21_WDATA(axiHbm[21].w.data),           // input wire [255 : 0] AXI_21_WDATA
                .AXI_21_WLAST(axiHbm[21].w.last),           // input wire AXI_21_WLAST
                .AXI_21_WSTRB(axiHbm[21].w.strb),           // input wire [31 : 0] AXI_21_WSTRB
                .AXI_21_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_21_WDATA_PARITY
                .AXI_21_WVALID(axiHbm[21].wvalid),          // input wire AXI_21_WVALID
                .AXI_22_ACLK(axiHbmClock[22]),              // input wire AXI_22_ACLK
                .AXI_22_ARESET_N(axiHbmResetN[22]),         // input wire AXI_22_ARESET_N
                .AXI_22_ARADDR(axiHbm[22].ar.addr),         // input wire [32 : 0] AXI_22_ARADDR
                .AXI_22_ARBURST(axiHbm[22].ar.burst),       // input wire [1 : 0] AXI_22_ARBURST
                .AXI_22_ARID(axiHbm[22].ar.id),             // input wire [5 : 0] AXI_22_ARID
                .AXI_22_ARLEN(axiHbm[22].ar.len),           // input wire [3 : 0] AXI_22_ARLEN
                .AXI_22_ARSIZE(axiHbm[22].ar.size),         // input wire [2 : 0] AXI_22_ARSIZE
                .AXI_22_ARVALID(axiHbm[22].arvalid),        // input wire AXI_22_ARVALID
                .AXI_22_AWADDR(axiHbm[22].aw.addr),         // input wire [32 : 0] AXI_22_AWADDR
                .AXI_22_AWBURST(axiHbm[22].aw.burst),       // input wire [1 : 0] AXI_22_AWBURST
                .AXI_22_AWID(axiHbm[22].aw.id),             // input wire [5 : 0] AXI_22_AWID
                .AXI_22_AWLEN(axiHbm[22].aw.len),           // input wire [3 : 0] AXI_22_AWLEN
                .AXI_22_AWSIZE(axiHbm[22].aw.size),         // input wire [2 : 0] AXI_22_AWSIZE
                .AXI_22_AWVALID(axiHbm[22].awvalid),        // input wire AXI_22_AWVALID
                .AXI_22_RREADY(axiHbm[22].rready),          // input wire AXI_22_RREADY
                .AXI_22_BREADY(axiHbm[22].bready),          // input wire AXI_22_BREADY
                .AXI_22_WDATA(axiHbm[22].w.data),           // input wire [255 : 0] AXI_22_WDATA
                .AXI_22_WLAST(axiHbm[22].w.last),           // input wire AXI_22_WLAST
                .AXI_22_WSTRB(axiHbm[22].w.strb),           // input wire [31 : 0] AXI_22_WSTRB
                .AXI_22_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_22_WDATA_PARITY
                .AXI_22_WVALID(axiHbm[22].wvalid),          // input wire AXI_22_WVALID
                .AXI_23_ACLK(axiHbmClock[23]),              // input wire AXI_23_ACLK
                .AXI_23_ARESET_N(axiHbmResetN[23]),         // input wire AXI_23_ARESET_N
                .AXI_23_ARADDR(axiHbm[23].ar.addr),         // input wire [32 : 0] AXI_23_ARADDR
                .AXI_23_ARBURST(axiHbm[23].ar.burst),       // input wire [1 : 0] AXI_23_ARBURST
                .AXI_23_ARID(axiHbm[23].ar.id),             // input wire [5 : 0] AXI_23_ARID
                .AXI_23_ARLEN(axiHbm[23].ar.len),           // input wire [3 : 0] AXI_23_ARLEN
                .AXI_23_ARSIZE(axiHbm[23].ar.size),         // input wire [2 : 0] AXI_23_ARSIZE
                .AXI_23_ARVALID(axiHbm[23].arvalid),        // input wire AXI_23_ARVALID
                .AXI_23_AWADDR(axiHbm[23].aw.addr),         // input wire [32 : 0] AXI_23_AWADDR
                .AXI_23_AWBURST(axiHbm[23].aw.burst),       // input wire [1 : 0] AXI_23_AWBURST
                .AXI_23_AWID(axiHbm[23].aw.id),             // input wire [5 : 0] AXI_23_AWID
                .AXI_23_AWLEN(axiHbm[23].aw.len),           // input wire [3 : 0] AXI_23_AWLEN
                .AXI_23_AWSIZE(axiHbm[23].aw.size),         // input wire [2 : 0] AXI_23_AWSIZE
                .AXI_23_AWVALID(axiHbm[23].awvalid),        // input wire AXI_23_AWVALID
                .AXI_23_RREADY(axiHbm[23].rready),          // input wire AXI_23_RREADY
                .AXI_23_BREADY(axiHbm[23].bready),          // input wire AXI_23_BREADY
                .AXI_23_WDATA(axiHbm[23].w.data),           // input wire [255 : 0] AXI_23_WDATA
                .AXI_23_WLAST(axiHbm[23].w.last),           // input wire AXI_23_WLAST
                .AXI_23_WSTRB(axiHbm[23].w.strb),           // input wire [31 : 0] AXI_23_WSTRB
                .AXI_23_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_23_WDATA_PARITY
                .AXI_23_WVALID(axiHbm[23].wvalid),          // input wire AXI_23_WVALID
                .AXI_24_ACLK(axiHbmClock[24]),              // input wire AXI_24_ACLK
                .AXI_24_ARESET_N(axiHbmResetN[24]),         // input wire AXI_24_ARESET_N
                .AXI_24_ARADDR(axiHbm[24].ar.addr),         // input wire [32 : 0] AXI_24_ARADDR
                .AXI_24_ARBURST(axiHbm[24].ar.burst),       // input wire [1 : 0] AXI_24_ARBURST
                .AXI_24_ARID(axiHbm[24].ar.id),             // input wire [5 : 0] AXI_24_ARID
                .AXI_24_ARLEN(axiHbm[24].ar.len),           // input wire [3 : 0] AXI_24_ARLEN
                .AXI_24_ARSIZE(axiHbm[24].ar.size),         // input wire [2 : 0] AXI_24_ARSIZE
                .AXI_24_ARVALID(axiHbm[24].arvalid),        // input wire AXI_24_ARVALID
                .AXI_24_AWADDR(axiHbm[24].aw.addr),         // input wire [32 : 0] AXI_24_AWADDR
                .AXI_24_AWBURST(axiHbm[24].aw.burst),       // input wire [1 : 0] AXI_24_AWBURST
                .AXI_24_AWID(axiHbm[24].aw.id),             // input wire [5 : 0] AXI_24_AWID
                .AXI_24_AWLEN(axiHbm[24].aw.len),           // input wire [3 : 0] AXI_24_AWLEN
                .AXI_24_AWSIZE(axiHbm[24].aw.size),         // input wire [2 : 0] AXI_24_AWSIZE
                .AXI_24_AWVALID(axiHbm[24].awvalid),        // input wire AXI_24_AWVALID
                .AXI_24_RREADY(axiHbm[24].rready),          // input wire AXI_24_RREADY
                .AXI_24_BREADY(axiHbm[24].bready),          // input wire AXI_24_BREADY
                .AXI_24_WDATA(axiHbm[24].w.data),           // input wire [255 : 0] AXI_24_WDATA
                .AXI_24_WLAST(axiHbm[24].w.last),           // input wire AXI_24_WLAST
                .AXI_24_WSTRB(axiHbm[24].w.strb),           // input wire [31 : 0] AXI_24_WSTRB
                .AXI_24_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_24_WDATA_PARITY
                .AXI_24_WVALID(axiHbm[24].wvalid),          // input wire AXI_24_WVALID
                .AXI_25_ACLK(axiHbmClock[25]),              // input wire AXI_25_ACLK
                .AXI_25_ARESET_N(axiHbmResetN[25]),         // input wire AXI_25_ARESET_N
                .AXI_25_ARADDR(axiHbm[25].ar.addr),         // input wire [32 : 0] AXI_25_ARADDR
                .AXI_25_ARBURST(axiHbm[25].ar.burst),       // input wire [1 : 0] AXI_25_ARBURST
                .AXI_25_ARID(axiHbm[25].ar.id),             // input wire [5 : 0] AXI_25_ARID
                .AXI_25_ARLEN(axiHbm[25].ar.len),           // input wire [3 : 0] AXI_25_ARLEN
                .AXI_25_ARSIZE(axiHbm[25].ar.size),         // input wire [2 : 0] AXI_25_ARSIZE
                .AXI_25_ARVALID(axiHbm[25].arvalid),        // input wire AXI_25_ARVALID
                .AXI_25_AWADDR(axiHbm[25].aw.addr),         // input wire [32 : 0] AXI_25_AWADDR
                .AXI_25_AWBURST(axiHbm[25].aw.burst),       // input wire [1 : 0] AXI_25_AWBURST
                .AXI_25_AWID(axiHbm[25].aw.id),             // input wire [5 : 0] AXI_25_AWID
                .AXI_25_AWLEN(axiHbm[25].aw.len),           // input wire [3 : 0] AXI_25_AWLEN
                .AXI_25_AWSIZE(axiHbm[25].aw.size),         // input wire [2 : 0] AXI_25_AWSIZE
                .AXI_25_AWVALID(axiHbm[25].awvalid),        // input wire AXI_25_AWVALID
                .AXI_25_RREADY(axiHbm[25].rready),          // input wire AXI_25_RREADY
                .AXI_25_BREADY(axiHbm[25].bready),          // input wire AXI_25_BREADY
                .AXI_25_WDATA(axiHbm[25].w.data),           // input wire [255 : 0] AXI_25_WDATA
                .AXI_25_WLAST(axiHbm[25].w.last),           // input wire AXI_25_WLAST
                .AXI_25_WSTRB(axiHbm[25].w.strb),           // input wire [31 : 0] AXI_25_WSTRB
                .AXI_25_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_25_WDATA_PARITY
                .AXI_25_WVALID(axiHbm[25].wvalid),          // input wire AXI_25_WVALID
                .AXI_26_ACLK(axiHbmClock[26]),              // input wire AXI_26_ACLK
                .AXI_26_ARESET_N(axiHbmResetN[26]),         // input wire AXI_26_ARESET_N
                .AXI_26_ARADDR(axiHbm[26].ar.addr),         // input wire [32 : 0] AXI_26_ARADDR
                .AXI_26_ARBURST(axiHbm[26].ar.burst),       // input wire [1 : 0] AXI_26_ARBURST
                .AXI_26_ARID(axiHbm[26].ar.id),             // input wire [5 : 0] AXI_26_ARID
                .AXI_26_ARLEN(axiHbm[26].ar.len),           // input wire [3 : 0] AXI_26_ARLEN
                .AXI_26_ARSIZE(axiHbm[26].ar.size),         // input wire [2 : 0] AXI_26_ARSIZE
                .AXI_26_ARVALID(axiHbm[26].arvalid),        // input wire AXI_26_ARVALID
                .AXI_26_AWADDR(axiHbm[26].aw.addr),         // input wire [32 : 0] AXI_26_AWADDR
                .AXI_26_AWBURST(axiHbm[26].aw.burst),       // input wire [1 : 0] AXI_26_AWBURST
                .AXI_26_AWID(axiHbm[26].aw.id),             // input wire [5 : 0] AXI_26_AWID
                .AXI_26_AWLEN(axiHbm[26].aw.len),           // input wire [3 : 0] AXI_26_AWLEN
                .AXI_26_AWSIZE(axiHbm[26].aw.size),         // input wire [2 : 0] AXI_26_AWSIZE
                .AXI_26_AWVALID(axiHbm[26].awvalid),        // input wire AXI_26_AWVALID
                .AXI_26_RREADY(axiHbm[26].rready),          // input wire AXI_26_RREADY
                .AXI_26_BREADY(axiHbm[26].bready),          // input wire AXI_26_BREADY
                .AXI_26_WDATA(axiHbm[26].w.data),           // input wire [255 : 0] AXI_26_WDATA
                .AXI_26_WLAST(axiHbm[26].w.last),           // input wire AXI_26_WLAST
                .AXI_26_WSTRB(axiHbm[26].w.strb),           // input wire [31 : 0] AXI_26_WSTRB
                .AXI_26_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_26_WDATA_PARITY
                .AXI_26_WVALID(axiHbm[26].wvalid),          // input wire AXI_26_WVALID
                .AXI_27_ACLK(axiHbmClock[27]),              // input wire AXI_27_ACLK
                .AXI_27_ARESET_N(axiHbmResetN[27]),         // input wire AXI_27_ARESET_N
                .AXI_27_ARADDR(axiHbm[27].ar.addr),         // input wire [32 : 0] AXI_27_ARADDR
                .AXI_27_ARBURST(axiHbm[27].ar.burst),       // input wire [1 : 0] AXI_27_ARBURST
                .AXI_27_ARID(axiHbm[27].ar.id),             // input wire [5 : 0] AXI_27_ARID
                .AXI_27_ARLEN(axiHbm[27].ar.len),           // input wire [3 : 0] AXI_27_ARLEN
                .AXI_27_ARSIZE(axiHbm[27].ar.size),         // input wire [2 : 0] AXI_27_ARSIZE
                .AXI_27_ARVALID(axiHbm[27].arvalid),        // input wire AXI_27_ARVALID
                .AXI_27_AWADDR(axiHbm[27].aw.addr),         // input wire [32 : 0] AXI_27_AWADDR
                .AXI_27_AWBURST(axiHbm[27].aw.burst),       // input wire [1 : 0] AXI_27_AWBURST
                .AXI_27_AWID(axiHbm[27].aw.id),             // input wire [5 : 0] AXI_27_AWID
                .AXI_27_AWLEN(axiHbm[27].aw.len),           // input wire [3 : 0] AXI_27_AWLEN
                .AXI_27_AWSIZE(axiHbm[27].aw.size),         // input wire [2 : 0] AXI_27_AWSIZE
                .AXI_27_AWVALID(axiHbm[27].awvalid),        // input wire AXI_27_AWVALID
                .AXI_27_RREADY(axiHbm[27].rready),          // input wire AXI_27_RREADY
                .AXI_27_BREADY(axiHbm[27].bready),          // input wire AXI_27_BREADY
                .AXI_27_WDATA(axiHbm[27].w.data),           // input wire [255 : 0] AXI_27_WDATA
                .AXI_27_WLAST(axiHbm[27].w.last),           // input wire AXI_27_WLAST
                .AXI_27_WSTRB(axiHbm[27].w.strb),           // input wire [31 : 0] AXI_27_WSTRB
                .AXI_27_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_27_WDATA_PARITY
                .AXI_27_WVALID(axiHbm[27].wvalid),          // input wire AXI_27_WVALID
                .AXI_28_ACLK(axiHbmClock[28]),              // input wire AXI_28_ACLK
                .AXI_28_ARESET_N(axiHbmResetN[28]),         // input wire AXI_28_ARESET_N
                .AXI_28_ARADDR(axiHbm[28].ar.addr),         // input wire [32 : 0] AXI_28_ARADDR
                .AXI_28_ARBURST(axiHbm[28].ar.burst),       // input wire [1 : 0] AXI_28_ARBURST
                .AXI_28_ARID(axiHbm[28].ar.id),             // input wire [5 : 0] AXI_28_ARID
                .AXI_28_ARLEN(axiHbm[28].ar.len),           // input wire [3 : 0] AXI_28_ARLEN
                .AXI_28_ARSIZE(axiHbm[28].ar.size),         // input wire [2 : 0] AXI_28_ARSIZE
                .AXI_28_ARVALID(axiHbm[28].arvalid),        // input wire AXI_28_ARVALID
                .AXI_28_AWADDR(axiHbm[28].aw.addr),         // input wire [32 : 0] AXI_28_AWADDR
                .AXI_28_AWBURST(axiHbm[28].aw.burst),       // input wire [1 : 0] AXI_28_AWBURST
                .AXI_28_AWID(axiHbm[28].aw.id),             // input wire [5 : 0] AXI_28_AWID
                .AXI_28_AWLEN(axiHbm[28].aw.len),           // input wire [3 : 0] AXI_28_AWLEN
                .AXI_28_AWSIZE(axiHbm[28].aw.size),         // input wire [2 : 0] AXI_28_AWSIZE
                .AXI_28_AWVALID(axiHbm[28].awvalid),        // input wire AXI_28_AWVALID
                .AXI_28_RREADY(axiHbm[28].rready),          // input wire AXI_28_RREADY
                .AXI_28_BREADY(axiHbm[28].bready),          // input wire AXI_28_BREADY
                .AXI_28_WDATA(axiHbm[28].w.data),           // input wire [255 : 0] AXI_28_WDATA
                .AXI_28_WLAST(axiHbm[28].w.last),           // input wire AXI_28_WLAST
                .AXI_28_WSTRB(axiHbm[28].w.strb),           // input wire [31 : 0] AXI_28_WSTRB
                .AXI_28_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_28_WDATA_PARITY
                .AXI_28_WVALID(axiHbm[28].wvalid),          // input wire AXI_28_WVALID
                .AXI_29_ACLK(axiHbmClock[29]),              // input wire AXI_29_ACLK
                .AXI_29_ARESET_N(axiHbmResetN[29]),         // input wire AXI_29_ARESET_N
                .AXI_29_ARADDR(axiHbm[29].ar.addr),         // input wire [32 : 0] AXI_29_ARADDR
                .AXI_29_ARBURST(axiHbm[29].ar.burst),       // input wire [1 : 0] AXI_29_ARBURST
                .AXI_29_ARID(axiHbm[29].ar.id),             // input wire [5 : 0] AXI_29_ARID
                .AXI_29_ARLEN(axiHbm[29].ar.len),           // input wire [3 : 0] AXI_29_ARLEN
                .AXI_29_ARSIZE(axiHbm[29].ar.size),         // input wire [2 : 0] AXI_29_ARSIZE
                .AXI_29_ARVALID(axiHbm[29].arvalid),        // input wire AXI_29_ARVALID
                .AXI_29_AWADDR(axiHbm[29].aw.addr),         // input wire [32 : 0] AXI_29_AWADDR
                .AXI_29_AWBURST(axiHbm[29].aw.burst),       // input wire [1 : 0] AXI_29_AWBURST
                .AXI_29_AWID(axiHbm[29].aw.id),             // input wire [5 : 0] AXI_29_AWID
                .AXI_29_AWLEN(axiHbm[29].aw.len),           // input wire [3 : 0] AXI_29_AWLEN
                .AXI_29_AWSIZE(axiHbm[29].aw.size),         // input wire [2 : 0] AXI_29_AWSIZE
                .AXI_29_AWVALID(axiHbm[29].awvalid),        // input wire AXI_29_AWVALID
                .AXI_29_RREADY(axiHbm[29].rready),          // input wire AXI_29_RREADY
                .AXI_29_BREADY(axiHbm[29].bready),          // input wire AXI_29_BREADY
                .AXI_29_WDATA(axiHbm[29].w.data),           // input wire [255 : 0] AXI_29_WDATA
                .AXI_29_WLAST(axiHbm[29].w.last),           // input wire AXI_29_WLAST
                .AXI_29_WSTRB(axiHbm[29].w.strb),           // input wire [31 : 0] AXI_29_WSTRB
                .AXI_29_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_29_WDATA_PARITY
                .AXI_29_WVALID(axiHbm[29].wvalid),          // input wire AXI_29_WVALID
                .AXI_30_ACLK(axiHbmClock[30]),              // input wire AXI_30_ACLK
                .AXI_30_ARESET_N(axiHbmResetN[30]),         // input wire AXI_30_ARESET_N
                .AXI_30_ARADDR(axiHbm[30].ar.addr),         // input wire [32 : 0] AXI_30_ARADDR
                .AXI_30_ARBURST(axiHbm[30].ar.burst),       // input wire [1 : 0] AXI_30_ARBURST
                .AXI_30_ARID(axiHbm[30].ar.id),             // input wire [5 : 0] AXI_30_ARID
                .AXI_30_ARLEN(axiHbm[30].ar.len),           // input wire [3 : 0] AXI_30_ARLEN
                .AXI_30_ARSIZE(axiHbm[30].ar.size),         // input wire [2 : 0] AXI_30_ARSIZE
                .AXI_30_ARVALID(axiHbm[30].arvalid),        // input wire AXI_30_ARVALID
                .AXI_30_AWADDR(axiHbm[30].aw.addr),         // input wire [32 : 0] AXI_30_AWADDR
                .AXI_30_AWBURST(axiHbm[30].aw.burst),       // input wire [1 : 0] AXI_30_AWBURST
                .AXI_30_AWID(axiHbm[30].aw.id),             // input wire [5 : 0] AXI_30_AWID
                .AXI_30_AWLEN(axiHbm[30].aw.len),           // input wire [3 : 0] AXI_30_AWLEN
                .AXI_30_AWSIZE(axiHbm[30].aw.size),         // input wire [2 : 0] AXI_30_AWSIZE
                .AXI_30_AWVALID(axiHbm[30].awvalid),        // input wire AXI_30_AWVALID
                .AXI_30_RREADY(axiHbm[30].rready),          // input wire AXI_30_RREADY
                .AXI_30_BREADY(axiHbm[30].bready),          // input wire AXI_30_BREADY
                .AXI_30_WDATA(axiHbm[30].w.data),           // input wire [255 : 0] AXI_30_WDATA
                .AXI_30_WLAST(axiHbm[30].w.last),           // input wire AXI_30_WLAST
                .AXI_30_WSTRB(axiHbm[30].w.strb),           // input wire [31 : 0] AXI_30_WSTRB
                .AXI_30_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_30_WDATA_PARITY
                .AXI_30_WVALID(axiHbm[30].wvalid),          // input wire AXI_30_WVALID
                .AXI_31_ACLK(axiHbmClock[31]),              // input wire AXI_31_ACLK
                .AXI_31_ARESET_N(axiHbmResetN[31]),         // input wire AXI_31_ARESET_N
                .AXI_31_ARADDR(axiHbm[31].ar.addr),         // input wire [32 : 0] AXI_31_ARADDR
                .AXI_31_ARBURST(axiHbm[31].ar.burst),       // input wire [1 : 0] AXI_31_ARBURST
                .AXI_31_ARID(axiHbm[31].ar.id),             // input wire [5 : 0] AXI_31_ARID
                .AXI_31_ARLEN(axiHbm[31].ar.len),           // input wire [3 : 0] AXI_31_ARLEN
                .AXI_31_ARSIZE(axiHbm[31].ar.size),         // input wire [2 : 0] AXI_31_ARSIZE
                .AXI_31_ARVALID(axiHbm[31].arvalid),        // input wire AXI_31_ARVALID
                .AXI_31_AWADDR(axiHbm[31].aw.addr),         // input wire [32 : 0] AXI_31_AWADDR
                .AXI_31_AWBURST(axiHbm[31].aw.burst),       // input wire [1 : 0] AXI_31_AWBURST
                .AXI_31_AWID(axiHbm[31].aw.id),             // input wire [5 : 0] AXI_31_AWID
                .AXI_31_AWLEN(axiHbm[31].aw.len),           // input wire [3 : 0] AXI_31_AWLEN
                .AXI_31_AWSIZE(axiHbm[31].aw.size),         // input wire [2 : 0] AXI_31_AWSIZE
                .AXI_31_AWVALID(axiHbm[31].awvalid),        // input wire AXI_31_AWVALID
                .AXI_31_RREADY(axiHbm[31].rready),          // input wire AXI_31_RREADY
                .AXI_31_BREADY(axiHbm[31].bready),          // input wire AXI_31_BREADY
                .AXI_31_WDATA(axiHbm[31].w.data),           // input wire [255 : 0] AXI_31_WDATA
                .AXI_31_WLAST(axiHbm[31].w.last),           // input wire AXI_31_WLAST
                .AXI_31_WSTRB(axiHbm[31].w.strb),           // input wire [31 : 0] AXI_31_WSTRB
                .AXI_31_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_31_WDATA_PARITY
                .AXI_31_WVALID(axiHbm[31].wvalid),          // input wire AXI_31_WVALID
                .APB_0_PWDATA(apb[0].wdata),                // input wire [31 : 0] APB_0_PWDATA
                .APB_0_PADDR(apb[0].address),               // input wire [21 : 0] APB_0_PADDR
                .APB_0_PCLK(clock),                         // input wire APB_0_PCLK
                .APB_0_PENABLE(apb[0].enable),              // input wire APB_0_PENABLE
                .APB_0_PRESET_N(resetApbN[0]),              // input wire APB_0_PRESET_N
                .APB_0_PSEL(apb[0].select),                 // input wire APB_0_PSEL
                .APB_0_PWRITE(apb[0].write),                // input wire APB_0_PWRITE
                .APB_1_PWDATA(apb[1].wdata),                // input wire [31 : 0] APB_1_PWDATA
                .APB_1_PADDR(apb[1].address),               // input wire [21 : 0] APB_1_PADDR
                .APB_1_PCLK(clock),                         // input wire APB_1_PCLK
                .APB_1_PENABLE(apb[1].enable),              // input wire APB_1_PENABLE
                .APB_1_PRESET_N(resetApbN[1]),              // input wire APB_1_PRESET_N
                .APB_1_PSEL(apb[1].select),                 // input wire APB_1_PSEL
                .APB_1_PWRITE(apb[1].write),                // input wire APB_1_PWRITE
                .AXI_00_ARREADY(axiHbmFb[0].arready),      // output wire AXI_00_ARREADY
                .AXI_00_AWREADY(axiHbmFb[0].awready),      // output wire AXI_00_AWREADY
                .AXI_00_RDATA_PARITY(),                     // output wire [31 : 0] AXI_00_RDATA_PARITY
                .AXI_00_RDATA(axiHbmFb[0].r.data),         // output wire [255 : 0] AXI_00_RDATA
                .AXI_00_RID(axiHbmFb[0].r.id),             // output wire [5 : 0] AXI_00_RID
                .AXI_00_RLAST(axiHbmFb[0].r.last),         // output wire AXI_00_RLAST
                .AXI_00_RRESP(axiHbmFb[0].r.resp),         // output wire [1 : 0] AXI_00_RRESP
                .AXI_00_RVALID(axiHbmFb[0].rvalid),        // output wire AXI_00_RVALID
                .AXI_00_WREADY(axiHbmFb[0].wready),        // output wire AXI_00_WREADY
                .AXI_00_BID(axiHbmFb[0].b.id),             // output wire [5 : 0] AXI_00_BID
                .AXI_00_BRESP(axiHbmFb[0].b.resp),         // output wire [1 : 0] AXI_00_BRESP
                .AXI_00_BVALID(axiHbmFb[0].bvalid),        // output wire AXI_00_BVALID
                .AXI_01_ARREADY(axiHbmFb[1].arready),      // output wire AXI_01_ARREADY
                .AXI_01_AWREADY(axiHbmFb[1].awready),      // output wire AXI_01_AWREADY
                .AXI_01_RDATA_PARITY(),                     // output wire [31 : 0] AXI_01_RDATA_PARITY
                .AXI_01_RDATA(axiHbmFb[1].r.data),         // output wire [255 : 0] AXI_01_RDATA
                .AXI_01_RID(axiHbmFb[1].r.id),             // output wire [5 : 0] AXI_01_RID
                .AXI_01_RLAST(axiHbmFb[1].r.last),         // output wire AXI_01_RLAST
                .AXI_01_RRESP(axiHbmFb[1].r.resp),         // output wire [1 : 0] AXI_01_RRESP
                .AXI_01_RVALID(axiHbmFb[1].rvalid),        // output wire AXI_01_RVALID
                .AXI_01_WREADY(axiHbmFb[1].wready),        // output wire AXI_01_WREADY
                .AXI_01_BID(axiHbmFb[1].b.id),             // output wire [5 : 0] AXI_01_BID
                .AXI_01_BRESP(axiHbmFb[1].b.resp),         // output wire [1 : 0] AXI_01_BRESP
                .AXI_01_BVALID(axiHbmFb[1].bvalid),        // output wire AXI_01_BVALID
                .AXI_02_ARREADY(axiHbmFb[2].arready),      // output wire AXI_02_ARREADY
                .AXI_02_AWREADY(axiHbmFb[2].awready),      // output wire AXI_02_AWREADY
                .AXI_02_RDATA_PARITY(),                     // output wire [31 : 0] AXI_02_RDATA_PARITY
                .AXI_02_RDATA(axiHbmFb[2].r.data),         // output wire [255 : 0] AXI_02_RDATA
                .AXI_02_RID(axiHbmFb[2].r.id),             // output wire [5 : 0] AXI_02_RID
                .AXI_02_RLAST(axiHbmFb[2].r.last),         // output wire AXI_02_RLAST
                .AXI_02_RRESP(axiHbmFb[2].r.resp),         // output wire [1 : 0] AXI_02_RRESP
                .AXI_02_RVALID(axiHbmFb[2].rvalid),        // output wire AXI_02_RVALID
                .AXI_02_WREADY(axiHbmFb[2].wready),        // output wire AXI_02_WREADY
                .AXI_02_BID(axiHbmFb[2].b.id),             // output wire [5 : 0] AXI_02_BID
                .AXI_02_BRESP(axiHbmFb[2].b.resp),         // output wire [1 : 0] AXI_02_BRESP
                .AXI_02_BVALID(axiHbmFb[2].bvalid),        // output wire AXI_02_BVALID
                .AXI_03_ARREADY(axiHbmFb[3].arready),      // output wire AXI_03_ARREADY
                .AXI_03_AWREADY(axiHbmFb[3].awready),      // output wire AXI_03_AWREADY
                .AXI_03_RDATA_PARITY(),                     // output wire [31 : 0] AXI_03_RDATA_PARITY
                .AXI_03_RDATA(axiHbmFb[3].r.data),         // output wire [255 : 0] AXI_03_RDATA
                .AXI_03_RID(axiHbmFb[3].r.id),             // output wire [5 : 0] AXI_03_RID
                .AXI_03_RLAST(axiHbmFb[3].r.last),         // output wire AXI_03_RLAST
                .AXI_03_RRESP(axiHbmFb[3].r.resp),         // output wire [1 : 0] AXI_03_RRESP
                .AXI_03_RVALID(axiHbmFb[3].rvalid),        // output wire AXI_03_RVALID
                .AXI_03_WREADY(axiHbmFb[3].wready),        // output wire AXI_03_WREADY
                .AXI_03_BID(axiHbmFb[3].b.id),             // output wire [5 : 0] AXI_03_BID
                .AXI_03_BRESP(axiHbmFb[3].b.resp),         // output wire [1 : 0] AXI_03_BRESP
                .AXI_03_BVALID(axiHbmFb[3].bvalid),        // output wire AXI_03_BVALID
                .AXI_04_ARREADY(axiHbmFb[4].arready),      // output wire AXI_04_ARREADY
                .AXI_04_AWREADY(axiHbmFb[4].awready),      // output wire AXI_04_AWREADY
                .AXI_04_RDATA_PARITY(),                     // output wire [31 : 0] AXI_04_RDATA_PARITY
                .AXI_04_RDATA(axiHbmFb[4].r.data),         // output wire [255 : 0] AXI_04_RDATA
                .AXI_04_RID(axiHbmFb[4].r.id),             // output wire [5 : 0] AXI_04_RID
                .AXI_04_RLAST(axiHbmFb[4].r.last),         // output wire AXI_04_RLAST
                .AXI_04_RRESP(axiHbmFb[4].r.resp),         // output wire [1 : 0] AXI_04_RRESP
                .AXI_04_RVALID(axiHbmFb[4].rvalid),        // output wire AXI_04_RVALID
                .AXI_04_WREADY(axiHbmFb[4].wready),        // output wire AXI_04_WREADY
                .AXI_04_BID(axiHbmFb[4].b.id),             // output wire [5 : 0] AXI_04_BID
                .AXI_04_BRESP(axiHbmFb[4].b.resp),         // output wire [1 : 0] AXI_04_BRESP
                .AXI_04_BVALID(axiHbmFb[4].bvalid),        // output wire AXI_04_BVALID
                .AXI_05_ARREADY(axiHbmFb[5].arready),      // output wire AXI_05_ARREADY
                .AXI_05_AWREADY(axiHbmFb[5].awready),      // output wire AXI_05_AWREADY
                .AXI_05_RDATA_PARITY(),                     // output wire [31 : 0] AXI_05_RDATA_PARITY
                .AXI_05_RDATA(axiHbmFb[5].r.data),         // output wire [255 : 0] AXI_05_RDATA
                .AXI_05_RID(axiHbmFb[5].r.id),             // output wire [5 : 0] AXI_05_RID
                .AXI_05_RLAST(axiHbmFb[5].r.last),         // output wire AXI_05_RLAST
                .AXI_05_RRESP(axiHbmFb[5].r.resp),         // output wire [1 : 0] AXI_05_RRESP
                .AXI_05_RVALID(axiHbmFb[5].rvalid),        // output wire AXI_05_RVALID
                .AXI_05_WREADY(axiHbmFb[5].wready),        // output wire AXI_05_WREADY
                .AXI_05_BID(axiHbmFb[5].b.id),             // output wire [5 : 0] AXI_05_BID
                .AXI_05_BRESP(axiHbmFb[5].b.resp),         // output wire [1 : 0] AXI_05_BRESP
                .AXI_05_BVALID(axiHbmFb[5].bvalid),        // output wire AXI_05_BVALID
                .AXI_06_ARREADY(axiHbmFb[6].arready),      // output wire AXI_06_ARREADY
                .AXI_06_AWREADY(axiHbmFb[6].awready),      // output wire AXI_06_AWREADY
                .AXI_06_RDATA_PARITY(),                     // output wire [31 : 0] AXI_06_RDATA_PARITY
                .AXI_06_RDATA(axiHbmFb[6].r.data),         // output wire [255 : 0] AXI_06_RDATA
                .AXI_06_RID(axiHbmFb[6].r.id),             // output wire [5 : 0] AXI_06_RID
                .AXI_06_RLAST(axiHbmFb[6].r.last),         // output wire AXI_06_RLAST
                .AXI_06_RRESP(axiHbmFb[6].r.resp),         // output wire [1 : 0] AXI_06_RRESP
                .AXI_06_RVALID(axiHbmFb[6].rvalid),        // output wire AXI_06_RVALID
                .AXI_06_WREADY(axiHbmFb[6].wready),        // output wire AXI_06_WREADY
                .AXI_06_BID(axiHbmFb[6].b.id),             // output wire [5 : 0] AXI_06_BID
                .AXI_06_BRESP(axiHbmFb[6].b.resp),         // output wire [1 : 0] AXI_06_BRESP
                .AXI_06_BVALID(axiHbmFb[6].bvalid),        // output wire AXI_06_BVALID
                .AXI_07_ARREADY(axiHbmFb[7].arready),      // output wire AXI_07_ARREADY
                .AXI_07_AWREADY(axiHbmFb[7].awready),      // output wire AXI_07_AWREADY
                .AXI_07_RDATA_PARITY(),                     // output wire [31 : 0] AXI_07_RDATA_PARITY
                .AXI_07_RDATA(axiHbmFb[7].r.data),         // output wire [255 : 0] AXI_07_RDATA
                .AXI_07_RID(axiHbmFb[7].r.id),             // output wire [5 : 0] AXI_07_RID
                .AXI_07_RLAST(axiHbmFb[7].r.last),         // output wire AXI_07_RLAST
                .AXI_07_RRESP(axiHbmFb[7].r.resp),         // output wire [1 : 0] AXI_07_RRESP
                .AXI_07_RVALID(axiHbmFb[7].rvalid),        // output wire AXI_07_RVALID
                .AXI_07_WREADY(axiHbmFb[7].wready),        // output wire AXI_07_WREADY
                .AXI_07_BID(axiHbmFb[7].b.id),             // output wire [5 : 0] AXI_07_BID
                .AXI_07_BRESP(axiHbmFb[7].b.resp),         // output wire [1 : 0] AXI_07_BRESP
                .AXI_07_BVALID(axiHbmFb[7].bvalid),        // output wire AXI_07_BVALID
                .AXI_08_ARREADY(axiHbmFb[8].arready),      // output wire AXI_08_ARREADY
                .AXI_08_AWREADY(axiHbmFb[8].awready),      // output wire AXI_08_AWREADY
                .AXI_08_RDATA_PARITY(),                     // output wire [31 : 0] AXI_08_RDATA_PARITY
                .AXI_08_RDATA(axiHbmFb[8].r.data),         // output wire [255 : 0] AXI_08_RDATA
                .AXI_08_RID(axiHbmFb[8].r.id),             // output wire [5 : 0] AXI_08_RID
                .AXI_08_RLAST(axiHbmFb[8].r.last),         // output wire AXI_08_RLAST
                .AXI_08_RRESP(axiHbmFb[8].r.resp),         // output wire [1 : 0] AXI_08_RRESP
                .AXI_08_RVALID(axiHbmFb[8].rvalid),        // output wire AXI_08_RVALID
                .AXI_08_WREADY(axiHbmFb[8].wready),        // output wire AXI_08_WREADY
                .AXI_08_BID(axiHbmFb[8].b.id),             // output wire [5 : 0] AXI_08_BID
                .AXI_08_BRESP(axiHbmFb[8].b.resp),         // output wire [1 : 0] AXI_08_BRESP
                .AXI_08_BVALID(axiHbmFb[8].bvalid),        // output wire AXI_08_BVALID
                .AXI_09_ARREADY(axiHbmFb[9].arready),      // output wire AXI_09_ARREADY
                .AXI_09_AWREADY(axiHbmFb[9].awready),      // output wire AXI_09_AWREADY
                .AXI_09_RDATA_PARITY(),                     // output wire [31 : 0] AXI_09_RDATA_PARITY
                .AXI_09_RDATA(axiHbmFb[9].r.data),         // output wire [255 : 0] AXI_09_RDATA
                .AXI_09_RID(axiHbmFb[9].r.id),             // output wire [5 : 0] AXI_09_RID
                .AXI_09_RLAST(axiHbmFb[9].r.last),         // output wire AXI_09_RLAST
                .AXI_09_RRESP(axiHbmFb[9].r.resp),         // output wire [1 : 0] AXI_09_RRESP
                .AXI_09_RVALID(axiHbmFb[9].rvalid),        // output wire AXI_09_RVALID
                .AXI_09_WREADY(axiHbmFb[9].wready),        // output wire AXI_09_WREADY
                .AXI_09_BID(axiHbmFb[9].b.id),             // output wire [5 : 0] AXI_09_BID
                .AXI_09_BRESP(axiHbmFb[9].b.resp),         // output wire [1 : 0] AXI_09_BRESP
                .AXI_09_BVALID(axiHbmFb[9].bvalid),        // output wire AXI_09_BVALID
                .AXI_10_ARREADY(axiHbmFb[10].arready),      // output wire AXI_10_ARREADY
                .AXI_10_AWREADY(axiHbmFb[10].awready),      // output wire AXI_10_AWREADY
                .AXI_10_RDATA_PARITY(),                     // output wire [31 : 0] AXI_10_RDATA_PARITY
                .AXI_10_RDATA(axiHbmFb[10].r.data),         // output wire [255 : 0] AXI_10_RDATA
                .AXI_10_RID(axiHbmFb[10].r.id),             // output wire [5 : 0] AXI_10_RID
                .AXI_10_RLAST(axiHbmFb[10].r.last),         // output wire AXI_10_RLAST
                .AXI_10_RRESP(axiHbmFb[10].r.resp),         // output wire [1 : 0] AXI_10_RRESP
                .AXI_10_RVALID(axiHbmFb[10].rvalid),        // output wire AXI_10_RVALID
                .AXI_10_WREADY(axiHbmFb[10].wready),        // output wire AXI_10_WREADY
                .AXI_10_BID(axiHbmFb[10].b.id),             // output wire [5 : 0] AXI_10_BID
                .AXI_10_BRESP(axiHbmFb[10].b.resp),         // output wire [1 : 0] AXI_10_BRESP
                .AXI_10_BVALID(axiHbmFb[10].bvalid),        // output wire AXI_10_BVALID
                .AXI_11_ARREADY(axiHbmFb[11].arready),      // output wire AXI_11_ARREADY
                .AXI_11_AWREADY(axiHbmFb[11].awready),      // output wire AXI_11_AWREADY
                .AXI_11_RDATA_PARITY(),                     // output wire [31 : 0] AXI_11_RDATA_PARITY
                .AXI_11_RDATA(axiHbmFb[11].r.data),         // output wire [255 : 0] AXI_11_RDATA
                .AXI_11_RID(axiHbmFb[11].r.id),             // output wire [5 : 0] AXI_11_RID
                .AXI_11_RLAST(axiHbmFb[11].r.last),         // output wire AXI_11_RLAST
                .AXI_11_RRESP(axiHbmFb[11].r.resp),         // output wire [1 : 0] AXI_11_RRESP
                .AXI_11_RVALID(axiHbmFb[11].rvalid),        // output wire AXI_11_RVALID
                .AXI_11_WREADY(axiHbmFb[11].wready),        // output wire AXI_11_WREADY
                .AXI_11_BID(axiHbmFb[11].b.id),             // output wire [5 : 0] AXI_11_BID
                .AXI_11_BRESP(axiHbmFb[11].b.resp),         // output wire [1 : 0] AXI_11_BRESP
                .AXI_11_BVALID(axiHbmFb[11].bvalid),        // output wire AXI_11_BVALID
                .AXI_12_ARREADY(axiHbmFb[12].arready),      // output wire AXI_12_ARREADY
                .AXI_12_AWREADY(axiHbmFb[12].awready),      // output wire AXI_12_AWREADY
                .AXI_12_RDATA_PARITY(),                     // output wire [31 : 0] AXI_12_RDATA_PARITY
                .AXI_12_RDATA(axiHbmFb[12].r.data),         // output wire [255 : 0] AXI_12_RDATA
                .AXI_12_RID(axiHbmFb[12].r.id),             // output wire [5 : 0] AXI_12_RID
                .AXI_12_RLAST(axiHbmFb[12].r.last),         // output wire AXI_12_RLAST
                .AXI_12_RRESP(axiHbmFb[12].r.resp),         // output wire [1 : 0] AXI_12_RRESP
                .AXI_12_RVALID(axiHbmFb[12].rvalid),        // output wire AXI_12_RVALID
                .AXI_12_WREADY(axiHbmFb[12].wready),        // output wire AXI_12_WREADY
                .AXI_12_BID(axiHbmFb[12].b.id),             // output wire [5 : 0] AXI_12_BID
                .AXI_12_BRESP(axiHbmFb[12].b.resp),         // output wire [1 : 0] AXI_12_BRESP
                .AXI_12_BVALID(axiHbmFb[12].bvalid),        // output wire AXI_12_BVALID
                .AXI_13_ARREADY(axiHbmFb[13].arready),      // output wire AXI_13_ARREADY
                .AXI_13_AWREADY(axiHbmFb[13].awready),      // output wire AXI_13_AWREADY
                .AXI_13_RDATA_PARITY(),                     // output wire [31 : 0] AXI_13_RDATA_PARITY
                .AXI_13_RDATA(axiHbmFb[13].r.data),         // output wire [255 : 0] AXI_13_RDATA
                .AXI_13_RID(axiHbmFb[13].r.id),             // output wire [5 : 0] AXI_13_RID
                .AXI_13_RLAST(axiHbmFb[13].r.last),         // output wire AXI_13_RLAST
                .AXI_13_RRESP(axiHbmFb[13].r.resp),         // output wire [1 : 0] AXI_13_RRESP
                .AXI_13_RVALID(axiHbmFb[13].rvalid),        // output wire AXI_13_RVALID
                .AXI_13_WREADY(axiHbmFb[13].wready),        // output wire AXI_13_WREADY
                .AXI_13_BID(axiHbmFb[13].b.id),             // output wire [5 : 0] AXI_13_BID
                .AXI_13_BRESP(axiHbmFb[13].b.resp),         // output wire [1 : 0] AXI_13_BRESP
                .AXI_13_BVALID(axiHbmFb[13].bvalid),        // output wire AXI_13_BVALID
                .AXI_14_ARREADY(axiHbmFb[14].arready),      // output wire AXI_14_ARREADY
                .AXI_14_AWREADY(axiHbmFb[14].awready),      // output wire AXI_14_AWREADY
                .AXI_14_RDATA_PARITY(),                     // output wire [31 : 0] AXI_14_RDATA_PARITY
                .AXI_14_RDATA(axiHbmFb[14].r.data),         // output wire [255 : 0] AXI_14_RDATA
                .AXI_14_RID(axiHbmFb[14].r.id),             // output wire [5 : 0] AXI_14_RID
                .AXI_14_RLAST(axiHbmFb[14].r.last),         // output wire AXI_14_RLAST
                .AXI_14_RRESP(axiHbmFb[14].r.resp),         // output wire [1 : 0] AXI_14_RRESP
                .AXI_14_RVALID(axiHbmFb[14].rvalid),        // output wire AXI_14_RVALID
                .AXI_14_WREADY(axiHbmFb[14].wready),        // output wire AXI_14_WREADY
                .AXI_14_BID(axiHbmFb[14].b.id),             // output wire [5 : 0] AXI_14_BID
                .AXI_14_BRESP(axiHbmFb[14].b.resp),         // output wire [1 : 0] AXI_14_BRESP
                .AXI_14_BVALID(axiHbmFb[14].bvalid),        // output wire AXI_14_BVALID
                .AXI_15_ARREADY(axiHbmFb[15].arready),      // output wire AXI_15_ARREADY
                .AXI_15_AWREADY(axiHbmFb[15].awready),      // output wire AXI_15_AWREADY
                .AXI_15_RDATA_PARITY(),                     // output wire [31 : 0] AXI_15_RDATA_PARITY
                .AXI_15_RDATA(axiHbmFb[15].r.data),         // output wire [255 : 0] AXI_15_RDATA
                .AXI_15_RID(axiHbmFb[15].r.id),             // output wire [5 : 0] AXI_15_RID
                .AXI_15_RLAST(axiHbmFb[15].r.last),         // output wire AXI_15_RLAST
                .AXI_15_RRESP(axiHbmFb[15].r.resp),         // output wire [1 : 0] AXI_15_RRESP
                .AXI_15_RVALID(axiHbmFb[15].rvalid),        // output wire AXI_15_RVALID
                .AXI_15_WREADY(axiHbmFb[15].wready),        // output wire AXI_15_WREADY
                .AXI_15_BID(axiHbmFb[15].b.id),             // output wire [5 : 0] AXI_15_BID
                .AXI_15_BRESP(axiHbmFb[15].b.resp),         // output wire [1 : 0] AXI_15_BRESP
                .AXI_15_BVALID(axiHbmFb[15].bvalid),        // output wire AXI_15_BVALID
                .AXI_16_ARREADY(axiHbmFb[16].arready),      // output wire AXI_16_ARREADY
                .AXI_16_AWREADY(axiHbmFb[16].awready),      // output wire AXI_16_AWREADY
                .AXI_16_RDATA_PARITY(),                     // output wire [31 : 0] AXI_16_RDATA_PARITY
                .AXI_16_RDATA(axiHbmFb[16].r.data),         // output wire [255 : 0] AXI_16_RDATA
                .AXI_16_RID(axiHbmFb[16].r.id),             // output wire [5 : 0] AXI_16_RID
                .AXI_16_RLAST(axiHbmFb[16].r.last),         // output wire AXI_16_RLAST
                .AXI_16_RRESP(axiHbmFb[16].r.resp),         // output wire [1 : 0] AXI_16_RRESP
                .AXI_16_RVALID(axiHbmFb[16].rvalid),        // output wire AXI_16_RVALID
                .AXI_16_WREADY(axiHbmFb[16].wready),        // output wire AXI_16_WREADY
                .AXI_16_BID(axiHbmFb[16].b.id),             // output wire [5 : 0] AXI_16_BID
                .AXI_16_BRESP(axiHbmFb[16].b.resp),         // output wire [1 : 0] AXI_16_BRESP
                .AXI_16_BVALID(axiHbmFb[16].bvalid),        // output wire AXI_16_BVALID
                .AXI_17_ARREADY(axiHbmFb[17].arready),      // output wire AXI_17_ARREADY
                .AXI_17_AWREADY(axiHbmFb[17].awready),      // output wire AXI_17_AWREADY
                .AXI_17_RDATA_PARITY(),                     // output wire [31 : 0] AXI_17_RDATA_PARITY
                .AXI_17_RDATA(axiHbmFb[17].r.data),         // output wire [255 : 0] AXI_17_RDATA
                .AXI_17_RID(axiHbmFb[17].r.id),             // output wire [5 : 0] AXI_17_RID
                .AXI_17_RLAST(axiHbmFb[17].r.last),         // output wire AXI_17_RLAST
                .AXI_17_RRESP(axiHbmFb[17].r.resp),         // output wire [1 : 0] AXI_17_RRESP
                .AXI_17_RVALID(axiHbmFb[17].rvalid),        // output wire AXI_17_RVALID
                .AXI_17_WREADY(axiHbmFb[17].wready),        // output wire AXI_17_WREADY
                .AXI_17_BID(axiHbmFb[17].b.id),             // output wire [5 : 0] AXI_17_BID
                .AXI_17_BRESP(axiHbmFb[17].b.resp),         // output wire [1 : 0] AXI_17_BRESP
                .AXI_17_BVALID(axiHbmFb[17].bvalid),        // output wire AXI_17_BVALID
                .AXI_18_ARREADY(axiHbmFb[18].arready),      // output wire AXI_18_ARREADY
                .AXI_18_AWREADY(axiHbmFb[18].awready),      // output wire AXI_18_AWREADY
                .AXI_18_RDATA_PARITY(),                     // output wire [31 : 0] AXI_18_RDATA_PARITY
                .AXI_18_RDATA(axiHbmFb[18].r.data),         // output wire [255 : 0] AXI_18_RDATA
                .AXI_18_RID(axiHbmFb[18].r.id),             // output wire [5 : 0] AXI_18_RID
                .AXI_18_RLAST(axiHbmFb[18].r.last),         // output wire AXI_18_RLAST
                .AXI_18_RRESP(axiHbmFb[18].r.resp),         // output wire [1 : 0] AXI_18_RRESP
                .AXI_18_RVALID(axiHbmFb[18].rvalid),        // output wire AXI_18_RVALID
                .AXI_18_WREADY(axiHbmFb[18].wready),        // output wire AXI_18_WREADY
                .AXI_18_BID(axiHbmFb[18].b.id),             // output wire [5 : 0] AXI_18_BID
                .AXI_18_BRESP(axiHbmFb[18].b.resp),         // output wire [1 : 0] AXI_18_BRESP
                .AXI_18_BVALID(axiHbmFb[18].bvalid),        // output wire AXI_18_BVALID
                .AXI_19_ARREADY(axiHbmFb[19].arready),      // output wire AXI_19_ARREADY
                .AXI_19_AWREADY(axiHbmFb[19].awready),      // output wire AXI_19_AWREADY
                .AXI_19_RDATA_PARITY(),                     // output wire [31 : 0] AXI_19_RDATA_PARITY
                .AXI_19_RDATA(axiHbmFb[19].r.data),         // output wire [255 : 0] AXI_19_RDATA
                .AXI_19_RID(axiHbmFb[19].r.id),             // output wire [5 : 0] AXI_19_RID
                .AXI_19_RLAST(axiHbmFb[19].r.last),         // output wire AXI_19_RLAST
                .AXI_19_RRESP(axiHbmFb[19].r.resp),         // output wire [1 : 0] AXI_19_RRESP
                .AXI_19_RVALID(axiHbmFb[19].rvalid),        // output wire AXI_19_RVALID
                .AXI_19_WREADY(axiHbmFb[19].wready),        // output wire AXI_19_WREADY
                .AXI_19_BID(axiHbmFb[19].b.id),             // output wire [5 : 0] AXI_19_BID
                .AXI_19_BRESP(axiHbmFb[19].b.resp),         // output wire [1 : 0] AXI_19_BRESP
                .AXI_19_BVALID(axiHbmFb[19].bvalid),        // output wire AXI_19_BVALID
                .AXI_20_ARREADY(axiHbmFb[20].arready),      // output wire AXI_20_ARREADY
                .AXI_20_AWREADY(axiHbmFb[20].awready),      // output wire AXI_20_AWREADY
                .AXI_20_RDATA_PARITY(),                     // output wire [31 : 0] AXI_20_RDATA_PARITY
                .AXI_20_RDATA(axiHbmFb[20].r.data),         // output wire [255 : 0] AXI_20_RDATA
                .AXI_20_RID(axiHbmFb[20].r.id),             // output wire [5 : 0] AXI_20_RID
                .AXI_20_RLAST(axiHbmFb[20].r.last),         // output wire AXI_20_RLAST
                .AXI_20_RRESP(axiHbmFb[20].r.resp),         // output wire [1 : 0] AXI_20_RRESP
                .AXI_20_RVALID(axiHbmFb[20].rvalid),        // output wire AXI_20_RVALID
                .AXI_20_WREADY(axiHbmFb[20].wready),        // output wire AXI_20_WREADY
                .AXI_20_BID(axiHbmFb[20].b.id),             // output wire [5 : 0] AXI_20_BID
                .AXI_20_BRESP(axiHbmFb[20].b.resp),         // output wire [1 : 0] AXI_20_BRESP
                .AXI_20_BVALID(axiHbmFb[20].bvalid),        // output wire AXI_20_BVALID
                .AXI_21_ARREADY(axiHbmFb[21].arready),      // output wire AXI_21_ARREADY
                .AXI_21_AWREADY(axiHbmFb[21].awready),      // output wire AXI_21_AWREADY
                .AXI_21_RDATA_PARITY(),                     // output wire [31 : 0] AXI_21_RDATA_PARITY
                .AXI_21_RDATA(axiHbmFb[21].r.data),         // output wire [255 : 0] AXI_21_RDATA
                .AXI_21_RID(axiHbmFb[21].r.id),             // output wire [5 : 0] AXI_21_RID
                .AXI_21_RLAST(axiHbmFb[21].r.last),         // output wire AXI_21_RLAST
                .AXI_21_RRESP(axiHbmFb[21].r.resp),         // output wire [1 : 0] AXI_21_RRESP
                .AXI_21_RVALID(axiHbmFb[21].rvalid),        // output wire AXI_21_RVALID
                .AXI_21_WREADY(axiHbmFb[21].wready),        // output wire AXI_21_WREADY
                .AXI_21_BID(axiHbmFb[21].b.id),             // output wire [5 : 0] AXI_21_BID
                .AXI_21_BRESP(axiHbmFb[21].b.resp),         // output wire [1 : 0] AXI_21_BRESP
                .AXI_21_BVALID(axiHbmFb[21].bvalid),        // output wire AXI_21_BVALID
                .AXI_22_ARREADY(axiHbmFb[22].arready),      // output wire AXI_22_ARREADY
                .AXI_22_AWREADY(axiHbmFb[22].awready),      // output wire AXI_22_AWREADY
                .AXI_22_RDATA_PARITY(),                     // output wire [31 : 0] AXI_22_RDATA_PARITY
                .AXI_22_RDATA(axiHbmFb[22].r.data),         // output wire [255 : 0] AXI_22_RDATA
                .AXI_22_RID(axiHbmFb[22].r.id),             // output wire [5 : 0] AXI_22_RID
                .AXI_22_RLAST(axiHbmFb[22].r.last),         // output wire AXI_22_RLAST
                .AXI_22_RRESP(axiHbmFb[22].r.resp),         // output wire [1 : 0] AXI_22_RRESP
                .AXI_22_RVALID(axiHbmFb[22].rvalid),        // output wire AXI_22_RVALID
                .AXI_22_WREADY(axiHbmFb[22].wready),        // output wire AXI_22_WREADY
                .AXI_22_BID(axiHbmFb[22].b.id),             // output wire [5 : 0] AXI_22_BID
                .AXI_22_BRESP(axiHbmFb[22].b.resp),         // output wire [1 : 0] AXI_22_BRESP
                .AXI_22_BVALID(axiHbmFb[22].bvalid),        // output wire AXI_22_BVALID
                .AXI_23_ARREADY(axiHbmFb[23].arready),      // output wire AXI_23_ARREADY
                .AXI_23_AWREADY(axiHbmFb[23].awready),      // output wire AXI_23_AWREADY
                .AXI_23_RDATA_PARITY(),                     // output wire [31 : 0] AXI_23_RDATA_PARITY
                .AXI_23_RDATA(axiHbmFb[23].r.data),         // output wire [255 : 0] AXI_23_RDATA
                .AXI_23_RID(axiHbmFb[23].r.id),             // output wire [5 : 0] AXI_23_RID
                .AXI_23_RLAST(axiHbmFb[23].r.last),         // output wire AXI_23_RLAST
                .AXI_23_RRESP(axiHbmFb[23].r.resp),         // output wire [1 : 0] AXI_23_RRESP
                .AXI_23_RVALID(axiHbmFb[23].rvalid),        // output wire AXI_23_RVALID
                .AXI_23_WREADY(axiHbmFb[23].wready),        // output wire AXI_23_WREADY
                .AXI_23_BID(axiHbmFb[23].b.id),             // output wire [5 : 0] AXI_23_BID
                .AXI_23_BRESP(axiHbmFb[23].b.resp),         // output wire [1 : 0] AXI_23_BRESP
                .AXI_23_BVALID(axiHbmFb[23].bvalid),        // output wire AXI_23_BVALID
                .AXI_24_ARREADY(axiHbmFb[24].arready),      // output wire AXI_24_ARREADY
                .AXI_24_AWREADY(axiHbmFb[24].awready),      // output wire AXI_24_AWREADY
                .AXI_24_RDATA_PARITY(),                     // output wire [31 : 0] AXI_24_RDATA_PARITY
                .AXI_24_RDATA(axiHbmFb[24].r.data),         // output wire [255 : 0] AXI_24_RDATA
                .AXI_24_RID(axiHbmFb[24].r.id),             // output wire [5 : 0] AXI_24_RID
                .AXI_24_RLAST(axiHbmFb[24].r.last),         // output wire AXI_24_RLAST
                .AXI_24_RRESP(axiHbmFb[24].r.resp),         // output wire [1 : 0] AXI_24_RRESP
                .AXI_24_RVALID(axiHbmFb[24].rvalid),        // output wire AXI_24_RVALID
                .AXI_24_WREADY(axiHbmFb[24].wready),        // output wire AXI_24_WREADY
                .AXI_24_BID(axiHbmFb[24].b.id),             // output wire [5 : 0] AXI_24_BID
                .AXI_24_BRESP(axiHbmFb[24].b.resp),         // output wire [1 : 0] AXI_24_BRESP
                .AXI_24_BVALID(axiHbmFb[24].bvalid),        // output wire AXI_24_BVALID
                .AXI_25_ARREADY(axiHbmFb[25].arready),      // output wire AXI_25_ARREADY
                .AXI_25_AWREADY(axiHbmFb[25].awready),      // output wire AXI_25_AWREADY
                .AXI_25_RDATA_PARITY(),                     // output wire [31 : 0] AXI_25_RDATA_PARITY
                .AXI_25_RDATA(axiHbmFb[25].r.data),         // output wire [255 : 0] AXI_25_RDATA
                .AXI_25_RID(axiHbmFb[25].r.id),             // output wire [5 : 0] AXI_25_RID
                .AXI_25_RLAST(axiHbmFb[25].r.last),         // output wire AXI_25_RLAST
                .AXI_25_RRESP(axiHbmFb[25].r.resp),         // output wire [1 : 0] AXI_25_RRESP
                .AXI_25_RVALID(axiHbmFb[25].rvalid),        // output wire AXI_25_RVALID
                .AXI_25_WREADY(axiHbmFb[25].wready),        // output wire AXI_25_WREADY
                .AXI_25_BID(axiHbmFb[25].b.id),             // output wire [5 : 0] AXI_25_BID
                .AXI_25_BRESP(axiHbmFb[25].b.resp),         // output wire [1 : 0] AXI_25_BRESP
                .AXI_25_BVALID(axiHbmFb[25].bvalid),        // output wire AXI_25_BVALID
                .AXI_26_ARREADY(axiHbmFb[26].arready),      // output wire AXI_26_ARREADY
                .AXI_26_AWREADY(axiHbmFb[26].awready),      // output wire AXI_26_AWREADY
                .AXI_26_RDATA_PARITY(),                     // output wire [31 : 0] AXI_26_RDATA_PARITY
                .AXI_26_RDATA(axiHbmFb[26].r.data),         // output wire [255 : 0] AXI_26_RDATA
                .AXI_26_RID(axiHbmFb[26].r.id),             // output wire [5 : 0] AXI_26_RID
                .AXI_26_RLAST(axiHbmFb[26].r.last),         // output wire AXI_26_RLAST
                .AXI_26_RRESP(axiHbmFb[26].r.resp),         // output wire [1 : 0] AXI_26_RRESP
                .AXI_26_RVALID(axiHbmFb[26].rvalid),        // output wire AXI_26_RVALID
                .AXI_26_WREADY(axiHbmFb[26].wready),        // output wire AXI_26_WREADY
                .AXI_26_BID(axiHbmFb[26].b.id),             // output wire [5 : 0] AXI_26_BID
                .AXI_26_BRESP(axiHbmFb[26].b.resp),         // output wire [1 : 0] AXI_26_BRESP
                .AXI_26_BVALID(axiHbmFb[26].bvalid),        // output wire AXI_26_BVALID
                .AXI_27_ARREADY(axiHbmFb[27].arready),      // output wire AXI_27_ARREADY
                .AXI_27_AWREADY(axiHbmFb[27].awready),      // output wire AXI_27_AWREADY
                .AXI_27_RDATA_PARITY(),                     // output wire [31 : 0] AXI_27_RDATA_PARITY
                .AXI_27_RDATA(axiHbmFb[27].r.data),         // output wire [255 : 0] AXI_27_RDATA
                .AXI_27_RID(axiHbmFb[27].r.id),             // output wire [5 : 0] AXI_27_RID
                .AXI_27_RLAST(axiHbmFb[27].r.last),         // output wire AXI_27_RLAST
                .AXI_27_RRESP(axiHbmFb[27].r.resp),         // output wire [1 : 0] AXI_27_RRESP
                .AXI_27_RVALID(axiHbmFb[27].rvalid),        // output wire AXI_27_RVALID
                .AXI_27_WREADY(axiHbmFb[27].wready),        // output wire AXI_27_WREADY
                .AXI_27_BID(axiHbmFb[27].b.id),             // output wire [5 : 0] AXI_27_BID
                .AXI_27_BRESP(axiHbmFb[27].b.resp),         // output wire [1 : 0] AXI_27_BRESP
                .AXI_27_BVALID(axiHbmFb[27].bvalid),        // output wire AXI_27_BVALID
                .AXI_28_ARREADY(axiHbmFb[28].arready),      // output wire AXI_28_ARREADY
                .AXI_28_AWREADY(axiHbmFb[28].awready),      // output wire AXI_28_AWREADY
                .AXI_28_RDATA_PARITY(),                     // output wire [31 : 0] AXI_28_RDATA_PARITY
                .AXI_28_RDATA(axiHbmFb[28].r.data),         // output wire [255 : 0] AXI_28_RDATA
                .AXI_28_RID(axiHbmFb[28].r.id),             // output wire [5 : 0] AXI_28_RID
                .AXI_28_RLAST(axiHbmFb[28].r.last),         // output wire AXI_28_RLAST
                .AXI_28_RRESP(axiHbmFb[28].r.resp),         // output wire [1 : 0] AXI_28_RRESP
                .AXI_28_RVALID(axiHbmFb[28].rvalid),        // output wire AXI_28_RVALID
                .AXI_28_WREADY(axiHbmFb[28].wready),        // output wire AXI_28_WREADY
                .AXI_28_BID(axiHbmFb[28].b.id),             // output wire [5 : 0] AXI_28_BID
                .AXI_28_BRESP(axiHbmFb[28].b.resp),         // output wire [1 : 0] AXI_28_BRESP
                .AXI_28_BVALID(axiHbmFb[28].bvalid),        // output wire AXI_28_BVALID
                .AXI_29_ARREADY(axiHbmFb[29].arready),      // output wire AXI_29_ARREADY
                .AXI_29_AWREADY(axiHbmFb[29].awready),      // output wire AXI_29_AWREADY
                .AXI_29_RDATA_PARITY(),                     // output wire [31 : 0] AXI_29_RDATA_PARITY
                .AXI_29_RDATA(axiHbmFb[29].r.data),         // output wire [255 : 0] AXI_29_RDATA
                .AXI_29_RID(axiHbmFb[29].r.id),             // output wire [5 : 0] AXI_29_RID
                .AXI_29_RLAST(axiHbmFb[29].r.last),         // output wire AXI_29_RLAST
                .AXI_29_RRESP(axiHbmFb[29].r.resp),         // output wire [1 : 0] AXI_29_RRESP
                .AXI_29_RVALID(axiHbmFb[29].rvalid),        // output wire AXI_29_RVALID
                .AXI_29_WREADY(axiHbmFb[29].wready),        // output wire AXI_29_WREADY
                .AXI_29_BID(axiHbmFb[29].b.id),             // output wire [5 : 0] AXI_29_BID
                .AXI_29_BRESP(axiHbmFb[29].b.resp),         // output wire [1 : 0] AXI_29_BRESP
                .AXI_29_BVALID(axiHbmFb[29].bvalid),        // output wire AXI_29_BVALID
                .AXI_30_ARREADY(axiHbmFb[30].arready),      // output wire AXI_30_ARREADY
                .AXI_30_AWREADY(axiHbmFb[30].awready),      // output wire AXI_30_AWREADY
                .AXI_30_RDATA_PARITY(),                     // output wire [31 : 0] AXI_30_RDATA_PARITY
                .AXI_30_RDATA(axiHbmFb[30].r.data),         // output wire [255 : 0] AXI_30_RDATA
                .AXI_30_RID(axiHbmFb[30].r.id),             // output wire [5 : 0] AXI_30_RID
                .AXI_30_RLAST(axiHbmFb[30].r.last),         // output wire AXI_30_RLAST
                .AXI_30_RRESP(axiHbmFb[30].r.resp),         // output wire [1 : 0] AXI_30_RRESP
                .AXI_30_RVALID(axiHbmFb[30].rvalid),        // output wire AXI_30_RVALID
                .AXI_30_WREADY(axiHbmFb[30].wready),        // output wire AXI_30_WREADY
                .AXI_30_BID(axiHbmFb[30].b.id),             // output wire [5 : 0] AXI_30_BID
                .AXI_30_BRESP(axiHbmFb[30].b.resp),         // output wire [1 : 0] AXI_30_BRESP
                .AXI_30_BVALID(axiHbmFb[30].bvalid),        // output wire AXI_30_BVALID
                .AXI_31_ARREADY(axiHbmFb[31].arready),      // output wire AXI_31_ARREADY
                .AXI_31_AWREADY(axiHbmFb[31].awready),      // output wire AXI_31_AWREADY
                .AXI_31_RDATA_PARITY(),                     // output wire [31 : 0] AXI_31_RDATA_PARITY
                .AXI_31_RDATA(axiHbmFb[31].r.data),         // output wire [255 : 0] AXI_31_RDATA
                .AXI_31_RID(axiHbmFb[31].r.id),             // output wire [5 : 0] AXI_31_RID
                .AXI_31_RLAST(axiHbmFb[31].r.last),         // output wire AXI_31_RLAST
                .AXI_31_RRESP(axiHbmFb[31].r.resp),         // output wire [1 : 0] AXI_31_RRESP
                .AXI_31_RVALID(axiHbmFb[31].rvalid),        // output wire AXI_31_RVALID
                .AXI_31_WREADY(axiHbmFb[31].wready),        // output wire AXI_31_WREADY
                .AXI_31_BID(axiHbmFb[31].b.id),             // output wire [5 : 0] AXI_31_BID
                .AXI_31_BRESP(axiHbmFb[31].b.resp),         // output wire [1 : 0] AXI_31_BRESP
                .AXI_31_BVALID(axiHbmFb[31].bvalid),        // output wire AXI_31_BVALID
                .APB_0_PRDATA(apbFb[0].rdata),                // output wire [31 : 0] APB_0_PRDATA
                .APB_0_PREADY(apbFb[0].ready),                // output wire APB_0_PREADY
                .APB_0_PSLVERR(apbFb[0].error),               // output wire APB_0_PSLVERR
                .APB_1_PRDATA(apbFb[1].rdata),                // output wire [31 : 0] APB_1_PRDATA
                .APB_1_PREADY(apbFb[1].ready),                // output wire APB_1_PREADY
                .APB_1_PSLVERR(apbFb[1].error),               // output wire APB_1_PSLVERR
                .apb_complete_0(stackApbReady[0]),          // output wire apb_complete_0
                .apb_complete_1(stackApbReady[1]),          // output wire apb_complete_1
                .DRAM_0_STAT_CATTRIP(stackThermalError[0]), // output wire DRAM_0_STAT_CATTRIP
                .DRAM_0_STAT_TEMP(stackTemperature[0]),     // output wire [6 : 0] DRAM_0_STAT_TEMP
                .DRAM_1_STAT_CATTRIP(stackThermalError[1]), // output wire DRAM_1_STAT_CATTRIP
                .DRAM_1_STAT_TEMP(stackTemperature[1])      // output wire [6 : 0] DRAM_1_STAT_TEMP
                );

  `else // !`ifndef SIMULATION
  // we are in simulation
  initial begin
    for (int stack=0; stack<Stacks; stack++) apbFb[stack] <= '0;
    stackApbReady = '{ Stacks { 1'b0 }};
    stackTemperature = '{ Stacks { 25 }};
    stackThermalError = '{ Stacks { 1'b0 }};
    #1us;
    stackApbReady = '{ Stacks { 1'b1 }};
    stackTemperature = '{ Stacks { 30 }};
    stackThermalError = '{ Stacks { 1'b0 }};
    while (1) begin
      #1us;
      for (int stack=0; stack<Stacks; stack++) begin
        if (({$random}%100) > 50) begin
          // goes up until it hits thermal error at 90, then goes down until it's 40
          stackTemperature[stack] = (stackThermalError[stack] ? (stackTemperature[stack] - 1) :
                                     (stackTemperature[stack] + 1));
          stackThermalError[stack] = (stackThermalError[stack] ? (stackTemperature[stack] > 40) :
                                      (stackTemperature[stack] > 90));
        end
      end
    end
  end
  always @(posedge clock) begin
    for (int stack=0; stack<Stacks; stack++) begin
      apbFb[stack] <= '0;
      apbFb[stack].ready <= apb[stack].enable;
    end
  end

  // instantiate a model of a memory with NxAXI ports
  sim_axi_memory #(.AxiPorts(HbmAxiPorts),
                   .SeparateMemorySpaces(`OC_DEF_1_ELSE_0(TARGET_HBM_SEPARATE_MEMORY_SPACES)))
  uSIM_MEMORY (.clockAxi(axiHbmClock), .resetAxi(~axiHbmResetN), .axi(axiHbm), .axiFb(axiHbmFb));

  `endif

  assign csrStatus[CsrAddressControl][14:8] = stackTemperature[0];
  assign csrStatus[CsrAddressControl][22:16] = stackTemperature[1];
  assign csrStatus[CsrAddressControl][24] = stackApbReady[0];
  assign csrStatus[CsrAddressControl][25] = stackApbReady[1];
  assign csrStatus[CsrAddressControl][30] = stackThermalError[0];
  assign csrStatus[CsrAddressControl][31] = stackThermalError[1];
  assign thermalError = (|stackThermalError);

`else // !`ifdef OC_LIBRARY_ULTRASCALE_PLUS
  // we don't have a supported architecture selected
  `ifdef SIMULATION
  // this is a bit different from above, because we are connecting directly to the user axi ports,
  // instead of the number that are in the HBM model
  sim_axi_memory #(.AxiPorts(AxiPorts),
                   .SeparateMemorySpaces(`OC_DEF_1_ELSE_0(TARGET_HBM_SEPARATE_MEMORY_SPACES)))
  uSIM_MEMORY (.clockAxi({AxiPorts{clockAxi}}), .resetAxi({AxiPorts{reset}}), .axi(axi), .axiFb(axiFb));
  assign thermalError = 1'b0;
  assign abbcOut = '0;
  initial begin
    $display("%t %m: OC_HBM: INFO: No supported architecture selected, using behavioral model", $realtime);
  end
  `else
  // and we aren't in simulation
  `OC_STATIC_ERROR("OC_HBM included, but no supported architecture has been selected");
  `endif
`endif

`ifdef SIM_ANNOUNCE
  initial begin
    `OC_ANNOUNCE_MODULE(oc_hbm);
    `OC_ANNOUNCE_PARAM_INTEGER(RefClockHz);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_hbm


/*

 OK, so to avoid going crazy when editing the above, here's the trick.  Paste the following template into
 a bash shell:

perl - <<'EOF'

 for (my $i=0; $i<32; $i++) {
    my $di = sprintf("%02d", $i);
    print <<SUBEOF;
                .AXI_${di}_ACLK(axiHbmClock[$i]),              // input wire AXI_${di}_ACLK
                .AXI_${di}_ARESET_N(axiHbmResetN[$i]),         // input wire AXI_${di}_ARESET_N
                .AXI_${di}_ARADDR(axiHbm[$i].ar.addr),         // input wire [32 : 0] AXI_${di}_ARADDR
                .AXI_${di}_ARBURST(axiHbm[$i].ar.burst),       // input wire [1 : 0] AXI_${di}_ARBURST
                .AXI_${di}_ARID(axiHbm[$i].ar.id),             // input wire [5 : 0] AXI_${di}_ARID
                .AXI_${di}_ARLEN(axiHbm[$i].ar.len),           // input wire [3 : 0] AXI_${di}_ARLEN
                .AXI_${di}_ARSIZE(axiHbm[$i].ar.size),         // input wire [2 : 0] AXI_${di}_ARSIZE
                .AXI_${di}_ARVALID(axiHbm[$i].arvalid),        // input wire AXI_${di}_ARVALID
                .AXI_${di}_AWADDR(axiHbm[$i].aw.addr),         // input wire [32 : 0] AXI_${di}_AWADDR
                .AXI_${di}_AWBURST(axiHbm[$i].aw.burst),       // input wire [1 : 0] AXI_${di}_AWBURST
                .AXI_${di}_AWID(axiHbm[$i].aw.id),             // input wire [5 : 0] AXI_${di}_AWID
                .AXI_${di}_AWLEN(axiHbm[$i].aw.len),           // input wire [3 : 0] AXI_${di}_AWLEN
                .AXI_${di}_AWSIZE(axiHbm[$i].aw.size),         // input wire [2 : 0] AXI_${di}_AWSIZE
                .AXI_${di}_AWVALID(axiHbm[$i].awvalid),        // input wire AXI_${di}_AWVALID
                .AXI_${di}_RREADY(axiHbm[$i].rready),          // input wire AXI_${di}_RREADY
                .AXI_${di}_BREADY(axiHbm[$i].bready),          // input wire AXI_${di}_BREADY
                .AXI_${di}_WDATA(axiHbm[$i].w.data),           // input wire [255 : 0] AXI_${di}_WDATA
                .AXI_${di}_WLAST(axiHbm[$i].w.last),           // input wire AXI_${di}_WLAST
                .AXI_${di}_WSTRB(axiHbm[$i].w.strb),           // input wire [31 : 0] AXI_${di}_WSTRB
                .AXI_${di}_WDATA_PARITY(32'd0),                // input wire [31 : 0] AXI_${di}_WDATA_PARITY
                .AXI_${di}_WVALID(axiHbm[$i].wvalid),          // input wire AXI_${di}_WVALID
SUBEOF
}

 for (my $i=0; $i<32; $i++) {
    my $di = sprintf("%02d", $i);
    print <<SUBEOF;
                .AXI_${di}_ARREADY(axiHbmFb[$i].arready),      // output wire AXI_${di}_ARREADY
                .AXI_${di}_AWREADY(axiHbmFb[$i].awready),      // output wire AXI_${di}_AWREADY
                .AXI_${di}_RDATA_PARITY(),                     // output wire [31 : 0] AXI_${di}_RDATA_PARITY
                .AXI_${di}_RDATA(axiHbmFb[$i].r.data),         // output wire [255 : 0] AXI_${di}_RDATA
                .AXI_${di}_RID(axiHbmFb[$i].r.id),             // output wire [5 : 0] AXI_${di}_RID
                .AXI_${di}_RLAST(axiHbmFb[$i].r.last),         // output wire AXI_${di}_RLAST
                .AXI_${di}_RRESP(axiHbmFb[$i].r.resp),         // output wire [1 : 0] AXI_${di}_RRESP
                .AXI_${di}_RVALID(axiHbmFb[$i].rvalid),        // output wire AXI_${di}_RVALID
                .AXI_${di}_WREADY(axiHbmFb[$i].wready),        // output wire AXI_${di}_WREADY
                .AXI_${di}_BID(axiHbmFb[$i].b.id),             // output wire [5 : 0] AXI_${di}_BID
                .AXI_${di}_BRESP(axiHbmFb[$i].b.resp),         // output wire [1 : 0] AXI_${di}_BRESP
                .AXI_${di}_BVALID(axiHbmFb[$i].bvalid),        // output wire AXI_${di}_BVALID
SUBEOF
}

EOF


*/
