
// SPDX-License-Identifier: MPL-2.0

module oc_pll #(
                  // Use these parameters for more vendor-independent, automatic clocking
                parameter integer RefClockHz = 100000000,
                parameter real    RefClockMHz = (RefClockHz/1000000.0),
                parameter integer Clocks = 1,
                parameter real    Out0MHz = 225.0, // try to hit this frequency exactly, everything else gets as close as possible
                parameter real    Out1MHz = 450.0,
                parameter real    Out2MHz = 250.0,
                parameter real    Out3MHz = 200.0,
                parameter real    Out4MHz = 100.0,
                parameter real    Out5MHz = 50.0,
                parameter real    Out6MHz = 50.0,
                parameter bit     AbbcEnable = 0,
                parameter integer AbbcNumber = 0,
                parameter bit     MeasureEnable = 0,
                parameter bit     ResetSync = 0,
                parameter bit     ThrottleMap = 1,
                parameter bit     ThrottleThermal = 1,
                  // If you like to drive manual, you can override the dividers here
                parameter real    MinVcoMHz = 1200.0,
                parameter real    MaxVcoMHz = 1600.0,
                parameter real    TargetVcoMHz = oclib_pkg::ComputeVcoFrequency(.vcoMin(MinVcoMHz),.vcoMax(MaxVcoMHz),
                                                                                .refFreq(RefClockMHz), .targetFreq(Out0MHz),
                                                                                .refMultMin(0.125), .refMultMax(100.000),
                                                                                .refMultStep(0.125),
                                                                                .outDivMin(0.125), .outDivMax(100.000),
                                                                                .outDivStep(0.125)),
                parameter integer DivD = 'd1, // fixed for now, don't need precision of both D and M
                parameter integer DivMx8 = $floor((8.0*TargetVcoMHz) / (real'(DivD)*RefClockMHz)), // 81
                parameter real    DivM = (real'(DivMx8) / 8.0), // 10.125
                parameter real    VcoMHz = (RefClockMHz*DivM / real'(DivD)), // 1582MHz
                parameter integer Div0x8 = $ceil((8.0*VcoMHz) / Out0MHz), // 51
                parameter real    Div0 = (real'(Div0x8) / 8.0), // 6.75
                // for clocks that don't exist, we provide safe default dividers since incoming param is probably 0
                parameter integer Div1 = (Clocks>1) ? $ceil(VcoMHz / Out1MHz) : 32, // 4
                parameter integer Div2 = (Clocks>2) ? $ceil(VcoMHz / Out2MHz) : 32, // 7
                parameter integer Div3 = (Clocks>3) ? $ceil(VcoMHz / Out3MHz) : 32, // 8
                parameter integer Div4 = (Clocks>4) ? $ceil(VcoMHz / Out4MHz) : 32, // 16
                parameter integer Div5 = (Clocks>5) ? $ceil(VcoMHz / Out5MHz) : 32, // 32
                parameter integer Div6 = (Clocks>6) ? $ceil(VcoMHz / Out6MHz) : 32  // 32
                )
  (
   input                     clock = 1'b0, // used if AbbcEnable, as the clock for support logic
   input                     reset = 1'b0, // optional, the MMCM doesn't need it, just AbbcEnable
   input                     thermalWarning = 1'b0,
   input                     thermalError = 1'b0,
   input                     clockRef,
   output logic [Clocks-1:0] clockOut,
   output logic [Clocks-1:0] resetOut,
   input                     oclib_pkg::abbc_s abbcIn = '0,
   output                    oclib_pkg::abbc_s abbcOut
   );

  localparam real RefPeriodNS  = (1000.0/RefClockMHz);

`ifdef OC_LIBRARY_ULTRASCALE_PLUS

  // See Xilinx UG572

  //                           v----------------------------------\
  // refClk --> [ /D ] --> [COMPARE] --> [ VCO ] -+-> [   /M  ] --/
  //                                              |-> [ /DIV0 ] --> out0Clock
  //                                              |-> [ /DIV1 ] --> out1Clock ..

  localparam real ActualOut0MHz = ((RefClockMHz * DivM) / (real'(DivD) * Div0));
  localparam real ActualOut1MHz = ((RefClockMHz * DivM) / (real'(DivD) * real'(Div1)));
  localparam real ActualOut2MHz = ((RefClockMHz * DivM) / (real'(DivD) * real'(Div2)));
  localparam real ActualOut3MHz = ((RefClockMHz * DivM) / (real'(DivD) * real'(Div3)));
  localparam real ActualOut4MHz = ((RefClockMHz * DivM) / (real'(DivD) * real'(Div4)));
  localparam real ActualOut5MHz = ((RefClockMHz * DivM) / (real'(DivD) * real'(Div5)));

  logic          fbClock;
  logic          clockOutPll [6:0]; // this is fixed based on the ports on the physical PLL
  logic          locked;

  oclib_pkg::drp_s    drp;
  oclib_pkg::drp_fb_s drpFb;

  localparam integer NumCsr = (Clocks+1+1); // 1 id, 1 main, 1 per clock
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdPll,
                                    7'd0, ThrottleThermal, ThrottleMap, MeasureEnable, 6'(Clocks)};
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  if (AbbcEnable) begin : abbc

    logic                     resetQ;
    oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

    localparam AddressSpaces = 2;
    logic [AddressSpaces-1:0] csrSelect;
    oclib_pkg::csr_s                        csr;
    oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

    oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
    uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

    // Implement address space 0
    localparam [31:0] MeasureMask = (MeasureEnable ? 32'hffff0000 : 32'h00000000);

    oclib_csr_array #(.NumCsr(NumCsr),
                      .CsrRwBits   ({ {Clocks{32'h0000ff03}}, 32'h40000031, 32'h00000000 }),
                      .CsrRoBits   ({ {Clocks{ MeasureMask}}, 32'h03330000, 32'h00000000 }),
                      .CsrFixedBits({ {Clocks{32'h00000000}}, 32'h00000000, 32'hffffffff }),
                      .CsrInitBits ({ {Clocks{32'h00000000}}, 32'h00000000, CsrId        }))
    uCSR (clock, resetQ, csrSelect[0], csr, csrFb[0], csrConfig, csrStatus);

    // Implement address space 1
    oclib_csr_to_drp #(.DataW(16), .AddressW(7))
    uCSR_TO_DRP (clock, resetQ, csrSelect[1], csr, csrFb[1], drp, drpFb);

  end else begin
    assign drp.enable = 1'b0;
    assign drp.address = '0;
    assign drp.write = 1'b0;
    assign drp.wdata = '0;
    assign csrConfig = '0;
  end

  MMCME4_ADV #(
               .CLKOUT0_DIVIDE_F(Div0),
               .CLKOUT1_DIVIDE(Div1),
               .CLKOUT2_DIVIDE(Div2),
               .CLKOUT3_DIVIDE(Div3),
               .CLKOUT4_DIVIDE(Div4),
               .CLKOUT5_DIVIDE(Div5),
               .CLKOUT6_DIVIDE(Div6),
               .CLKFBOUT_MULT_F(DivM),
               .DIVCLK_DIVIDE(DivD),
               .CLKIN1_PERIOD(RefPeriodNS),
               .CLKIN2_PERIOD(RefPeriodNS)
               )
  uPLL (
        .CLKIN1(clockRef),
        .CLKIN2(1'b0),
        .CLKFBIN(fbClock),
        .DCLK(clock),
        .PSCLK(),
        .RST(reset || csrConfig[1][0]),
        .CLKINSEL(1'b1),
        .DWE(drp.write),
        .DEN(drp.enable),
        .DADDR(drp.address[6:0]),
        .DI(drp.wdata),
        .PSINCDEC(),
        .PSEN(),
        .CDDCREQ(csrConfig[1][4]),
        .CLKOUT0(clockOutPll[0]),
        .CLKOUT1(clockOutPll[1]),
        .CLKOUT2(clockOutPll[2]),
        .CLKOUT3(clockOutPll[3]),
        .CLKOUT4(clockOutPll[4]),
        .CLKOUT5(clockOutPll[5]),
        .CLKOUT6(clockOutPll[6]),
        .CLKOUT0B(),
        .CLKOUT1B(),
        .CLKOUT2B(),
        .CLKOUT3B(),
        .CLKFBOUT(fbClock),
        .CLKFBOUTB(),
        .LOCKED(locked),
        .DO(drpFb.rdata),
        .DRDY(drpFb.ready),
        .PSDONE(),
        .CLKINSTOPPED(csrStatus[1][24]),
        .CLKFBSTOPPED(csrStatus[1][25]),
        .CDDCDONE(csrStatus[1][17]),
        .PWRDWN(csrConfig[1][5])
        );

  assign csrStatus[1][16] = locked;
  assign csrStatus[1][20] = thermalWarning;
  assign csrStatus[1][21] = thermalError;

  for (genvar c=0; c<Clocks; c++) begin : clock_iter

    oclib_clock_control #(.ThrottleMapW(8),
                          .ThrottleMap(ThrottleMap),
                          .ThrottleThermal(ThrottleThermal))
    uCLOCK_CONTROL (.clockIn(clockOutPll[c]),
                    .reset(reset),
                    .clockOut(clockOut[c]),
                    .throttleMap(csrConfig[c+2][15:8]),
                    .thermalWarning((thermalWarning && csrConfig[c+2][1]) || csrConfig[c+2][0]));

    oclib_reset #(.StartPipeCycles(3), .ResetCycles(128))
    uRESET (.clock(clockOut[c]), .in(reset || !locked), .out(resetOut[c]));

    if (MeasureEnable) begin : meas
      // this block is coded to close timing at very high speed, so we don't do any more than 16-bit
      logic [9:0] div1, div2;
      logic       div1TC, div2TC;
      logic [15:0] count;
      logic        resetLocal;

      oclib_reset #(.ResetCycles(4))
      uRESET (.clock(clockOut[c]), .in(!locked), .out(resetLocal));

      always_ff @(posedge clockOut[c]) begin
        if (resetLocal) begin
          div1TC <= 1'b0;
          div2TC <= 1'b0;
          div1 <= '0;
          div2 <= '0;
          count <= '0;
        end
        else begin
          div1TC <= (div1 == 'h3e6); // will assert while div1 == 3e7 (999)
          div2TC <= (div2 == 'h3e7) && (div1 == 'h3e6); // will assert while div1&2 == 3e7
          div1 <= (div1TC ? '0 : (div1 + 'd1));
          div2 <= (div2TC ? '0 : div1TC ? (div2 + 'd1) : div2);
          count <= (div2TC ? (count + 'd1) : count);
        end
      end
      assign csrStatus[c+2][31:16] = count;

    end // if (MeasureEnable)
  end // for (genvar c=0; c<Clocks; c++) ...

`else // !`ifdef OC_LIBRARY_ULTRASCALE_PLUS

  // BEHAVIORAL IMPLEMENTATION

  // Note this implementation provides only the most basic functionality (i.e. legal behavior at outputs). It doesn't
  // implement any CSRs.  The indended use for this mode is bringing up a design in a vendor-neutral fashion, without
  // requiring any vendor libraries (i.e. just generic SystemVerilog simulation).

  localparam real Out0PeriodNS = (1000.0/Out0MHz);
  localparam real Out1PeriodNS = (1000.0/Out1MHz);
  localparam real Out2PeriodNS = (1000.0/Out2MHz);
  localparam real Out3PeriodNS = (1000.0/Out3MHz);
  localparam real Out4PeriodNS = (1000.0/Out4MHz);
  localparam real Out5PeriodNS = (1000.0/Out5MHz);
  localparam real Out6PeriodNS = (1000.0/Out6MHz);

  initial begin
    for (int c=0; c<Clocks; c++) clockOut[c] = 1'b1;
  end

  for (genvar c=0; c<Clocks; c++) begin
    always_ff @(posedge clockOut[c]) resetOut[c] <= reset;
  end

  always #((Out0PeriodNS/2) * 1ns) clockOut[0] = ~clockOut[0];
  if (Clocks>1) begin
    always #((Out1PeriodNS/2) * 1ns) clockOut[1] = ~clockOut[1];
  end
  if (Clocks>2) begin
    always #((Out2PeriodNS/2) * 1ns) clockOut[2] = ~clockOut[2];
  end
  if (Clocks>3) begin
    always #((Out3PeriodNS/2) * 1ns) clockOut[3] = ~clockOut[3];
  end
  if (Clocks>4) begin
    always #((Out4PeriodNS/2) * 1ns) clockOut[4] = ~clockOut[4];
  end
  if (Clocks>5) begin
    always #((Out5PeriodNS/2) * 1ns) clockOut[5] = ~clockOut[5];
  end
  if (Clocks>6) begin
    always #((Out6PeriodNS/2) * 1ns) clockOut[6] = ~clockOut[6];
  end

  assign abcOut = '0;

`endif // !`ifdef OC_LIBRARY_ULTRASCALE_PLUS

`ifdef SIM_ANNOUNCE
  `include "../lib/oclib_defines.vh"
  initial begin
    `OC_ANNOUNCE_MODULE(oc_pll);
    `OC_ANNOUNCE_PARAM_INTEGER(RefClockHz);
    `OC_ANNOUNCE_PARAM_REAL(RefClockMHz);
    `OC_ANNOUNCE_PARAM_INTEGER(Clocks);
    `OC_ANNOUNCE_PARAM_REAL(Out0MHz);
    `OC_ANNOUNCE_PARAM_REAL(Out1MHz);
    `OC_ANNOUNCE_PARAM_REAL(Out2MHz);
    `OC_ANNOUNCE_PARAM_REAL(Out3MHz);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(MeasureEnable);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_pll
