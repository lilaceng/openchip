
// SPDX-License-Identifier: MPL-2.0

// chip_test.sv -- this is the testbench for the default OpenChip userspace app (which implements self-test functions)

`define ERROR $finish // comment out to ignore errors

module chip_test;

  chip_harness uHARNESS ();

  // TESTBENCH MAIN

  logic [31:0] readData;
  logic [63:0] readData64;
  logic [7:0]  status;
  logic [3:0]  temp4;
  logic [31:0] temp32;
  integer      abbcChannels;
  integer      uartChannels;
  integer      firstUserChannel;
  integer      channel;
  integer      foundPlls;
  integer      foundChipmons;
  integer      foundProtects;
  integer      foundIics;
  integer      foundLeds;
  integer      foundGpios;
  integer      foundHbms;
  integer      foundCmacs;

  initial begin
    //uHARNESS.uUART.debug = 10;
    $display("%t %m: ****************************", $realtime);
    $display("%t %m: START", $realtime);
    $display("%t %m: ****************************", $realtime);
    #200ns;
    uHARNESS.uUART.RxPrompt();
    channel = 0; // we iterate through all channels, testing each a little

    uHARNESS.uUART.accelerate = 1;


    $display("%t %m: ****************************", $realtime);
    $display("%t %m: Dumping Chip Info", $realtime);
    $display("%t %m: ****************************", $realtime);
    uHARNESS.uUART.TxByte("I"); // info dump
    uHARNESS.uUART.ShiftInit();
    uHARNESS.uUART.ShiftReadData(4);
    $display("%t %m: BitstreamID      : %08x", $realtime, uHARNESS.uUART.shiftReadData);
    uHARNESS.uUART.ShiftReadData(4);
    $display("%t %m: Build Date       : %04x/%02x/%02x", $realtime, uHARNESS.uUART.shiftReadData[31:16],
                                                                    uHARNESS.uUART.shiftReadData[15:8],
                                                                    uHARNESS.uUART.shiftReadData[7:0]);
    uHARNESS.uUART.ShiftReadData(2);
    $display("%t %m: Build Time       : %02x:%02x", $realtime, uHARNESS.uUART.shiftReadData[15:8], uHARNESS.uUART.shiftReadData[7:0]);
    uHARNESS.uUART.ShiftReadData(2);
    $display("%t %m: Target Vendor    : %04x (%s)", $realtime, uHARNESS.uUART.shiftReadData[15:0],
             oclib_pkg::TargetVendorString(uHARNESS.uUART.shiftReadData[15:0]));
    uHARNESS.uUART.ShiftReadData(2);
    $display("%t %m: Target Board     : %04x (%s)", $realtime, uHARNESS.uUART.shiftReadData[15:0],
             oclib_pkg::TargetBoardString(uHARNESS.uUART.shiftReadData[15:0]));
    uHARNESS.uUART.ShiftReadData(2);
    $display("%t %m: Target Library   : %04x (%s)", $realtime, uHARNESS.uUART.shiftReadData[15:0],
             oclib_pkg::TargetLibraryString(uHARNESS.uUART.shiftReadData[15:0]));
    uHARNESS.uUART.ShiftReadData(2);
    abbcChannels = uHARNESS.uUART.shiftReadData[15:8];
    firstUserChannel = uHARNESS.uUART.shiftReadData[7:0];
    $display("%t %m: Number of Abbcs  : %0d (tty: top@%0d-%0d; userspace@%0d-%0d)", $realtime, abbcChannels,
             0, firstUserChannel-1, firstUserChannel, abbcChannels-1);
    uHARNESS.uUART.ShiftReadData(1);
    uartChannels = uHARNESS.uUART.shiftReadData[7:0];
    $display("%t %m: Number of Uarts  : %0d (tty: uarts@%0d-%0d)", $realtime, uartChannels,
             abbcChannels, abbcChannels + uartChannels - 1);
    uHARNESS.uUART.ShiftUntil(32);
    uHARNESS.uUART.ReadString(16);
    $display("%t %m: Userspace Loaded : %s", $realtime, uHARNESS.uUART.shiftReadString);
    uHARNESS.uUART.ReadString(16);
    $display("%t %m: UUID             : %08x%08x%08x%08x", $realtime,
             uHARNESS.uUART.shiftReadString[0:3], uHARNESS.uUART.shiftReadString[4:7],
             uHARNESS.uUART.shiftReadString[8:11], uHARNESS.uUART.shiftReadString[12:15]);
    uHARNESS.uUART.ShiftUntil(64);
    uHARNESS.uUART.RxPrompt();

    foundPlls = 0;
    foundChipmons = 0;
    foundProtects = 0;
    foundIics = 0;
    foundLeds = 0;
    foundGpios = 0;
    foundHbms = 0;
    foundCmacs = 0;

    $display("%t %m: Starting scan of top level channels", $realtime);
    for (channel=0; channel<firstUserChannel; channel++) begin
      // determine the type
      uHARNESS.uMASTER.CsrRead(channel,     0, 'h0000, status, readData);
      if (readData[31:16] == oclib_pkg::AbbcIdPll) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: PLL %0d on channel %0d", $realtime, foundPlls, channel);
        $display("%t %m: ****************************", $realtime);
        uHARNESS.uMASTER.CsrWrite    (channel,     0, 'h0001,        32'h00000001); // assert reset to PLL
        uHARNESS.uMASTER.CsrReadCheck(channel,     1, 'h0008, 8'h00, 32'h00001000, .mask(32'h00001000));
        uHARNESS.uMASTER.CsrWrite    (channel,     0, 'h0001,        32'h00000000); // deassert reset to PLL
        uHARNESS.uMASTER.CsrRead     (channel,     0, 'h0001, status, readData); // read csr 0
        uHARNESS.uMASTER.CsrWrite    (channel,     0, 'h0002,        32'h00000001); // trigger thermal warning
        uHARNESS.uMASTER.CsrWrite    (channel,     0, 'h0002,        32'h00001101); // throttle to 25%
        uHARNESS.uMASTER.CsrWrite    (channel,     0, 'h0002,        32'h00000000); // throttling disabled
        foundPlls++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdChipmon) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: CHIPMON %0d on channel %0d", $realtime, foundChipmons, channel);
        $display("%t %m: ****************************", $realtime);
        uHARNESS.uMASTER.CsrRead     (channel, 1, 'h0000, status, readData); // read temp, hard to predict
        if (uHARNESS.uCHIP.uTOP.RefClockHz[uHARNESS.uCHIP.uTOP.RefClockTop] == 100000000) begin
          uHARNESS.uMASTER.CsrReadCheck(channel, 1, 'h0042, 8'h00, 32'h00001600);
        end
        if (uHARNESS.uCHIP.uTOP.RefClockHz[uHARNESS.uCHIP.uTOP.RefClockTop] == 156250000) begin
          uHARNESS.uMASTER.CsrReadCheck(channel, 1, 'h0042, 8'h00, 32'h00002300);
        end
        uHARNESS.uMASTER.CsrReadCheck(channel, 1, 'h0050, 8'h00, 32'h0000bab9, .ascii(1));
        uHARNESS.uMASTER.CsrReadCheck(channel, 1, 'h0053, 8'h00, 32'h0000cb00);
        foundChipmons++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdProtect) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: PROTECT %0d on channel %0d", $realtime, foundProtects, channel);
        $display("%t %m: ****************************", $realtime);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0001, 8'h00, 32'h00000000);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0002, 8'h00, 32'h00000000);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0003, 8'h00, 32'h00000000);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0004, 8'h00, 32'h01234567);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0005, 8'h00, 32'h89abcdef);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0006, 8'h00, 32'h00000000);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0007, 8'h00, 32'h00000000);
        // write unlock key
        uHARNESS.uMASTER.CsrWrite    (channel, 0, 'h0002,        32'hadb37072);
        uHARNESS.uMASTER.CsrWrite    (channel, 0, 'h0003,        32'h28e05070);
        uHARNESS.uMASTER.CsrWrite    (channel, 0, 'h0001,        32'h00000001);
        // check unlocked
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0001, 8'h00, 32'hc0000001);
        uHARNESS.uMASTER.CsrWrite    (channel, 0, 'h0001,        32'h00000000);
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0001, 8'h00, 32'h80000000);
        foundProtects++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdHbm) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: HBM %0d on channel %0d", $realtime, foundHbms, channel);
        $display("%t %m: ****************************", $realtime);
        uHARNESS.uMASTER.CsrRead(channel, 0, 'h0001, status, readData);
        uHARNESS.uMASTER.CsrRead(channel, 1, 'h5550, status, readData);
        uHARNESS.uMASTER.CsrRead(channel, 2, 'haaa0, status, readData);
        foundHbms++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdIic) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: IIC %0d on channel %0d", $realtime, foundIics, channel);
        $display("%t %m: ****************************", $realtime);
        if (uHARNESS.uCHIP.uTOP.IicOffloadEnable) begin
          uHARNESS.uMASTER.CsrReadCheck(channel, 1, 'h0104, 8'h00, 32'h000000C0); // should see both FIFOs empty
          uHARNESS.uMASTER.CsrRead(channel, 1, 'h0128, status, readData); // TSUSTA
          uHARNESS.uMASTER.CsrRead(channel, 1, 'h013c, status, readData); // THIGH
        end
        foundIics++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdLed) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: LED %0d on channel %0d", $realtime, foundLeds, channel);
        $display("%t %m: ****************************", $realtime);
        uHARNESS.uMASTER.CsrRead(channel, 0, 'h0001, status, readData); // Prescale
        uHARNESS.uMASTER.CsrWrite(channel, 0, 'h0002, 32'h00003f01); // turn LED 0 on, full brightness
        uHARNESS.uMASTER.CsrWrite(channel, 0, 'h0003, 32'h00001f01); // turn LED 1 on, half brightness
        uHARNESS.uMASTER.CsrWrite(channel, 0, 'h0004, 32'h00001f03); // turn LED 2 to heartbeat, half brightness
        #50us;
        foundLeds++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdGpio) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: GPIO %0d on channel %0d", $realtime, foundGpios, channel);
        $display("%t %m: ****************************", $realtime);
        uHARNESS.uMASTER.CsrRead(channel, 0, 'h0001, status, readData); // GPIO 0 state
        foundGpios++;
      end
      else if (readData[31:16] == oclib_pkg::AbbcIdCmac) begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: CMAC %0d on channel %0d", $realtime, foundCmacs, channel);
        $display("%t %m: ****************************", $realtime);
        while ($realtime < 110us) #1us; // make sure it's been long enough for MAC to come up
        uHARNESS.uMASTER.CsrReadCheck(channel, 0, 'h0003, 8'h00, 32'h00030000); // check RX is aligned is up
        uHARNESS.uMASTER.CsrWrite    (channel, 0, 'h0002,        32'h00000001); // enable TX
        uHARNESS.uMASTER.CsrWrite    (channel, 0, 'h0003,        32'h00000001); // enable RX
        foundCmacs++;
      end
      else begin
        $display("%t %m: ****************************", $realtime);
        $display("%t %m: ERROR: unknown ring IP on channel %0d", $realtime, channel);
        $display("%t %m: ****************************", $realtime);
      end
    end

    $display("%t %m: *******************************************************************", $realtime);
    $display("%t %m: TEST USERSPACE FUNCTION 0 on channel %0d - work on wide words", $realtime, channel);
    $display("%t %m: *******************************************************************", $realtime);
    uHARNESS.uUART.WordWrite   (channel, 256'h1f1e1d1c1b1a191817161514131211100f0e0d0c0b0a09080706050403020100, 32);
    uHARNESS.uUART.WordRead    (channel, readData64, 9);
    channel ++;

    if (uHARNESS.uCHIP.uTOP.UserAxiMemoryPorts) begin
      $display("%t %m: *******************************************************************", $realtime);
      $display("%t %m: TEST USERSPACE FUNCTION 1 on channel %0d - membist", $realtime, channel);
      $display("%t %m: *******************************************************************", $realtime);
      uHARNESS.uMASTER.accelerate = 1;
      // write a burst of 256 words, on all ports
      for (temp4=1; temp4<=8; temp4++) begin
        uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0100 + ((temp4-1)*4),  {8{temp4}}); // write data pattern
      end
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0004,        32'h000000ff); // op_count 256
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0008,        32'hffffffff); // all engines
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0018,        32'h00000020); // address increment
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h004c,        32'h00ff0010); // port shift (all bits, << 16)
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h00000101); // go, write
      uHARNESS.uMASTER.CsrReadCheck(channel,  0, 'h0000, 8'h00, 32'h80000101); // confirm done
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h00000000); // clear go

      // read a burst of 256 words, on all ports, checking signatures from first 4 ports when done
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h00010001); // go, read
      uHARNESS.uMASTER.CsrReadCheck(channel,  0, 'h0000, 8'h00, 32'h80010001);
      for (temp32=0; temp32<4; temp32++) begin
        uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0008,        ('d1 << temp32)); // cycle through engines
        uHARNESS.uMASTER.CsrReadCheck(channel,  0, 'h0028, 8'h00, 32'ha22221c8);
      end
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h00000000); // clear go
      channel ++;
    end

    for (temp4=0; temp4<uHARNESS.uCHIP.uTOP.CmacCount; temp4++) begin
      $display("%t %m: *******************************************************************", $realtime);
      $display("%t %m: TEST USERSPACE FUNCTION %0d on channel %0d - cmac", $realtime, temp4, channel);
      $display("%t %m: *******************************************************************", $realtime);
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0001,        32'h0000007f); // 128 cycles between packets
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h0040000a); // enable generator and checker, 64B packets
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h00000008); // disable generator
      uHARNESS.uMASTER.CsrWrite    (channel,  0, 'h0000,        32'h00000000); // disable checker
      uHARNESS.uMASTER.CsrRead     (channel,  0, 'h0012, status, readData); // read packet count
      uHARNESS.uMASTER.CsrRead     (channel,  0, 'h0013, status, readData); // read packet count
      uHARNESS.uMASTER.CsrRead     (channel,  0, 'h0014, status, readData); // read byte count
      channel ++;
    end

    #1us;
    $display("%t %m: ****************************", $realtime);
    $display("%t %m: GENERATE A SYNTAX ERROR", $realtime);
    $display("%t %m: ****************************", $realtime);
    uHARNESS.uUART.TxByte("a"); // cause syntax error
    uHARNESS.uUART.RxByteCheck('h0d); // now expect the error message
    uHARNESS.uUART.RxByteCheck("\n");
    uHARNESS.uUART.RxByteCheck("S");
    uHARNESS.uUART.RxByteCheck("Y");
    uHARNESS.uUART.RxByteCheck("N");
    uHARNESS.uUART.RxByteCheck("T");
    uHARNESS.uUART.RxByteCheck("A");
    uHARNESS.uUART.RxByteCheck("X");
    uHARNESS.uUART.RxByteCheck(" ");
    uHARNESS.uUART.RxByteCheck("E");
    uHARNESS.uUART.RxByteCheck("R");
    uHARNESS.uUART.RxByteCheck("R");
    uHARNESS.uUART.RxByteCheck("O");
    uHARNESS.uUART.RxByteCheck("R");
    uHARNESS.uUART.RxByteCheck('h0d);
    uHARNESS.uUART.RxByteCheck("\n");
    uHARNESS.uUART.RxPrompt(); // and another prompt
    uHARNESS.uUART.TxByte("\n"); // hit return
    uHARNESS.uUART.RxPrompt(); // and another prompt
    #1us;

    if (uHARNESS.uCHIP.uTOP.uMANAGER.ManagerReset) begin
      $display("%t %m: ****************************", $realtime);
      $display("%t %m: MANAGER RESET", $realtime);
      $display("%t %m: ****************************", $realtime);
      // first do N-1 repetitions, this will check we get echo for all of them
      for (int i=0; i<uHARNESS.uCHIP.uTOP.uMANAGER.ManagerResetLength-1; i++) begin
        uHARNESS.uUART.TxByte(uHARNESS.uCHIP.uTOP.uMANAGER.ManagerResetByte);
      end
      uHARNESS.uUART.accelerate = 0;
      // now this one we shouldn't get a response because it will reset chip on arrival
      uHARNESS.uUART.TxByteRaw(uHARNESS.uCHIP.uTOP.uMANAGER.ManagerResetByte);
      // we should see a prompt after it resets
      uHARNESS.uUART.RxPrompt(); // and another prompt
      #1us;
    end

    $display("%t %m: ****************************", $realtime);
    $display("%t %m: DONE (PASSED)", $realtime);
    $display("%t %m: ****************************", $realtime);
    $finish;
  end

endmodule // chip_test
