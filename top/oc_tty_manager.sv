
// SPDX-License-Identifier: MPL-2.0

module oc_tty_manager #(
                        parameter integer            ClockHz = 100000000,
                        parameter integer            UartCount = 1, // must be >1, since one is used as the master
                        parameter integer            UartManagerChannel = 0, // currently must be fixed to 0
                        parameter integer            UartBaud [UartCount-1:0] = { 115200 },
                        parameter integer            ManagerReset = `OC_FROM_DEFINE_ELSE(TARGET_MANAGER_RESET, 1),
                        parameter integer            ManagerResetByte = `OC_FROM_DEFINE_ELSE(TARGET_MANAGER_RESET_BYTE, "!"),
                        parameter integer            ManagerResetLength = `OC_FROM_DEFINE_ELSE(TARGET_MANAGER_RESET_LENGTH, 64),
                        parameter integer            UptimeCounters = `OC_FROM_DEFINE_ELSE(TARGET_UPTIME_COUNTERS, 1),
                        parameter logic [0:3] [7:0]  BitstreamID = `OC_FROM_DEFINE_ELSE(TARGET_BITSTREAM_ID, 32'h89abcdef),
                        parameter logic [0:3] [7:0]  BuildDate = `OC_FROM_DEFINE_ELSE(OC_BUILD_DATE, 32'h20210925),
                        parameter logic [0:1] [7:0]  BuildTime = `OC_FROM_DEFINE_ELSE(OC_BUILD_TIME, 16'h1200),
                        parameter logic [0:3] [7:0]  BuildUuid0 = `OC_FROM_DEFINE_ELSE(OC_BUILD_UUID0, 32'd0),
                        parameter logic [0:3] [7:0]  BuildUuid1 = `OC_FROM_DEFINE_ELSE(OC_BUILD_UUID1, 32'd0),
                        parameter logic [0:3] [7:0]  BuildUuid2 = `OC_FROM_DEFINE_ELSE(OC_BUILD_UUID2, 32'd0),
                        parameter logic [0:3] [7:0]  BuildUuid3 = `OC_FROM_DEFINE_ELSE(OC_BUILD_UUID3, 32'd0),
                        parameter logic [0:15] [7:0] BuildUuid = `OC_FROM_DEFINE_ELSE(OC_BUILD_UUID, {BuildUuid0,BuildUuid1,\
                                                     BuildUuid2,BuildUuid3}),
                        parameter logic [0:1] [7:0]  TargetVendor = `OC_FROM_DEFINE_ELSE(OC_VENDOR, 16'hffff),
                        parameter logic [0:1] [7:0]  TargetBoard = `OC_FROM_DEFINE_ELSE(OC_BOARD, 16'hffff),
                        parameter logic [0:1] [7:0]  TargetLibrary = `OC_FROM_DEFINE_ELSE(OC_LIBRARY, 16'hffff),
                        parameter logic [0:15] [7:0] UserSpace = `OC_FROM_DEFINE_ELSE(USER_APP_STRING, "default         "),
                        parameter integer            Abbcs = 0,
                                                     `OC_CREATE_SAFE_WIDTH(Abbcs),
                        parameter integer            AbbcFirstUser = 0,
                        parameter bit                ResetSync = 1
                      )
  (
   input                        clock,
   input                        reset,
   input [UartCount-1:0]        uartRx,
   output logic [UartCount-1:0] uartTx,
   output logic                 resetOut,
   output                       oclib_pkg::abbc_s abbcOut [AbbcsSafe-1:0],
   input                        oclib_pkg::abbc_s abbcIn [AbbcsSafe-1:0]
   );

  logic                         resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  // **************************
  // UARTS
  // **************************

  // N x UART to BYTE CHANNEL
  oclib_pkg::bbc_s bbcToUart [UartCount-1:0];
  oclib_pkg::bbc_s bbcFromUart [UartCount-1:0];
  logic [UartCount-1:0] [15:0] uartError;
  for (genvar i=0; i<UartCount; i++) begin : uart_ins
    oclib_uart #(.ClockHz(ClockHz), .Baud(UartBaud[i]))
    uUART (.clock(clock), .reset(reset), .error(uartError[i]),
           .rx(uartRx[i]), .tx(uartTx[i]),
           .bbcIn(bbcToUart[i]), .bbcOut(bbcFromUart[i]));
  end

  // N x ABC to BYTE CHANNEL
  oclib_pkg::bbc_s bbcToAbbc [AbbcsSafe-1:0];
  oclib_pkg::bbc_s bbcFromAbbc [AbbcsSafe-1:0];
  for (genvar i=0; i<Abbcs; i++) begin : abc_ins
    oclib_abbc_to_bbc
    uABBC_TO_BBC (.clock(clock), .reset(resetQ),
                  .abbcIn(abbcIn[i]), .abbcOut(abbcOut[i]),
                  .bbcIn(bbcToAbbc[i]), .bbcOut(bbcFromAbbc[i]));
  end

  // **************************
  // SWITCH MATRIX
  // **************************

  oclib_pkg::bbc_s masterOut, masterIn;
  logic [7:0] masterChannel;

  logic [7:0] commandRxDataSelect;
  logic       commandRxValidSelect;
  logic [7:0] commandTxData;
  logic       commandTxValid;
  logic       commandTxReadySelect;

  always_comb begin
    for (integer i=0; i<UartCount; i++) begin : uart_mux
      if (i == UartManagerChannel) begin
        commandRxDataSelect = bbcFromUart[i].data;
        commandRxValidSelect = bbcFromUart[i].valid;
        commandTxReadySelect = bbcFromUart[i].ready;
        bbcToUart[i].ready = 1'b1;
        bbcToUart[i].data = commandTxData;
        bbcToUart[i].valid = commandTxValid;
      end
      else begin
        bbcToUart[i].ready = (masterOut.ready && (masterChannel==(i+Abbcs)));
        bbcToUart[i].data = masterOut.data;
        bbcToUart[i].valid = (masterOut.valid && (masterChannel==(i+Abbcs)));
      end
    end
  end

  always_comb begin
    for (integer i=0; i<Abbcs; i++) begin : abc_mux
      bbcToAbbc[i].ready = (masterOut.ready && (masterChannel==i));
      bbcToAbbc[i].data = masterOut.data;
      bbcToAbbc[i].valid = (masterOut.valid && (masterChannel==i));
    end
    masterIn.data = bbcFromAbbc[masterChannel].data;
    masterIn.valid = bbcFromAbbc[masterChannel].valid;
    masterIn.ready = bbcFromAbbc[masterChannel].ready;
  end

  // **************************
  // CLASSIFY RX BYTE
  // **************************

  logic [7:0] commandRxData;
  logic       commandRxValid;
  logic       commandRxReturn;
  logic       commandRxEscape;
  logic       commandRxSpace;
  logic       commandRxHex;
  logic [3:0] commandRxHexValue;
  logic       commandTxReady;

  always_ff @(posedge clock) begin
    commandRxData <= commandRxDataSelect;
    commandRxValid <= commandRxValidSelect;
    commandRxReturn <= (commandRxDataSelect == "\n"); // \n=0a=<lf>
    commandRxEscape <= (commandRxDataSelect == "~");
    commandRxSpace <= ((commandRxDataSelect == " ") || (commandRxDataSelect == 'h0d)); // 0d=<cr>
    commandRxHex <= (((commandRxDataSelect >= "0") && (commandRxDataSelect <= "9")) ||
                     ((commandRxDataSelect >= "a") && (commandRxDataSelect <= "f")));
    commandRxHexValue <= (((commandRxDataSelect >= "0") && (commandRxDataSelect <= "9")) ?
                          (commandRxDataSelect - "0") : (commandRxDataSelect - "a" + 'd10));
    commandTxReady <= commandTxReadySelect;
  end

  // **************************
  // OPTIONAL SELF-RESET LOGIC
  // **************************

  if (ManagerReset) begin
    localparam ManagerResetCounterW = $clog2(ManagerResetLength+1);
    logic [ManagerResetCounterW-1:0] resetCount;
    always @(posedge clock) begin
      if (resetQ) begin
        resetCount <= '0;
        resetOut <= 1'b0;
      end
      else begin
        if (commandRxValid) begin
          resetCount <= ((commandRxData == ManagerResetByte) ? (resetCount + 'd1) : '0);
          end
        if (resetCount >= ManagerResetLength) begin
          resetOut <= 1'b1; // when the reset comes, it will clear us out of this state, all hail the reset
        end
      end
    end
  end

  // **************************
  // OPTIONAL UPTIME TIMERS
  // **************************

  // readback to main state machine
  logic readTimers; // controlled by main SM
  logic [7:0] timerByte; // read data back to the main SM
  logic [7:0] counter; // controlled by main SM, serves as read address for ROMs

  if (UptimeCounters) begin

    // first we detect the reload event, essentially generating a "power on reset"
    logic [7:0]                                       reloadStable = 8'h0;
    logic                                             reloadDetect;
    always_ff @(posedge clock) begin
      reloadStable <= { reloadStable[6:0], 1'b1 };
      reloadDetect <= !reloadStable[7];
    end

    // count time since reload
    localparam PrescaleBits = $clog2(ClockHz);
    logic                                             reloadUptimePulse;
    logic [PrescaleBits-1:0]                          reloadUptimePrescale;
    logic [31:0]                                      reloadUptimeSeconds;
  // also count cycles design has been held in reset, for debugging reset issues (is it long enough from source XXX,
  // did something stop because of a reset, etc). Needs to be cleared on reload because it has no other reset.
    logic [31:0]                                      cyclesUnderReset;
    always_ff @(posedge clock) begin
      if (reloadDetect) begin
        reloadUptimePulse <= 1'b0;
        reloadUptimePrescale <= '0;
        reloadUptimeSeconds <= '0;
        cyclesUnderReset <= '0;
      end
      else begin
        reloadUptimePulse <= (reloadUptimePrescale == (ClockHz-2));
        reloadUptimePrescale <= (reloadUptimePulse ? '0 : (reloadUptimePrescale + 'd1));
        reloadUptimeSeconds <= (reloadUptimeSeconds + {'0, reloadUptimePulse});
        cyclesUnderReset <= (cyclesUnderReset + {'0, resetQ});
      end
    end

    logic                                             resetUptimePulse;
    logic [PrescaleBits-1:0]                          resetUptimePrescale;
    logic [31:0]                                      resetUptimeSeconds;
    logic [31:0]                                      cyclesSinceReset;

    always_ff @(posedge clock) begin
      if (resetQ) begin
        resetUptimePulse <= 1'b0;
        resetUptimePrescale <= '0;
        resetUptimeSeconds <= '0;
        cyclesSinceReset <= '0;
      end
      else begin
        resetUptimePulse <= (resetUptimePrescale == (ClockHz-2));
        resetUptimePrescale <= (resetUptimePulse ? '0 : (resetUptimePrescale + 'd1));
        resetUptimeSeconds <= (resetUptimeSeconds + {'0, resetUptimePulse});
        cyclesSinceReset <= (cyclesSinceReset + 'd1);
      end
    end

    logic [15:0] [7:0] timerCache;
    always_ff @(posedge clock) begin
      if (readTimers) begin
        timerCache <= { cyclesSinceReset,
                        cyclesUnderReset,
                        resetUptimeSeconds,
                        reloadUptimeSeconds };
      end
      timerByte <= timerCache[counter[3:0]];
    end
  end // if (UptimeCounters)
  else begin
    assign timerByte = 8'hFF;
  end // else: !if(UptimeCounters)

  // **************************
  // ROMS FOR SERIAL DATA
  // **************************

  // This will get efficiently implemented as 8 LUTs, up to depth 32
  logic [7:0] syntaxROM;
  always_comb begin
    case (counter[4:0])
      0 : syntaxROM = 'h0d;
      1 : syntaxROM = "\n";
      2 : syntaxROM = "S";
      3 : syntaxROM = "Y";
      4 : syntaxROM = "N";
      5 : syntaxROM = "T";
      6 : syntaxROM = "A";
      7 : syntaxROM = "X";
      8 : syntaxROM = " ";
      9 : syntaxROM = "E";
      10 : syntaxROM = "R";
      11 : syntaxROM = "R";
      12 : syntaxROM = "O";
      13 : syntaxROM = "R";
      14 : syntaxROM = 'h0d;
      15 : syntaxROM = "\n";
      default : syntaxROM = '0;
    endcase // case (counter[4:0])
  end

  logic [7:0] infoROM;
  always_comb begin
    case (counter[5:0])
      6'h00   : infoROM = BitstreamID[0];
      6'h01   : infoROM = BitstreamID[1];
      6'h02   : infoROM = BitstreamID[2];
      6'h03   : infoROM = BitstreamID[3];
      6'h04   : infoROM = BuildDate[0];
      6'h05   : infoROM = BuildDate[1];
      6'h06   : infoROM = BuildDate[2];
      6'h07   : infoROM = BuildDate[3];
      6'h08   : infoROM = BuildTime[0];
      6'h09   : infoROM = BuildTime[1];
      6'h0a   : infoROM = TargetVendor[0];
      6'h0b   : infoROM = TargetVendor[1];
      6'h0c   : infoROM = TargetBoard[0];
      6'h0d   : infoROM = TargetBoard[1];
      6'h0e   : infoROM = TargetLibrary[0];
      6'h0f   : infoROM = TargetLibrary[1];
      6'h10   : infoROM = Abbcs;
      6'h11   : infoROM = AbbcFirstUser;
      6'h12   : infoROM = UartCount;
      6'h20   : infoROM = UserSpace[0];
      6'h21   : infoROM = UserSpace[1];
      6'h22   : infoROM = UserSpace[2];
      6'h23   : infoROM = UserSpace[3];
      6'h24   : infoROM = UserSpace[4];
      6'h25   : infoROM = UserSpace[5];
      6'h26   : infoROM = UserSpace[6];
      6'h27   : infoROM = UserSpace[7];
      6'h28   : infoROM = UserSpace[8];
      6'h29   : infoROM = UserSpace[9];
      6'h2a   : infoROM = UserSpace[10];
      6'h2b   : infoROM = UserSpace[11];
      6'h2c   : infoROM = UserSpace[12];
      6'h2d   : infoROM = UserSpace[13];
      6'h2e   : infoROM = UserSpace[14];
      6'h2f   : infoROM = UserSpace[15];
      6'h30   : infoROM = BuildUuid[0];
      6'h31   : infoROM = BuildUuid[1];
      6'h32   : infoROM = BuildUuid[2];
      6'h33   : infoROM = BuildUuid[3];
      6'h34   : infoROM = BuildUuid[4];
      6'h35   : infoROM = BuildUuid[5];
      6'h36   : infoROM = BuildUuid[6];
      6'h37   : infoROM = BuildUuid[7];
      6'h38   : infoROM = BuildUuid[8];
      6'h39   : infoROM = BuildUuid[9];
      6'h3a   : infoROM = BuildUuid[10];
      6'h3b   : infoROM = BuildUuid[11];
      6'h3c   : infoROM = BuildUuid[12];
      6'h3d   : infoROM = BuildUuid[13];
      6'h3e   : infoROM = BuildUuid[14];
      6'h3f   : infoROM = BuildUuid[15];
      default : infoROM = '0;
    endcase // case (counter[5:0])
  end // always_comb

  // **************************
  // MAIN STATE MACHINE
  // **************************

  function logic [7:0] hex2ascii (input [3:0] hex);
    if (hex > 'd9) return "a" + (hex - 'd10);
    else return "0" + hex;
  endfunction

  enum logic [5:0] { StPrompt, StPrompt2, StPrompt3, StTxWait, StIdle, StSyntax,
                     StAsciiConnect, StAsciiConnected, StAsciiResponse,
                     StBinaryConnect, StBinaryTransmit, StBinaryReceive,
                     StAsciiInfo, StBinaryInfo, StAsciiTimer, StBinaryTimer } state, nextState;

  logic [7:0] tempRegister;
  logic       tempRegisterFull;


  always_ff @(posedge clock) begin
    if (resetQ) begin
      state <= StPrompt;
      nextState <= StPrompt;
      commandTxData <= '0;
      commandTxValid <= 1'b0;
      counter <= '0;
      readTimers <= 1'b0;
      tempRegister <= '0;
      tempRegisterFull <= '0;
      masterChannel <= '0;
      masterOut.data <= '0;
      masterOut.valid <= 1'b0;
      masterOut.ready <= 1'b0;
    end
    else begin
      masterOut.ready <= 1'b0;
      masterOut.valid <= (masterOut.valid && ~masterIn.ready);
      readTimers <= 1'b0;
      case (state)
        StPrompt : begin
          commandTxData <= 'h0d;
          commandTxValid <= 1'b1;
          state <= StTxWait;
          nextState <= StPrompt2;
        end
        StPrompt2 : begin
          commandTxData <= 'h0a;
          commandTxValid <= 1'b1;
          state <= StTxWait;
          nextState <= StPrompt3;
        end
        StPrompt3 : begin
          commandTxData <= ">";
          commandTxValid <= 1'b1;
          state <= StTxWait;
          nextState <= StIdle;
        end
        StTxWait : begin
          if (commandTxReady) begin
            commandTxValid <= 1'b0;
            state <= nextState;
          end
        end
        StIdle : begin
          counter <= '0;
          tempRegister <= '0;
          if (commandRxValid) begin
            commandTxData <= commandRxData;
            commandTxValid <= 1'b1;
            state <= StTxWait;
            if (commandRxReturn) nextState <= StPrompt;
            else if (commandRxData == "c") nextState <= StAsciiConnect;
            else if (commandRxData == "C") nextState <= StBinaryConnect;
            else if (commandRxData == "i") nextState <= StAsciiInfo;
            else if (commandRxData == "I") nextState <= StBinaryInfo;
            else if (commandRxData == "!") nextState <= StIdle; // ignore incoming reset chars, don't syntax error
            else if (commandRxData == "t") begin
              nextState <= StAsciiTimer;
              readTimers <= 1'b1;
            end
            else if (commandRxData == "T") begin
              nextState <= StBinaryTimer;
              readTimers <= 1'b1;
            end
            else if (commandRxSpace) nextState <= StIdle;
           else nextState <= StSyntax;
          end
        end
        StAsciiConnect : begin
          tempRegisterFull <= 1'b0;
          if (commandRxValid) begin
            commandTxData <= commandRxData;
            commandTxValid <= 1'b1;
            state <= StTxWait;
            if (commandRxSpace) nextState <= StAsciiConnect;
            else if (commandRxReturn) begin
              counter <= '0;
              if (counter == 'd2) begin
                nextState <= StAsciiConnected;
                masterChannel <= masterOut.data;
              end else begin
                nextState <= StSyntax;
              end
            end
            else if (commandRxHex) begin
              if (counter > 'd1) begin
                nextState <= StSyntax;
                counter <= '0;
              end
              else begin
                nextState <= StAsciiConnect;
                counter <= (counter + 'd1);
                masterOut.data <= {masterOut.data[3:0], commandRxHexValue};
              end
            end
          end
        end
        StAsciiConnected : begin
          // we are connected to the interface indicated by "masterChannel"
          if (commandRxValid) begin
            if (commandRxEscape) state <= StPrompt;
            else if (commandRxSpace || commandRxReturn) begin
              commandTxData <= commandRxData;
              commandTxValid <= 1'b1;
              state <= StTxWait;
              nextState <= StAsciiConnected;
              if (commandRxReturn) begin
                masterOut.data <= tempRegister;
                masterOut.valid <= tempRegisterFull;
                tempRegisterFull <= 1'b0;
              end
            end
            else if (!commandRxHex) state <= StSyntax;
            else begin
              commandTxData <= commandRxData;
              commandTxValid <= 1'b1;
              state <= StTxWait;
              nextState <= StAsciiConnected;
              counter <= (counter + 'd1);
              tempRegister <= {tempRegister[3:0], commandRxHexValue};
              if (counter == 'd0) begin
                masterOut.data <= tempRegister;
                masterOut.valid <= tempRegisterFull;
                tempRegisterFull <= 1'b0;
              end
              if (counter == 'd1) begin
                counter <= '0;
                tempRegisterFull <= 1'b1;
              end
            end
          end
          else if (masterIn.valid && !masterOut.ready) begin
            // we've received a byte
            commandTxData <= hex2ascii(masterIn.data[7:4]);
            commandTxValid <= 1'b1;
            state <= StTxWait;
            nextState <= StAsciiResponse;
            tempRegister <= {4'd0,masterIn.data[3:0]};
            masterOut.ready <= 1'b1;
          end
        end
        StAsciiResponse : begin
          commandTxData <= hex2ascii(tempRegister[3:0]);
          commandTxValid <= 1'b1;
          state <= StTxWait;
          nextState <= StAsciiConnected;
        end
        StBinaryConnect : begin
          // waiting for the masterChannel and tx length and rx length
          if (commandRxValid) begin
            counter <= (counter + 'd1);
            if (counter == 'd0) masterChannel <= commandRxData; // which channel
            else if (counter == 'd1) tempRegister <= commandRxData; // number of tx bytes to send
            else if (counter == 'd2) begin // number of rx bytes to receive
              // tempRegister held the number of tx bytes, which is loaded into counter (which is no longer needed)
              tempRegister <= commandRxData;
              counter <= (tempRegister ? tempRegister : commandRxData); // if we have no tx bytes, skip straight to rx
              state <= (tempRegister ? StBinaryTransmit : StBinaryReceive);
            end
          end
        end
        StBinaryTransmit : begin
          // we are connected to the interface indicated by "masterChannel"
          if (commandRxValid) begin
            masterOut.valid <= 1'b1;
            masterOut.data <= commandRxData;
            counter <= (counter - 'd1);
            if (counter == 'd1) begin
              counter <= tempRegister;
              state <= (tempRegister ? StBinaryReceive : StIdle);
            end
          end
        end
        StBinaryReceive : begin
          if (masterIn.valid && !masterIn.ready) begin
            masterOut.ready <= 1'b1;
            commandTxValid <= 1'b1;
            commandTxData <= masterIn.data;
            counter <= (counter - 'd1);
            nextState <= ((counter == 'd1) ? StIdle : StBinaryReceive);
            state <= StTxWait;
          end
        end
        StSyntax : begin
          if (syntaxROM == 0) begin
            state <= StPrompt;
          end
          else begin
            commandTxData <= syntaxROM;
            commandTxValid <= 1'b1;
            state <= StTxWait;
            nextState <= StSyntax;
            counter <= (counter + 'd1);
          end
        end
        StAsciiInfo : begin
          commandTxData <= hex2ascii(tempRegister[0] ? infoROM[3:0] : infoROM[7:4]);
          commandTxValid <= 1'b1;
          state <= StTxWait;
          counter[3:0] <= (counter + tempRegister[0]);
          tempRegister <= tempRegister ^ 8'h01;
          nextState <= (((counter == 'd63) && tempRegister[0]) ? StPrompt : StBinaryInfo);
        end
        StBinaryInfo : begin
          commandTxData <= infoROM;
          commandTxValid <= 1'b1;
          state <= StTxWait;
          counter <= (counter + 'd1);
          nextState <= ((counter == 'd63) ? StPrompt : StBinaryInfo);
        end
        StAsciiTimer : begin
          commandTxData <= hex2ascii(tempRegister[0] ? timerByte[3:0] : timerByte[7:4]);
          commandTxValid <= 1'b1;
          state <= StTxWait;
          counter[3:0] <= (counter + tempRegister[0]);
          tempRegister <= tempRegister ^ 8'h01;
          nextState <= (((counter == 'd15) && tempRegister[0]) ? StPrompt : StBinaryTimer);
        end
        StBinaryTimer : begin
          commandTxData <= timerByte;
          commandTxValid <= 1'b1;
          state <= StTxWait;
          counter <= (counter + 'd1);
          nextState <= ((counter == 'd15) ? StPrompt : StBinaryTimer);
        end
      endcase // case (state)
    end
  end

  // **************************
  // DEBUG
  // **************************

`ifdef TARGET_TTY_MANAGER_ILA
  ila_0 uILA (
              .clk(clock),
//              .trig_in(1'b0),
//              .trig_out_ack(1'b0),
              .probe0({'0,
                       masterIn,
                       masterOut,
                       masterChannel,
                       tempRegister,
                       counter
                       }),
              .probe1({'0,
                       reset,
                       resetQ,
                       state,
                       nextState,
                       commandRxData,
                       commandRxValid,
                       commandTxData,
                       commandTxValid,
                       commandTxReady,
                       uartRx,
                       uartTx
                       })
              );
  `endif

`ifdef SIM_ANNOUNCE
  initial begin
    `OC_ANNOUNCE_MODULE(oc_tty_manager);
    `OC_ANNOUNCE_PARAM_INTEGER(ClockHz);
    `OC_ANNOUNCE_PARAM_INTEGER(UartCount);
    `OC_ANNOUNCE_PARAM_INTEGER(UartManagerChannel);
    `OC_ANNOUNCE_PARAM_INTEGER(UartBaud[UartManagerChannel]);
    `OC_ANNOUNCE_PARAM_INTEGER(Abbcs);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_tty_manager
