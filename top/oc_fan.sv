
// SPDX-License-Identifier: MPL-2.0

module oc_fan #(
                parameter integer ClockHz = 100000000,
                parameter integer FanCount = 1,
                parameter integer FanDefaultDuty = `OC_FROM_DEFINE_ELSE(TARGET_FAN_DEFAULT_DUTY, 50),
                parameter integer FanDebounceCycles = `OC_FROM_DEFINE_ELSE(TARGET_FAN_DEBOUNCE_CYCLES, 5),
                parameter bit     AbbcEnable = 1'b1,
                parameter integer AbbcNumber = 0,
                parameter bit     ResetSync = 1'b0
                )
 (
  input                       clock,
  input                       reset,
  input [FanCount-1:0]        fanSense,
  output logic [FanCount-1:0] fanPwm,
  input                       oclib_pkg::abbc_s abbcIn = '0,
  output                      oclib_pkg::abbc_s abbcOut
  );

  `OC_STATIC_ASSERT(FanCount>=1); // we shouldn't be instantiating this block if we don't have FANs
  `OC_STATIC_ASSERT(AbbcEnable==1); // we need ABBC CSRs for FAN to work

  logic        resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  localparam AddressSpaces = 1;
  logic [AddressSpaces-1:0] csrSelect;
  oclib_pkg::csr_s                        csr;
  oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

  oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
  uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

  // Implement address space 0

  // 0 : CSR ID
  // 1->(FanCount+1) : Fan
  //     [6:0] dutyCycle (0=0%, 100=100%)
  //     [25:16] pulsesPerSecond

  localparam integer NumCsr = 1 + FanCount; // 1 id
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdFan, 8'd0, 8'(FanCount)};
  localparam logic [31:0] FanInitCsr = { 24'd0, 1'b0, 7'(FanDefaultDuty) };
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  oclib_csr_array #(.NumCsr(NumCsr),
                    .CsrRwBits   ({ {FanCount{32'h0000007f}}, 32'h00000000 }),
                    .CsrRoBits   ({ {FanCount{32'h03ff0000}}, 32'h00000000 }),
                    .CsrInitBits ({ {FanCount{  FanInitCsr}}, CsrId        }),
                    .CsrFixedBits({ {FanCount{32'h00000000}}, 32'hffffffff }))
  uCSR (clock, resetQ, csrSelect[0], csr, csrFb[0], csrConfig, csrStatus);

  // prescaler
  // this is supposed to divide input clock to generate prescalePulse at 2.5MHz (25KHz PWM freq * 100)

  localparam integer        PrescaleDivider = (ClockHz / 2500000);
  localparam integer        PrescaleW = $clog2(PrescaleDivider);
  localparam integer        PrescaleM2 = (PrescaleDivider-2);

  logic [PrescaleW-1:0]     prescaleCounter;
  logic                     prescalePulse;
  always_ff @(posedge clock) begin
    prescaleCounter <= (resetQ ? '0 : (prescalePulse ? '0 : (prescaleCounter+1)));
    prescalePulse <= (prescaleCounter == PrescaleM2);
  end

  // PWM phase counter
  // pwmValue counts from 0-99 every 40us, so we have ~25KHz PWM period to the fans

  localparam integer        PwmTC = 99;
  localparam integer        PwmW = 7; // needs to count up to 99

  logic [PwmW-1:0]          pwmCounter;
  logic                     pwmMax;
  always_ff @(posedge clock) begin
    pwmCounter <= (resetQ ? '0 : (prescalePulse ? (pwmMax ? '0 : (pwmCounter+1)) : pwmCounter));
    pwmMax <= (pwmCounter == PwmTC);
  end

  // Second counter
  // secondCounter counts from (roughly) 0-24999, incrementing every ~40us, so we can count fan pulses per second
  // Note that we reuse the PWM related prescalers to save flops.  However, the nature of the prescale value (i.e.
  // only 40 with a 100Mhz ClockHz) means that there is potential for a few percent error in the ~25KHz of the PWM,
  // which is OK for driving the fan.  For SENSING the fan, we adjust for the actual period of the PWM so that we
  // count very close to one second here.

  localparam integer        SecondTC = (ClockHz/(100*PrescaleDivider))-1;
  localparam integer        SecondW = 15; // needs to count up to ~23-27K

  logic [SecondW-1:0]       secondCounter;
  logic                     secondMax;
  logic                     secondPulse;
  always_ff @(posedge clock) begin
    secondCounter <= (resetQ ? SecondTC :
                     (prescalePulse && pwmMax) ? (secondMax ? '0 : (secondCounter+1)) :
                     secondCounter);
    secondMax <= (secondCounter == SecondTC);
    secondPulse <= (prescalePulse && pwmMax && secondMax);
  end

  // per-FAN logic
  logic [FanCount-1:0] fanSenseClean;
  logic [FanCount-1:0] fanSenseCleanQ;
  logic [FanCount-1:0] fanSensePulse;
  logic [FanCount-1:0] [9:0] pulsesThisPeriod;
  logic [FanCount-1:0] [9:0] pulsesLastPeriod;

  generate
    for (genvar i=0; i<FanCount; i++) begin

      // Output a PWM waveform per fan, forcing fans on during reset
      always_ff @(posedge clock) begin
        fanPwm[i] <= (resetQ ? 1'b1 : (csrConfig[1+i][6:0] > pwmCounter));
      end

      // Debounce the fan sense input
      oclib_debounce #(.Cycles(FanDebounceCycles))
      uDEBOUNCE (.clock(clock), .reset(resetQ), .in(fanSense[i]), .out(fanSenseClean[i]));

      // find rising edge of debounced fan sense input
      always_ff @(posedge clock) begin
        fanSenseCleanQ[i] <= fanSenseClean[i];
        fanSensePulse[i] <= (fanSenseClean[i] && !fanSenseCleanQ[i]);
      end

      // Count the pulses in this period, and keep the count from the last second
      always_ff @(posedge clock) begin
        pulsesThisPeriod[i] <= (secondPulse ? '0 : (pulsesThisPeriod[i] + fanSensePulse[i]));
        pulsesLastPeriod[i] <= (secondPulse ? pulsesThisPeriod[i] : pulsesLastPeriod[i]);
      end

      // send back status to CSRs
      assign csrStatus[1+i][25:16] = pulsesLastPeriod[i];
    end
  endgenerate

`ifdef SIM_ANNOUNCE
  `include "../lib/oclib_defines.vh"
  initial begin
    `OC_ANNOUNCE_MODULE(oc_fan);
    `OC_ANNOUNCE_PARAM_INTEGER(ClockHz);
    `OC_ANNOUNCE_PARAM_INTEGER(FanCount);
    `OC_ANNOUNCE_PARAM_INTEGER(FanDefaultDuty);
    `OC_ANNOUNCE_PARAM_INTEGER(FanDebounceCycles);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
  end
`endif

endmodule // oc_fan
