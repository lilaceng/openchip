
// SPDX-License-Identifier: MPL-2.0

module oc_iic #(
                parameter integer ClockHz = 100000000,
                parameter bit     OffloadEnable = 1'b0,
                parameter bit     AbbcEnable = 1'b1,
                parameter integer AbbcNumber = 0,
                parameter bit     ResetSync = 1'b0
                )
 (
  input        clock,
  input        reset,
  input        iicScl,
  output logic iicSclTristate,
  input        iicSda,
  output logic iicSdaTristate,
  input        oclib_pkg::abbc_s abbcIn = '0,
  output       oclib_pkg::abbc_s abbcOut
  );

  `OC_STATIC_ASSERT(AbbcEnable==1); // we need ABBC CSRs for IIC to work

  logic        resetQ;
  oclib_synchronizer #(.Enable(ResetSync)) uRESET_SYNC (clock, reset, resetQ);

  localparam AddressSpaces = (OffloadEnable ? 2 : 1);
  logic [AddressSpaces-1:0] csrSelect;
  oclib_pkg::csr_s                        csr;
  oclib_pkg::csr_fb_s [AddressSpaces-1:0] csrFb;

  oclib_abbc_to_csr #(.AddressSpaces(AddressSpaces))
  uABBC_TO_CSR (clock, resetQ, abbcIn, abbcOut, csrSelect, csr, csrFb);

  // Implement address space 0

  // 0 : CSR ID
  // 1 :
  //     [0] sclManual
  //     [1] sclTristate
  //     [3] sclIn
  //     [4] sdaManual
  //     [5] sdaTristate
  //     [7] sdaIn
  //     [30] offloadDebug
  //     [31] offloadInterrupt

  localparam integer NumCsr = 2; // 1 id
  localparam logic [31:0] CsrId = { oclib_pkg::AbbcIdIic,
                                    15'd0, OffloadEnable};
  logic [NumCsr-1:0] [31:0] csrConfig;
  logic [NumCsr-1:0] [31:0] csrStatus;

  oclib_csr_array #(.NumCsr(NumCsr),
                    .CsrRwBits   ({ 32'h00000033, 32'h00000000 }),
                    .CsrRoBits   ({ 32'hc0000088, 32'h00000000 }),
                    .CsrFixedBits({ 32'h00000000, 32'hffffffff }),
                    .CsrInitBits ({ 32'h00000000, CsrId        }))
  uCSR (clock, resetQ, csrSelect[0], csr, csrFb[0], csrConfig, csrStatus);

  logic                     csrSclManual;
  logic                     csrSclTristate;
  logic                     csrSclIn;
  logic                     csrSdaManual;
  logic                     csrSdaTristate;
  logic                     csrSdaIn;
  logic                     offloadDebug;
  logic                     offloadInterrupt;

  assign csrSclManual = csrConfig[1][0];
  assign csrSclTristate = csrConfig[1][1];

  assign csrSdaManual = csrConfig[1][4];
  assign csrSdaTristate = csrConfig[1][5];

  assign csrStatus[1][3] = csrSclIn;
  assign csrStatus[1][7] = csrSdaIn;
  assign csrStatus[1][30] = offloadDebug;
  assign csrStatus[1][31] = offloadInterrupt;

  oclib_synchronizer #(.Width(2)) uSYNC (.clock(clock), .in({iicSda, iicScl}), .out({csrSdaIn, csrSclIn}));

  if (OffloadEnable) begin

`ifdef OC_LIBRARY_ULTRASCALE_PLUS

    oclib_pkg::axi_lite_s      axil;
    oclib_pkg::axi_lite_fb_s   axilFb;

    logic offloadSclTristate;
    logic offloadSdaTristate;

    // Implement address space 1

    oclib_csr_to_axi_lite #(.DataW(32), .AddressW(9))
    uCSR_TO_AXIL (clock, resetQ, csrSelect[1], csr, csrFb[1], axil, axilFb);

    xip_iic uIP (
                 .s_axi_aclk(clock), .s_axi_aresetn(!resetQ),
                 .sda_i(iicSda), .sda_o(), .sda_t(offloadSdaTristate),
                 .scl_i(iicScl), .scl_o(), .scl_t(offloadSclTristate),
                 .s_axi_awaddr(axil.awaddr[8:0]),
                 .s_axi_awvalid(axil.awvalid), .s_axi_awready(axilFb.awready),
                 .s_axi_wdata(axil.wdata), .s_axi_wstrb(axil.wstrb),
                 .s_axi_wvalid(axil.wvalid), .s_axi_wready(axilFb.wready),
                 .s_axi_araddr(axil.araddr[8:0]),
                 .s_axi_arvalid(axil.arvalid), .s_axi_arready(axilFb.arready),
                 .s_axi_bresp(axilFb.bresp),
                 .s_axi_bvalid(axilFb.bvalid), .s_axi_bready(axil.bready),
                 .s_axi_rdata(axilFb.rdata), .s_axi_rresp(axilFb.rresp),
                 .s_axi_rvalid(axilFb.rvalid), .s_axi_rready(axil.rready),
                 .iic2intc_irpt(offloadInterrupt), .gpo(offloadDebug)
                 );

    assign iicSclTristate = (csrSclManual ? csrSclTristate : offloadSclTristate);
    assign iicSdaTristate = (csrSdaManual ? csrSdaTristate : offloadSdaTristate);

`else
    `OC_STATIC_ERROR("Only support OffloadEnable in IIC for OC_LIBRARY_ULTRASCALE_PLUS");
`endif
  end
  else begin
    assign iicSclTristate = csrSclTristate;
    assign iicSdaTristate = csrSdaTristate;
    assign offloadDebug = 1'b0;
    assign offloadInterrupt = 1'b0;
  end

`ifdef SIM_ANNOUNCE
  `include "../lib/oclib_defines.vh"
  initial begin
    `OC_ANNOUNCE_MODULE(oc_iic);
    `OC_ANNOUNCE_PARAM_INTEGER(ClockHz);
    `OC_ANNOUNCE_PARAM_BIT(AbbcEnable);
    `OC_ANNOUNCE_PARAM_INTEGER(AbbcNumber);
    `OC_ANNOUNCE_PARAM_BIT(ResetSync);
    `OC_ANNOUNCE_PARAM_BIT(OffloadEnable);
  end
`endif

endmodule // oc_iic
