
// SPDX-License-Identifier: MPL-2.0

module oc_top
  #(
    // Misc
    parameter integer Seed = 0, // seed to generate varying implementation results
    // RefClocks
    parameter integer RefClockCount = 1, // number of RefClocks
    parameter integer RefClockHz [RefClockCount-1:0] = '{ RefClockCount { 100000000 } }, // frequency, per RefClock
    parameter integer RefClockTop = 0, // which refclock should be used to clock slow top level logic
    parameter integer DiffRefClockCount = 0, // number of (differential) DiffRefClocks
    `OC_CREATE_SAFE_WIDTH(DiffRefClockCount),
    parameter integer DiffRefClockHz [DiffRefClockCountSafe-1:0] = '{DiffRefClockCountSafe{156250000}}, // freq, per DiffRefClock
    // PLLs
    parameter integer PllCount = 1, // number of PLLs
    `OC_CREATE_SAFE_WIDTH(PllCount),
    parameter integer PllClockRef [PllCountSafe-1:0] = '{ PllCountSafe { 0 } }, // which reference clock
    parameter bit     PllAbbcEnable [PllCountSafe-1:0] = '{ PllCountSafe { 1 } }, // whether to include CSRs
    parameter bit     PllMeasureEnable [PllCountSafe-1:0] = '{ PllCountSafe { 1 } }, // whether to include measure logic
    parameter bit     PllThrottleMap [PllCountSafe-1:0] = '{ PllCountSafe { 1 } }, // whether to include programmable throttle
    parameter bit     PllThrottleThermal [PllCountSafe-1:0] = '{ PllCountSafe { 1 } }, // whether to include thermal throttle
    parameter integer PllClocksMax = 4, // max number of clocks per PLL
    parameter integer PllClocks [PllCountSafe-1:0] = '{ PllCountSafe { 1 } }, // number of clocks, per PLL
    parameter integer PllClockHz [PllCountSafe-1:0] [PllClocksMax-1:0] = // frequency, per clock, per PLL
                      '{ '{ 0, 0, 0, 500000000 } }, // 500MHz for Pll[0]/Clock[0]
    // User Space
    parameter integer UserAbbcs = `OC_FROM_DEFINE_ELSE(USER_ABBCS, 1),
    parameter integer UserClocks = `OC_FROM_DEFINE_ELSE(USER_CLOCKS, 1),
    parameter integer UserPllTemp [UserClocks-1:0] = '{UserClocks{0}} , // workaround vivado bug
    parameter integer UserPll [UserClocks-1:0] = `OC_FROM_DEFINE_ELSE(USER_PLL, UserPllTemp),
    parameter integer UserPllClock [UserClocks-1:0] = `OC_FROM_DEFINE_ELSE(USER_PLL_CLOCK,UserPllTemp),
    parameter integer UserAxiMemoryPorts = `OC_FROM_DEFINE_ELSE(USER_AXI_MEMORY_PORTS, 0),
    `OC_CREATE_SAFE_WIDTH(UserAxiMemoryPorts),
    // ChipMons
    parameter integer ChipMonCount = 0, // number of CHIPMONs
    `OC_CREATE_SAFE_WIDTH(ChipMonCount),
    parameter bit     ChipMonAbbcEnable [ChipMonCountSafe-1:0] = '{ ChipMonCountSafe { 1 } },
    parameter bit     ChipMonI2CEnable [ChipMonCountSafe-1:0] = '{ ChipMonCountSafe { 1 } },
    // Protection
    parameter integer ProtectCount = 0, // number of PROTECTION blocks
    `OC_CREATE_SAFE_WIDTH(ProtectCount),
    // IIC
    parameter integer IicCount = 0, // number of IIC blocks
    `OC_CREATE_SAFE_WIDTH(IicCount),
    parameter integer IicOffloadEnable = 0,
    // LED
    parameter integer LedCount = 0, // number of LEDs
    `OC_CREATE_SAFE_WIDTH(LedCount),
    // GPIO
    parameter integer GpioCount = 0, // number of GPIOs
    `OC_CREATE_SAFE_WIDTH(GpioCount),
    // FAN
    parameter integer FanCount = 0, // number of FANs
    `OC_CREATE_SAFE_WIDTH(FanCount),
    // UARTs
    parameter integer UartCount = 1, // number of UARTs
    `OC_CREATE_SAFE_WIDTH(UartCount),
    parameter integer UartManagerChannel = 0, // which UART is the management channel
    parameter integer UartBaud [UartCountSafe-1:0] = '{ UartCountSafe { 115200 } }, // baud rate, per UART
    // HBM
    parameter integer HbmCount = 0, // number of HBM blocks
    `OC_CREATE_SAFE_WIDTH(HbmCount),
    parameter integer HbmRefClock [HbmCountSafe-1:0] = '{ HbmCountSafe { 0 } },
    parameter integer HbmAxiMemoryPorts [HbmCountSafe-1:0] = '{ HbmCountSafe { UserAxiMemoryPorts } },
    parameter integer HbmAxiMemoryPortBase [HbmCountSafe-1:0] = '{ HbmCountSafe { 0 } }, // multiple HBM, or types of memory
    parameter integer HbmAxiClocks [HbmCountSafe-1:0] = '{ HbmCountSafe { 1 } },
    parameter integer HbmAxiUserClock [HbmCountSafe-1:0] = '{ HbmCountSafe { 0 } }, // HBM tied to first user clock by default
    parameter bit     HbmAbbcEnable [HbmCountSafe-1:0] = '{ HbmCountSafe { 1 } },
    // CMAC
    parameter integer CmacCount = 0, // number of CMAC blocks
    `OC_CREATE_SAFE_WIDTH(CmacCount),
    parameter integer CmacDiffRefClock [CmacCountSafe-1:0] = '{ CmacCountSafe { 0 } },
    parameter bit     CmacAbbcEnable [CmacCountSafe-1:0] = '{ CmacCountSafe { 1 } }
    )
  (
   input [RefClockCount-1:0]              clockRef,
   input [DiffRefClockCountSafe-1:0]      clockDiffRefP = {DiffRefClockCountSafe{1'b0}},
   input [DiffRefClockCountSafe-1:0]      clockDiffRefN = {DiffRefClockCountSafe{1'b1}},
   input                                  resetPin = 1'b0, // we have a power on reset function
   input [IicCountSafe-1:0]               iicScl = {IicCountSafe{1'b1}},
   output logic [IicCountSafe-1:0]        iicSclTristate,
   input [IicCountSafe-1:0]               iicSda = {IicCountSafe{1'b1}},
   output logic [IicCountSafe-1:0]        iicSdaTristate,
   output logic [LedCountSafe-1:0]        ledOut,
   output logic [GpioCountSafe-1:0]       gpioOut,
   output logic [GpioCountSafe-1:0]       gpioTristate,
   input [GpioCountSafe-1:0]              gpioIn = {GpioCountSafe{1'b0}},
   output logic [FanCountSafe-1:0]        fanPwm,
   input [FanCountSafe-1:0]               fanSense = {FanCountSafe{1'b0}},
   input [UartCountSafe-1:0]              uartRx,
   output logic [UartCountSafe-1:0]       uartTx,
   input [ChipMonCountSafe-1:0]           chipMonScl = {ChipMonCountSafe{1'b1}},
   output logic [ChipMonCountSafe-1:0]    chipMonSclTristate,
   input [ChipMonCountSafe-1:0]           chipMonSda = {ChipMonCountSafe{1'b1}},
   output logic [ChipMonCountSafe-1:0]    chipMonSdaTristate,
   output logic [CmacCountSafe-1:0] [3:0] cmacTxP,
   output logic [CmacCountSafe-1:0] [3:0] cmacTxN,
   input [CmacCountSafe-1:0] [3:0]        cmacRxP = {CmacCountSafe{4'h0}},
   input [CmacCountSafe-1:0] [3:0]        cmacRxN = {CmacCountSafe{4'hf}},
   output logic                           thermalError,
   output logic                           thermalWarning,
   output logic [7:0]                     debug
   );

  localparam integer ClockTopHz = RefClockHz[RefClockTop]; // should be ~100MHz, some IP cannot go faster
  logic         clockTop; // will be used to clock the OC infra, the top IPs, and their APB/DRP/AXIL/etc
  assign clockTop = clockRef[RefClockTop];

  logic         resetManager; // from the oc_tty_manager
  logic         resetTop; // to everyone
  oclib_reset #(.StartPipeCycles(8+Seed)) uRESET (.clock(clockTop), .in(resetPin || resetManager), .out(resetTop));

  // Compute the number of ABBC busses, and first ABBC number for each type of IP
  localparam AbbcFirstPll = 0;
  localparam AbbcFirstChipMon = AbbcFirstPll + PllCount;
  localparam AbbcFirstProtect = AbbcFirstChipMon + ChipMonCount;
  localparam AbbcFirstIic = AbbcFirstProtect + ProtectCount;
  localparam AbbcFirstLed = AbbcFirstIic + IicCount;
  localparam AbbcFirstGpio = AbbcFirstLed + (LedCount ? 1 : 0); // we drive all LEDs from one IP
  localparam AbbcFirstFan = AbbcFirstGpio + (GpioCount ? 1 : 0); // we drive all GPIOs from one IP
  localparam AbbcFirstHbm = AbbcFirstFan + (FanCount ? 1 : 0); // we drive all FANs from one IP
  localparam AbbcFirstCmac = AbbcFirstHbm + HbmCount;
  localparam AbbcFirstUser = AbbcFirstCmac + CmacCount;
  localparam Abbcs = AbbcFirstUser + UserAbbcs;

  `OC_CREATE_SAFE_WIDTH(Abbcs); // doubt we'd ever see zero ABC's but it's possible

  oclib_pkg::abbc_s abbcToClient [AbbcsSafe-1:0];
  oclib_pkg::abbc_s abbcFromClient [AbbcsSafe-1:0];

  // PLLs
  // since each PLL can have different number of clocks, our 2-D arrays must be sized based on PllClocksMax
  logic [PllCountSafe-1:0] [PllClocksMax-1:0] clockPll;
  logic [PllCountSafe-1:0] [PllClocksMax-1:0] resetPll;
  for (genvar i=0; i<PllCount; i++) begin : pll
    localparam Clocks = PllClocks[i];
    oc_pll #(.RefClockHz(RefClockHz[i]), .Clocks(Clocks),
             .Out0MHz(PllClockHz[i][0] / 1000000.0), // TODO: move everything to Hz, and maybe an array param
             .Out1MHz(PllClockHz[i][1] / 1000000.0),
             .Out2MHz(PllClockHz[i][2] / 1000000.0),
             .Out3MHz(PllClockHz[i][3] / 1000000.0),
             .AbbcEnable(PllAbbcEnable[i]), .AbbcNumber(AbbcFirstPll+i),
             .MeasureEnable(PllMeasureEnable[i]), .ResetSync(1))
    uPLL (.clock(clockTop), .reset(resetTop), .thermalWarning(thermalWarning),
          .clockRef(clockRef[PllClockRef[i]]), .clockOut(clockPll[i][Clocks-1:0]), .resetOut(resetPll[i][Clocks-1:0]),
          .abbcIn(abbcToClient[AbbcFirstPll+i]), .abbcOut(abbcFromClient[AbbcFirstPll+i]));
    // since each PLL can have different number of clocks, we need to tie off any extra wires in the 2-D arrays
    for (genvar j=Clocks; j<PllClocksMax; j++) begin
      assign clockPll[i][j] = 1'b0;
      assign resetPll[i][j] = 1'b0;
    end
  end

  // USER SPACE
  oclib_pkg::ring_status_s ringStatus;
  oclib_pkg::user_status_s userStatus;
  oclib_pkg::axi3_s [UserAxiMemoryPorts-1:0] axiMemory;
  oclib_pkg::axi3_fb_s [UserAxiMemoryPorts-1:0] axiMemoryFb;
`ifdef TARGET_CMAC_COUNT
  logic [CmacCount-1:0] clockCmac;
  oclib_pkg::axi4st_512_s [CmacCount-1:0] axiCmacTx;
  oclib_pkg::axi4st_512_fb_s [CmacCount-1:0] axiCmacTxFb;
  oclib_pkg::axi4st_512_s [CmacCount-1:0] axiCmacRx;
  oclib_pkg::axi4st_512_fb_s [CmacCount-1:0] axiCmacRxFb;
`endif
  logic [UserClocks-1:0] clockUser;
  logic [UserClocks-1:0] resetUser;
  logic [ProtectCountSafe:0] unlocked;
  for (genvar i=0; i<UserClocks; i++) assign clockUser[i] = clockPll[UserPll[i]][UserPllClock[i]];
  for (genvar i=0; i<UserClocks; i++) assign resetUser[i] = resetPll[UserPll[i]][UserPllClock[i]];
  oc_user #(// ideally user should be checking these parameters, making sure they match requirements, or reconfiguring to match
            .Clocks(UserClocks),
`ifdef TARGET_HBM_COUNT
            .AxiMemoryPorts(UserAxiMemoryPorts),
`endif
`ifdef TARGET_CMAC_COUNT
            .CmacCount(CmacCount),
`endif
            .Abbcs(UserAbbcs), .AbbcNumber(AbbcFirstUser))
  uUSER (.clock(clockUser), .reset(resetUser),
         .clockTop(clockTop), .resetTop(resetTop),
         .ringStatus(ringStatus), .userStatus(userStatus),
`ifdef TARGET_HBM_COUNT
         .axiMemory(axiMemory), .axiMemoryFb(axiMemoryFb),
`endif
`ifdef TARGET_CMAC_COUNT
         .clockCmac(clockCmac), .axiCmacTx(axiCmacTx), .axiCmacTxFb(axiCmacTxFb), .axiCmacRx(axiCmacRx), .axiCmacRxFb(axiCmacRxFb),
`endif
         .abbcIn(abbcToClient[AbbcFirstUser+UserAbbcs-1:AbbcFirstUser]),
         .abbcOut(abbcFromClient[AbbcFirstUser+UserAbbcs-1:AbbcFirstUser]));

  assign ringStatus.unlocked = unlocked;
  assign ringStatus.thermalError = thermalError;
  assign ringStatus.thermalWarning = thermalWarning;

  // ChipMons
  logic [ChipMonCountSafe-1:0] chipMonThermalError;
  logic [ChipMonCountSafe-1:0] chipMonThermalWarning;
  logic chipMonMergedThermalError;
  logic chipMonMergedThermalWarning;
  for (genvar i=0; i<ChipMonCount; i++) begin : chipmon
    oc_chipmon #(.ClockHz(ClockTopHz),
                 .AbbcEnable(ChipMonAbbcEnable[i]), .AbbcNumber(AbbcFirstChipMon+i),
                 .ResetSync(1))
    uCHIPMON (.clock(clockTop), .reset(resetTop),
              .scl(chipMonScl[i]), .sclTristate(chipMonSclTristate[i]),
              .sda(chipMonSda[i]), .sdaTristate(chipMonSdaTristate[i]),
              .thermalError(chipMonThermalError[i]), .thermalWarning(chipMonThermalWarning[i]),
              .abbcIn(abbcToClient[AbbcFirstChipMon+i]), .abbcOut(abbcFromClient[AbbcFirstChipMon+i]));
  end
  assign chipMonMergedThermalError = (ChipMonCount ? (|chipMonThermalError) : 1'b0);
  assign chipMonMergedThermalWarning = (ChipMonCount ? (|chipMonThermalWarning) : 1'b0);

  // Protects
  for (genvar i=0; i<ProtectCount; i++) begin : protect
    oc_protect #(.ResetSync(1), .AbbcNumber(AbbcFirstProtect+i))
    uPROTECT (.clock(clockTop), .reset(resetTop), .unlocked(unlocked[i]),
              .abbcIn(abbcToClient[AbbcFirstProtect+i]), .abbcOut(abbcFromClient[AbbcFirstProtect+i]));
  end
  if (ProtectCount == 0) begin
    assign unlocked = '1;
  end

  // IICs
  for (genvar i=0; i<IicCount; i++) begin : iic
    oc_iic #(.ClockHz(ClockTopHz), .OffloadEnable(IicOffloadEnable), .ResetSync(1), .AbbcNumber(AbbcFirstIic+i))
    uIIC (.clock(clockTop), .reset(resetTop),
          .iicScl(iicScl), .iicSclTristate(iicSclTristate),
          .iicSda(iicSda), .iicSdaTristate(iicSdaTristate),
          .abbcIn(abbcToClient[AbbcFirstIic+i]), .abbcOut(abbcFromClient[AbbcFirstIic+i]));
  end
  if (IicCount == 0) begin
    assign iicSclTristate = { IicCountSafe { 1'b1 }};
    assign iicSdaTristate = { IicCountSafe { 1'b1 }};
  end

  // LEDs
  if (LedCount) begin
    oc_led #(.ClockHz(ClockTopHz), .LedCount(LedCount), .ResetSync(1), .AbbcNumber(AbbcFirstLed))
    uLED (.clock(clockTop), .reset(resetTop),
          .ledOut(ledOut),
          .abbcIn(abbcToClient[AbbcFirstLed]), .abbcOut(abbcFromClient[AbbcFirstLed]));
  end
  else begin
    assign ledOut = '0;
  end

  // GPIOs
  if (GpioCount) begin
    oc_gpio #(.GpioCount(GpioCount), .ResetSync(1), .AbbcNumber(AbbcFirstGpio))
    uGPIO (.clock(clockTop), .reset(resetTop),
           .gpioOut(gpioOut), .gpioTristate(gpioTristate), .gpioIn(gpioIn),
           .abbcIn(abbcToClient[AbbcFirstGpio]), .abbcOut(abbcFromClient[AbbcFirstGpio]));
  end
  else begin
    assign gpioOut = '0;
    assign gpioTristate = '1;
  end

  // FANs
  if (FanCount) begin
    oc_fan #(.FanCount(FanCount), .ResetSync(1), .AbbcNumber(AbbcFirstFan))
    uFAN (.clock(clockTop), .reset(resetTop),
           .fanPwm(fanPwm), .fanSense(fanSense),
           .abbcIn(abbcToClient[AbbcFirstFan]), .abbcOut(abbcFromClient[AbbcFirstFan]));
  end
  else begin
    assign fanPwm = '0;
  end

  // HBMS
  logic                    hbmMergedThermalError;
`ifdef TARGET_HBM_COUNT
  logic [HbmCountSafe-1:0] hbmThermalError;
  for (genvar i=0; i<HbmCount; i++) begin : hbm
    oclib_pkg::axi3_s [HbmAxiMemoryPorts[i]-1:0] axiHbm;
    oclib_pkg::axi3_fb_s [HbmAxiMemoryPorts[i]-1:0] axiHbmFb;
    for (genvar axi=0; axi<HbmAxiMemoryPorts[i]; axi++) begin
      assign axiHbm[axi] = axiMemory[HbmAxiMemoryPortBase[i] + axi];
      assign axiMemoryFb[HbmAxiMemoryPortBase[i] + axi] = axiHbmFb[axi];
    end
    oc_hbm #(.RefClockHz(RefClockHz[HbmRefClock[i]]), .HbmRefClocks(1), .SeparateFabricClocks(0),
             .AxiPorts(HbmAxiMemoryPorts[i]), .AxiPortIla(`OC_FROM_DEFINE_ELSE(TARGET_HBM_AXI_PORT_ILA,0)),
             .AbbcEnable(HbmAbbcEnable[i]), .AbbcNumber(AbbcFirstHbm+i),
             .ResetSync(1))
    uHBM (.clock(clockTop), .reset(resetTop || resetUser[HbmAxiUserClock[i]]),
          .thermalError(hbmThermalError[i]),
          .clockRefHbm(clockRef[HbmRefClock[i]]),
          .clockAxi(clockUser[HbmAxiUserClock[i]]),
          .axi(axiHbm), .axiFb(axiHbmFb),
          .abbcIn(abbcToClient[AbbcFirstHbm+i]), .abbcOut(abbcFromClient[AbbcFirstHbm+i]));
  end
  assign hbmMergedThermalError = (|hbmThermalError);
`else
  assign hbmMergedThermalError = 1'b0;
`endif

  // CMACS
`ifdef TARGET_CMAC_COUNT // TBD: do this instead of just relying on param? (dont pull in code, will it make unused module in list?)
  for (genvar i=0; i<CmacCount; i++) begin : cmac
    oc_cmac #(.Instance(i),
              .AbbcEnable(CmacAbbcEnable[i]), .AbbcNumber(AbbcFirstCmac+i),
              .ResetSync(1))
    uCMAC (.clock(clockTop), .reset(resetTop), .clockAxi(clockCmac[i]),
           .txP(cmacTxP[i]), .txN(cmacTxN[i]), .rxP(cmacRxP[i]), .rxN(cmacRxN[i]),
           .clockRefP(clockDiffRefP[CmacDiffRefClock[i]]), .clockRefN(clockDiffRefN[CmacDiffRefClock[i]]),
           .axiRx(axiCmacRx[i]), .axiRxFb(axiCmacRxFb[i]), .axiTx(axiCmacTx[i]), .axiTxFb(axiCmacTxFb[i]),
           .abbcIn(abbcToClient[AbbcFirstCmac+i]), .abbcOut(abbcFromClient[AbbcFirstCmac+i]));
  end
`endif

  // UARTs
  oc_tty_manager #(.ClockHz(ClockTopHz),
                   .UartCount(UartCount), .UartManagerChannel(UartManagerChannel), .UartBaud(UartBaud),
                   .Abbcs(Abbcs), .AbbcFirstUser(AbbcFirstUser), .ResetSync(1))
  uMANAGER (.clock(clockTop), .reset(resetTop), .resetOut(resetManager),
            .uartRx(uartRx), .uartTx(uartTx),
            .abbcOut(abbcToClient), .abbcIn(abbcFromClient));

  // Combine any errors and warnings noted by the above modules
  // for now, we never report thermal error, this output is expected to shut down FPGA power supplies, we're not there yet...
  //  assign thermalError = (chipMonMergedErrorThermal || hbmMergedThermalError);
  assign thermalError = 1'b0;
  assign thermalWarning = chipMonMergedThermalWarning;
  assign debug = resetUser[0];

endmodule // oc_top
